<?php

use Laravel\Socialite\Facades\Socialite as Socialize;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([

	"prefix" => app()->getLocale() . "-" . config("country.code")

], function ($route) {

	$route->get('/country/current', function () {
		$ip = request()->get("ip");
		return strtolower(geoip($ip)->iso_code);
	});

	$route->get('/google34e1137b7be403dc.html', function () {
		return response()->file(public_path('google34e1137b7be403dc.html'), [
			'Content-type' => 'text/html'
		]);
	});

	$route->get('/BingSiteAuth.xml', function () {
		return response()->file(public_path('BingSiteAuth.xml'), [
			'Content-type' => 'text/xml'
		]);
	});

	$route->get('/robots.txt', function () {
		return response()->file(public_path('robots.txt'), [
			'Content-type' => 'text/plain'
		]);
	});

	$route->get('/sitemap-ads.xml', function () {
		return response()->file(public_path('sitemap-ads.xml'), [
			'Content-type' => 'text/xml'
		]);
	});

	$route->get('/', ["as" => "home.index", "uses" => 'HomeController@index']);

	$route->get('/ads/{slug}', ["as" => "ads.details", "uses" => 'DetailsController@ad']);

	// Sitemaps
	$route->get('sitemap-update-index', ['as' => 'sitemap.update.index', 'uses' => 'SitemapController@indexUpdate']);;
	$route->get('sitemap-update-categories', ['as' => 'sitemap.update.categories', 'uses' => 'SitemapController@categoriesUpdate']);;
	$route->get('sitemap-update-videos', ['as' => 'sitemap.update.videos', 'uses' => 'SitemapController@videosUpdate']);;
	$route->get('sitemap-update-articles', ['as' => 'sitemap.update.articles', 'uses' => 'SitemapController@articlesUpdate']);;
	$route->get('sitemap-update-ads', ['as' => 'sitemap.update.ads', 'uses' => 'SitemapController@adsUpdate']);;
	$route->get('sitemap-update-tags', ['as' => 'sitemap.update.tags', 'uses' => 'SitemapController@tagsUpdate']);;
	$route->get('sitemap.xml', ['as' => 'sitemap.index', 'uses' => 'SitemapController@index']);;


	$route->get('/blog/{category_slug?}', ["as" => "posts.index", "uses" => 'PostsController@blog']);
	$route->get('/blog/{category_slug}/{slug}', ["as" => "posts.details", "uses" => 'DetailsController@article']);
	$route->get('/videos/{category_slug?}', ["as" => "videos.index", "uses" => 'PostsController@videos']);
	$route->get('/videos/{category_slug}/{slug}', ["as" => "videos.details", "uses" => 'DetailsController@video']);

	$route->get('/posts/paginate', ["as" => "posts.paginate", "uses" => 'PostsController@posts_paginate']);

	$route->any('/create_ad', ["as" => "ads.create", "uses" => 'AdsController@create']);
	$route->any('/verification/{user_id}', ["as" => "ads.verification", "uses" => 'AdsController@verification']);
	$route->any('/edit_ad/{id}', ["as" => "ads.edit", "uses" => 'AdsController@edit']);
	$route->any('/sold/{id}', ["as" => "ads.sold", "uses" => 'AdsController@sold']);

	// Comments
	$route->get('/comments/replies', ["as" => "comments.replies.index", "uses" => 'CommentController@replies']);
	$route->post('/comments/replies/add', ["as" => "comments.replies.add", "uses" => 'CommentController@addReply']);

	$route->get('/comments/{id}', ["as" => "comments.index", "uses" => 'CommentController@comments']);
	$route->post('/comments/{id}/add', ["as" => "comments.add", "uses" => 'CommentController@addComment']);

	/**
	 * Profile
	 */
	Route::group(['prefix' => 'profile'], function ($router) {
		// Offers
		$router->get('offers', ['as' => 'profile.offers.index', 'uses' => 'Profile\OffersController@index']);
		$router->post('offer', ['as' => 'profile.offers.add', 'uses' => 'Profile\OffersController@addOffer']);
		$router->post('offers/reply', ['as' => 'profile.offers.reply', 'uses' => 'Profile\OffersController@addReply']);
		$router->get('offers/replies', ['as' => 'profile.offers.replies', 'uses' => 'Profile\OffersController@getReplies']);
		$router->get('offers/json', ['as' => 'profile.offers.json', 'uses' => 'Profile\OffersController@getOffersJson']);

		// Ads
		$router->get('ads', ['as' => 'profile.ads.index', 'uses' => 'AdsController@myAds']);

		$router->get('favourites', ['as' => 'profile.ads.favourites', 'uses' => 'AdsController@my_favourites']);


		//Users
		$router->any('edit', ['as' => 'profile.edit.form', 'uses' => 'Profile\UserController@profileEdit']);

	});
	/**
	 * Auth
	 */

	Route::any('/login', "AuthController@login")->name("login");
	Route::any('/activation/{code?}', "AuthController@activation")->name("activation");
	Route::any('/register', "AuthController@register")->name("register");
	Route::any('/forget_password', "AuthController@forget_password")->name("forget_password");
	Route::get('/logout', "AuthController@logout")->name("logout");
	Route::get('/login/facebook', "AuthController@facebook");
	Route::get('/login/twitter', "AuthController@twitter");
	Route::get('/login/google', "AuthController@google");

	Route::group(['prefix' => 'auth'], function ($route) {

		Session::put("previous_url", URL::previous());

		$route->get('facebook', function () {
			return Socialize::driver('facebook')->redirect();
		})->name("facebook.login");

		$route->get('twitter', function () {
			return Socialize::driver('twitter')->redirect();
		})->name("twitter.login");

		$route->get('google', function () {
			return Socialize::driver('google')->redirect();
		})->name("google.login");

	});

	Route::prefix('ajax')->group(function ($route) {
		$route->get('/search_ads', ["as" => "ajax.search_ads", "uses" => 'AjaxController@search_ads']);
		$route->get('/search_cities', ["as" => "ajax.search_cities", "uses" => 'AjaxController@search_cities']);
		$route->get('/get_sub_categories', ["as" => "ajax.get_sub_categories", "uses" => 'AjaxController@get_sub_categories']);
		$route->get('/mob_get_sub_categories', ["as" => "ajax.mob_get_sub_categories", "uses" => 'AjaxController@mob_get_sub_categories']);
		$route->get('/search_get_sub_categories', ["as" => "ajax.search_get_sub_categories", "uses" => 'AjaxController@search_get_sub_categories']);
		$route->get('/ca_get_sub_categories', ["as" => "ajax.ca_get_sub_categories", "uses" => 'AjaxController@ca_get_sub_categories']);
		$route->get('/like', ["as" => "ajax.like", "uses" => 'AjaxController@like']);
		$route->get('/get_sub_places', ["as" => "ajax.sub_places", "uses" => 'AjaxController@get_sub_places']);
		$route->post('/image_upload', ["as" => "ajax.image_upload", "uses" => 'AjaxController@image_upload']);
		$route->post('/video_upload', ["as" => "ajax.video_upload", "uses" => 'AjaxController@video_upload']);
		$route->get('/regions', ["as" => "ajax.regions", "uses" => 'AjaxController@get_regions']);
	});

	Route::get('/media/regenerate/{offset?}', ['as' => 'media.cron', 'uses' => 'MediaRegenerator@regenerate_media']);

	Route::get('/media/resize/{id?}', ['as' => 'media.resize', 'uses' => 'MediaRegenerator@resize']);

	Route::get('/reindex/{offset?}', ['as' => 'index.cron', 'uses' => 'IndexController@index']);
	Route::get('/ads_shift/{offset?}', ['as' => 'scripts.ads_shift', 'uses' => 'ScriptsController@ads_shift']);

	Route::get('/search', ['as' => 'search.index', 'uses' => 'SearchController@index']);

	Route::get('/search_paginate', ['as' => 'search.paginate', 'uses' => 'SearchController@search_paginate']);

	Route::get('/sub_category_paginate', ['as' => 'category.sub_category_paginate', 'uses' => 'CategoryController@sub_category_paginate']);


	Route::get('/page/{slug}', ['as' => 'page.index', 'uses' => 'DetailsController@page']);

	Route::get('/gdpr', ['as' => 'pages.gdpr', 'uses' => function () {
		$page = \App\Models\Page::withoutGlobalScopes()->where('slug', 'gdpr')->where('status', 1)->first();

		if (!$page) {
			abort(404);
		}

		return view('page', ['page' => $page]);
	}]);

	Route::any('/contact_us', ['as' => 'contact_us', 'uses' => 'HomeController@contact_us']);
	// Tags
	Route::get('/tag/{slug}', ['as' => 'tag.index', 'uses' => 'TagsController@index']);
	Route::get('/tag/{slug}/paginate', ['as' => 'tag.paginate', 'uses' => 'TagsController@tags_paginate']);


	Route::get('/test_mail', ['uses' => function () {


		$user = \App\User::first();

		\Illuminate\Support\Facades\Mail::to('ebtehalshemais@gmail.com')->send(new \App\Mail\UserActivationMail($user));
		\Illuminate\Support\Facades\Mail::to('ebtehal.shemais@gmail.com')->send(new \App\Mail\UserActivationMail($user));

//        \Illuminate\Support\Facades\Mail::send('emails.activation', ["user" => $user], function ($message) use ($user) {
//            $message->to('mostafa.elbardeny@gmail.com', 'teffa')->subject('تفعيل الحساب');
//        });
	}]);

	Route::get('/users_mig', ['uses' => function () {
		$t = \Pusher::trigger('public-notifications-channel', 'new-content', ['title' => 'hhhhhh', 'message' => 'uuuuuuuu', 'object_url' => '5555555']);
		dd($t);
		$x = new \Dot\Ads\Jobs\NotificationJob('livestream', 0, \Dot\Options\Facades\Option::get('livestream_url'));
		$x->handle();
		die;
		dispatch(new \Dot\Ads\Jobs\NotificationJob('new_article'));
		die;

		$api_key = 'AIzaSyCOprMrY4U6QRirrg5SGOmkg1Rj5xyagPM';

		$legacy_server_key = 'AIzaSyDGY1rKbHYB5xo4tSKOCwgc9fHXRFlH80g';

		$fcmUrl = 'https://fcm.googleapis.com/fcm/send';

		$token = 'dgzFwDgb1Wo:APA91bGvEJ711ibA7r1u2cAivv8xRN2HWPcmjq53u5PhDF5te49Kx0YHsKrLKNV7gXm3kvZwIhndOpJ7UiI0Am0g1WpMylOkL7CswFKXRxrHb6p-an11Iz0mpVAar13muNK3M6vApkyCrGIo4Dp-UzsjemnV-roSIQ';


		$notification = [
			'title' => 'blah blah blah',
			'body' => 'this is test',
			'sound' => true,
		];

		$extraNotificationData = ["message" => 'testing new', "type" => '5'];

		$temp = [
			'to' => $token,
			'notification' => $notification,
			'data' => $extraNotificationData
		];

		$options = [
			'body' => \GuzzleHttp\json_encode($temp),
			'headers' => [
				'Authorization' => 'key=' . $legacy_server_key,
				'Content-Type' => 'application/json'
			]
		];

		$client = new \GuzzleHttp\Client();

		try {
			$response = $client->request('POST', $fcmUrl, $options);

			$t = \GuzzleHttp\json_decode($response->getBody(), true);

			dd($t);

		} catch (\Exception $e) {
			dd($e);
		}

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
//        $result = curl_exec($ch);
//        curl_close($ch);


		//dd($result);

		$file_n = public_path('home.csv');

		$file = fopen($file_n, "r");

		$all_reds = [];

		while (($data = fgetcsv($file, 2000, ",")) !== FALSE) {

			if (count($data) >= 3) {
				$from = urldecode($data[0]);
				$to = urldecode($data[2]);

				$all_reds[] = ['from_url' => $from, 'to_url' => $to];
			}
		}

		if (count($all_reds)) {
			\Illuminate\Support\Facades\DB::table('redirects')->insert($all_reds);
		}

		fclose($file);

		dd($all_reds);


		$cats = \App\Models\Category::get();

		foreach ($cats as $cat) {
			$missing = true;

			if ($cat->parent) {
				$temp = $cat->parent_category;

				while ($temp) {
					if (!$temp->parent) {
						$missing = false;

						break;
					}

					$temp = $temp->parent_category;
				}
			} else {
				$missing = false;
			}

			if ($missing) {
				$cat->missing_parent = 1;
				$cat->save();
			}
		}

		die('done');

		$localIP = getHostByName(getHostName());
		dd($localIP);

		$ad = \App\Indices\Ad::where("slug", 'مطعم-الحلقة')->where("categories.id", 33)->first();

		dd($ad);

		$str = 'ahmed' . ' ' . 'مصطفى';

		echo mb_detect_encoding($str);

		die;
		$ads = \App\Models\Ad::whereHas("categories", function ($query) {
			$query->where("categories.id", 345);
		})->get();

		$category = \App\Models\Category::where('id', 1905)->with(['parent_category'])->first();

		foreach ($ads as $ad) {
			$ad->categories()->detach();

			$cat = $category;

			while (true) {
				$ad->categories()->attach($cat->id, ['direct' => ($cat->id == 1905 ? 1 : 0)]);

				if ($cat->parent == 0) {
					break;
				} else {
					$cat = $cat->parent_category;
				}
			}

			$new_custom_attributes = \App\Models\Category::get_attributes($category);

			foreach ($ad->custom_attributes as $ad_att) {
				if (!$new_custom_attributes->where('id', $ad_att->id)->count()) {
					$ad->custom_attributes()->detach($ad_att->id);
				}
			}

			\App\Index::save($ad->id);
		}

		dd('done');

		die;
//        $reps = [];
//
//        $users = \Illuminate\Support\Facades\DB::table("users")->get();
//
//        foreach ($users as $user) {
//
//            $q = \Illuminate\Support\Facades\DB::table("users");
//
//            $q->where('email', $user->email);
//
//            $q->whereNotNull('email');
//
//            $q->where('id', '!=', $user->id);
//
//            $r_user = $q->first();
//
//            if ($r_user) {
//                $reps[] = $r_user;
//            }
//        }
//        dd($reps);
//        die('done');

		$new_users = [];

		$old_users = \Illuminate\Support\Facades\DB::table("old_users")->get();

		if (count($old_users)) {
			foreach ($old_users as $o_user) {

				if ($o_user->USERNAME || $o_user->USEREMAIL) {
					$flag = false;

					$q = \Illuminate\Support\Facades\DB::table('users');

					if ($o_user->USEREMAIL) {
						$q->where('email', $o_user->USEREMAIL);
						$flag = true;
					} else if ($o_user->USERNAME) {
						$q->where('username', $o_user->USERNAME);
						$flag = true;
					}

					$user = $q->first();

					if (!$user && $flag) {
						\Illuminate\Support\Facades\DB::table("users")->insert([
							"first_name" => $o_user->FirstName,
							"last_name" => $o_user->LastName,
							"email" => $o_user->USEREMAIL,
							"password" => $o_user->IsFacebook ? \Illuminate\Support\Facades\Hash::make("q^#(ej><dffsfwe324") : \Illuminate\Support\Facades\Hash::make($o_user->PASSWORD),
							"username" => $o_user->USERNAME,
							"provider" => $o_user->IsFacebook ? 'facebook' : ($o_user->IsTwitter ? 'twitter' : null),
							"provider_id" => $o_user->IsFacebook ? $o_user->FACEBOOKID : null,
							"role_id" => $o_user->ISAdmin ? 1 : 0,
							"status" => !$o_user->DISABLE,
							"backend" => $o_user->ISAdmin,
							"photo_id" => 0,
							"lang" => "ar"
						]);

						$new_users[] = $o_user;
					}
				}

			}
		}

		dd($new_users);

		dd('done');

	}]);


	$route->fallback('CategoryController@index');

});

function getCountry($ip){
	return trim(file_get_contents("https://dubarter.com/ar-eg/country/current?ip=" . $ip));
}
