<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api-user')->get('/user', function (Request $request) {
    return $request->guard('api-user')->user();
});
*/

Route::group(['namespace' => 'Api', 'middleware' => 'apiLocale', 'prefix' => '{locale}'], function ($locale) {



    Route::group(['prefix' => 'user'], function () {
        Route::post('register', 'AuthenticationController@register');
        Route::post('login', 'AuthenticationController@login');
        Route::post('forget_password', 'AuthenticationController@forget_password');
        Route::post('refreshToken', 'AuthenticationController@refresh_token');
        Route::any('social_register', 'AuthenticationController@social_register');
        Route::get('check_status', 'AuthenticationController@check_status');
        Route::get('resend_activation', 'AuthenticationController@resend_activation');
        Route::group(['middleware' => 'auth:mobile-api'], function () {
            Route::get('profile', 'UsersController@getProfile');
            Route::post('edit', 'UsersController@updateProfile');
            Route::post('edit_mobile_token', 'UsersController@update_mobile_token');
            Route::post('likes', 'UsersController@userAdsLikes');
            Route::post('likes_counts', 'UsersController@userLikesCount');
            Route::post('like_dislike', 'UsersController@likeDislikeAd');
        });
    });

    Route::get('comments/livestream_comments', 'PostsController@list_livestream_comments');

    Route::group(['middleware' => 'auth:mobile-api', 'prefix' => 'comments'], function () {
        Route::post('post_comment', 'CommentsController@postComment');
        Route::post('post_reply', 'CommentsController@postReply');
        Route::post('add_livestream_comment', 'PostsController@add_livestream_comment');
    });

    Route::group(['prefix' => 'places'], function () {
        Route::get('/', 'PlacesController@listPlaces');
        Route::get('get', 'PlacesController@getPlacesByParentUId');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoriesController@listCategories');
        Route::get('get', 'CategoriesController@getCategories');
        Route::get('main', 'CategoriesController@getMainCategory');
        Route::get('parents', 'CategoriesController@get_parents');
    });

    Route::get('posts/livestream_options', 'PostsController@livestream_options');

    Route::group(['prefix' => 'videos'], function () {
        Route::get('/', 'PostsController@listVideos');
        Route::get('get', 'PostsController@getVideo');
        Route::get('related_ads', 'PostsController@relatedAds');
        Route::group(['prefix' => 'comments'], function () {
            Route::get('/', 'CommentsController@getVideosComments');
            Route::get('get', 'CommentsController@getCommentReplies');
        });
    });

    Route::group(['prefix' => 'articles'], function () {
        Route::get('/', 'PostsController@listArticles');
        Route::get('get', 'PostsController@getArticle');
        Route::get('related_ads', 'PostsController@relatedAds');
        Route::group(['prefix' => 'comments'], function () {
            Route::get('/', 'CommentsController@getArticlesComments');
            Route::get('get', 'CommentsController@getCommentReplies');
        });
    });


    Route::group(['prefix' => 'attributes'], function () {
        Route::get('get', 'AttributesController@getAttributesByCateogryId');
    });


    Route::group(['prefix' => 'ads'], function () {
        Route::any('/', 'AdsController@listAds');
        Route::get('get', 'AdsController@getAd');
        Route::get('related', 'AdsController@getRelatedAds');
        Route::group(['middleware' => 'auth:mobile-api'], function () {
            Route::post('create', 'AdsController@createAdd');
            Route::post('store_images', 'AdsController@store_images');
        });
    });

});
