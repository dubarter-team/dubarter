
var delay = 0;
var offset = 150;

document.addEventListener('invalid', function (e) {
    $(e.target).addClass("invalid");
    $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset}, delay);
}, true);
document.addEventListener('change', function (e) {
    $(e.target).removeClass("invalid")
}, true);

render_select2();

function render_select2() {

    var options = {
        dir: 'rtl'
    };

    $(".select2-dropdown").each(function () {
        var placeholder = $(this).attr('placeholder');

        if (typeof placeholder !== typeof undefined && placeholder !== false) {
            options.placeholder = placeholder;
        }

        $(this).select2(options);
    });

    $(".ca-select2-dropdown").select2({
        dir: 'rtl'
    });
}


$(document).ready(function () {

    var buttons = {
        previous: $('#jslidernews2 .button-previous'),
        next: $('#jslidernews2 .button-next')
    };
    $('#jslidernews2').lofJSidernews({
        interval: 5000,
        easing: 'easeInOutQuad',
        duration: 1200,
        auto: true,
        mainWidth: 730,
        mainHeight: 460,
        navigatorHeight: 115,
        navigatorWidth: 396,
        maxItemDisplay: 4,
        buttons: buttons
        , rtl: true
    });
    $(".open-advanced-search span").click(function () {
        $(".advanced-search").show();
    });
    $(".mobile-download-bar-close").click(function () {
        $(".mobile-download-bar").hide();
        $(".scrolltop, .add-ads-button-round").addClass("bar-close");
    });
    $(".gdpr-close").click(function () {
        $(".gdpr").hide();
    });
    $(".expired-ad-link").click(function () {
        $('html,body').animate({
                scrollTop: $("#related-ads-item").offset().top - 60
            },
            'slow');
    });
    $(".flex-slide").each(function () {
        $(this).click(function () {
            $(".flex-slide").css({width: "10%"}),
                $(".header-box h3").css({opacity: "0"}),
                $(this).css({width: "60%"}),
                $(this).find(".header-box h3").css({opacity: "1"}),
                $(this).find(".header-box").css({opacity: "1"}),
                $(this).find(".slide-details").css({opacity: "1"})
        });
    });
    $(".owl-carousel").owlCarousel({
        rtl: !0,
        loop: !0,
        margin: 10,
        nav: !0,
        responsive: {0: {items: 1}, 750: {items: 2}, 1200: {items: 3}}
    });
});

//
// var pusher = new Pusher('9f1603c1f23654f25529', {
//     cluster: 'eu'
// });
//
// var channel = pusher.subscribe('public-notifications-channel');
//
//
// channel.bind('new-content', function (data) {
//     $('#notif_fl a.alert-link').html(data.message).attr('href', data.object_url);
//     //$('#notif_fl').show();
// });

$('#notif_fl .close').click(function () {
    $('#notif_fl').hide();
});


function item_details_height() {
    var maxHeight = -1;

    $('.item .custom-box .item-detailes').each(function () {
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
        }
    });

    $('.item .custom-box .item-detailes').height(maxHeight);


}

images_lazy_load();

function images_lazy_load() {
    $("img.lzd-image").Lazy({
        scrollDirection: 'vertical',
        effect: 'fadeIn'
    });
}

$(document).ready(function () {
    item_details_height();

    $("img.lzd-image").Lazy({
        scrollDirection: 'vertical',
        effect: 'fadeIn'
    });
});


$('a').each(function () {
    var ths = $(this);
    var href = ths.attr('href');

    if (typeof href !== typeof undefined && href !== false) {

        if (href.indexOf("dubarter.com") < 0) {
            ths.attr('rel', 'nofollow');
        }

    }
});

