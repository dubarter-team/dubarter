$("body").on("click", ".shareBtn", function () {

    var base = $(this);
    var url = window.location.href + "?" + Date.now();

    if (base.hasClass("facebook")) {
        link = "https://www.facebook.com/sharer/sharer.php?u=" + url;
    }

    if (base.hasClass("twitter")) {
        var title = $('meta[name="twitter:title"]').attr("content");
        link = "https://twitter.com/intent/tweet?url=" + $(".short-url").val() + "&text=" + title.replace('#', '');
    }

    if (base.hasClass("google")) {
        link = "https://plus.google.com/share?url=" + url;
    }

    if (base.hasClass("linkedin")) {
        link = "https://www.linkedin.com/shareArticle?mini=true&url=" + url;
    }

    if (base.hasClass("pinterest")) {
        link = "http://pinterest.com/pin/create/button/?url=" + url;
    }

    var winWidth = 650;
    var winHeight = 350;
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);

    window.open(link, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);

    return false;

});
