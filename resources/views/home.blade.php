@extends("layouts.master")

@push('meta')
    <title>{{ option('site_title') }}</title>

    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:type" content="website"/>
    <meta name="author" content="{{option('site_name')}}">
    <meta name="twitter:site" content="@dubarter">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="robots" content="index, follow">
    <meta name="title" content="{{ option('site_title') }}">
    <meta name="description" content="{{ option('site_description') }}"/>
    <meta property="og:title" content="{{ option('site_title') }}"/>
    <meta property="og:site_name" content="{{ option('site_title') }}"/>
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:description" content="{{ option('site_description') }}">
    <meta property="og:image" content="{{ assets("frontend") }}/assets/img/logo-og.jpg">
    <meta name="twitter:title" content="{{ option('site_title') }}">
    <meta name="twitter:description" content="{{ option('site_description') }}">
    <meta name="twitter:image" content="{{ assets("frontend") }}/assets/img/logo-og.jpg">
    <meta name="twitter:url" content="{{ url('/') }}">
    <link rel="alternate" hreflang="ar-iq" href="https://dubarter.com/ar-iq"/>
    <link rel="alternate" hreflang="ar-jo" href="https://dubarter.com/ar-jo"/>
    <link rel="alternate" hreflang="ar-om" href="https://dubarter.com/ar-om"/>
    <link rel="alternate" hreflang="ar-sy" href="https://dubarter.com/ar-sy"/>
    <link rel="alternate" hreflang="ar-kw" href="https://dubarter.com/ar-kw "/>
    @if(\App\Models\Place::getCurrentCountry()->code == "eg")
    <link rel="alternate" hreflang="ar-sa" href="https://dubarter.com/ar-sa"/>
    <link rel="alternate" hreflang="ar-ly" href="https://dubarter.com/ar-ly"/>
    <link rel="alternate" hreflang="ar-ye" href="https://dubarter.com/ar-ye"/>
    <link rel="alternate" hreflang="ar-ae" href="https://dubarter.com/ar-ae"/>
    <link rel="alternate" hreflang="ar-qa" href="https://dubarter.com/ar-qa"/>
    <link rel="alternate" hreflang="ar-lb" href="https://dubarter.com/ar-lb"/>
    <link rel="alternate" hreflang="ar-sd" href="https://dubarter.com/ar-sd"/>
    <link rel="alternate" hreflang="ar-bh" href="https://dubarter.com/ar-bh"/>
    <link rel="alternate" hreflang="ar-ma" href="https://dubarter.com/ar-ma"/>
    <link rel="alternate" hreflang="ar-dz" href="https://dubarter.com/ar-dz"/>
    <link rel="alternate" hreflang="ar-mr" href="https://dubarter.com/ar-mr"/>
    <link rel="alternate" hreflang="ar-so" href="https://dubarter.com/ar-so"/>
    <link rel="alternate" hreflang="ar-km" href="https://dubarter.com/ar-km"/>
    <link rel="alternate" hreflang="ar-ps" href="https://dubarter.com/ar-ps"/>
    <link rel="alternate" hreflang="ar-tn" href="https://dubarter.com/ar-tn"/>
    <link rel="alternate" hreflang="ar-dj" href="https://dubarter.com/ar-dj"/>
    @endif

    @if(\App\Models\Place::getCurrentCountry()->code == "iq")
    <meta name="google-site-verification" content="JpfosHMr8J_s-mdZP1elxpgVqPkuANC0uE-Iay3icqo" />
    @endif

@endpush

@section("content")

    <h1 style="display: none;">اعلانات مبوبة مجانية في القاهرة</h1>
    <h2 style="display: none;">اعلانات مبوبة مجانية فى القاهرة مصر و السعودية و الامارات</h2>


    <div id="home-page">
        <!-- Start search section -->
        <section id="search">
            <div class="container">
                <div class="logo-search">
                    <img src="{{ assets("frontend") }}/assets/img/dubarter-logo.png" style="width: 253px;height: 52px;"
                         alt="" class="img-fluid">
                </div>
                @include('partials.home.search_bar')
                @include("partials.home.categories")
            </div>
            {{--<a href="#videos" class="scroll-section">--}}
                {{--<span class="d-block">{{trans('home.cars')}}</span>--}}
                {{--<img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt="">--}}
            {{--</a>--}}
        </section>

        {{--@include('partials.home.cars')--}}

        {{--@include('partials.home.realestate')--}}

        {{--@include('partials.home.style')--}}

        {{--@include('partials.home.food')--}}

        {{--@include('partials.home.mobwaba')--}}

    </div>
@stop
