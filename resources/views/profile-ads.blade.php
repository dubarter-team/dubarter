@extends("layouts.master")

@section("content")
    <section id="my-account-page">
        <section class="account-barter-search my-account-ads pb-0 category-search">
            <div class="container">
                <div class="m-auto search w-75">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="filter">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item">
                                            <h5>
                                                <i class="my-account-ico my-account-ads-ico"></i>{{trans('profile.my-ads')}}
                                            </h5>
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="quick-search mt-4">
                    <div class="row">
                        <div class="col p-0 text-center border-left">
                            <a href="{{ route('profile.edit.form') }}" class="item">{{trans('profile.my-account')}}</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="{{ route('profile.ads.index') }}"
                               class="active item">{{trans('profile.my-ads')}}</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="{{ route('profile.offers.index') }}" class="item">{{trans('profile.offers')}}  </a>
                        </div>
                        <div class="col p-0 text-center">
                            <a href="{{route('profile.ads.favourites')}}"
                               class="item">{{trans('profile.favorites')}}  </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="result cats-filter my-account-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="mb-2"><i
                                    class="my-account-ico my-account-ads-ico"></i>{{trans('profile.my-ads')}}</h5>
                    </div>

                    <div class="col-md-12">

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissable">
                                <p>{{ session('status') }}</p>
                            </div>
                        @endif

                        <div class="my-account-table my-4">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">{{trans('profile.ad-name')}}</th>
                                        <th scope="col">{{trans('profile.history')}}</th>
                                        <th scope="col">{{trans('profile.status')}}</th>
                                        <th scope="col">{{trans('profile.edit')}}</th>
                                        <th scope="col">{{trans('profile.sold')}}</th>
                                        {{--<th scope="col">الغاء</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody id="search-ads-listing">
                                    @if($ads_count)
                                        @include('partials.ad.ads-list',['ads'=>$ads])
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                <div class="no-content">{{ trans('category.not_found') }}</div>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                </table>
                            </div>
                        </div>
                    </div>


                    @if($ads_count == $per_page)
                        <div class="load-more-btn" id="search-load-more-ads">
                            <a class="btn btn-defult green-btn text-white"
                               @if($ads_count == 0) style="display: none" @endif>{{ trans('search.more_ads') }}</a>
                        </div>
                    @endif

                    <div class="col-md-12">
                        <div class="ads-cat mb-3">
                            <img src="{{ assets("frontend") }}/assets/img/ads1.png" alt=""
                                 class="d-block img-fluid m-auto">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>


@stop


@push("footer")

    <script>

        var offset = {{ $offset }};

        $(document).ready(function () {

            $(document).on('click', '#search-load-more-ads a', function () {


                $('#search-load-more-ads a').html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

                $.ajax({
                    url: '{{route('profile.ads.index')}}',
                    data: {offset: offset},
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        $('#search-ads-listing').append(res.ads_html);

                        offset += res.ads_count;

                        if (res.ads_count < {{$per_page}}) {

                            $('#search-load-more-ads').remove();
                        }
                    },
                    complete: function () {
                        $('#search-load-more-ads a').html('{{ trans("search.more_ads") }}');
                    }
                });

            });

        });

    </script>

@endpush
