@extends("layouts.master")

@section("content")


    <?php
    $ids = [81,59,26,721,314,5,8,831,511,697,21,87,600,4,33,88,401,708,62,89];

    $tags = \App\Models\Tag::whereIn('id', $ids)->orderByRaw(DB::raw("FIELD(id, " . implode(',', $ids) . ")"))->get();

    ?>

    <div id="cats-page" class="not-found-page">
        <div class="text-center animated fadeInDown">

            <div class="not-found-img"><img src="{{ assets("frontend") }}/assets/images/404.png" alt=""
                                            class="align-top"></div>

            <h3 class="font-bold">{{ trans('common.404_title') }}</h3>

            <div class="error-desc">{{ trans('common.404_message') }}</div>
            <a class="btn btn-primary go-home" href="{{ url("/")}}">{{ trans('common.404_gohome') }}</a>
        </div>
        <section id="discover" class="container">
            <ul class="list-unstyled discover-list row">

                @foreach($tags as $tag)
                    <li class="col-md-3 col-sm-6">
                        <a href="{{ route('tag.index', ['slug' => $tag->slug]) }}">{{ $tag->name }}</a>
                    </li>
                @endforeach

            </ul>
        </section>
    </div>
@stop
