@extends("layouts.master")

@section("content")
    <section id="my-account-page">
        <section class="account-barter-search pb-0 category-search">
            <div class="container">
                <div class="m-auto search w-75">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="filter">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item">
                                            <h5><i class="my-account-ico user-info-ico"></i>{{trans('profile.my-account')}}</h5>
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="quick-search mt-4">
                    <div class="row">
                        <div class="col p-0 text-center border-left">
                            <a href="{{ route('profile.edit.form') }}" class="active item">{{trans('profile.my-account')}}</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="{{ route('profile.ads.index') }}" class="item">{{trans('profile.my-ads')}}</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="{{ route('profile.offers.index') }}" class="item">{{trans('profile.offers')}}  </a>
                        </div>
                        <div class="col p-0 text-center">
                            <a href="{{route('profile.ads.favourites')}}" class="item">{{trans('profile.favorites')}} </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="cats-filter my-account-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="mb-2"><i class="my-account-ico user-info-ico"></i>{{trans('common.profile_edit')}}
                        </h5>
                    </div>

                    <div class="col-md-9">
                        @if(Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a class="panel-close close" data-dismiss="alert">×</a>
                                <p>{{Session::get('message')}}</p>
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissable">
                                <p>{{ session('status') }}</p>
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissable">
                                <a class="panel-close close" data-dismiss="alert">×</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="my-account-form my-3">
                            <div class="form-container">
                                <form class="form-horizontal" action="{{route('profile.edit.form')}}" method="post"
                                      enctype="multipart/form-data" role="form" id="profile-edit">
                                    <div class="form-group">
                                        <label>{{trans('common.first_name')}}:</label>
                                        <div class="custom-input user-icon">
                                            <i class="fas fa-user"></i>
                                            <input class="form-control" name="first_name"
                                                   placeholder="{{trans('common.first_name')}}"
                                                   type="text" required value="{{old('first_name',$user->first_name)}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{trans('common.last_name')}}:</label>
                                        <div class="custom-input user-icon">
                                            <i class="fas fa-user"></i>
                                            <input class="form-control" name="last_name" type="text" required
                                                   placeholder="{{trans('common.last_name')}}"
                                                   value="{{old('last_name',$user->last_name)}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>{{trans('common.email')}}:</label>
                                        <div class="custom-input email-icon">
                                            <i class="fas fa-envelope"></i>
                                            <input class="form-control" name="email" type="email" required
                                                   placeholder="{{trans('common.email')}}"
                                                   value="{{old('email',$user->email)}}" disabled>
                                        </div>
                                    </div>

                                    @if(!$user->provider)
                                    <div class="form-group">
                                        <label>{{trans('common.username')}}:</label>
                                        <div class="custom-input user-icon">
                                            <i class="fas fa-user"></i>
                                            <input class="form-control" name="username" required
                                                   placeholder="{{trans('common.username')}}"
                                                   type="text" value="{{old('username',$user->username)}}">
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label>{{trans('common.phone')}}:</label>
                                        <div class="custom-input user-icon">
                                            <i class="fas fa-phone"></i>
                                            <input class="form-control" name="phone" required
                                                   placeholder="{{trans('common.phone')}}"
                                                   type="number" value="{{old('phone',$user->phone)}}">
                                        </div>
                                    </div>
                                    @if(!$user->provider)
                                    <div class="form-group">
                                        <label>{{trans('common.password')}}:</label>
                                        <div class="custom-input user-icon">
                                            <i class="fas fa-key"></i>
                                            <input class="form-control" name="password" type="password"
                                                   placeholder="{{trans('common.password')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{trans('common.password_confirm')}}:</label>
                                        <div class="custom-input user-icon">
                                            <i class="fas fa-key"></i>
                                            <input class="form-control" type="password" name="repassword"
                                                   placeholder="{{trans('common.password_confirm')}}">
                                        </div>
                                    </div>
                                    @endif
                                    <input type="submit" class="btn btn-defult btn-block login"
                                           value="{{trans('common.save')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="dubarter-account-img">
                            <img src="{{thumbnail($user->image->path)}}" id="profile-image" width="255" height="255"
                                 class="avatar img-circle"
                                 alt="avatar">
                            <input type="file" name="photo" id="profile-photo-input" form="profile-edit"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="ads-cat mb-3">
                            <img src="assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

@endsection


@push("footer")
    <script>
        $(function () {
            $('#profile-photo-input').change(function (event) {
                $('#profile-image').attr('src', window.URL.createObjectURL((event.target.files[0])));
            })
        })
    </script>
@endpush
