@extends("layouts.master")


@push('meta')
    <title>{{ $page->title . ' - ' .  trans('common.dubarter') }}</title>
@endpush


@section("content")

    <section id="search-result-page">
        <div class="container">
            <h3 class="tag-title">{{$page->title}}</h3>
            @if($page->image_id)
                <img src="{{ thumbnail($page->image->path, 'large') }}" style="margin-bottom: 20px;"
                     alt="{{ $page->title }}"/>
            @endif

            <div>
                {!! $page->content !!}
            </div>
        </div>
    </section>


@stop
