@foreach($comments as $comment)

    <?php $repliesCount = ($comment->replies()->count());?>
    <li class="comment-wrapper" data-comment-id="{{$comment->id}}"
        data-replies="{{$repliesCount}}">
        <div class="comment-main-level">
            <!-- Avatar -->
            <div class="comment-avatar">
                <img src="{{ $comment->user->photo ? thumbnail($comment->user->photo->path) : '' }}" alt="">
            </div>

            <div class="comment-box">
                <div class="comment-head">
                    <h6 class="comment-name">
                        <a href="#">{{ $comment->user->display_name }}</a>
                    </h6>
                    <span>{{ $comment->created_at }}</span>
                    <a href="javascript:void(0)" class="reply-show">
                        <i class="fa fa-reply"></i>
                    </a>
                    <i class="fa fa-heart"></i>
                </div>
                <div class="comment-content">{{ $comment->content }}</div>
            </div>
        </div>

        <ul class="comments-list reply-list">
            @if($repliesCount)
                <input type="hidden" class="replies-flag" name="replies" value="{{$comment->id}}"/>
            @endif
        </ul>
        @if($repliesCount)
            <a href="javascript:void(0)" class="more-reply">{{trans('common.loadmore')}}</a>
        @endif
    </li>

@endforeach