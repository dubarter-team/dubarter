@foreach($replies as $reply)
    <li>
        <!-- Avatar -->
        <div class="comment-avatar">
            <img src="{{ $reply->user->photo ? thumbnail($reply->user->photo->path) : '' }}"
                 alt="">
        </div>

        <div class="comment-box">
            <div class="comment-head">
                <h6 class="comment-name">
                    <a href="#">{{ $reply->user->display_name }}</a>
                </h6>
                <span>{{ $reply->created_at }}</span>
                <i class="fa fa-reply"></i>
                <i class="fa fa-heart"></i>
            </div>
            <div class="comment-content">{{ $reply->content }}</div>
        </div>
    </li>
@endforeach