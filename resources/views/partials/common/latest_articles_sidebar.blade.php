@if(count($latest_articles))

    <div class="box-gallery">
        <a href="{{ url("blog/". $main_category->slug) }}">
           <span class="box-title"
                 style="{{ $main_category->color ? "color:#fff;background: $main_category->color;" : "" }}">
               <i class="far fa-clone"></i> {{ trans('category.latest_articles') }}
           </span>
        </a>
        <div class="box-content"
             style="{{ $main_category->color ? "border-color: $main_category->color;" : "" }}">
            <ul class="mb-0">

                @foreach($latest_articles as $article)

                    <li>
                        <div class="img">
                            <a href="{{ $article->url }}">
                                <img src="{{ $article->image ? thumbnail($article->image->path) : '' }}"
                                     alt="..." class="rounded">
                            </a>
                        </div>

                        <div class="list-info">
                            <h5 class="{{ dir_class($article->title) }}"><a href="{{ $article->url }}">{{ $article->title }}</a></h5>
                            {{-- <div class="date">
                                <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png"
                                     alt="" class="icon">
                                <span class="d-inline-block">{{ date("Y-m-d", strtotime($article->created_at)) }}</span>
                            </div> --}}
                            <div class="viewer">
                                <span><i class="fas fa-eye"></i></span>
                                <span>{{ $article->views }}</span>
                            </div>

                        </div>

                    </li>

                @endforeach
                <a href="{{ url("blog/". $main_category->slug) }}" class="show-more" style="{{ $main_category->color ? "color: $main_category->color;" : "" }}">{{trans('common.more')}} <i class="fas fa-angle-double-{{ app()->getLocale() == 'ar' ? 'left' : 'right' }}"></i></a>
            </ul>
        </div>
    </div>

    <ins class="adsbygoogle"
         style="display:inline-block;width:336px;height:280px"
         data-ad-client="ca-pub-9085617575772553"
         data-ad-slot="6145269080"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>

@endif
