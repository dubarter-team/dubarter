<span class="favourite {{ $ad->liked ? 'fav-color' : '' }}" data-id="{{ $ad->_id }}" {!!  Auth::guard('frontend')->check() ? '' : 'data-toggle="modal" data-target="#fav_login_modal"' !!}><i class="fas fa-star"></i><span class="fav-text">{{ $ad->liked ? trans('ad.remove_from_favorites') : trans('ad.add_to_favorites') }}</span></span>

