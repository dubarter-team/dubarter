@if(count($latest_videos))

    <div class="box-gallery">
        <a href="{{ url("videos/". $main_category->slug) }}">
         <span class="box-title"
               style="{{ $main_category->color ? "color:#fff;background: $main_category->color;" : "" }}">
             <i class="fas fa-play-circle"></i> {{ trans('category.latest_videos') }}
         </span>
        </a>
        <div class="box-content" style="{{ $main_category->color ? "border-color: $main_category->color;" : "" }}">
            <ul class="mb-0">

                @foreach($latest_videos as $video)
                    <li>
                        <div class="img">
                            <a href="{{ $video->url }}">
                                <div class="play-icon"><span class="play"><i class="fa fa-play"
                                                                             aria-hidden="true"></i></span>
                                </div>
                                <img src="{{ $video->image ? thumbnail($video->image->path) : '' }}"
                                     alt="..." class="rounded">
                            </a>
                        </div>
                        <div class="list-info">
                            <h5 class="{{ dir_class($video->title) }}"><a href="{{ $video->url }}">{{ $video->title }}</a></h5>
                            {{-- <div class="date">
                                <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png"
                                     alt="" class="icon">
                                <span class="d-inline-block">{{ date("Y-m-d", strtotime($video->created_at)) }}</span>
                            </div> --}}
                            <div class="viewer">
                                <span><i class="fas fa-eye"></i></span>
                                <span>{{ $video->views }}</span>
                            </div>

                        </div>
                    </li>

                @endforeach
                <a href="{{ url("videos/". $main_category->slug) }}" class="show-more"
                   style="{{ $main_category->color ? "color: $main_category->color;" : "" }}">{{trans('common.more')}}
                    <i class="fas fa-angle-double-{{ app()->getLocale() == 'ar' ? 'left' : 'right' }}"></i></a>
            </ul>
        </div>
    </div>

    <ins class="adsbygoogle"
         style="display:inline-block;width:336px;height:280px"
         data-ad-client="ca-pub-9085617575772553"
         data-ad-slot="6145269080"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>

@endif
