@if(check_gdpr())
    <div class="gdpr" style="opacity: 0.9;">
        <div class="container">
            <div class="gdpr-text" style="font-size: 12px;">
                @php

                    $page = \App\Models\Page::get_page('GDPR_bar');

                    $content = $page ? $page->content : '';

                @endphp

                {!! $content !!}
            </div>
            <div class="gdpr-close"><i class="fas fa-times"></i></div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="gdpr-content" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">GDPR Compliance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @php

                        $page = \App\Models\Page::get_page('cookies_policy');

                        $content = $page ? $page->content : '';

                    @endphp

                    {!! $content !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
                </div>
            </div>
        </div>
    </div>
@endif

@push('head')



@endpush
