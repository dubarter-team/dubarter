@if(!Auth::guard('frontend')->check())
    <!-- Modal -->
    <div class="modal fade" id="fav_login_modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content fav-popup">
                <div class="fav-popup-header">
                    <h4><i class="fas fa-star"></i> {{trans('common.favorites')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="fav-popup-body">
                    <h5>{{trans('common.to_save_favorites_please')}} <a
                                href="{{ route('login') }}">{{trans('common.login')}}</a> {{trans('common.first')}}
                    </h5>
                    <h5>{{trans('common.if_you_do_not_have_an_account_you_can_create')}} <a
                                href="{{ route('register') }}">{{trans('common.new_account')}}</a></h5>
                </div>
                <button type="button" class="btn btn-link"
                        data-dismiss="modal">{{trans('common.on_thanks')}}</button>
            </div>
        </div>
    </div>
@endif