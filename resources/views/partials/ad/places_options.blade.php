<option value="0">{{ trans("ad.all_places") }}</option>

@foreach($places as $place)

    <option value="{{ $place->id }}">{{ $place->name }}</option>

@endforeach