@foreach($ads as $ad)
    <tr>
        <td>
            <label class="control control--checkbox">
                <input type="checkbox"/>
                <div class="control__indicator"></div>
            </label>
        </td>
        <td><a href="{{ url("ads/" . $ad->slug) }}">{{$ad->title}}</a></td>

        <td>{{$ad->published_at}}</td>

        {{--<td>--}}
            {{--<a class="btn btn-delete"><i class="table-ico delete-table-ico"></i></a>--}}
        {{--</td>--}}
    </tr>
@endforeach
