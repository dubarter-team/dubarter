<div class="col-md-6 mb-2 ca-category-dp">
    <div class="custom-select-2">

        <select class="ca-select2-dropdown" required>
            <option value="">
                {{ isset($first) ? trans("ad.choose_category") : trans("ad.choose_sub_category") }}
            </option>
            @if(count($categories))

                @foreach($categories as $category)

                    @if($category->frontend_user_enabled)
                        <option {{ (isset($cat_id) && $cat_id == $category->id) ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->custom_name }}</option>
                    @endif

                @endforeach

            @endif
        </select>

    </div>
</div>
