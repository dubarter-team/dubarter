@if(count($images))

    @foreach($images as $image)

        <li>
            <span class="remove-uploaded-file">x</span>
            <div class="card img-card">
                <img src="{{ thumbnail($image->path) }}" alt="{{ $image->title }}" class="img-fluid">
                <input type="hidden" name="media_ids[]" class="images-ids-inputs" value="{{ $image->id }}" />
            </div>
        </li>

    @endforeach

@endif