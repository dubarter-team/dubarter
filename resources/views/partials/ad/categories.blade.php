@php
    $parents = (isset($cat_id) && $cat_id) ? \App\Models\Category::get_parents($cat_id) : [];
@endphp

<div class="properities mt-5">

    <div class="header-icon">
        <span class="icon"><i class="fas fa-cog"></i></span>
        <span>{{ trans('ad.category') }}</span>
    </div>


    <div class="row" id="ca-categories-div">

        @if(count($parents))

            @foreach($parents as $parent)

                @include('partials.ad.categories_dropdown', ['categories' => $categories, 'cat_id' => $parent, 'first' => $loop->first])

                @php
                    $categories = \App\Models\Category::get_childs($parent);
                @endphp

            @endforeach

            @if($categories->count())

                @include('partials.ad.categories_dropdown', ['categories' => $categories, 'cat_id' => $parent, 'first' => false])

            @endif

        @else

            @include('partials.ad.categories_dropdown', ['categories' => $categories, 'first' => true])

        @endif


        <input type="hidden" name="category_id" id="choosen-category-id" value="{{ isset($cat_id) ? $cat_id : '' }}"/>

    </div>
</div>

<div id="ca-custom-attributes">

    @php
        if(isset($cat_id) && $cat_id && empty($attributes_html)){
            $last_category = \App\Models\Category::where('id', $cat_id)->with(['custom_attributes', 'parent_category'])->first();

            if($last_category && $last_category->listing_type == 'ads'){
                $attributes_ca = \App\Models\Category::get_attributes($last_category, true);
                $attributes_html = view('partials.ad.custom_attributes', ['attributes' => $attributes_ca])->render();
            }
        }
    @endphp

    {!!  isset($attributes_html) ? $attributes_html : ''  !!}

</div>

@push("footer")

    <script>

        $(document).ready(function () {

            $(document).on('change', '.ca-category-dp select', function () {
                var ths = $(this);

                var cat_id = ths.val();

                ths.parents('.ca-category-dp').nextAll('.ca-category-dp').remove();

                $('#ca-custom-attributes').html('');

                if (ths.val() != 0) {
                    $('#choosen-category-id').val(ths.val());

                    $.ajax({
                        url: '{{ route('ajax.ca_get_sub_categories') }}',
                        data: {cat_id: cat_id},
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            if (res.status == true) {
                                if (res.count > 0) {
                                    $('#ca-categories-div').append(res.categories_html);
                                }

                                $('#ca-custom-attributes').html(res.attributes_html);

                                render_select2();
                            }
                        }
                    });
                } else {
                    var prev_dropdown = ths.parents('.ca-category-dp').prev('.ca-category-dp').find('select');

                    if (ths.parents('.ca-category-dp').index() > 0) {
                        ths.parents('.ca-category-dp').remove();
                    }

                    $.ajax({
                        url: '{{ route('ajax.ca_get_sub_categories') }}',
                        data: {cat_id: prev_dropdown.val()},
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            if (res.status == true) {
                                if (res.count > 0) {
                                    $('#ca-categories-div').append(res.categories_html);
                                }

                                $('#ca-custom-attributes').html(res.attributes_html);

                                render_select2();
                            }
                        }
                    });

                    if (prev_dropdown.length > 0) {
                        $('#choosen-category-id').val(prev_dropdown.val());
                    } else {
                        $('#choosen-category-id').val(0);
                    }
                }

            });

        });

    </script>

@endpush
