<div class="item {{ $ad->featured ? 'featured' : '' }}">

    <div class="card custom-box">
        <div class="card-img-header">
            @include('partials.common.like_button', ['ad' => $ad])

            <a href="{{ $ad->url }}">
                @if($ad->sponsored)
                    <img src="{{ assets("frontend") }}/assets/img/placeholder.jpg" data-src="{{ $ad->main_image ? thumbnail($ad->main_image, "medium") : '' }}"
                         alt="{{ $ad->title }}" class="img-fluid lzd-image">
                    @else
                    <img src="{{ assets("frontend") }}/assets/img/placeholder.jpg" data-src="{{ $ad->main_image ? thumbnail($ad->main_image, "small") : '' }}"
                         alt="{{ $ad->title }}" class="img-fluid lzd-image">
                    @endif

            </a>

            @if(!$ad->check_category(\App\Models\Category::FOODBARTER))
                <div class="price w-100">
                    <span class="quantity">{{ $ad->price ? $ad->price : '-' }}</span>
                    <span class="currancy">{{ $ad->price ? $ad->currency : '' }}</span>
                </div>
            @endif
        </div>

        <div class="card-body">
            <a href="{{ $ad->url }}" class="item-name {{ dir_class($ad->title) }}">{{ $ad->title }}</a>
            <div class="info">
                <div class="location">

                    @if($ad->country_id)
                        <img src="{{ assets("frontend") }}/assets/img/map-icon.png" alt="" class="icon" style="width: 8px;height: 12px;">
                        <span>{{ $ad->city_id ? $ad->city_name . ', ' : '' }}</span>
                        <span>{{ $ad->country_name }}</span>
                    @endif

                </div>

                <div class="item-detailes">

                    @foreach($ad->single_attributes->slice(0, 3) as $attribute)
                        <div class="icon-box">
                            <img src="{{ $attribute->small_icon_url }}"
                                 title="{{ $attribute->attribute_title }}"
                                 class="icon" style="width: 14px;height: 14px;">



                            @if(!is_array($attribute->value_title))
                             <span>{{ $attribute->value_title }}</span>
                                @else
                                <span>{{ array_values($attribute->value_title)[0] }}</span>
                            @endif
                        </div>
                    @endforeach

                </div>
                @if(!$ad->check_category(\App\Models\Category::STYLEBARTER) && !$ad->check_category(\App\Models\Category::FOODBARTER))
                <div class="date float-right">
                    <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png" alt="" class="icon" style="width: 14px;height: 13px;">
                    <span>{{ $ad->date }}</span>
                </div>
                @endif
                <script>
                    $('.custom-box .item-detailes').css('border', '0px');
                </script>
            </div>

        </div>
    </div>

</div>
