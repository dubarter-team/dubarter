@if(count($attributes))

    <div class="properities mt-5">

        <div class="header-icon">
            <span class="icon"><i class="fas fa-cog"></i></span>
            <span>{{ trans('ad.custom_attributes') }}</span>
        </div>


        <div class="row">

            @foreach($attributes as $attr)

                <?php
                $value = (isset($custom_attributes) && count($custom_attributes) && $custom_attributes->firstWhere('id', $attr->id)) ? $custom_attributes->firstWhere('id', $attr->id)->pivot->value : [old('attrs.' . $attr->slug)];
                ?>

                <div class="col-md-3  mb-2">
                    <label class="form-label">{!! $attr->required ? '<span style="color: red;">*</span> ' : '' !!}{{ $attr->name }}</label>

                    @if($attr->template == 'dropdown')

                        <div class="custom-select-2">
                            <select class="select2-dropdown" name="attrs[{{ $attr->slug }}]" aria-placeholder="" {{ $attr->required ? 'required' : '' }}>
                                <option value="">{{trans('ad.choose_value')}}</option>

                                @if(isset($attr->options[app()->getLocale()]) && count($attr->options[app()->getLocale()]))

                                    @foreach($attr->options[app()->getLocale()] as $slug => $option)

                                        <option {{ (count($value) && isset($value[0]) && ($value[0] == $slug)) ? 'selected' : '' }} value="{{ $slug }}">{{ $option }}</option>

                                    @endforeach

                                @endif

                            </select>
                        </div>

                    @else

                        <input type="{{ $attr->data_type == 'number' ? 'number' : 'text' }}" class="form-control"
                               name="attrs[{{ $attr->slug }}]" {!! $attr->data_type == 'number' ? "min='0'" : '' !!} value="{{ isset($value[0]) ? $value[0] : '' }}" {{ $attr->required ? 'required' : '' }}>

                    @endif
                </div>

            @endforeach

        </div>

    </div>

@endif