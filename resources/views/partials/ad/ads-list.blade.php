@foreach($ads as $ad)
    <tr>
        <td>
            <label class="control control--checkbox">
                <input type="checkbox"/>
                <div class="control__indicator"></div>
            </label>
        </td>
        <td>

            <a href="{{ route("ads.details", ["slug" => $ad->slug]) }}">{{$ad->title}}</a>

            <br>

            <a class="du-btn" href="{{ route("ads.details", ["slug" => $ad->slug]) }}">{{ trans("profile.preview") }}</a>

        </td>

        <td>{{$ad->created_at}}</td>

        <td>{{ trans('category.status.' . $ad->status) }}</td>

        <td>
            <a class="btn btn-edit" href="{{ route('ads.edit', ['id' => $ad->id]) }}"><i class="table-ico edit-table-ico"></i></a>
        </td>

        <td>
            @if($ad->status == 4)
            <a href="{{ route('ads.sold', ['id' => $ad->id]) }}">{{trans('profile.sold')}}</a>
                @else
                <span>{{trans('profile.no')}}</span>
            @endif
        </td>
        {{--<td>--}}
            {{--<a class="btn btn-delete"><i class="table-ico delete-table-ico"></i></a>--}}
        {{--</td>--}}
    </tr>
@endforeach


<style>

    .du-btn {
        margin-top: 5px;
        position: relative;
        top: 5px;
        font-size: 13px;
        padding: 4px;
        text-decoration: none !important;
        color: #fff !important;
        background: #616060 !important;
        border-radius: 3px;
        cursor: pointer !important;
    }

    .du-btn:visited, .du-btn.active {
        color: #fff !important;
    }

</style>
