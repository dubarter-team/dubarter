@if($video)

    <div class="card img-card float-right">
        <img src="{{ assets('frontend') }}/assets/img/video-img.jpg" alt="" class="img-fluid">
        <input type="hidden" name="media_ids[]" class="video-id-input" value="{{ $video->id }}" />
    </div>

@endif