@if($ad->branches)
    <h4>{{ trans('common.branches') }}</h4>

    <ul class="branches">
        @foreach($ad->branches as $branch)
            @if($branch['status'])
                <li>
                    <h5><i class="fas fa-flag"></i> {{ $branch['title'] }}</h5>
                    <div class="branche-address"><i class="fas fa-map-marker-alt"></i> {{ $branch['address'] }}</div>
                    <div class="branche-phone"><i class="fas fa-phone"></i>{{ $branch['phone'] }}</div>

                    @if(isset($branch['available_from']) && isset($branch['available_to']) && $branch['available_from'] && $branch['available_to'])
                        <div class="branche-time"><i
                                    class="fas fa-clock"></i><span>{{ $branch['available_from'] }}</span> <span>-</span>
                            <span>{{ $branch['available_to'] }}</span></div>
                    @endif

                    @if(isset($branch['lat']) && isset($branch['lng']) && $branch['lat'] && $branch['lng'])
                        <a data-toggle="modal" data-target="#map-popup-{{ $branch['id'] }}"><i class="fas fa-map"></i>{{ trans('common.branche_loc') }}</a>
                        <!-- Modal -->
                        <div class="modal fade bd-example-modal-lg" id="map-popup-{{ $branch['id'] }}" tabindex="-1"
                             role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close map-close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="map-{{ $branch['id'] }}" style="height: 400px;"></div>
                                </div>
                            </div>
                        </div>
                    @endif
                </li>
            @endif
        @endforeach
    </ul>
@endif

@push("footer")
    @if($ad->branches)
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCw-GKQL--li9_CxcdeBrQelj5HXulk38&callback=initMap"></script>

        <script>

            var map;
            var marker;

            function initMap() {
                var myLatlng = {lat: 30.0594699, lng: 31.1884238};

                var options = {
                    zoom: 8,
                    center: myLatlng
                };



                @foreach($ad->branches as $branch)
                    @if($branch['status'])
                        @if(isset($branch['lat']) && isset($branch['lng']) && $branch['lat'] && $branch['lng'])
                            map{{ $branch['id'] }} = new google.maps.Map(document.getElementById('map-{{ $branch['id'] }}'), options);

                            var myLatLng{{ $branch['id'] }} = { lat: {{ $branch['lat'] }}, lng: {{ $branch['lng'] }} };

                            var marker{{ $branch['id'] }} = new google.maps.Marker({
                                position: myLatLng{{ $branch['id'] }},
                                map: map{{ $branch['id'] }}
                            });
                        @endif
                    @endif
                @endforeach

            }
        </script>
    @endif
@endpush