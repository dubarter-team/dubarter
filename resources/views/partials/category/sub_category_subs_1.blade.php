@php
    $half_cats = $cats->slice(0, $per_page / 2);
@endphp

@if(count($half_cats))
    <div class="row">
        @foreach($half_cats as $cat)

            <div class="col-md-4 col-sm-6 pl-0">
                <div class="card brand-box">
                    <div class="card-body p-0">
                        <div class="brand-img">
                            <a href="{{ $cat->url }}">
                              <img src="{{ $cat->logo ? thumbnail($cat->logo->path, 'thumbnail') : '' }}" alt="{{ $cat->name }}" class="brand-img img-fluid">
                            </a>
                        </div>
                        <div class="brand-info">
                            <a href="{{ $cat->url }}">
                                <span class="brand-name">{{ $cat->custom_name }}</span>
                                <span class="count float-right">({{ $cat->ads_count }})</span>
                            </a>
                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div>
            </div>

        @endforeach
    </div>

    <div class="ads-search mb-3">


        <!-- home page resposive ads -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-9085617575772553"
             data-ad-slot="8947623489"
             data-ad-format="auto"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>

    </div>

    @php
        $half_cats = $cats->slice(($per_page / 2), $per_page / 2);
    @endphp

    @if(count($half_cats))

        <div class="row">

            @foreach($half_cats as $cat)

                <div class="col-md-4 col-sm-6 pl-0">
                    <div class="card brand-box">
                        <div class="card-body p-0">
                            <div class="brand-img">
                                <a href="{{ $cat->url }}">
                                  <img src="{{ $cat->logo ? thumbnail($cat->logo->path, 'thumbnail') : '' }}" alt="{{ $cat->name }}" class="brand-img img-fluid">
                                </a>
                            </div>
                            <div class="brand-info">
                                <a href="{{ $cat->url }}">
                                    <span class="brand-name">{{ $cat->custom_name }}</span>
                                    <span class="count float-right">({{ $cat->ads_count }})</span>
                                </a>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>

            @endforeach

        </div>

        <div class="ads-search mb-3">

            <!-- responsive ads replaced 728*90 -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9085617575772553"
                 data-ad-slot="5498890495"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>

    @endif

@endif
