<div class="custom-select-2 mob-category-dropdown">

    <select class="select2-dropdown" aria-placeholder="">
        <option value="">{{ trans('category.mobwaba_all_subs') }}</option>

        @if(count($cats))
            @foreach($cats as $cat)
                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
            @endforeach
        @endif

    </select>

</div>