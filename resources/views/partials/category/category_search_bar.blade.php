<div class="row">
    <div class="col-lg-12">
        <h5 class="d-inline-block mb-0 title"
            style="{{ $main_category->color ? "color:#fff;background: $main_category->color;" : "" }}">
            @if($main_category->id == 1)
                <img src="{{ assets("frontend") }}/assets/img/shape-five.png" alt=""
                     class="img-fluid">
            @elseif($main_category->id == 2)
                <img src="{{ assets("frontend") }}/assets/img/shape-four.png" alt=""
                     class="img-fluid">
            @elseif($main_category->id == 3)
                <img src="{{ assets("frontend") }}/assets/img/shape-three.png" alt=""
                     class="img-fluid">
            @elseif($main_category->id == 4)
                <img src="{{ assets("frontend") }}/assets/img/shape-one.png" alt=""
                     class="img-fluid">
            @elseif($main_category->id == 5)
                <img src="{{ assets("frontend") }}/assets/img/shape-two.png" alt=""
                     class="img-fluid">
            @endif

            {{ $main_category->name }}
        </h5>

        <a class="detailed-search" onclick="$('.cat-form form').submit()"  style="display:none; color:{{ $main_category->color }}" href="javascript:void(0)">{{ trans('category.detailed_search') }}</a>

        <div  class="filter cat-form" style="{{ $main_category->color ? "color:#fff;background: $main_category->color;" : "" }}">

            <form action="{{ route('search.index') }}" method="get" class="mb-0" >

                <div class="row">
                    <div class="col-lg-12 d-flex changable-region">

                        <div class="custom-select-2 mob-category-dropdown b-left">
                            <input name="q" autocomplete="off" class="typeahead form-control border-0" placeholder="{{ trans('home.search_for') }} ....">
                        </div>

                        <div class="custom-select-2 b-left">
                            <select class="select2-dropdown city-dp-ch-region" name="city_id" aria-placeholder="">
                                <option value="">{{ trans("category.city") }}</option>

                                @if(count($cities))
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="custom-select-2">
                            <select class="select2-dropdown region-dp" name="region_id" aria-placeholder="">
                                <option value="">{{trans('common.all_region')}}</option>
                            </select>
                        </div>

                        <input type="hidden" id="last-chosen-cat" name="category_id" value="{{ $main_category->id }}"/>

                        <button type="submit" class="btn search-btn"><span>{{ trans('category.search') }}</span></button>
                    </div>
                </div>

            </form>

        </div>
    </div>

</div>
