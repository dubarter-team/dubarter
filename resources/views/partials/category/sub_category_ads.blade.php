@php
    $half_ads = $ads->slice(0, $per_page / 2);
@endphp
@if(count($ads))
    <div class="row">
        @foreach($half_ads as $ad)
            <div class="col-md-4 col-sm-6 pl-0">
                @include('partials.ad.ad_card', ['ad' => $ad])
            </div>

        @endforeach
    </div>

    <div class="ads-search mb-3">


        <!-- home page resposive ads -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-9085617575772553"
             data-ad-slot="8947623489"
             data-ad-format="auto"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>

    </div>

    @php
        $half_ads = $ads->slice(($per_page / 2));
    @endphp

    @if(count($half_ads))

        <div class="row">

            @foreach($half_ads as $ad)

                <div class="col-md-4 col-sm-6 pl-0">
                    @include('partials.ad.ad_card', ['ad' => $ad])
                </div>

            @endforeach

        </div>

        <div class="ads-search mb-3">


            <!-- responsive ads replaced 728*90 -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9085617575772553"
                 data-ad-slot="5498890495"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>

    @endif

@endif
