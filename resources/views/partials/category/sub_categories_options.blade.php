@if(count($cats))
    @foreach($cats as $cat)
        <option value="{{ $cat->id }}">{{ $cat->custom_name }}</option>
    @endforeach
@endif