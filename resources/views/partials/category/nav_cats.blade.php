<div class="quick-search">
    <div class="row m-0">

        @if(count($nav_cats))
            @foreach($nav_cats as $cat)

                @if(!in_array($cat->id, [3, 6]) or config("country.code") == "eg")

                    <div class="col text-center border-left p-0">
                        <a href="{{ $cat->url }}" class="item {{ (isset($selected_cat_id) && ($selected_cat_id == $cat->id)) ? 'active' : '' }}">{{ $cat->custom_name }}</a>
                    </div>

                @endif

            @endforeach
        @endif

            @if(config("country.code") == "eg")
        @if($main_category->videos_count)
            <div class="col text-center border-left p-0">
                <a href="{{ route('videos.index', ['category_slug' => $main_category->slug]) }}"
                   class="item {{ (isset($posts_type) && ($posts_type == 'videos')) ? 'active' : '' }}">{{ trans('category.videos') }}</a>
            </div>
        @endif
            @endif

        @if($main_category->articles_count)
            <div class="col text-center border-left p-0">
                <a href="{{ route('posts.index', ['category_slug' => $main_category->slug]) }}"
                   class="item {{ (isset($posts_type) && ($posts_type == 'articles')) ? 'active' : '' }}">{{ trans('category.articles') }}</a>
            </div>

        @endif
    </div>
    <style>
      .category-search .quick-search a:hover, .category-search .quick-search .active.item {
         background: {{ $main_category->color}} !important;
      }
    </style>
</div>
