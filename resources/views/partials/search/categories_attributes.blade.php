@if(count($attributes))

    @foreach($attributes as $attr)

        <div class="col-md-4 mb-2 search-custom-attrs">
            <div class="custom-select-2">
                <select class="select2-dropdown" name="attrs[{{ $attr->slug }}][]" placeholder="{{ $attr->name }}" multiple>
                    @if(isset($attr->options[app()->getLocale()]) && count($attr->options[app()->getLocale()]))

                        @foreach($attr->options[app()->getLocale()] as $slug => $option)

                            <option {{ (Request::filled("attrs." . $attr->slug) && is_array(Request::input("attrs." . $attr->slug)) && in_array($slug, Request::input("attrs." . $attr->slug, []))) ? 'selected' : '' }} value="{{ $slug }}">{{ $option }}</option>

                        @endforeach

                    @endif
                </select>
            </div>
        </div>

    @endforeach

@endif
