<div class="col-md-4 search-category-dp mb-2">
<div class="custom-select-2">
    <select class="select2-dropdown">

        <option value="0">{{ trans("search.$label") }}</option>

        @if(count($categories))

            @foreach($categories as $category)

                <option {{ (isset($cat_id) && $cat_id == $category->id) ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->custom_name }}</option>

            @endforeach

        @endif
    </select>
</div>
</div>


