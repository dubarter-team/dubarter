@php
    $parents = (isset($cat_id) && $cat_id) ? \App\Models\Category::get_parents($cat_id) : [];
@endphp

<div class="" id="search-categories-div">

    <div id="search-all-categories" class=" row filter-row">

        @if(count($parents))

            @foreach($parents as $parent)

                @include('partials.search.categories_dropdown', ['categories' => $categories, 'cat_id' => $parent, 'label' => $loop->first ? 'category' : 'all_categories'])

                @php
                    $categories = \App\Models\Category::get_childs($parent);
                @endphp

            @endforeach

            @if($categories->count())

                @include('partials.search.categories_dropdown', ['categories' => $categories, 'cat_id' => $parent, 'label' => 'all_categories'])

            @endif

        @else

            @include('partials.search.categories_dropdown', ['categories' => $categories, 'label' => 'category'])

        @endif


        {!! $attributes_html !!}

        <div class="custom-select-2 col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <input type="number" name="price_from" value="{{ request('price_from') }}" step="500"
                           class="search-box"
                           placeholder="{{trans('category.price_form')}}">
                </div>
                <div class="col-md-6">
                    <input type="number" name="price_to" value="{{ request('price_to') }}" step="500"
                           class="search-box"
                           placeholder="{{trans('category.price_to')}}">
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" name="category_id" id="choosen-category-id" value="{{ isset($cat_id) ? $cat_id : 0 }}"/>

</div>

@push("footer")

    <script>

        $(document).ready(function () {

            $(document).on('change', '.search-category-dp select', function () {
                var ths = $(this);

                var cat_id = ths.val();

                ths.parents('.search-category-dp').nextAll('.search-category-dp').remove();

                $('.search-custom-attrs').remove();

                if (ths.val() != 0) {
                    $('#choosen-category-id').val(ths.val());

                    $.ajax({
                        url: '{{ route('ajax.search_get_sub_categories') }}',
                        data: {cat_id: cat_id},
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            if (res.status == true) {
                                if (res.count > 0) {
                                    var last_cdp = $('#search-all-categories .search-category-dp').last();

                                    $(res.categories_html).insertAfter(last_cdp);
                                }

                                var last_cdp = $('#search-all-categories .search-category-dp').last();

                                $(res.attributes_html).insertAfter(last_cdp);

                                render_select2();
                            }
                        }
                    });
                } else {
                    var prev_dropdown = ths.parents('.search-category-dp').prev('.search-category-dp').find('select');

                    if (ths.parents('.search-category-dp').index() > 0) {
                        ths.parents('.search-category-dp').remove();
                    }

                    if (prev_dropdown.length > 0) {
                        $('#choosen-category-id').val(prev_dropdown.val());

                        $.ajax({
                            url: '{{ route('ajax.search_get_sub_categories') }}',
                            data: {cat_id: prev_dropdown.val()},
                            type: 'get',
                            dataType: 'json',
                            success: function (res) {
                                if (res.status == true) {
                                    if (res.count > 0) {
                                        var last_cdp = $('#search-all-categories .search-category-dp').last();

                                        $(res.categories_html).insertAfter(last_cdp);
                                    }

                                    var last_cdp = $('#search-all-categories .search-category-dp').last();

                                    $(res.attributes_html).insertAfter(last_cdp);

                                    render_select2();
                                }
                            }
                        });
                    } else {
                        $('#choosen-category-id').val(0);
                    }
                }

            });

        });

    </script>

@endpush
