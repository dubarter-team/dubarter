<!-- Start navbar section -->
<nav class="navbar navbar-expand-xl navbar-dark bg-dark p-0 fixed-top">
    <div class="mobile-navbar">
        <ul class="navbar-nav">
            @if(Auth::guard('frontend')->check())

                <li class="nav-item mr-0 dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        {{ Auth::guard('frontend')->user()->display_name }}
                        <span class="nav-icon"><i class="fas fa-sort-down"></i></span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item"
                           href="{{ route('profile.ads.index') }}">{{ trans('common.my_ads') }}</a>
                        <a class="dropdown-item"
                           href="{{ route('profile.offers.index') }}">{{ trans('common.my_offers') }}</a>
                        <a class="dropdown-item"
                           href="{{ route('profile.ads.favourites') }}">{{ trans('common.favourites') }}</a>
                        <a class="dropdown-item"
                           href="{{ route('profile.edit.form') }}">{{ trans('common.edit_profile') }}</a>
                        <a class="dropdown-item" href="{{ route('logout') }}">{{ trans('common.nav_logout') }}</a>
                    </div>
                </li>

            @else

                <li class="nav-item mr-0 active">
                    <a class="nav-link" href="{{ route('login') }}">{{ trans('common.nav_login') }} <span
                                class="sr-only">(current)</span></a>
                </li>

            @endif

            <li class="nav-item mr-0 dropdown country">
                <a class="nav-link dropdown-toggle" href="#" role="button"
                   data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    {{ json_decode(config("country.name"))->{app()->getLocale()} }}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="list-countries">
                        @foreach($_countries as $country)
                            <a class="dropdown-item country-list"
                               href="{{ url("country?country=". $country->code) }}">{{ $country->name }}</a>
                        @endforeach
                    </div>
                </div>
            </li>

            <li class="nav-item mr-0">
                @if(app()->getLocale() == "ar")
                    <a class="nav-link" href="{{  asset("en-". \App\Models\Place::getCurrentCountry()->code) }}">
                        <i class="fas fa-globe"></i> {{ trans('common.en') }}
                    </a>
                @else
                    <a class="nav-link" href="{{ asset("ar-". \App\Models\Place::getCurrentCountry()->code) }}">
                        <i class="fas fa-globe"></i> {{ trans('common.ar') }}
                    </a>
                @endif
            </li>


        </ul>
    </div>

    <div class="container">

        <a class="navbar-brand" href="{{ url("/")}}"><img
                    src="{{ assets("frontend") }}/assets/img/logo{{ app()->getLocale() == 'ar' ? '' : '-en' }}.png"
                    alt=""
                    class="align-top" style="width: 115px;height: 29px;"></a>
        <a href="{{ route('ads.create') }}" class="nav-link put-ads mobile-new-show-only"
           primary-color>{{ trans('common.nav_add_ad') }}</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                aria-controls="navbarTogglerDemo01"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav {{ app()->getLocale() == 'ar' ? 'mr-auto mr-0' : 'ml-auto ml-0' }} mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">{{ trans('common.nav_categories') }} <span class="nav-icon"><i
                                    class="fas fa-sort-down"></i></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($_categories as $category)
                            @if($category->id != 3)
                            @if(!in_array($category->id, [3, 6]) or config("country.code") == "eg")
                                <a class="dropdown-item" href="{{ $category->url }}"> {{ $category->name }}</a>
                            @endif
                            @endif
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        {{ trans('common.nav_articles') }}
                        <span class="nav-icon"><i class="fas fa-sort-down"></i></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($_categories as $category)
                            @if($category->id != 3)
                            @if(!in_array($category->id, [3, 6]) or config("country.code") == "eg")
                                <a class="dropdown-item"
                                   href="{{ url("blog/". $category->slug) }}">{{ $category->name }}</a>
                            @endif
                            @endif
                        @endforeach
                    </div>
                </li>

                @if(config("country.code") == "eg")
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            {{ trans('common.nav_videos') }}
                            <span class="nav-icon"><i class="fas fa-sort-down"></i></span>

                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach($_categories as $category)
                                @if($category->id != 3)
                                @if(!in_array($category->id, [3, 6]) or config("country.code") == "eg")
                                    <a class="dropdown-item"
                                       href="{{ url("videos/". $category->slug) }}">{{ $category->name }}</a>
                                @endif
                                @endif
                            @endforeach
                        </div>
                    </li>
                @endif
                {{-- <li class="nav-item">
                     <a class="nav-link" href="#">أتصل بنا</a>
                 </li>--}}
            </ul>

            <ul class="navbar-nav sec-navbar {{ app()->getLocale() == 'ar' ? 'ml-auto ml-0' : 'mr-auto mr-0' }} mt-2 mt-lg-0">

                <li class="nav-item mr-0 dropdown country">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        {{ json_decode(config("country.name"))->{app()->getLocale()} }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <div class="list-countries">

                            @php

                                $request_path = request()->getRequestUri();

                                $request_path_segments = array_filter(explode("/", $request_path));

                                if(isset($request_path_segments[1])){
                                    unset($request_path_segments[1]);
                                }

                                $request_path = join("/", $request_path_segments);

                            @endphp

                            @foreach($_countries as $country)
                                <a class="dropdown-item country-list"
                                   href="{{ asset(app()->getLocale()."-". $country->code. "/". $request_path) }}">{{ $country->name }}</a>
                            @endforeach

                            {{--@foreach($_countries as $country)
                                <a class="dropdown-item country-list"
                                   href="{{ url("country?country=". $country->code) }}">{{ $country->name }}</a>
                            @endforeach--}}
                        </div>
                    </div>
                </li>

                @if(Auth::guard('frontend')->check())

                    <li class="nav-item mr-0 dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            {{ Auth::guard('frontend')->user()->display_name }}
                            <span class="nav-icon"><i class="fas fa-sort-down"></i></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item"
                               href="{{ route('profile.ads.index') }}">{{ trans('common.my_ads') }}</a>
                            <a class="dropdown-item"
                               href="{{ route('profile.offers.index') }}">{{ trans('common.my_offers') }}</a>
                            <a class="dropdown-item"
                               href="{{ route('profile.ads.favourites') }}">{{ trans('common.favourites') }}</a>
                            <a class="dropdown-item"
                               href="{{ route('profile.edit.form') }}">{{ trans('common.edit_profile') }}</a>
                            <a class="dropdown-item" href="{{ route('logout') }}">{{ trans('common.nav_logout') }}</a>
                        </div>
                    </li>

                @else

                    <li class="nav-item mr-0 active">
                        <a class="nav-link" href="{{ route('login') }}">{{ trans('common.nav_login') }} <span
                                    class="sr-only">(current)</span></a>
                    </li>

                @endif

                @if(false)
                    <li class="nav-item mr-0 dropdown country">
                        <a class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            {{ json_decode(config("country.name"))->{app()->getLocale()} }}
                            <span class="nav-icon"><i class="fas fa-sort-down"></i></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <div class="list-countries">
                                @foreach($_countries as $country)
                                    <a class="dropdown-item country-list"
                                       href="{{ url("country?country=". $country->code) }}">{{ $country->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </li>
                @endif


                <li class="nav-item mr-0">
                    @if(app()->getLocale() == "ar")
                        <a class="nav-link" href="{{  asset("en-". \App\Models\Place::getCurrentCountry()->code) }}">
                            <i class="fas fa-globe"></i> {{ trans('common.en') }}
                        </a>
                    @else
                        <a class="nav-link" href="{{ asset("ar-". \App\Models\Place::getCurrentCountry()->code) }}">
                            <i class="fas fa-globe"></i> {{ trans('common.ar') }}
                        </a>
                    @endif
                </li>

                <li>
                    <a href="{{ route('ads.create') }}" class="nav-link put-ads"
                       primary-color>{{ trans('common.nav_add_ad') }}</a>
                </li>
            </ul>

        </div>
    </div>
</nav>


<!-- End navbar section -->
