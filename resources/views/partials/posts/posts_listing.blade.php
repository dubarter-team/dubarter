@foreach($posts as $post)

    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="card custom-box">
            <div class="card-img-header">
                <a href="{{ $post->url }}">
                    <img src="{{ $post->image ? thumbnail($post->image->path, 'medium') : '' }}"
                         alt="" class="img-fluid w-100">
                </a>
            </div>
            <div class="card-body">
                {{-- <div class="date">
                    <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png" alt="" class="icon">
                    <span>{{ date("Y-m-d", strtotime($post->created_at)) }}</span>
                </div> --}}
                <div class="title">
                    <a href="{{ $post->url }}" class="{{ dir_class($post->title) }}">
                        {{ $post->title }}
                    </a>
                </div>

                <div class="desc {{ dir_class($post->title) }}">
                    <p>{{ str_limit($post->excerpt, 100) }}</p>
                </div>

                @if($post->format == 'article')
                    <a href="{{ $post->url }}" class="read-more float-right">{{ trans('common.read_article') }}</a>
                @elseif($post->format == 'video')
                    <a href="{{ $post->url }}" class="read-more float-right">{{ trans('common.watch_video') }}</a>
                @endif
            </div>
        </div>
    </div>

@endforeach
