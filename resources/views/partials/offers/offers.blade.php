@foreach($offers as $offer)
    <tr>
        <td>
            <label class="control control--checkbox">
                <input type="checkbox"/>
                <div class="control__indicator"></div>
            </label>
        </td>
        <td><a href="{{ route('ads.details', ['slug' => $offer->ad->slug]) }}">{{ $offer->ad->title }}</a> </td>
        <td>{{$offer->price}}</td>
        <td>{{$offer->created_at->render()}}</td>
    </tr>
@endforeach
