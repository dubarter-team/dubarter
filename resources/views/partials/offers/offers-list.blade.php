@foreach($offers as $offer)

    <?php $repliesCount = ($offer->replies()->count());?>

    <li class="comment-wrapper" data-comment-id="{{$offer->id}}"
        data-replies="{{$repliesCount}}">
        <div class="comment-main-level">
            <!-- Avatar -->
            <div class="comment-avatar">
                <img src="{{ $offer->sender->photo ? thumbnail($offer->sender->photo->path) : '' }}" alt="">
            </div>

            <div class="comment-box">
                <div class="comment-head">
                    <h6 class="comment-name">
                        <a href="#">{{ $offer->sender->display_name }}</a>
                    </h6>
                    <span>{{ $offer->created_at }}</span>
                    <a href="javascript:void(0)" class="reply-show">
                        <i class="fa fa-reply"></i>
                    </a>
                </div>
                <div class="comment-content">{{ $offer->comment }}</div>
            </div>
        </div>

        <ul class="comments-list reply-list">
            @if($repliesCount)
                <input type="hidden" class="replies-flag" name="replies" value="{{$offer->id}}"/>
            @endif
        </ul>
        @if($repliesCount)
            <a href="javascript:void(0)" class="more-reply">{{trans('common.loadmore')}}</a>
        @endif
    </li>

@endforeach