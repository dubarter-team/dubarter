<div class="comments-container">
    @if($mine)
        <h3>{{trans('ad.comments')}}</h3>
    @else
        <h3>{{trans('ad.negotiate_the_price')}}</h3>
    @endif

    <ul id="comments-list" class="comments-list">
        @if(count($offers))
            @include('partials.offers.offers-list',['offers'=>$offers])
        @endif
    </ul>

    <div class="load-more-btn" id="more-comment">
        <a class="btn btn-defult green-btn text-white"
           @if(count($offers) <= 4) style="display: none" @endif>{{ trans('search.more_ads') }}</a>
    </div>

    @if(count($offers)==0)
        <div>{{ trans('common.no_comments') }}</div>
    @endif
</div>

@push('footer')
    <style>
        .load-more-btn {
            margin: 0px;
        }

        .more-reply {
            float: left;
            padding: 0 0 0 20px;
        }

        .reply-show {
            color: #000;
        }

        .reply-show:hover {
            color: #8cad10;
        }
    </style>
    <script>
        // More comments
        $(document).ready(function (e) {
            var url = "{{route('profile.offers.json')}}";
            var offset = 5;
            $('#more-comment a').click(function (e) {
                $self = $(this);
                $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
                $.ajax({
                    url: url,
                    data: {
                        offset: offset,
                        ad_id:{{ $ad_id }}
                    },
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        $(res.offersHtml).hide().appendTo('#comments-list').fadeIn(1000);
                        offset += res.count;

                        if (res.count < 4) {
                            $self.fadeOut(1000).remove();
                        }
                    },
                    complete: function () {
                        updateReplies(2);
                        $self.html('{{ trans("search.more_ads") }}');
                    }
                });
            });
        });


        // More Replies
        $(document).ready(function (e) {
            let url = "{{route('profile.offers.replies')}}";
            $('.comments-container').on('click', '.more-reply', function (e) {
                var $self = $(this);
                var $commentParent = $self.closest('.comment-wrapper');
                var $targetList = $commentParent.find('.reply-list');
                let offset = $commentParent.find('ul.reply-list li').length;
                $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
                $.ajax({
                    url: url,
                    data: {
                        offset: offset,
                        offer_id: $commentParent.data('commentId'),
                    },
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        // $targetList.append(res.repliesHtml);
                        $(res.repliesHtml).hide().appendTo($targetList).fadeIn(1000);
                        if (res.count < 2) {
                            $self.fadeOut(1000).remove();
                        }        // More comments

                    },
                    complete: function () {
                        $self.html('{{ trans("search.more_ads") }}');
                    }
                });
            });
        });

        // replies-flag

        $(document).ready(function (e) {
            updateReplies(2);
        });

        function updateReplies(limit) {

            $('input.replies-flag').each(function (index, input) {
                var $self = $(input);
                var $commentParent = $self.closest('.comment-wrapper');
                var $targetList = $commentParent.find('.reply-list');
                var $loadMore = $commentParent.find('.more-reply');
                $.ajax({
                    url: "{{route('profile.offers.replies')}}",
                    data: {
                        offset: 0,
                        offer_id: $commentParent.data('commentId'),
                        limit: limit
                    },
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        $(res.repliesHtml).hide().appendTo($targetList).fadeIn(1000);
                        if (res.replies_count < 2) {
                            $loadMore.remove();
                        }
                        // remove input
                        $self.remove()
                    },
                    complete: function () {
                        $loadMore.html('{{ trans("search.more_ads") }}');
                    }
                });
            });

        }


    </script>
    <?php $user = Auth::guard('frontend')->user() ?>
    <script>


        $(document).ready(function (e) {


            // make Reply
            $('.comments-container').on('click', '.send-button', function (e) {
                var $self = $(this);
                var $commentParent = $self.closest('.comment-wrapper');
                var $targetList = $commentParent.find('.reply-list');
                var $form = $commentParent.find('.reply-form');
                var $textarea = $targetList.find('textarea.reply-content');
                $self.attr('disabled', true);
                $textarea.attr('disabled', true);
                $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
                $.ajax({
                    url: "{{route('profile.offers.reply')}}",
                    data: {
                        comment: $textarea.val(),
                        offer_id: $self.data('commentId')
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        if (res.repliesHtml) {
                            $(res.repliesHtml).hide().insertAfter($form).fadeIn(1000);
                        }
                    },
                    complete: function () {
                        $self.attr('disabled', false);
                        $textarea.attr('disabled', false);
                        $textarea.attr('disabled', false);
                        $textarea.html('');
                        $textarea.val('');
                        $textarea.trigger('input');
                        $self.html('{{trans('category.send')}}');
                    }
                });
            });


            // Creating Relpy form on click
            $('.comments-container').on('click', '.reply-show', function (e) {
                $self = $(this);
                var $commentParent = $self.closest('.comment-wrapper');
                var $targetList = $commentParent.find('.reply-list');
                if ($targetList.find('.reply-form').length == 0) {
                    let replyObject = createReplyObject(($commentParent.data('commentId')));
                    $(replyObject).hide().prependTo($targetList).fadeIn(500);
                }

            });


            $('.comments-container').on('input', 'textarea', function () {
                if ($(this).val().trim() == 0) {
                    $(this).closest('li').find('button').attr('disabled', true);
                    return
                }
                $(this).closest('li').find('button').attr('disabled', false);
            });

        });

        /**
         * Create Rely form
         * @param comment_id
         * @returns {string}
         */
        function createReplyObject(comment_id) {
            return '<li class="reply-form"><div class="comment-avatar">' +
                '<img src="{{ $user->photo ? thumbnail($user->photo->path) : '' }}"alt=""></div>' +
                '<div class="comment-box">' +
                '<div class="comment-head">' +
                '<h6 class="comment-name"><a href="#">{{ $user->display_name }}</a></h6>' +
                '<span>{{ $user->created_at }}</span>' +
                '<div class="pull-left">' +
                '<button class="btn btn-defult green-btn text-white send-button" disabled data-comment-id="' + comment_id + '"' +
                '>{{trans('category.send')}}' +
                '</button></div>' +
                '</div><div class="comment-content">' +
                '<textarea name="content" class="form-control reply-content" rows="2"></textarea></div></div></li>';
        }
    </script>
@endpush