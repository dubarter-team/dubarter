@foreach($replies as $reply)
    <li>
        <!-- Avatar -->
        <div class="comment-avatar">
            <img src="{{ $reply->sender->photo ? thumbnail($reply->sender->photo->path) : '' }}"
                 alt="">
        </div>

        <div class="comment-box">
            <div class="comment-head">
                <h6 class="comment-name">
                    <a href="#">{{ $reply->sender->display_name }}</a>
                </h6>
                <span>{{ $reply->created_at }}</span>
            </div>
            <div class="comment-content">{{ $reply->comment }}</div>
        </div>
    </li>
@endforeach