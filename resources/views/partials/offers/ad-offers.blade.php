@if($offer)

    @include('partials.offers.my-offers',['offers'=>collect([$offer]),'ad_id'=>$ad->_id, 'mine' => false])

@else

    <div id="offer-add-options">
        <h4 id="offer-title">{{empty($offer)?trans('common.add_offer'):trans('common.offer')}}</h4>

        <div class="error-message" id="offer-error-message">

        </div>

        <div class="form-row offer-form">
            <div class="col-lg-6">
                <select name="type" id="offer-type" {{empty($offer)?'':'readonly disabled'}}  class="form-control">
                    <option disabled selected> -- {{trans('common.select_offer_type')}} --</option>
                    <option value="bid" {{!empty($offer)&&$offer->type=="bid"?'selected':''}}> {{trans('common.bid')}}</option>
                    <option value="bart" {{!empty($offer)&&$offer->type=="bart"?'selected':''}}> {{trans('common.bart')}}</option>
                </select>
            </div>

            <div class="col-lg-6">
                <input type="text" id="offer-price" class="form-control"
                       {{empty($offer)?'':'readonly disabled'}} placeholder="{{trans('common.price')}}"
                       value="{{!empty($offer)?$offer->price:''}}">
            </div>

            <div class="col-lg-12">
                <textarea name="comment" {{empty($offer)?'':'readonly disabled'}}  class="form-control"
                          placeholder="{{trans('common.message')}}" id="offer-comment"
                          rows="5">{{!empty($offer)?$offer->comment:''}}</textarea>
            </div>

            @if(empty($offer))
                <div id="submit-offer">
                    <a class="btn btn-defult green-btn text-white">{{ trans('common.send') }}</a>
                </div>
            @endif
        </div>

    </div>

@endif


@if(empty($offer))
    @push('footer')
        <script>
            var $message = $('#offer-error-message');
            $(document).on('click', '#submit-offer', function () {
                $message.html('');
                var data = {
                    type: $('.offer-form #offer-type').val(),
                    price: $('.offer-form #offer-price').val(),
                    comment: $('.offer-form #offer-comment').val(),
                    ad_slug: "{{$ad->slug}}"
                };
                var $self = $(this);
                var orginalHtml = $self.html();
                $self.attr('disabled', true);
                $self.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
                $.ajax({
                    url: "{{route('profile.offers.add')}}",
                    data: data,
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        if (res.status) {
                            $self.remove();
                            $('#offer-title').html('{{trans('common.sent_offer')}}');
                            $('.offer-form').remove();
                            $('#offer-add-options').html(res.offer_html);
                            return;
                        } else {
                            var errors = res.errors.map(function (error) {
                                return '<li>' + error + '</li>'
                            });
                            var html = '<ul>' + errors.join('') + '</ul>';
                            $message.html(html);
                            $self.attr('disabled', true);
                            $self.html(orginalHtml);
                        }
                    },
                    complete: function () {
                        $($self).html(orginalHtml);
                    }
                });
            });
        </script>
    @endpush
@endif