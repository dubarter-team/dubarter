<div class="filter">
    <form method="get" action="{{ route('search.index') }}">
        <div class="row justify-content-md-center">
            <div class="col-lg-12 changable-region">
                <div class="col-sm-5">
                    <input type="text" class="home-search-input" name="q" placeholder="{{ trans('home.search_for') }} ....">
                </div>

                <div class="col-sm-3">
                    <select class="choose-city select2-dropdown city-dp-ch-region" name="city_id" aria-placeholder="">
                        <option value="">{{ trans("home.city") }}</option>
                        @if(count($cities))
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-sm-3">
                    <select class="choose-city select2-dropdown region-dp" name="region_id" aria-placeholder="">
                        <option value="">{{trans('common.all_region')}}</option>
                    </select>
                </div>

                <input type="hidden" name="category_id" value="{{ $category_id }}" />

                <div class="col-sm-1">
                    <button type="submit" class="search-icon btn">
                        <img src="{{ assets("frontend") }}/assets/img/search-icon.png" alt="">
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>