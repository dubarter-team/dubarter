<section id="aqura-barter">
    <div class="container">
        <h4 class="text-center font-weight-bold py-4 mb-0">
          <a href="{{ $ms_main_cats->where('id', \App\Models\Category::AQARBARTETR)->first()->url }}">{{ $ms_main_cats->where('id', \App\Models\Category::AQARBARTETR)->first()->custom_name }}</a>
        </h4>

        @include('partials.home.sections_search_bar', ['category_id' => \App\Models\Category::AQARBARTETR])

        <ul class="nav nav-pills justify-content-center m-0" id="pills-tab" role="tablist">
            @foreach($featured_realestate as $type => $ads)
                <li class="nav-item">
                    <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="pills-{{ $type }}-tab"
                       data-toggle="pill" href="#pills-{{ $type }}" role="tab"
                       aria-controls="pills-{{ $type }}"
                       aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ trans('home.' . $type) }}</a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content" id="pills-tabContent">

            @foreach($featured_realestate as $type => $ads)

                <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="pills-{{ $type }}"
                     role="tabpanel"
                     aria-labelledby="pills-{{ $type }}-tab">
                    <div class="owl-carousel owl-theme">
                        @if(count($ads))
                            @foreach($ads as $ad)

                                @include('partials.ad.ad_card', ['ad' => $ad])

                            @endforeach
                        @endif
                    </div>
                </div>

            @endforeach

        </div>
        <div class="advertise">

            <!-- home page resposive ads -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9085617575772553"
                 data-ad-slot="8947623489"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>

    </div>
    <a href="#style-barter" class="scroll-section">
        <span class="d-block">{{ trans("home.style_title") }}</span>
        <img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt="">
    </a>
</section>
