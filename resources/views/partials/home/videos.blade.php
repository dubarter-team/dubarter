<!-- Start videos section -->
<section id="videos">
    <div class="container">
        <h4 class="text-center mb-0">{{ trans('home.videos_title') }}</h4>

        <ul class="nav nav-pills justify-content-center m-0" id="pills-tab" role="tablist">
            @foreach($featured_videos as $type => $videos)
                <li class="nav-item">
                    <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="pills-{{ $type }}-tab" data-toggle="pill"
                       href="#pills-{{ $type }}" role="tab"
                       aria-controls="pills-{{ $type }}"
                       aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ trans('home.' . $type) }}</a>
                </li>
            @endforeach

        </ul>
        <div class="tab-content" id="pills-tabContent">

            @foreach($featured_videos as $type => $videos)

                <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="pills-{{ $type }}"
                     role="tabpanel" aria-labelledby="pills-{{ $type }}-tab">

                    <div class="owl-carousel owl-theme">

                        @foreach($videos as $video)

                            <div class="item">

                                <div class="card viedo">
                                    <a href="{{ $video->url }}">
                                        <div class="icon"><span class="play-icon-video"><i class="fa fa-play"
                                                                                           aria-hidden="true"></i></span>
                                        </div>

                                        @if($video->thumbnail)
                                            <img src="{{ assets("frontend") }}/assets/img/placeholder-videos.jpg" data-src="{{ $video->thumbnail ? thumbnail($video->thumbnail->path, "video_thumbnail") : '' }}"
                                                 alt="" class=" lzd-image">
                                        @else
                                            <img src="{{ assets("frontend") }}/assets/img/placeholder-videos.jpg" data-src="{{ $video->image ? thumbnail($video->image->path, "medium") : '' }}"
                                                 alt="" class=" lzd-image">
                                        @endif
                                    </a>

                                    <div class="video-details d-none">
                                        <div class="title text-center border-bottom">
                                            <a href="{{ url("videos/".$video->category->slug) }}" class="color-white">
                                                <span>
                                                    @if($video->category->id == 1)
                                                        <img src="{{ assets("frontend") }}/assets/img/shape-five.png"
                                                             alt=""
                                                             style="width: 33px !important;height: 27px !important;"
                                                             class="img-fluid">
                                                    @elseif($video->category->id == 2)
                                                        <img src="{{ assets("frontend") }}/assets/img/shape-four.png"
                                                             alt=""
                                                             style="width: 33px !important;height: 27px !important;"
                                                             class="img-fluid">
                                                    @elseif($video->category->id == 3)
                                                        <img src="{{ assets("frontend") }}/assets/img/shape-three.png"
                                                             alt=""
                                                             style="width: 33px !important;height: 27px !important;"
                                                             class="img-fluid">
                                                    @elseif($video->category->id == 4)
                                                        <img src="{{ assets("frontend") }}/assets/img/shape-one.png"
                                                             alt=""
                                                             style="width: 33px !important;height: 27px !important;"
                                                             class="img-fluid">
                                                    @elseif($video->category->id == 5)
                                                        <img src="{{ assets("frontend") }}/assets/img/shape-two.png"
                                                             alt=""
                                                             style="width: 33px !important;height: 27px !important;"
                                                             class="img-fluid">
                                                    @endif

                                                    </span>

                                                {{ $video->category->name }}
                                            </a>

                                        </div>
                                        <div class="desc">
                                            <a href="{{ $video->url }}" class="color-white {{ dir_class($video->title) }}">{{ $video->title }}</a>
                                        </div>

                                        <div class="info py-2 px-3">
                                            {{-- <div class="d-inline-block"><i class="fas fa-calendar-alt"></i>
                                                <span class="date">{{ $video->date }}</span>
                                            </div> --}}

                                            <div class="d-inline-block float-right">
                                                <i class="fas fa-eye"></i>
                                                <span class="views">{{ $video->views }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>

                </div>

            @endforeach



            {{--<div class="tab-pane fade" id="pills-load-more" role="tabpanel" aria-labelledby="pills-load-more-tab">--}}
            {{--more--}}
            {{--</div>--}}
        </div>
        <div class="advertise">

            <!-- home page resposive ads -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9085617575772553"
                 data-ad-slot="8947623489"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>
    </div>
    <a href="#artical-barter" class="scroll-section">
        <span class="d-block">{{trans('home.dubarter_articles')}}</span>
        <img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt="">
    </a>
</section>
<!-- End videos section -->
