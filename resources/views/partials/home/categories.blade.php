<?php
$useragent = $_SERVER['HTTP_USER_AGENT'];
$is_mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
?>


<div id="categories">
    <div class="cat-box">
        <ul>

            <li class="{{ $is_mobile ? 'flex-slide' : 'flex-slide-row' }}"
                onClick="location.href='{{ $ms_cars->url }}'">
                <div class="header-box"
                     style="{{ $ms_cars->color ? "background: $ms_cars->color !important;" : "" }}">
                    <h3><a href="{{ $ms_cars->url }}">{{ $ms_cars->custom_name }}</a></h3>
                    <span class="circle"
                          style="{{ $ms_cars->color ? "background: $ms_cars->color !important;" : "" }}">
                        <img src="{{ assets("frontend") }}/assets/img/shape-five.png" style="width: 33px;height: 27px;"
                             alt="" class="img-fluid">

            </span>
                </div>
                <div class="body-box">
                    <div class="gray-overlay"></div>
                    <img class="main-image" style="width: 233px;height: 361px;"
                         src="{{ $ms_cars->image ? uploads_url($ms_cars->image->path) : '' }}">

					<?php /*
                    <div class="slide-details">
                        <div class="row">
                            @if($ms_cars_main_sub_cats->count())
                                @foreach($ms_cars_main_sub_cats as $ms_cat)

                                    <div class="col-md-6 no-margin no-padding">
                                        <div class="panel panel-default">

                                            <div class="panel-heading">{{ $ms_cat->custom_name }}</div>

                                            @if($ms_cat->id == \App\Models\Category::NEW_CARS)

                                                <div class="panel-body brands-list cars">
                                                    <div class="row">
                                                        @foreach(${'ms_cars_sub_cats_' . $ms_cat->id} as $ms_subcat)
                                                            <div class="col-md-4 car-logo">
                                                                <a href="{{ $ms_subcat->url }}" class="cars-brand">
                                                                    <img src="{{ assets("frontend") }}/assets/img/placeholder-cars.jpg" data-src="{{ $ms_subcat->logo ? thumbnail($ms_subcat->logo->path) : '' }}"
                                                                         alt="{{ $ms_subcat->name }}" class="lzd-image" style="width: 49px;height: 33px;">
                                                                <!--<span class="count">{{ $ms_subcat->ads_count }}</span>-->
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>

                                            @elseif($ms_cat->id == \App\Models\Category::USED_CARS)

                                                <div class="panel-body  vertical-list blog">
                                                    <div class="row">
                                                        @foreach(${'ms_cars_sub_cats_' . $ms_cat->id} as $ms_subcat)
                                                            <a class="link" href="{{ $ms_subcat->url }}">
                                                                <span>{{ $ms_subcat->custom_name }}</span>
                                                                <span class="ads-count">{{ $ms_subcat->ads_count }}</span>
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </div>

                                            @endif

                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center custom-btn">
                                    <a href="{{ route('search.index', ['category_id' => \App\Models\Category::AUTOBARTER]) }}" class="btn btn-green">{{ trans('home.buy_a_car') }}</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center custom-btn">
                                    <a href="{{ route('ads.create') }}" class="btn btn-green">{{ trans('home.sell_your_car') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    */ ?>
                </div>
            </li>
            <!-- <div class="col-md-4 no-margin no-padding text-center button-area">
                                <a href="" class="btn-main"></a>
                            </div> -->
            <li class="{{ $is_mobile ? 'flex-slide' : 'flex-slide-row' }}"
                onClick="location.href='{{ $ms_realestate->url }}'">
                <div class="header-box"
                     style="{{ $ms_realestate->color ? "background: $ms_realestate->color !important;" : "" }}">
                    <h3><a href="{{ $ms_realestate->url }}">{{ $ms_realestate->custom_name }}</a></h3>
                    <span class="circle"
                          style="{{ $ms_realestate->color ? "background: $ms_realestate->color !important;" : "" }}">
                        <img src="{{ assets("frontend") }}/assets/img/shape-four.png" style="width: 36px;height: 33px;"
                             alt="" class="img-fluid">

            </span>
                </div>
                <div class="body-box">
                    <div class="gray-overlay"></div>
                    <img class="main-image" style="width: 233px;height: 361px;"
                         src="{{ $ms_realestate->image ? uploads_url($ms_realestate->image->path) : '' }}">
				<?php /*
                    <div class="slide-details">
                        <div class="row">

                            @if($ms_realestate_main_sub_cats->count())
                                @foreach($ms_realestate_main_sub_cats as $ms_cat)

                                    <div class="col-md-6 no-margin no-padding">
                                        <div class="panel panel-default">

                                            <div class="panel-heading">{{ $ms_cat->custom_name }}</div>

                                            <div class="panel-body vertical-list blog">

                                                @foreach(${'ms_realestate_sub_cats_' . $ms_cat->id} as $ms_subcat)

                                                    <a class="link" href="{{ $ms_subcat->url }}">
                                                        <span>{{ $ms_subcat->custom_name }}</span>
                                                    </a>

                                                @endforeach

                                            </div>

                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center custom-btn">
                                    <a href="{{ route('ads.create') }}" class="btn btn-green">{{trans('home.sell_your_property')}}</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center custom-btn">
                                    <a href="{{ route('ads.create') }}" class="btn btn-green">{{trans('home.rent_your_property')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                */ ?>
            </li>

            @if(config("country.code") == "eg" && false)
                <li class="{{ $is_mobile ? 'flex-slide' : 'flex-slide-row' }}"
                    onClick="location.href='{{ $ms_style->url }}'">
                    <div class="header-box"
                         style="{{ $ms_style->color ? "background: $ms_style->color !important;" : "" }}">
                        <h3><a href="{{ $ms_style->url }}">{{ $ms_style->custom_name }}</a></h3>
                        <span class="circle"
                              style="{{ $ms_style->color ? "background: $ms_style->color !important;" : "" }}">
                            <img src="{{ assets("frontend") }}/assets/img/shape-three.png"
                                 style="width: 40px;height: 49px;" alt="" class="img-fluid">

                </span>
                    </div>
                    <div class="body-box">
                        <div class="gray-overlay"></div>
                        <img class="main-image" style="width: 233px;height: 361px;"
                             src="{{ $ms_style->image ? uploads_url($ms_style->image->path, "thumbnail") : '' }}">
						<?php /*
                    <div class="slide-details  lifestyle-area">

                        <div class="row">

                            @if($ms_style_main_sub_cats->count())
                                @foreach($ms_style_main_sub_cats->slice(0,2) as $ms_cat)

                                    <div class="col-md-6 no-margin no-padding">
                                        <div class="panel panel-default">

                                            <div class="panel-heading">{{ $ms_cat->custom_name }}</div>

                                            <div class="panel-body brands-list style">
                                                <div class="row">
                                                    @foreach(${'ms_style_sub_cats_' . $ms_cat->id} as $ms_subcat)

                                                        <a class="col-md-6 no-margin no-padding"
                                                           href="{{ $ms_subcat->url }}">
                                                            <div class="home-slider-thumb"><img src="{{ $ms_subcat->logo ? thumbnail($ms_subcat->logo->path, "thumbnail") : '' }}"
                                                                 alt="{{ $ms_subcat->custom_name }}"></div>
                                                        </a>

                                                    @endforeach
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>

                        <?php $style_third_cat = $ms_style_main_sub_cats->last(); ?>

                        @if($style_third_cat)
                            <div class="row">
                                <div class="col-md-12 no-margin no-padding decore">

                                    <div class="panel panel-default">

                                        <div class="panel-heading">{{ $style_third_cat->custom_name }}</div>

                                        <div class="panel-body">

                                            <div class="panel-body brands-list">
                                              <div class="row">
                                                @foreach(${'ms_style_sub_cats_' . $style_third_cat->id} as $ms_subcat)
                                                  <div class="col-md-6">
                                                    <a href="{{ $ms_subcat->url }}">
                                                        <img src="{{ $ms_subcat->logo ? thumbnail($ms_subcat->logo->path, "thumbnail") : '' }}"
                                                             alt="{{ $ms_subcat->custom_name }}">
                                                    </a>
                                                  </div>
                                                @endforeach
                                              </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endif

                    </div>
                    */ ?>
                    </div>
                </li>
            @endif
            <li class="{{ $is_mobile ? 'flex-slide' : 'flex-slide-row' }}"
                onClick="location.href='{{ $ms_mobawba->url }}'">
                <div class="header-box"
                     style="{{ $ms_mobawba->color ? "background: $ms_mobawba->color !important;" : "" }}">
                    <h3><a href="{{ $ms_mobawba->url }}">{{ $ms_mobawba->custom_name }}</a></h3>
                    <span class="circle"
                          style="{{ $ms_mobawba->color ? "background: $ms_mobawba->color !important;" : "" }}">
                    <img src="{{ assets("frontend") }}/assets/img/shape-two.png" style="width: 35px;height: 35px;"
                         alt="" class="img-fluid">

                    </span>
                </div>
                <div class="body-box">
                    <div class="gray-overlay"></div>
                    <img class="main-image" style="width: 233px;height: 361px;"
                         src="{{ $ms_mobawba->image ? uploads_url($ms_mobawba->image->path) : '' }}">
					<?php /*
                    <div class="slide-details">

                        <div class="row">

                            @if($ms_mobawba_main_sub_cats->count())
                                @foreach($ms_mobawba_main_sub_cats as $ms_cat)

                                    <div class="col-md-6 no-margin no-padding">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">{{ $ms_cat->custom_name }}</div>
                                            <div class="panel-body vertical-list blog">
                                                @foreach(${'ms_mobawba_sub_cats_' . $ms_cat->id} as $ms_subcat)

                                                    <a class="link" href="{{ $ms_subcat->url }}">
                                                        <span>{{ $ms_subcat->custom_name }}</span>
                                                    </a>

                                                @endforeach
                                            </div>

                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="text-center custom-btn">
                                    <a href="{{ route('search.index', ['category_id' => \App\Models\Category::JOBS]) }}" class="btn btn-green">{{trans('home.search_jobs')}}</a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="text-center custom-btn">
                                    <a href="{{ route('ads.create') }}" class="btn btn-green">{{trans('home.sell_your_mobile')}}</a>
                                </div>
                            </div>

                        </div>

                    </div>
                    */ ?>
                </div>
            </li>
            <li class="{{ $is_mobile ? 'flex-slide' : 'flex-slide-row' }}"
                onClick="location.href='{{ $ms_food->url }}'">
                <div class="header-box"
                     style="{{ $ms_food->color ? "background: $ms_food->color !important;" : "" }}">
                    <h3><a href="{{ $ms_food->url }}">{{ $ms_food->custom_name }}</a></h3>
                    <span class="circle" style="{{ $ms_food->color ? "background: $ms_food->color !important;" : "" }}">
                    <img src="{{ assets("frontend") }}/assets/img/shape-one.png" style="width: 38px;height: 31px;"
                         alt="" class="img-fluid">
                    </span>
                </div>
                <div class="body-box">

                    <div class="gray-overlay"></div>
                    <img class="main-image" style="width: 233px;height: 361px;"
                         src="{{ $ms_food->image ? uploads_url($ms_food->image->path) : '' }}">

					<?php /*
                    <div class="slide-details">
                        <div class="row">

                            @if($ms_food_main_topics->count())
                                @foreach($ms_food_main_topics as $ms_topic)

                                    <div class="col-md-6 no-margin no-padding">
                                        <div class="panel panel-default">

                                            <div class="panel-heading">{{ $ms_topic->custom_name }}</div>

                                            <div class="panel-body food">
                                                @foreach(${'ms_food_tags_' . $ms_topic->id} as $ms_tag)

                                                    <a class="link" href="{{ route('tag.index', ['slug' => $ms_tag->slug]) }}">
                                                        <span>{{ $ms_tag->name }}</span>
                                                    </a>

                                                @endforeach

                                            </div>

                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>

                    </div>
                    */ ?>
                </div>
            </li>

        </ul>
    </div>
</div>


@if(config("country.code") == "iq")
    <style>

        .cat-box ul li {
            width: 25% !important;
        }

        .cat-box .main-image, .cat-box .gray-overlay {
            width: 100% !important;
        }

        @media (max-width: 768px) {
            #categories .cat-box li.flex-slide:nth-child(3), #categories .cat-box li.flex-slide:nth-child(4) {
                width: 48% !important;
            }
        }

    </style>
@endif
