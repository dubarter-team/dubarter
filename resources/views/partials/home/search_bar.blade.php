<form method="get" action="{{ route('search.index') }}" class="search">
    <div class="search-input d-inline-block changable-region">
        <div class="row">
            <div class="custom-input-search col col-md-6">
                <input name="q" autocomplete="off" class="typeahead form-control border-0"
                       placeholder="{{ trans('home.search_for') }} ....">
            </div>
            <div class="custom-input-search col col-md-3 border-search">
            <!--<input class="typeahead form-control border-0" id="city-search-input" placeholder="{{ trans('home.city') }}">-->
                <select class="choose-city select2-dropdown city-dp-ch-region" name="city_id" aria-placeholder="">
                    <option value="">{{ trans("home.city") }}</option>
                    @if(count($cities))
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="custom-input-search col col-md-3 border-search">
            <!--<input class="typeahead form-control border-0" id="city-search-input" placeholder="{{ trans('home.city') }}">-->
                <select class="choose-city select2-dropdown region-dp" name="region_id" aria-placeholder="">
                    <option value="">{{trans('common.all_region')}}</option>
                </select>
            </div>
        </div>

    </div>
    <button class="btn btn-defult green-btn translate">
        <span>{{ trans('home.search') }}</span>
    </button>
</form>
