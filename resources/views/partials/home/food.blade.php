<section id="food-barter">
    <div class="container">
        <h4 class="text-center font-weight-bold mb-0"><a href="{{ $ms_main_cats->where('id', \App\Models\Category::FOODBARTER)->first()->url }}">{{ $ms_main_cats->where('id', \App\Models\Category::FOODBARTER)->first()->custom_name }}</a></h4>

        @include('partials.home.sections_search_bar', ['category_id' => \App\Models\Category::FOODBARTER])

        <div class="filter-search">
            <div class="row">
                @if($day_restaurant)
                    <div class="col-md-4">
                        <div class="card food-card">

                            <a href="{{ $day_restaurant->url }}" class="item-name"><img src="{{ assets("frontend") }}/assets/img/placeholder-today-restaurant.jpg" data-src="{{ $day_restaurant->main_image ? thumbnail($day_restaurant->main_image, "medium") : '' }}" alt="{{ $day_restaurant->title }}" class="img-fluid lzd-image"></a>

                        </div>
                    </div>
                    <div class="col-md-5">
                        <h5 class="font-weight-bold p-0 mb-0">{{ trans('home.today-rest') }}</h5>
                        <hr class="mb-2">
                        <div class="resturant-info">
                            <h6 class="mb-3">
                                <a href="{{ $day_restaurant->url }}">
                                    {{ $day_restaurant->title }}
                                </a>
                            </h6>
                            <div>
                                <div class="card resturant-logo d-inline-block">

                                    <a href="{{ $day_restaurant->url }}" class="item-name"><img src="{{ assets("frontend") }}/assets/img/placeholder-today-restaurant-thumb.jpg" data-src="{{ $day_restaurant->main_image ? thumbnail($day_restaurant->main_image) : '' }}" alt="" class="img-fluid lzd-image"></a>
                                </div>
                                <div class="resturant-desc">
                                    <p class="mb-2">
                                        {{ str_limit(strip_tags($day_restaurant->content), 200) }}
                                    </p>
                                    <bdi class="rate">
                                        <span>4</span> / 5
                                    </bdi>
                                    <bdi class="star mx-2">
                                        <a class="start-icon star-full">
                                            <i class="fas fa-star"></i>
                                        </a>
                                        <a class="start-icon star-full">
                                            <i class="fas fa-star"></i>
                                        </a>
                                        <a class="start-icon star-full">
                                            <i class="fas fa-star"></i>
                                        </a>
                                        <a class="start-icon star-full">
                                            <i class="fas fa-star"></i>
                                        </a>
                                        <a class="start-icon star-empty">
                                            <i class="fas fa-star"></i>
                                        </a>
                                    </bdi>
                                    <bdi class="views d-block mt-2"><span class="eye-icon"><i class="fas fa-eye"></i><span class="viewer-count">12.295</span>
                                    </bdi>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-md-3 restaurant-sidebar">

                    <ul class="list-unstyled">

                        @foreach($latest_restaurants as $restaurant)
                            <li class="row restaurant-row">
                                <div class="restaurant-row-image">

                                    <a href="{{ $restaurant->url }}" class="item-name"><img class="lzd-image" src="{{ assets("frontend") }}/assets/img/placeholder-restaurant-sidebar.jpg" data-src="{{ $restaurant->main_image ? thumbnail($restaurant->main_image) : '' }}" alt=""></a>

                                </div>

                                <div class="restaurant-row-title">
                                    <a href="{{ $restaurant->url }}">{{ $restaurant->title }}</a>
                                    <span>{{ isset($restaurant->attrs['branches-count']) ? $restaurant->attrs['branches-count'][0] : 0 }} فرع </span>
                                </div>
                            </li>
                        @endforeach

                    </ul>

                    <a href="{{ $kitchen_category->url }}" class="more-restaurants">
                        {{ trans('home.rests-link') }}<span class="icon-duble-angle {{ app()->getLocale() == 'ar' ? 'mr-2' : 'ml-2' }}"><i
                                    class="fas fa-angle-double-{{ app()->getLocale() == 'ar' ? 'left' : 'right' }}"></i></span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="restaurants-box card">
                        <ul class="restaurants-list list-unstyled mb-0 p-0">

                            @foreach($featured_restaurants as $restaurant)
                                <li>
                                    <a href="{{ $restaurant->url }}">
                                        <img class="lzd-image" src="{{ assets("frontend") }}/assets/img/placeholder-restaurant-bar.jpg" data-src="{{ $restaurant->main_image ? thumbnail($restaurant->main_image) : '' }}"/>
                                    </a>
                                </li>
                            @endforeach

                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="advertise">

            <!-- responsive ads replaced 728*90 -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9085617575772553"
                 data-ad-slot="5498890495"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>
    </div>

    <a href="#blog-barter" class="scroll-section">
        <span class="d-block">{{trans('home.classifieds')}}</span>
        <img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt="">
    </a>
</section>
