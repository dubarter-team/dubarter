<section id="artical-barter">
    <div class="container">
        <h4 class="text-center font-weight-bold mb-0"> <a href="">{{ trans('home.articles_title') }}</a></h4>
        <ul class="nav nav-pills justify-content-center" id="pills-tab" role="tablist">

            @foreach($featured_articles as $type => $articles)
                <li class="nav-item">
                    <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="pills-{{ $type }}-tab" data-toggle="pill" href="#pills-{{ $type }}" role="tab"
                       aria-controls="pills-{{ $type }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ trans('home.' . $type) }}</a>
                </li>
            @endforeach

        </ul>
        <div class="tab-content" id="pills-tabContent">
            @foreach($featured_articles as $type => $articles)

                <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="pills-{{ $type }}" role="tabpanel"
                     aria-labelledby="pills-{{ $type }}-tab">
                    <div class="owl-carousel owl-theme">
                        @foreach($articles as $article)
                            <div class="item">
                                <div class="card custom-box">
                                    <div class="card-img-header">
                                        <a href="{{ $article->url }}">
                                            <img src="{{ assets("frontend") }}/assets/img/placeholder.jpg" data-src="{{ thumbnail($article->image->path, "medium") }}" alt=""
                                             class="img-fluid lzd-image">
                                        </a>
                                    </div>
                                    <div class="card-body">

                                      {{-- <div class="date">
                                        <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png" alt="" style="width: 14px;height: 13px;" class="icon">
                                        <bdi>{{ $article->date }}</bdi>
                                      </div> --}}

                                      <div class="title">
                                                <a href="{{ $article->url }}" class="{{ dir_class($article->title) }}">
                                                {{ $article->title }}
                                                </a>
                                        </div>

                                        <div class="desc {{ dir_class($article->excerpt) }}">
                                            <p>{{ $article->excerpt }}</p>
                                        </div>

                                      <a href="{{ $article->url }}" class="read-more float-right">{{ trans('common.read_article') }}</a>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>

                </div>

            @endforeach

        </div>
        <div class="advertise">

            <!-- responsive ads replaced 728*90 -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9085617575772553"
                 data-ad-slot="5498890495"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>
    </div>
    <a href="#auto-barter" class="scroll-section">
        <span class="d-block">{{trans('home.cars')}}</span>
        <img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt="">
    </a>
</section>
