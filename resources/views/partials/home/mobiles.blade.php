<div class="owl-carousel owl-theme">
    @foreach($featured_mobiles as $ad)
        <div class="item">
            <div class="card custom-box">
                <div class="card-img-header">
                    @include('partials.common.like_button', ['ad' => $ad])
                    @if($ad->price)
                        <div class="price w-100">
                            <span class="quantity">{{ $ad->price }}</span>
                             <span class="currancy">{{$ad->currency}}s</span>
                        </div>
                    @endif
                    <img class="img-fluid"
                            src="{{ $ad->image("medium") }}"
                            alt="Card image cap">

                </div>
                <div class="card-body">
                    <a href="{{ $ad->url }}"  class="item-name">{{ $ad->title }}</a>
                    <div class="info">
                        <div class="location mobile-location">
                            <img src="{{ assets("frontend") }}/assets/img/map-icon.png" alt="" class="icon">
                            @if($ad->city_name)
                            <span>{{ $ad->city_name }}</span>,
                                @endif , <span>{{ $ad->country_name }}</span>
                        </div>
                        <div class="date float-right">
                            <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png" alt="" class="icon">
                            <span>{{ $ad->date }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<!-- <div class="col-6 p-0">
    <div class="mobiles">
        <div class="mobs-block w-75">
            <h6 class="text-center py-3">موبيلات</h6>
            <div class="card p-2 pl-0 py-2 pr-2 pl-0">
                <div class="row">
                    <div class="col-12">
                        <div class="filter">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <div class="pl-1  custom-select-input">
                                        <select name="categories[]" class="js-example-data-array best-offer"
                                                aria-placeholder="" id="mobiles-types">
                                            <option value=""> نوع الموبيل</option>
                                            @foreach($mobiles_brands as $brand)
                                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="pl-1 custom-select-input">
                                        <select name="categories[]" class="js-example-data-array department"
                                                aria-placeholder="" id="mobiles-models">
                                            <option value="0">{{ trans("home.mobile_brand") }}</option>
                                        </select>
                                    </div>
                                    {{-- <div class="pl-1 custom-select-input">
                                         <select class="js-example-data-array department"
                                                 aria-placeholder="">
                                             <option value="">السعر</option>
                                         </select>
                                     </div>--}}
                                    <a href="" class="search-icon"><img
                                                src="{{ assets("frontend") }}/assets/img/search-icon.png"
                                                alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="filter-result mt-3">
                        
                        </div>

                    </div>
                </div>
                <div class="ads-card">
                    <img src="{{ assets("frontend") }}/assets/img/ads3.png" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div> -->



@push('footer')

    <script>
        $(document).ready(function () {

            $('#mobiles-types').change(function () {
                var cat_id = $(this).val();

                var default_option = '<option value="0">{{ trans("home.mobile_brand") }}</option>';

                if (cat_id != '') {
                    $.ajax({
                        url: "{{ route("ajax.get_sub_categories") }}",
                        data: {cat_id: cat_id},
                        type: 'get',
                        timeout: 10000,
                        dataType: "json",
                        success: function (res) {
                            if (res.status == true) {
                                $('#mobiles-models').html(default_option + res.data);
                            } else {
                                $('#mobiles-models').html(default_option);
                            }
                        },
                        error: function (x, y, z) {
                            $('#mobiles-models').html(default_option);
                        }
                    });
                } else {
                    $('#mobiles-models').html(default_option);
                }

            });

        });
    </script>

@endpush
