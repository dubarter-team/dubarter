<section id="style-barter">
    <div class="container">
        <h4 class="text-center font-weight-bold mb-0"><a href="{{ $ms_main_cats->where('id', \App\Models\Category::STYLEBARTER)->first()->url }}">{{ $ms_main_cats->where('id', \App\Models\Category::STYLEBARTER)->first()->name }}</a></h4>
        <div class="filter">
            <form method="get" action="{{ route('search.index') }}">
                <div class="row justify-content-md-center">
                    <div class="col-lg-10">
                        <div class="col-sm-5">
                            <input type="text" class="home-search-input" name="q"
                                   placeholder="{{ trans('home.search_for') }} ....">
                        </div>
                      <div class="col-sm-5">
                            <select class="choose-city select2-dropdown" name="city_id" aria-placeholder="">
                                <option value="">{{ trans("home.city") }}</option>
                                @if(count($cities))
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <input type="hidden" name="category_id" value="{{ \App\Models\Category::STYLEBARTER }}" />
                        <div class="col-sm-2">
                          <button type="submit" class="search-icon btn"><img
                                    src="{{ assets("frontend") }}/assets/img/search-icon.png"
                                    alt=""></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <div class="filter-result">
            <div class="row">
                <div class="col-md-8">
                    <div class="owl-carousel style-barter owl-theme">
                        @if(count($style_slider_ads))
                            @foreach($style_slider_ads as $ad)
                                <div class="item">
                                    <a href="{{ $ad->url }}">
                                        <div class="card custom-box mb-0">
                                            <div class="card-img-header">
                                                <a href="{{ $ad->url }}">
                                                    <img src="{{ $ad->main_image ? thumbnail($ad->main_image) : '' }}"
                                                                                                            alt="{{ $ad->title }}" class="img-fluid">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <a href="{{ $ad->url }}" class="item-name">{{ $ad->title }}</a>
                                                <div class="item-price">
                                                    <span class="quantity">{{ $ad->price }}</span>
                                                    <span class="currancy">{{ $ad->currency }}</span>
                                                </div>

                                                <div class="item-detailes">

                                                    @foreach(array_slice($ad->attributes, 0, 3) as $attribute)
                                                        <div class="icon-box">
                                                            <img src="{{ $attribute->small_icon_url }}" alt="" class="icon">
                                                            <span>{{ $attribute->value_title }}</span>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="shope-here text-center">
                        <a href="{{ $ms_main_cats->where('id', \App\Models\Category::STYLEBARTER)->first()->url }}">{{ trans("home.style_shop_here") }}</a>
                    </div>

                </div>
                <div class="col-md-4">
                    @if($style_featured_article)
                        <div class="card custom-box style-artical mb-0">
                            <div class="card-img-header">
                                <img class="img-fluid"
                                     src="{{ $style_featured_article->image ? thumbnail($style_featured_article->image->path, "medium") : '' }}"
                                     alt="{{ $style_featured_article->title }}">
                            </div>

                            <div class="card-body">
                                <a href="{{ $style_featured_article->url }}" class="item-name">
                                    {{ $style_featured_article->title }}
                                </a>
                                <p class="card-text">{{ $style_featured_article->excerpt }}</p>
                            </div>
                        </div>
                    @endif

                </div>

            </div>
        </div>
        <div class="advertise">
            <img src="{{ assets("frontend") }}/assets/img/ads1.png" alt="" class="img-fluid">
        </div>
    </div>

    <a href="#food-barter" class="scroll-section">
        <span class="d-block">{{ $ms_main_cats->where('id', \App\Models\Category::FOODBARTER)->first()->name }}</span>
        <img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt="">
    </a>
</section>
