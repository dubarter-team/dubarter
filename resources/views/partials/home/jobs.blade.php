<div class="owl-carousel owl-theme p-0">
    @foreach($featured_jobs as $ad)
        <div class="item">
            <div class="card custom-box">
                <div class="card-img-header">
                    <span class="favourite"><i class="far fa-heart"></i> </span>
                    <!-- @if($ad->price)
                        <div class="price"><span class="quantity">{{ $ad->price }}</span> 
                        <span class="currancy">{{$ad->currency}}s</span></div>
                    @endif -->

                    <img class="card-img-top"
                            src="{{ $ad->image("medium") }}"
                            alt="Card image cap">

                </div>
                <div class="card-body">
                    <a class="item-name" href="{{ $ad->url }}">{{ $ad->title }}</a>
                    <div class="info">
                        <div class="location">
                            <img src="{{ assets("frontend") }}/assets/img/map-icon.png" alt="" class="icon">
                            @if($ad->city_name)
                            <span>{{ $ad->city_name }}</span>,
                                @endif , <span>{{ $ad->country_name }}</span>
                        </div>
                        <div class="item-detailes">
                            <div class="icon-box">
                                <img src="{{ assets("frontend") }}/assets/img/job-icon.png" alt="" class="icon ml-3">
                                <span>{{trans('home.dentistry')}}</span>
                            </div>
                            <div class="icon-box">
                                <img src="{{ assets("frontend") }}/assets/img/job-type-icon.png" alt="" class="icon ml-3">
                                <span>{{trans('home.fulltime')}}</span>
                            </div>
                        </div>
                        <div class="date float-right">
                            <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png" alt="" class="icon">
                            <span>{{ $ad->date }}</span>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>


<!-- <div class="col-6 p-0">
    <div class="jobs">
        <div class="jobs-block w-75">
            <h6 class="text-center py-3">وظائف</h6>
            <div class="card p-2 pl-0 py-2 pr-2 pl-0">
                <div class="row">
                    <div class="col-12">
                        <div class="filter">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pl-1 custom-select-input">
                                        <select name="attrs[{{ $fields->slug }}]" class="js-example-data-array best-offer"
                                                aria-placeholder="">
                                            <option value="">{{ $fields->name }}</option>
                                            @foreach($fields->options as $key => $name)
                                                <option value="{{ $key }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="pl-1 custom-select-input">
                                        <select name="attrs[{{ $job_type->slug }}]" class="js-example-data-array department"
                                                aria-placeholder="">
                                            <option value="">{{ $job_type->name }}</option>
                                            @foreach($job_type->options as $key => $name)
                                                <option value="{{ $key }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="pl-1 custom-select-input">
                                        <select name="attrs[{{ $qualification->slug }}]" class="js-example-data-array department"
                                                aria-placeholder="">
                                            <option value="">{{ $qualification->name }}</option>
                                            @foreach($qualification->options as $key => $name)
                                                <option value="{{ $key }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <a href="" class="search-icon"><img
                                                src="{{ assets("frontend") }}/assets/img/search-icon.png"
                                                alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="filter-result mt-3">
                            
                        </div>

                    </div>
                </div>
                <div class="ads-card">
                    <img src="{{ assets("frontend") }}/assets/img/ads3.png" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div> -->
