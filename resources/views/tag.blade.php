@extends("layouts.master")



@push('meta')
    <title>{{ $tag->name . ' - ' .  trans('common.dubarter') }}</title>

    <meta name="author" content="{{option('site_name')}}">
    <meta name="robots" content="index, follow">

    <meta name="title" content="{{ $tag->name }}">
    <meta name="description" content="{{ strip_tags($tag->description) }}"/>
@endpush


@section("content")

    <h1 style="display: none;">{{ $tag->name }}</h1>
    <h2 style="display: none;">{{ str_limit($tag->description, 70) }}</h2>


    <section id="search-result-page">
        <form method="get">
            <section class="search-result">
                <div class="container">
                    <div class="m-auto box-filter search">
                        <div class="row">
                            <div class="col-lg-12 p-0">
                                <h5 class="d-inline-block mb-0 title">
                                    <i class="fas fa-search"></i>
                                    {{ trans('common.dubarter')}}
                                </h5>
                                <div class="filter">

                                    <div class="row changable-region">
                                        <div class="col-lg-12 d-flex tag-page changable-region">

                                            <div class="custom-select-2 b-left">
                                                <input type="text" name="q" value="{{ request('q') }}"
                                                       class="search-box"
                                                       placeholder="{{ trans('search.search_for') }} ..">
                                            </div>

                                            <div class="custom-select-2 b-left">
                                                <select class="select2-dropdown city-dp-ch-region" name="city_id"
                                                        aria-placeholder="">
                                                    <option value="">{{ trans('search.city') }}</option>

                                                    @if(count($cities))
                                                        @foreach($cities as $city)
                                                            <option {{ request('city_id', 0) == $city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="custom-select-2 b-left">
                                                <select class="select2-dropdown region-dp" name="region_id"
                                                        aria-placeholder="">
                                                    <option value="">{{trans('common.all_region')}}</option>

                                                    @if(count($regions))
                                                        @foreach($regions as $region)
                                                            <option {{ request('region_id', 0) == $region->id ? 'selected' : '' }} value="{{ $region->id }}">{{ $region->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>

                                            <button type="submit" class="btn search-btn">
                                                <span>{{ trans('category.search') }}</span></button>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <h3 class="tag-title">{{$tag->name}}</h3>
            </div>
        </form>


        <section>
            <div class="result">
                <div class="container">
                    {{--<div class="mx-auto mt-4 ads-search">--}}
                    {{--<img src="/assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">--}}
                    {{--</div>--}}

                    <div class="row">
                        <div class="col-md-4">
                            {{--                            <h6 class="px-2 mb-0 d-inline-block font-weight-bold">{{$tag->name}}</h6>--}}

                            <div class="count-ads">
                                <span class="font-weight-bold">{{ $ads_total }} {{ trans('search.ads') }} </span>
                                <span class="text-secondary"> ({{ ceil($ads_total / $per_page) }} {{ trans('search.pages') }}
                                    )</span>
                            </div>
                        </div>
                        <div class="col-md-8">
                        </div>
                    </div>

                    <div id="search-ads-listing">
                        @if($ads_count)

                            {!! $ads_html !!}

                        @else
                            <div class="no-content">{{ trans('search.no_ads') }}</div>
                        @endif
                    </div>

                    @if($ads_count == $per_page)
                        <div class="load-more-btn" id="search-load-more-ads">
                            <a class="btn btn-defult green-btn text-white"
                               @if($ads_count == 0) style="display: none" @endif>{{ trans('search.more_ads') }}</a>
                        </div>
                    @endif

                </div>
            </div>

        </section>

    </section>


@stop


@push("footer")

    <script>

        var search_paginate_url = '{{ route('tag.paginate',['slug'=>$tag->slug]) . (count(Request::except('offset')) ? '?' . http_build_query(Request::except('offset')) : '') }}';

        var search_offset = {{ $offset }};

        var search_perpage = {{ $per_page }};

        var search_flag = true;

        var search_load_more_idle = true;

        $(document).ready(function () {

            $(document).on('click', '#search-load-more-ads a', function () {

                if (search_flag && search_load_more_idle) {

                    search_load_more_idle = false;

                    $('#search-load-more-ads a').html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

                    $.ajax({
                        url: search_paginate_url,
                        data: {offset: search_offset},
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            $('#search-ads-listing').append(res.ads_html);

                            search_offset += res.ads_count;

                            item_details_height();

                            images_lazy_load();

                            if (res.ads_count < search_perpage) {
                                search_flag = false;

                                $('#search-load-more-ads').remove();
                            }
                        },
                        complete: function () {
                            search_load_more_idle = true;

                            $('#search-load-more-ads a').html('{{ trans("search.more_ads") }}');
                        }
                    });

                }

            });

        });

    </script>

@endpush
