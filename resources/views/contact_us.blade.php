@extends("layouts.master")


@section("content")
    <section id="my-account-page">
        <section class="account-barter-search category-search contact-top">
            <div class="container">
                <div class="m-auto search w-75">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="filter">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item">
                                            <h5><i class="my-account-ico contact-ico"></i>اتصل بنا</h5>
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="cats-filter my-account-wrapper contact-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5>{{ trans('common.cnus_contact_us') }}</h5>
                        <div class="contact-info">
                            <div class="contact-info-item"><span class="cont-ico"><i class="far fa-envelope"></i></span>
                                info@dubarter.com
                            </div>
                            <div class="contact-info-item" style="direction: ltr;"><span class="cont-ico"><i class="fas fa-phone"></i></span>
                                +2 0225610127
                            </div>
                            <div class="contact-info-item" style="direction: ltr;"><span class="cont-ico"><i class="fas fa-phone"></i></span>
                                +2 0100-2566-282
                            </div>
                            <div class="contact-info-item"><span class="cont-ico"><i class="fas fa-map-marker-alt"></i></span>
                                الحي الخامس ، المنطقة السادسة ، فيلا 69 امام مدرسة اخناتون
                                التجمع الخامس , القاهرة,
                            </div>
                        </div>
                        <div class="contact-map" id="map">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h5>{{ trans('common.cnus_title') }}</h5>

                        @if (session('status'))
                            <div class="alert alert-success">
                                <p class="my-1 px-2">{{ session('status') }}</p>
                            </div>
                        @endif

                        @if(count($errors))
                            <div class="alert alert-danger">
                                <p class="my-1 px-2">{{ $errors->first() }}</p>
                            </div>
                        @endif

                        <form class="contact-form" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="form-group">
                                <input class="form-control" name="name" required placeholder="{{ trans('common.cnus_name') }}"
                                       value="{{ Auth::guard('frontend')->check() ? Auth::guard('frontend')->user()->first_name : '' }}">
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="email" required placeholder="{{ trans('common.cnus_email') }}"
                                       value="{{ Auth::guard('frontend')->check() ? Auth::guard('frontend')->user()->email : '' }}">
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="phone" placeholder="{{ trans('common.cnus_phone') }}" value="">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="{{ trans('common.cnus_content') }}"
                                          rows="5"></textarea>
                            </div>
                            <button type="submit" class="btn">{{ trans('common.cnus_send') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </section>
@stop

@push("footer")
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCw-GKQL--li9_CxcdeBrQelj5HXulk38&callback=initMap">
    </script>

    <script>
        // Initialize and add the map
        function initMap() {
            // The location of Uluru
            var uluru = {lat: 30.004527, lng: 31.413918};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 18, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>
@endpush
