@extends("layouts.master")

@section("content")
    <section id="add-post-page">
        <section class="category-search ad-post-banner pb-0"
                 style="background-image: url({{ assets('frontend') . '/assets/img/post-ad-banner.jpg' }});">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-md-9">
                        <h4 class="add-post mb-5 py-3 px-5"><span class="{{ app()->getLocale() == 'ar' ? 'ml-3' : 'mr-3' }}"><i
                                        class="far fa-plus-square"></i></span>{{ trans('ad.create_ad') }}</h4>
                    </div>

                </div>
            </div>
        </section>
        <div class="container my-5">

            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if(count($errors->all()))
                <div class="alert alert-danger" role="alert">
                    @foreach ($errors->all() as $message)
                        <div>{{ $message }}</div>
                    @endforeach
                </div>
            @endif

            @if($ad->status == 3)
                <div class="alert alert-danger" role="alert">
                    <div>{{ trans('ad.you_ad_rejected') }}</div>
                    <div>{{ $ad->rejection_reason->title }}</div>

                    @if($ad->rejection_reason_message)
                        <div>{{ $ad->rejection_reason_message }}</div>
                    @endif
                </div>
            @endif

            <form action="" method="post" id="create_ad_form">

                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-6 mb-2">
                        <label class="form-label">{{ trans('ad.ad_title') }}</label> <span class="required">*</span>
                        <div class="custom-select-2">
                            <input type="text" name="title" maxlength="70" class="form-control" id="ad-title"
                                   value="{{ @Request::old("title", $ad->title) }}"
                                   placeholder="{{ trans('ad.ad_title_placeholder') }}" required>
                        </div>
                        <small class="form-text text-muted float-right mt-0">{{ trans('ad.ad_title_remaining_chars') }}
                            <span id="title-remaining-chars">{{ 70 - strlen($ad->title) }}</span>.
                        </small>
                    </div>

                    <div class="col-md-6 mb-2">

                        <label class="form-label">{{ trans('ad.ad_price') }}</label>
                        <div class="input-group mb-2">
                            <input type="number" name="price" value="{{ @Request::old("price", $ad->price) }}"
                                   class="form-control" placeholder="0">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ @json_decode(config('country.currency'), 1)[app()->getLocale()] }}</div>
                            </div>
                        </div>


                    </div>



                </div>
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label class="form-label">{{ trans('ad.ad_content') }}</label>
                        <textarea class="form-control" name="content" rows="3"
                                  placeholder="{{ trans('ad.ad_content_placeholder') }}">{{ @Request::old("content", $ad->content) }}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4  mb-2">
                        <label class="form-label">{{ trans('ad.ad_country') }}</label> <span class="required">*</span>
                        <div class="custom-select-2">
                            <select class="select2-dropdown" id="input-country" name="country_id" aria-placeholder=""
                                    required>

                                <option value="">{{ trans("common.all") }}</option>

                                @if(count($countries))

                                    @foreach($countries as $country)

                                        <option {{ @Request::old("country_id", $ad->country_id) == $country->id ? 'selected' : '' }} value="{{ $country->id }}">{{ $country->name }}</option>

                                    @endforeach

                                @endif

                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label class="form-label">{{ trans('ad.ad_city') }}</label>
                        <div class="custom-select-2">
                            <select class="select2-dropdown" id="input-city" name="city_id" aria-placeholder="">
                                <option value="0">{{ trans("common.all") }}</option>

                                @if(count($cities))
                                    @foreach($cities as $place)
                                        <option {{ @Request::old("city_id", $ad->city_id) == $place->id ? 'selected' : '' }} value="{{ $place->id }}">{{ $place->name->{app()->getLocale()} }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4  mb-2">
                        <label class="form-label">{{ trans('ad.ad_region') }}</label>
                        <div class="custom-select-2">
                            <select class="select2-dropdown" id="input-region" name="region_id" aria-placeholder="">
                                <option value="0">{{ trans("common.all") }}</option>

                                @if(count($regions))
                                    @foreach($regions as $place)
                                        <option {{ @Request::old("region_id", $ad->region_id) == $place->id ? 'selected' : '' }} value="{{ $place->id }}">{{ $place->name->{app()->getLocale()} }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

            @include('partials.ad.categories', ['categories' => $categories, 'cat_id' => @Request::old('category_id', isset($direct_category) ? $direct_category->id : ''), 'attributes_html' => $attributes_html])


            <!-- add image -->
                <div class="add-img mt-5">
                    <div class="header-icon">
                        <span class="icon"><i class="far fa-image"></i></span>
                        <span>{{ trans('ad.add_images') }}</span>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 mb-3 px-5">
                            <div class="input-group" style="display: none;" id="uploading-images-status">
                                {{ trans('ad.uploading_images') }}
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text file-icon">
                                        <span class="{{ app()->getLocale() == 'ar' ? 'ml-2' : 'mr-2' }}"> <i class="fas fa-folder-open"></i></span>
                                        <input id="fileupload" type="file" multiple>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="image-list">
                                <ul class="mb-0 list-unstyled p-0" id="uploaded_images">

                                    @if($ad->id && $ad->media->where('type', 'image')->count())

                                        @include('partials.ad.uploaded_images', ['images' => $ad->media->where('type', 'image')])

                                    @endif

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <!--/ add image -->

                <!-- add video -->
                <div class="add-video mt-5">
                    <div class="header-icon">
                        <span class="icon-play icon"><i class="fas fa-play"></i></span>
                        <span>{{ trans('ad.add_video') }}</span>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-10 mb-2 pr-5">
                            <div class="input-group" style="display: none;" id="uploading-video-status">
                                {{ trans('ad.uploading_video') }}
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text file-icon">
                                        <span class="{{ app()->getLocale() == 'ar' ? 'ml-2' : 'mr-2' }}"> <i class="fas fa-folder-open"></i></span>
                                        <input id="videoupload" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2" id="uploaded_video">
                            @if($ad->id && $ad->media->where('type', 'video')->count())

                                @include('partials.ad.uploaded_video', ['video' => $ad->media->where('type', 'video')->first()])

                            @endif
                        </div>
                    </div>
                </div>
                <!--/ add video -->


                <div class="add-video mt-5">
                    <div class="header-icon">
                        <span class="icon-play icon"><i class="fas fa-user"></i></span>
                        <span>{{ trans('ad.profile_details') }}</span>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-10 mb-2 pr-5">


                            <div class="col-md-6 mb-2">
                                <label class="form-label">{{ trans('ad.ad_email') }}</label> <span class="required">*</span>
                                <input type="email" name="email" maxlength="70" class="form-control"
                                       value="{{ @Request::old("email", $ad->id ? $ad->email : auth('frontend')->user()->email) }}"
                                       required>
                            </div>

                            <div class="col-md-6 mb-2">
                                <label class="form-label">{{ trans('ad.ad_phone') }}</label> <span class="required">*</span>
                                <input type="text" name="phone" class="form-control"
                                       value="{{ @Request::old("phone", $ad->phone) }}" placeholder="0xxxxxxxxxx" required>
                            </div>

                        </div>

                    </div>
                </div>




                <!-- select locatipn -->
                <div class="select-location mt-5">
                    <div class="row mt-3">
                        <div class="col-md-3 mb-2">
                            <div class="header-icon p-0">
                                <span class="icon"><i class="fas fa-map-marker"></i></span>
                                <span>{{ trans('ad.map_location') }}</span>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="lng" id="ad-lng" value=""/>
                            <input type="hidden" name="lat" id="ad-lat" value=""/>

                            <div class="card p-0" id="map" style="height: 300px;">
                            </div>

                        </div>
                    </div>
                </div>
                <!--/ select locatipn -->
                <div class="text-center mt-5">
                    <button type="submit" class="btn btn-dark">{{ trans('ad.ad_save') }}</button>
                </div>
            </form>
        </div>
    </section>

@stop

@push('after_head')

    <script>
        fbq('track', 'AddToCart');
    </script>

@endpush

@push("head")

    <link rel="stylesheet" href="{{ assets("frontend") }}/assets/css/add-post.css">

@endpush

@push("footer")
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCw-GKQL--li9_CxcdeBrQelj5HXulk38&callback=initMap">
    </script>

    <script>
        $('#create_ad_form').data('serialize', $('#create_ad_form').serialize());

        var $form = $('#create_ad_form'),
            origForm = $form.serialize();

        $(document).ready(function () {

            window.onbeforeunload = function (e) {
                if(origForm != $form.serialize()) {
                    var message = "are you sure you want to leave this page with the unsaved data ?";
                    e.returnValue = message;
                    return message;
                }

                return;
            };

            $('#create_ad_form').submit(function() {
                window.onbeforeunload = null;
            });

            var title_limit = 70;

            $('#ad-title').on('input', function (e) {
                if ($(this).val().length <= title_limit) {
                    $('#title-remaining-chars').html(title_limit - $(this).val().length);
                } else {
                    e.preventDefault();
                }
            });

            var default_place_option = '<option value="0">{{ trans("common.all") }}</option>';

            $('#input-country').change(function () {
                var country_id = $(this).val();

                $('#input-city').html(default_place_option);
                $('#input-region').html(default_place_option);

                $('#input-city').trigger("change");
                $('#input-region').trigger("change");

                if (country_id != 0) {
                    place_change(country_id, '#input-city');
                }
            });

            $('#input-city').change(function () {
                var city_id = $(this).val();

                $('#input-region').html(default_place_option);

                $('#input-region').trigger("change");

                if (city_id != 0) {
                    place_change(city_id, '#input-region');
                }
            });

            function place_change(parent_id, child_id) {
                $.ajax({
                    url: "{{ route("ajax.sub_places") }}",
                    data: {id: parent_id},
                    type: 'get',
                    dataType: "json",
                    success: function (data) {
                        if (data.count > 0) {

                            if (child_id != undefined && $(child_id).length > 0) {

                                $(child_id).html(data.places);

                                if(child_id != '#input-region'){
                                    $('#input-city').trigger("change");
                                }

                                $('#input-region').trigger("change");

                            }

                        }
                    }
                });
            }

            var image_upload_flag = true;

            var images_limit = 6;

            var uploaded_images_count = $('.images-ids-inputs').length;

            $('#fileupload').change(function () {

                if (uploaded_images_count < images_limit) {

                    if (image_upload_flag) {

                        image_upload_flag = false;

                        $('#uploading-images-status').show();

                        var formData = new FormData();

                        var files = $('input[type=file]')[0].files;

                        if (files.length > 0) {

                            for (var i = 0; i < files.length; i++) {
                                if (uploaded_images_count < images_limit) {
                                    formData.append('files[]', files[i]);
                                }

                                uploaded_images_count++;
                            }

                            $('.images-ids-inputs').each(function () {
                                formData.append('uploaded_ids[]', $(this).val());
                            });

                            $.ajax({
                                url: '{{ route('ajax.image_upload') }}',
                                data: formData,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function (res) {
                                    if (res.count > 0) {
                                        $('#uploaded_images').append(res.media_html);
                                    }

                                    if (res.message != '') {
                                        alert('{{ trans('ad.allowed_images_message') }}');
                                    }
                                },
                                error: function () {
                                    alert('{{ trans('ad.image_upload_error') }}');
                                },
                                complete: function (jq, st) {
                                    image_upload_flag = true;

                                    uploaded_images_count = $('.images-ids-inputs').length;

                                    $('#uploading-images-status').hide();

                                }
                            });
                        }

                    } else {
                        alert('{{ trans('ad.wait_for_process') }}');
                    }

                } else {
                    alert('{{ trans('ad.images_limit') }}');
                }
            });

            var video_upload_flag = true;

            $('#videoupload').change(function () {

                if (video_upload_flag) {

                    video_upload_flag = false;

                    $('#uploading-video-status').show();

                    var formData = new FormData();

                    formData.append('file', $('input[type=file]')[1].files[0]);

                    $.ajax({
                        url: '{{ route('ajax.video_upload') }}',
                        data: formData,
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        success: function (res) {
                            if (res.status) {
                                $('#uploaded_video').html(res.media_html);
                            } else {
                                alert('{{ trans('ad.allowed_video_types') }}');
                            }
                        },
                        error: function () {
                            alert('{{ trans('ad.video_error') }}');
                        },
                        complete: function (jq, st) {
                            video_upload_flag = true;

                            $('#uploading-video-status').hide();
                        }
                    });

                } else {
                    alert('{{ trans('ad.wait_for_process') }}');
                }

            });

            $(document).on('click', '.remove-uploaded-file', function () {
                var ths = $(this);

                uploaded_images_count--;

                ths.parents('li').remove();
            });

            $('#create_ad_form').submit(function(){
                if(!video_upload_flag || !image_upload_flag){
                    alert('من فضلك انتظر قليلا حتى يتم رفع الملفات');

                    return false;
                }
            });

        });


        var map;
        var marker;

        function initMap() {
            var myLatlng = {lat: 30.0594699, lng: 31.1884238};

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: myLatlng
            });

            map.addListener('click', function (e) {
                add_marker(e, map);
            });

                    @if($ad->id && !empty($ad->lat) && !empty($ad->lng))

            var myLatLng = {lat: {{ $ad->lat }}, lng: {{ $ad->lng }} };

            marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });

            @endif
        }


        function add_marker(e, map) {
            if (marker != undefined) {
                marker.setMap(null);
            }

            marker = new google.maps.Marker({
                position: e.latLng,
                map: map
            });

            document.getElementById('ad-lat').value = e.latLng.lat();
            document.getElementById('ad-lng').value = e.latLng.lng();

            marker.setMap(map);
        }
    </script>


@endpush
