<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ app()->getLocale() == 'ar' ? 'rtl' : 'ltr' }}">
<head>
    <meta name="msvalidate.01" content="27D5259BCA34F09EA5F5BB4852C60CC0"/>
    <meta charset="utf-8">
    <link rel="canonical" href="{{ urldecode(get_current_url()) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--   @if(env('APP_DEBUG')==true)
           <meta name="robots" content="noindex, nofollow">
           <meta name="googlebot" content="noindex, nofollow">
       @endif--}}
    @stack("meta")
    <link rel="icon" type="image/x-icon" href="{{ assets("frontend") }}/assets/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css">
    <link href="{{ assets("frontend/assets/css/app.css") }}?ver=1.3" rel="stylesheet" type="text/css"/>
    <link href="{{ assets("frontend/assets/css/custom.css") }}?ver=1.3" rel="stylesheet" type="text/css"/>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    @stack("head")
    <link href="https://dubarter.com" rel="dns-prefetch">
    <link href="//connect.facebook.net" rel="dns-prefetch">
    <link href="//facebook.com" rel="dns-prefetch">
    <link href="//google-analytics.com" rel="dns-prefetch">
    <link href="//google.com" rel="dns-prefetch">
    <link href="//googleads.g.doubleclick.net" rel="dns-prefetch">
    <link href="//googleadservices.com" rel="dns-prefetch">
    <link href="//googletagmanager.com" rel="dns-prefetch">
    <link href="//googletagservices.com" rel="dns-prefetch">
    <link href="//partner.googleadservices.com" rel="dns-prefetch">
    <link href="//securepubads.g.doubleclick.net" rel="dns-prefetch">
    <link href="//staticxx.facebook.com" rel="dns-prefetch">
    <link href="//tpc.googlesyndication.com" rel="dns-prefetch">
    <link href="//pagead2.googlesyndication.com" rel="dns-prefetch">
    <link href="//fonts.googleapis.com" rel="dns-prefetch">
    <link href="//fonts.gstatic.com" rel="dns-prefetch">
    <link href="//ajax.googleapis.com" rel="dns-prefetch">
    <link href="//apis.google.com" rel="dns-prefetch">
    <link href="//google-analytics.com" rel="dns-prefetch">
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="//youtube.com" rel="dns-prefetch">
    <link href="//www.youtube.com" rel="dns-prefetch">
    <link rel="alternate" hreflang="ar-{{ config('country.code') }}" href="{{ urldecode(get_current_url()) }}"/>

    @if(\App\Models\Place::getCurrentCountry()->code == "eg")

        <script type="text/javascript">
            _atrk_opts = {atrk_acct: "U/Dam1agbiF2N8", domain: "dubarter.com", dynamic: true};
            (function () {
                var as = document.createElement('script');
                as.type = 'text/javascript';
                as.async = true;
                as.src = "https://certify-js.alexametrics.com/atrk.js";
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(as, s);
            })();
        </script>
        <noscript>
            <img src="https://certify.alexametrics.com/atrk.gif?account=U/Dam1agbiF2N8" style="display:none"
                 height="1" width="1" alt=""/>
        </noscript>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67538055-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-67538055-2');

            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-67538055-2', 'auto');
            ga('send', 'pageview');
            ga('require', 'linkid', 'linkid.js');
            ga('require', 'displayfeatures');
            setTimeout("ga('send','event','Profitable Engagement','time on page more than 30 seconds')", 30000);

            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '230961377237477');
            fbq('track', 'PageView');
            fbq('track', 'ViewContent');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=230961377237477&ev=PageView&noscript=1"/>
        </noscript>
@endif

@if(\App\Models\Place::getCurrentCountry()->code == "iq")

    <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '2456478834382867');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id=2456478834382867&ev=PageView
&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67538055-4"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-67538055-4');
        </script>

        <!-- Snap Pixel Code -->
        <script type='text/javascript'>
            (function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
            {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
                a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
                r.src=n;var u=t.getElementsByTagName(s)[0];
                u.parentNode.insertBefore(r,u);})(window,document,
                'https://sc-static.net/scevent.min.js');

            snaptr('init', '8a9423b1-5d09-4a81-a11a-2de40382b0a4', {
                'user_email': 'dubarter1@gmail.com'
            });

            snaptr('track', 'PAGE_VIEW');

        </script>
        <!-- End Snap Pixel Code -->
@endif

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <style>
        #categories .cat-box li {
            width: 25%;
        }

        .body-box .main-image {
            width: 100% !important;
        }

        .gray-overlay {
            width: 100% !important;
        }

        @media (max-width: 768px){
            #categories .cat-box li.flex-slide:nth-child(3), #categories .cat-box li.flex-slide:nth-child(4), #categories .cat-box li.flex-slide:nth-child(5) {
                 width: 48% !important;
            }
        }
    </style>
</head>
<body class="{{ app()->getLocale() == 'ar' ? 'rtl' : 'ltr' }} dynamic-navbar">
@stack('after_head')
<div id="fb-root"></div>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=616439015181948&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
@include("partials.navbar")

@yield("content")
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 text-center">

                <a href="https://www.facebook.com/Dubarter/" class="du-fb-page">
                    <img src="{{ assets("frontend/images/fb.jpg") }}" alt="">
                </a>
            </div>
            {{--<div class="fb-page" data-href="https://www.facebook.com/Dubarter/" data-small-header="false"
                 data-adapt-container-width="true"
                 data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
                <blockquote cite="https://www.facebook.com/Dubarter/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/Dubarter/">{{trans('common.dubarter')}}</a></blockquote>
            </div>--}}

            <div class="col-lg-4 col-md-4 text-center right-left-border">

                <ul class="footer-links clearfix">
                    @foreach($_categories as $category)
                        @if($category->id != 3)
                        <li>
                            <a href="{{ $category->url }}">{{ $category->name }}</a>
                        </li>
                        @endif
                    @endforeach

                    <li>
                        <a href="{{ route('ads.create') }}">{{trans('common.add_ad')}}</a>
                    </li>

                    <li>
                        <a href="{{ route('page.index', ['slug' => 'privacy-policy']) }}">{{trans('common.privacy_policy')}}</a>
                    </li>

                    <li>
                        <a href="{{ route('page.index', ['slug' => 'terms-and-conditions']) }}">{{trans('common.terms_and_conditions')}}</a>
                    </li>

                    <li>
                        <a href="{{ route('page.index', ['slug' => 'cookies_policy']) }}">{{trans('common.cookies_policy')}}</a>
                    </li>

                    <li>
                        <a href="{{ route('pages.gdpr') }}">{{ trans('common.gdpr_page') }}</a>
                    </li>

                    <li>
                        <a href="{{ route('page.index', ['slug' => 'about_us']) }}">{{trans('common.about_us')}}</a>
                    </li>

                    <li>
                        <a href="{{ route('contact_us') }}">اتصل بنا</a>
                    </li>
                </ul>
                <div class="social-area text-center clearfix">

                    <ul class="social-area-pages">
                        <li>
                            <a class="color-facebook" target="_blank" href="https://www.facebook.com/Dubarter/">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a class="color-youtube" target="_blank"
                               href="https://www.youtube.com/channel/UCEbcAZmTsgfXrynHQ_qYmDA">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a class="color-twitter" target="_blank" href="https://twitter.com/dubarter">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a class="color-google" target="_blank" href="https://plus.google.com/+Dubarter">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a class="color-instagram" target="_blank" href="https://www.instagram.com/dubarter/">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mobile-download">

                <div class="row text-center device">
                    <img src="{{ asset("app.png") }}" alt="">
                </div>

                <div class="stores">

{{--                    <div class="col-md-2 col-sm-2 col-xs-2">--}}
{{--                    </div>--}}

                    <div class="store">
                        <a href="https://play.google.com/store/apps/details?id=com.dubarter.androidApp" target="_blank">
                            <img src="{{ asset("android.png") }}" alt="android app">
                        </a>
                    </div>
                    <div class="store">
                        <a href="https://itunes.apple.com/us/app/dubarter-%D8%AF%D9%88%D8%A8%D8%A7%D8%B1%D8%AA%D8%B1/id1461675341?mt=8"
                           target="_blank">
                            <img src="{{ asset("ios.png") }}" alt="ios app">
                        </a>
                    </div>

{{--                    <div class="col-md-2 col-sm-2 col-xs-2">--}}
{{--                    </div>--}}
                </div>

            </div>
            <div class="mobile-download-bar">


                <div class="mobile-download-bar-close"><i class="fas fa-times"></i></div>
                <a href="https://play.google.com/store/apps/details?id=com.dubarter.androidApp"
                   target="_blank">{{trans('common.download_app')}}</a>
            </div>
        </div>
    </div>
    <div class="copyrights">
        © Copyright 2018. All Rights Reserved
    </div>

    <div class="notifications-alert alert alert-success alert-dismissible fade show" style="display: none;"
         id="notif_fl" role="alert">
        <a href="#" class="alert-link">an example link</a>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

</footer>
{{--@include('partials.common.gdpr')--}}
</div>
</div>
<a href="{{ route('ads.create') }}" class="add-ads-button-round"><span>اعلان</span><i class="plus-ico"></i></a>
@include('partials.common.favourite_login_popup')
</body>
<script src="{{ assets("frontend/assets/js/app.js") }}?ver=1.01"></script>
<script type="application/ld+json">
    {"@context":"http://schema.org","@type":"WebSite","name":"dubarter.com","url":"https://dubarter.com/"},
    {"@context":"http://schema.org","@type":"Organization","url":"https://dubarter.com/","logo":"https://dubarter.com/frontend/assets/img/logo.png","sameAs":["https://www.facebook.com/Dubarter/","https://twitter.com/dubarter","https://www.youtube.com/channel/UCEbcAZmTsgfXrynHQ_qYmDA","https://www.instagram.com/dubarter/","https://plus.google.com/+Dubarter"]}

</script>

<script type="application/ld+json">
{"@context": "http://schema.org",
       "@type": "Organization",
       "name": "dubarter.com",
        "@id": "#dubartercom",
      "url": "https://dubarter.com/",
      "logo": {"@type": "imageObject","url":"https://dubarter.com/frontend/assets/img/logo.png","width":"115","height":"29"},
"contactPoint" : [
{ "@type" : "ContactPoint", "telephone" : "+2-25610127", "contactType" : "customer service"}
],
"sameAs" : [
"https://www.facebook.com/Dubarter/",
"https://twitter.com/dubarter",
"https://www.youtube.com/channel/UCEbcAZmTsgfXrynHQ_qYmDA",
"https://www.instagram.com/dubarter/",
"https://plus.google.com/+Dubarter"
]}


</script>
<script>
    $(document).ready(function () {

        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

        @if(Auth::guard('frontend')->check())
        $(document).on('click', '.favourite', function (e) {

            e.preventDefault();

            var ths = $(this);

            var id = ths.attr('data-id');

            if (id != '' && id != undefined) {
                $.ajax({
                    url: "{{ route("ajax.like") }}",
                    data: {id: id},
                    type: 'get',
                    timeout: 10000,
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            if (res.liked == true) {
                                ths.addClass('fav-color');
                                ths.find('.fav-text').html('{{ trans('ad.remove_from_favorites') }}');
                            } else {
                                ths.removeClass('fav-color');
                                ths.find('.fav-text').html('{{ trans('ad.add_to_favorites') }}');
                            }
                        }
                    }
                });
            }

        });
        @endif

        $('.city-dp-ch-region').change(function () {
            var ths = $(this);

            var city_value = ths.val();

            if (city_value != '') {
                ths.parents('.changable-region').find('.region-dp').html('<option value="">{{trans('common.all_region')}}</option>');

                $.ajax({
                    url: "{{ route("ajax.regions") }}",
                    data: {id: city_value},
                    type: 'get',
                    timeout: 10000,
                    dataType: "json",
                    success: function (res) {
                        ths.parents('.changable-region').find('.region-dp').html(res.places);
                    },
                    error: function (x, y, z) {
                        ths.parents('.changable-region').find('.region-dp').html('<option value="">{{trans('common.all_region')}}</option>');
                    }
                });

            } else {
                ths.parents('.changable-region').find('.region-dp').html('<option value="">{{trans('common.all_region')}}</option>');
            }
        });

    });
</script>
@stack("footer")
</html>
