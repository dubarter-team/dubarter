@extends("layouts.master")


@push('meta')
    <meta name="robots" content="noindex,follow,noodp"/>
    <title>{{ trans('search.search') . ' - ' .  trans('common.dubarter') }}</title>
@endpush

@section("content")

    <section id="search-result-page">
        <form method="get" id="search-page-form">
            <section class="search-result">
                <div class="container advanced-search">
                      <div class="search-icon"><img src="{{ assets("frontend") }}/assets/images/advanced-search-icon.png"
                                                    alt="">{{ trans('common.advanced_search') }} </div>
                      <div class="row changable-region">
                        <div class="custom-select-2 col-md-4">
                            <input type="text" name="q" value="{{ request('q') }}"
                                   class="search-box"
                                   placeholder="{{ trans('search.search_for') }} ..">
                        </div>

                        <div class="custom-select-2 col-md-4">
                            <select class="select2-dropdown city-dp-ch-region" name="city_id" aria-placeholder="">
                                <option value="">{{ trans('search.city') }}</option>

                                @if(count($cities))
                                    @foreach($cities as $city)
                                        <option {{ request('city_id', 0) == $city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="custom-select-2 col-md-4">
                            <select class="select2-dropdown region-dp" name="region_id" aria-placeholder="">
                                <option value="">{{trans('common.all_region')}}</option>

                                @if(count($regions))
                                    @foreach($regions as $region)
                                        <option {{ request('region_id', 0) == $region->id ? 'selected' : '' }} value="{{ $region->id }}">{{ $region->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                      </div>

                      @include('partials.search.categories', ['categories' => $categories, 'cat_id' => request('category_id', 0), 'attributes_html' => $attributes_html])

                      <div class="text-center pt-3 clearfix">
                          <button type="submit" class="btn btn-black">{{ trans('common.search') }}</button>
                      </div>
                </div>
            </section>
            <div class="search-sort">
            <div class="container">
                <div class="quike-search mb-3">
                    <div class="row">
                      <div class="col-lg-8">
                        @if(Request::filled('q'))
                            <h6 class="mb-0 d-inline-block font-weight-bold" style="line-height:34px;">{{ trans('search.search_results_for') }} {{ Request::get('q') }}</h6>
                        @endif
                      </div>
                        <div class="col-lg-4">
                            <div class="d-inline-block ml-2">{{ trans('search.order_by') }}</div>
                            <div class="custom-select-2">
                                <select class="select2-dropdown sub-on-change" name="order_by" aria-placeholder="">

                                    @if(count($order_by))

                                        @foreach($order_by as $v)

                                            <option {{ request('order_by') == $v ? 'selected' : '' }} value="{{ $v }}">{{ trans("search.$v") }}</option>

                                        @endforeach

                                    @endif
                                </select>
                            </div>

                        </div>
                    <!--<div class="col-lg-4">
                            <div class="float-right">
                                    <span class="show">
                                        {{ trans('common.show') }}
                            </span>
                        <a class="table-style">
                            <i class="fa fa-table"></i>
                        </a>
                        <a class="list-style">
                            <i class="fa fa-list"></i>
                        </a>
                    </div>

                </div>-->
                    </div>
                </div>
            </div>
          </div>
        </form>


        <section>
            <div class="result">
                <div class="container">
                    {{--<div class="mx-auto mt-4 ads-search">--}}
                    {{--<img src="/assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">--}}
                    {{--</div>--}}

                    <div class="row">
                        <div class="col-md-4">
                            <div class="count-ads">
                                <span class="font-weight-bold">{{ $ads_total }} {{ trans('search.ads') }} </span>
                                <span class="text-secondary"> ({{ ceil($ads_total / $per_page) }} {{ trans('search.pages') }}
                                    )</span>
                            </div>
                        </div>
                        <div class="col-md-8">
                        </div>
                    </div>

                    <div id="search-ads-listing">
                        @if($ads_count)

                            {!! $ads_html !!}

                        @else
                            <div class="no-content">{{ trans('search.no_ads') }}</div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-12">
                            @if($ads_count == $per_page)
                                <div class="float-right my-3" id="search-load-more-ads">
                                    <a class="load-more"><span>{{ trans('search.more_ads') }}</span></a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </section>


@stop

@push('after_head')

    <script>
        fbq('track', 'Search');
    </script>

@endpush


@push("footer")

    <script>

        var search_paginate_url = '{!! route('search.paginate', Request::except('offset')) !!}';

        var search_offset = {{ $offset }};

        var search_perpage = {{ $per_page }};

        var search_flag = true;

        var search_load_more_idle = true;

        $(document).ready(function () {

            $(document).on('click', '#search-load-more-ads a', function () {

                if (search_flag && search_load_more_idle) {

                    search_load_more_idle = false;

                    $('#search-load-more-ads a').html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

                    $.ajax({
                        url: search_paginate_url,
                        data: {offset: search_offset},
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            $('#search-ads-listing').append(res.ads_html);

                            search_offset += res.ads_count;

                            item_details_height();

                            images_lazy_load();

                            if (res.ads_count < search_perpage) {
                                search_flag = false;

                                $('#search-load-more-ads').remove();
                            }
                        },
                        complete: function () {
                            search_load_more_idle = true;

                            $('#search-load-more-ads a').html('{{ trans("search.more_ads") }}');
                        }
                    });

                }

            });

            $('.sub-on-change').on('change', function () {
                $('#search-page-form').submit();
            });

        });

    </script>

@endpush
