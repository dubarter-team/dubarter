@extends("layouts.master")

@push('meta')
    <meta name="robots" content="noindex,follow,noodp"/>
@endpush


@section("content")

    <section id="login-page" class="py-5">
        <div class="container">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <div class="login-bg">
                            <div class="avater-icon">
                                <img src="{{ assets('frontend') }}/assets/img/avatar.png" alt="">
                                <h6 class="text-white mt-3">{{ trans('auth.login') }}</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-container">
                            <form class="pt-5" method="post" id="login_form">

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>{{ trans('auth.username') }}</label>
                                    <div class="custom-input user-icon">
                                        <input class="form-control" name="username" id="login-username"
                                               placeholder="{{ trans('auth.username_placeholder') }}">
                                    </div>
                                    @if(count($errors))
                                        <div class="alert alert-danger p-0 m-0 mt-2"
                                             id="username-invalid-error-message">
                                            <p class="my-1 px-2">{{ $errors->first() }}</p>
                                        </div>
                                    @endif

                                    <div class="alert alert-danger p-0 m-0 mt-2" style="display: none;"
                                         id="username-error-message">
                                        <p class="my-1 px-2">{{ trans('auth.username_required') }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('auth.password') }}</label>
                                    <div class="custom-input pass-icon">
                                        <input type="password" name="password" class="form-control" id="login-password">
                                    </div>
                                    <div class="alert alert-danger p-0 m-0 mt-2" style="display: none;"
                                         id="password-error-message">
                                        <p class="my-1 px-2">{{ trans('auth.password_required') }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control control--checkbox d-inline-block">
                                        <input type="checkbox"/>
                                        <span class="">{{ trans('auth.remember_password') }}</span>
                                        <div class="control__indicator"></div>
                                    </label>
                                </div>
                                <button type="submit"
                                        class="btn btn-defult btn-block login">{{ trans('auth.login_button') }}</button>
                                <div class="forget-pass text-center">
                                    <a href="{{ route('forget_password') }}"
                                       class="mt-2 d-inline-block">{{ trans('auth.forget_password') }}</a>
                                </div>
                                <hr>

                            </form>
                            <div class="another-way-login">
                                <h6 class="text-center mb-3">{{ trans('auth.login_with') }}</h6>
                                <a href="{{ route('twitter.login') }}" class="btn btn-defult twitter p-0">
                                            <span class="twitter-icon">
                                                    <i class="fab fa-twitter"></i>
                                            </span>
                                    <strong> {{ trans('auth.twitter') }} </strong>
                                </a>
                                <a href="{{ route('facebook.login') }}" type="submit"
                                   class="btn btn-defult facebook p-0 mr-3">

                                        <span class="facebook-icon">
                                                    <i class="fab fa-facebook-f"></i>
                                            </span>

                                    <strong> {{ trans('auth.facebook') }} </strong>
                                </a>
                            </div>
                            <hr>
                            <div>
                                <h6 class="text-center mb-3">{!! trans('auth.register_link', ['link_tag' => '<a href="' . route('register') . '" class="text-green">', 'link_end' => '</a>']) !!}</h6>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

@stop


@push('footer')

    <script>
        $(document).ready(function () {

            $('#login_form').submit(function (e) {
                var submit_flag = true;

                $('#username-invalid-error-message').hide();

                if ($('#login-username').val().trim() == '') {
                    $('#username-error-message').show();

                    submit_flag = false;
                }

                if ($('#login-password').val().trim() == '') {
                    $('#password-error-message').show();

                    submit_flag = false;
                }

                return submit_flag;
            });

        });
    </script>

@endpush

