@extends("layouts.master")

@push('meta')
    <meta name="robots" content="noindex,follow,noodp"/>
@endpush

@section("content")

    <section id="register-page" class="py-5">
        <div class="container">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <div class="login-bg">
                            <div class="avater-icon">
                                <img src="{{ assets('frontend') }}/assets/img/avatar.png" alt="">
                                <h6 class="text-white mt-3"> {{ trans('auth.create_new_account') }} </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-container">
                            <form class="pt-5" method="post" id="register_form">

                                {{ csrf_field() }}

                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label>{{ trans('auth.username') }}</label>
                                    <div class="custom-input user-icon">
                                        <input name="username" maxlength="50" minlength="5" class="form-control" id=""
                                               placeholder="{{ trans('auth.username_placeholder') }}" value="{{ old('username') }}">
                                    </div>

                                    @if($errors->has('username'))
                                        <div class="alert alert-danger p-0 m-0 mt-2"
                                             id="username-invalid-error-message">
                                            <p class="my-1 px-2">{{ $errors->get('username')[0] }}</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('auth.email') }}</label>
                                    <div class="custom-input email-icon">
                                        <input type="email" class="form-control" maxlength="30" name="email" id=""
                                               placeholder="{{ trans('auth.email_placeholder') }}" value="{{ old('email') }}">
                                    </div>

                                    @if($errors->has('email'))
                                        <div class="alert alert-danger p-0 m-0 mt-2"
                                             id="username-invalid-error-message">
                                            <p class="my-1 px-2">{{ $errors->get('email')[0] }}</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('auth.password') }}</label>
                                    <div class="custom-input pass-icon">
                                        <input type="password" maxlength="20" minlength="8" name="password"
                                               class="form-control">
                                    </div>

                                    @if($errors->has('password'))
                                        <div class="alert alert-danger p-0 m-0 mt-2"
                                             id="username-invalid-error-message">
                                            <p class="my-1 px-2">{{ $errors->get('password')[0] }}</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('auth.repeat_password') }}</label>
                                    <div class="custom-input pass-icon">
                                        <input type="password" name="password_confirmation" class="form-control">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>{{ trans('auth.image') }}</label>
                                    <div class="custom-input">
                                        <input type="file" name="photo" id="profile-photo-input" form="profile-edit"
                                               class="form-control">
                                    </div>

                                    @if($errors->has('photo'))
                                        <div class="alert alert-danger p-0 m-0 mt-2"
                                             id="photo-invalid-error-message">
                                            <p class="my-1 px-2">{{ $errors->get('photo')[0] }}</p>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="control control--checkbox d-inline-block">
                                                <span class="mb-3">
                                                 {{ trans('auth.you_agree') }} <a
                                                            href="{{ route('page.index', ['slug' => 'terms-and-conditions']) }}"
                                                            class="text-green">{{ trans('auth.terms_cond') }}</a>

                                                </span>
                                        <span class="mt-2 d-block">
                                                        {{ trans('auth.and_also') }} <a
                                                    href="{{ route('page.index', ['slug' => 'privacy-policy']) }}"
                                                    class="text-green">{{ trans('auth.privacy') }}</a>

                                                       </span>
                                        <input type="checkbox" id="privacy_check" name="privacy_check" value="1"/>
                                        <div class="control__indicator"></div>

                                        @if($errors->has('privacy_check'))
                                            <div class="alert alert-danger p-0 m-0 mt-2"
                                                 id="username-invalid-error-message">
                                                <p class="my-1 px-2">{{ trans('auth.terms_required') }}</p>
                                            </div>
                                        @endif
                                    </label>
                                </div>
                                <button type="submit"
                                        class="btn btn-defult btn-block login">{{ trans('auth.create_account_button') }}</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@push('after_head')


    @if (session('status'))
        <script>
            fbq('track', 'Purchase');
        </script>
    @else
        <script>
            fbq('track', 'CompleteRegistration');
        </script>
    @endif

@endpush

@push('footer')

    <script>
        $(document).ready(function () {

            $('#register_form').submit(function (e) {

            });

        });
    </script>

@endpush
