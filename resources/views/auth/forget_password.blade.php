@extends("layouts.master")

@section("content")

    <section id="login-page" class="py-5">
        <div class="container">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <div class="login-bg">
                            <div class="avater-icon">
                                <img src="{{ assets('frontend') }}/assets/img/avatar.png" alt="">
                                <h6 class="text-white mt-3">{{ trans('auth.login') }}</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-container">
                            <form class="pt-5" method="post" id="login_form">

                                {{ csrf_field() }}

                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{ session('error') }}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label>{{ trans('auth.email') }}</label>

                                    <div class="custom-input user-icon">
                                        <input class="form-control" name="email" value="{{ request('email') }}" id="forget-email"
                                               placeholder="{{ trans('auth.email_placeholder') }}">
                                    </div>

                                    @if(count($errors))
                                        <div class="alert alert-danger p-0 m-0 mt-2"
                                             id="email-invalid-error-message">
                                            <p class="my-1 px-2">{{ $errors->first() }}</p>
                                        </div>
                                    @endif

                                    <div class="alert alert-danger p-0 m-0 mt-2" style="display: none;"
                                         id="email-error-message">
                                        <p class="my-1 px-2">{{ trans('auth.email_required') }}</p>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-defult btn-block login">{{ trans('auth.send_new_pass') }}</button>

                            </form>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

@stop


@push('footer')

    <script>
        $(document).ready(function () {

            $('#login-page').submit(function (e) {
                var submit_flag = true;

                $('#email-invalid-error-message').hide();

                if ($('#forget-email').val().trim() == '') {
                    $('#email-error-message').show();

                    submit_flag = false;
                }

                return submit_flag;
            });

        });
    </script>

@endpush

