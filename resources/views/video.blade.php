@extends("layouts.master")

@section("content")

    <section id="cats-page">
        <section class="category-search pb-0">
            <div class="container">
                <div class="m-auto search w-75">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5 class="d-inline-block mb-0 title">
                                <i class="fas fa-car"></i> اوتوبرتر</h5>
                            <div class="filter">
                                <div class="row">
                                    <div class="col-lg-12 d-flex">
                                        <div class="custom-select-2">
                                            <select class="js-example-data-array car-make" name="state" aria-placeholder="">
                                                <option value="">براند السياره</option>
                                            </select>
                                        </div>
                                        <div class="custom-select-2">
                                            <select class="js-example-data-array car-model" name="state" aria-placeholder="">
                                                <option value="">الموديل</option>
                                            </select>
                                        </div>
                                        <div class="custom-select-2">
                                            <select class="js-example-data-array city" name="state" aria-placeholder="">
                                                <option value="">حاله السياره</option>
                                            </select>
                                        </div>

                                        <a href="" class="search-btn">بحث</a>
                                    </div>
                                    <div class="col-lg-3"></div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="text-center font-weight-bold mt-2 mb-4">
                    <span>بحث مفصل</span>
                </div>
                <div class="quick-search">
                    <div class="row">
                        <div class="col pl-0 text-center border-left">
                            <a href="">سيارات جديده للبيع</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="">سيارات مستعمله للبيع</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href=""> مركبات اخري</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="">فديوهات </a>
                        </div>
                        <div class="col p-0 text-center">
                            <a href="">مقالات </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="slider">
            <div class="container">
                <div class="ads-cat my-3">
                    <img src="assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">
                </div>
                <div class="row">
                    <div class="col-sm-8 slide-cont">
                        <h3> كيا اسبورتاج 2015 </h3>
                        <div class="slide-info">
                            <ul>
                                <li>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <span>26-2-2018</span>
                                </li>
                                <li>
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    <span>
                                        280
                                    </span>
                                </li>

                            </ul>
                        </div>
                        <div class="image-d mb-5">
                            <img src="assets/img/slideimg.png" width="100%">
                        </div>

                        <h4 class="font-weight-bold"> 5 معلومات هامة يجب أن تعرفها عن رينو كليو قبل الشراء </h4>
                        <p>
                            تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة للحيرة في نفس الوقت، حيث
                            تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة الماضية، كل منها بشخصية
                            مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها. </p>
                        <p>تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة
                            للحيرة في نفس الوقت، حيث تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة
                            الماضية، كل منها بشخصية مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها.</p>
                        <h5 class="font-weight-bold">
                            1 -التصميم
                        </h5>
                        <p>
                            تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة للحيرة في نفس الوقت، حيث
                            تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة الماضية، كل منها بشخصية
                            مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها. </p>
                        <p>تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة
                            للحيرة في نفس الوقت، حيث تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة
                            الماضية، كل منها بشخصية مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها.</p>
                        <h5 class="font-weight-bold">
                            1 -التصميم
                        </h5>
                        <p>
                            تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة للحيرة في نفس الوقت، حيث
                            تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة الماضية، كل منها بشخصية
                            مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها. </p>
                        <p>تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة
                            للحيرة في نفس الوقت، حيث تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة
                            الماضية، كل منها بشخصية مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها.</p>

                        <img src="assets/img/ads5.png" class="ads" width="100%">

                        <h5 class="font-weight-bold">
                            1 -التصميم
                        </h5>
                        <p>
                            تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة للحيرة في نفس الوقت، حيث
                            تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة الماضية، كل منها بشخصية
                            مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها. </p>
                        <p>تعتبر فئة السيارات المستعملة تحت الـ 100 ألف جنيه، الفئة الأكثر مبيعا في مصر، والفئة الأكثر إثارة
                            للحيرة في نفس الوقت، حيث تمتلئ بالسيارات من التسعينات وبداية الألفية الجديدة، وحتى الأعوام القليلة
                            الماضية، كل منها بشخصية مختلفة وتجهيزات متفاوتة، ومن كل فئة قد تتذكرها.</p>
                        <hr>
                        <div class="new-car">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <h6 class="d-inline-block font-weight-bold"> اعلانات مشابهه </h6>
                                        <div class="owl-carousel owl-theme">
                                            <div class="item">
                                                <div class="card">
                                                    <div class="card-img-header">
                                                        <span class="favourite">
                                                            <i class="far fa-heart"></i>
                                                        </span>
                                                        <div class="price">
                                                            <span class="quantity">185000</span>
                                                            <span class="currancy">ج.م</span>
                                                        </div>
                                                        <img src="assets/img/car4.jpg" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div class="car-name">
                                                            <h6 class="mb-0"> هيونداي إلنترا 2011</h6>
                                                        </div>
                                                        <div class="location">
                                                            <img src="assets/img/map-icon.png" alt="" class="map-icon">
                                                            <span>القاهرة</span>,
                                                            <span>مصر</span>
                                                        </div>
                                                        <div class="car-info">
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/auto-icon.png" alt="" class="auto-icon">
                                                                <bdi>Automatic</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/dashboard-icon.png" alt="" class="dashboard-icon">
                                                                <bdi>10500</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/date-icon.png" alt="" class="date-icon">
                                                                <bdi>2012</bdi>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-muted bg-white">
                                                        <div class="date float-right">
                                                            <span>30-11-2017 </span>
                                                            <img src="assets/img/date-icon.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="card">
                                                    <div class="card-img-header">
                                                        <span class="favourite">
                                                            <i class="far fa-heart"></i>
                                                        </span>
                                                        <div class="price">
                                                            <span class="quantity">185000</span>
                                                            <span class="currancy">ج.م</span>
                                                        </div>
                                                        <img src="assets/img/car4.jpg" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div class="car-name">
                                                            <h6 class="mb-0"> هيونداي إلنترا 2011</h6>
                                                        </div>
                                                        <div class="location">
                                                            <img src="assets/img/map-icon.png" alt="" class="map-icon">
                                                            <span>القاهرة</span>,
                                                            <span>مصر</span>
                                                        </div>
                                                        <div class="car-info">
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/auto-icon.png" alt="" class="auto-icon">
                                                                <bdi>Automatic</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/dashboard-icon.png" alt="" class="dashboard-icon">
                                                                <bdi>10500</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/date-icon.png" alt="" class="date-icon">
                                                                <bdi>2012</bdi>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-muted bg-white">
                                                        <div class="date float-right">
                                                            <span>30-11-2017 </span>
                                                            <img src="assets/img/date-icon.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="card">
                                                    <div class="card-img-header">
                                                        <span class="favourite">
                                                            <i class="far fa-heart"></i>
                                                        </span>
                                                        <div class="price">
                                                            <span class="quantity">185000</span>
                                                            <span class="currancy">ج.م</span>
                                                        </div>
                                                        <img src="assets/img/car4.jpg" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div class="car-name">
                                                            <h6 class="mb-0"> هيونداي إلنترا 2011</h6>
                                                        </div>
                                                        <div class="location">
                                                            <img src="assets/img/map-icon.png" alt="" class="map-icon">
                                                            <span>القاهرة</span>,
                                                            <span>مصر</span>
                                                        </div>
                                                        <div class="car-info">
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/auto-icon.png" alt="" class="auto-icon">
                                                                <bdi>Automatic</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/dashboard-icon.png" alt="" class="dashboard-icon">
                                                                <bdi>10500</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/date-icon.png" alt="" class="date-icon">
                                                                <bdi>2012</bdi>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-muted bg-white">
                                                        <div class="date float-right">
                                                            <span>30-11-2017 </span>
                                                            <img src="assets/img/date-icon.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="card">
                                                    <div class="card-img-header">
                                                        <span class="favourite">
                                                            <i class="far fa-heart"></i>
                                                        </span>
                                                        <div class="price">
                                                            <span class="quantity">185000</span>
                                                            <span class="currancy">ج.م</span>
                                                        </div>
                                                        <img src="assets/img/car4.jpg" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div class="car-name">
                                                            <h6 class="mb-0"> هيونداي إلنترا 2011</h6>
                                                        </div>
                                                        <div class="location">
                                                            <img src="assets/img/map-icon.png" alt="" class="map-icon">
                                                            <span>القاهرة</span>,
                                                            <span>مصر</span>
                                                        </div>
                                                        <div class="car-info">
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/auto-icon.png" alt="" class="auto-icon">
                                                                <bdi>Automatic</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/dashboard-icon.png" alt="" class="dashboard-icon">
                                                                <bdi>10500</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/date-icon.png" alt="" class="date-icon">
                                                                <bdi>2012</bdi>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-muted bg-white">
                                                        <div class="date float-right">
                                                            <span>30-11-2017 </span>
                                                            <img src="assets/img/date-icon.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="card">
                                                    <div class="card-img-header">
                                                        <span class="favourite">
                                                            <i class="far fa-heart"></i>
                                                        </span>
                                                        <div class="price">
                                                            <span class="quantity">185000</span>
                                                            <span class="currancy">ج.م</span>
                                                        </div>
                                                        <img src="assets/img/car4.jpg" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div class="car-name">
                                                            <h6 class="mb-0"> هيونداي إلنترا 2011</h6>
                                                        </div>
                                                        <div class="location">
                                                            <img src="assets/img/map-icon.png" alt="" class="map-icon">
                                                            <span>القاهرة</span>,
                                                            <span>مصر</span>
                                                        </div>
                                                        <div class="car-info">
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/auto-icon.png" alt="" class="auto-icon">
                                                                <bdi>Automatic</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/dashboard-icon.png" alt="" class="dashboard-icon">
                                                                <bdi>10500</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/date-icon.png" alt="" class="date-icon">
                                                                <bdi>2012</bdi>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-muted bg-white">
                                                        <div class="date float-right">
                                                            <span>30-11-2017 </span>
                                                            <img src="assets/img/date-icon.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="card">
                                                    <div class="card-img-header">
                                                        <span class="favourite">
                                                            <i class="far fa-heart"></i>
                                                        </span>
                                                        <div class="price">
                                                            <span class="quantity">185000</span>
                                                            <span class="currancy">ج.م</span>
                                                        </div>
                                                        <img src="assets/img/car4.jpg" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div class="car-name">
                                                            <h6 class="mb-0"> هيونداي إلنترا 2011</h6>
                                                        </div>
                                                        <div class="location">
                                                            <img src="assets/img/map-icon.png" alt="" class="map-icon">
                                                            <span>القاهرة</span>,
                                                            <span>مصر</span>
                                                        </div>
                                                        <div class="car-info">
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/auto-icon.png" alt="" class="auto-icon">
                                                                <bdi>Automatic</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/dashboard-icon.png" alt="" class="dashboard-icon">
                                                                <bdi>10500</bdi>
                                                            </div>
                                                            <div class="d-inline-block ml-2">
                                                                <img src="assets/img/date-icon.png" alt="" class="date-icon">
                                                                <bdi>2012</bdi>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="card-footer text-muted bg-white">
                                                        <div class="date float-right">
                                                            <span>30-11-2017 </span>
                                                            <img src="assets/img/date-icon.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="comments-container">
                            <h3>التعليقات </h3>

                            <ul id="comments-list" class="comments-list">
                                <li>
                                    <div class="comment-main-level">
                                        <!-- Avatar -->
                                        <div class="comment-avatar">
                                            <img src="assets/img/userprofile.png" alt="">
                                        </div>
                                        <!-- Contenedor del Comentario -->
                                        <div class="comment-box">
                                            <div class="comment-head">
                                                <h6 class="comment-name">
                                                    <a href="#"> احمد عثمان</a>
                                                </h6>
                                                <span>22/2/2018</span>
                                                <i class="fa fa-reply"></i>
                                                <i class="fa fa-heart"></i>
                                            </div>
                                            <div class="comment-content">
                                                تعمل رينو كليو الجيل الثاني بمحرك رباعي الأسطوانات سعة 1.2 لتر بقوة 74 حصان وعزم 105 نيوتن.متر، وهو محرك اقتصادي جيد قادر
                                                على توفير أداء متميز للسيارة بفضل خفة وزنها، ونشاط المحرك وقدرته على تقديم
                                                ارقام الأداء بشكل جيد على الطريق. ويوفر هذا المحرك معدل استهلاك اقتصادي للوقود،
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Respuestas de los comentarios -->
                                    <ul class="comments-list reply-list">
                                        <li>
                                            <!-- Avatar -->
                                            <div class="comment-avatar">
                                                <img src="assets/img/userprofile.png" alt="">
                                            </div>
                                            <!-- Contenedor del Comentario -->
                                            <div class="comment-box">
                                                <div class="comment-head">
                                                    <h6 class="comment-name">
                                                        <a href="#"> احمد عثمان</a>
                                                    </h6>
                                                    <span>22/2/2018</span>
                                                    <i class="fa fa-reply"></i>
                                                    <i class="fa fa-heart"></i>
                                                </div>
                                                <div class="comment-content">
                                                    تعمل رينو كليو الجيل الثاني بمحرك رباعي الأسطوانات سعة 1.2 لتر بقوة 74 حصان وعزم 105 نيوتن.متر، وهو محرك اقتصادي جيد قادر
                                                    على توفير أداء متميز للسيارة بفضل خفة وزنها، ونشاط المحرك وقدرته على
                                                    تقديم ارقام الأداء بشكل جيد على الطريق. ويوفر هذا المحرك معدل استهلاك
                                                    اقتصادي للوقود،
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <!-- Avatar -->
                                            <div class="comment-avatar">
                                                <img src="assets/img/userprofile.png" alt="">
                                            </div>
                                            <!-- Contenedor del Comentario -->
                                            <div class="comment-box">
                                                <div class="comment-head">
                                                    <h6 class="comment-name">
                                                        <a href="#"> احمد عثمان</a>
                                                    </h6>
                                                    <span>22/2/2018</span>
                                                    <i class="fa fa-reply"></i>
                                                    <i class="fa fa-heart"></i>
                                                </div>
                                                <div class="comment-content">
                                                    تعمل رينو كليو الجيل الثاني بمحرك رباعي الأسطوانات سعة 1.2 لتر بقوة 74 حصان وعزم 105 نيوتن.متر، وهو محرك اقتصادي جيد قادر
                                                    على توفير أداء متميز للسيارة بفضل خفة وزنها، ونشاط المحرك وقدرته على
                                                    تقديم ارقام الأداء بشكل جيد على الطريق. ويوفر هذا المحرك معدل استهلاك
                                                    اقتصادي للوقود،
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <div class="comment-main-level">
                                        <!-- Avatar -->
                                        <div class="comment-avatar">
                                            <img src="assets/img/userprofile.png" alt="">
                                        </div>
                                        <!-- Contenedor del Comentario -->
                                        <div class="comment-box">
                                            <div class="comment-head">
                                                <h6 class="comment-name">
                                                    <a href="#"> احمد عثمان</a>
                                                </h6>
                                                <span>22/2/2018</span>
                                                <i class="fa fa-reply"></i>
                                                <i class="fa fa-heart"></i>
                                            </div>
                                            <div class="comment-content">
                                                تعمل رينو كليو الجيل الثاني بمحرك رباعي الأسطوانات سعة 1.2 لتر بقوة 74 حصان وعزم 105 نيوتن.متر، وهو محرك اقتصادي جيد قادر
                                                على توفير أداء متميز للسيارة بفضل خفة وزنها، ونشاط المحرك وقدرته على تقديم
                                                ارقام الأداء بشكل جيد على الطريق. ويوفر هذا المحرك معدل استهلاك اقتصادي للوقود،
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box-info">
                            <h5> اهم العناوين </h5>
                            <hr>
                            <h6>1 - 5 معلومات هامة يجب أن تعرفها عن رينو كليو قبل الشراء
                            </h6>
                            <p>- سعة المحرك (سي سي) - سعة المحرك (سي سي)</p>
                            <h6>1 - 5 معلومات هامة يجب أن تعرفها عن رينو كليو قبل الشراء
                            </h6>
                            <p>- سعة المحرك (سي سي) - سعة المحرك (سي سي)</p>
                            <h6>1 - 5 معلومات هامة يجب أن تعرفها عن رينو كليو قبل الشراء
                            </h6>
                            <p>- سعة المحرك (سي سي) - سعة المحرك (سي سي)</p>
                            <h6>1 - 5 معلومات هامة يجب أن تعرفها عن رينو كليو قبل الشراء
                            </h6>
                            <p>- سعة المحرك (سي سي) - سعة المحرك (سي سي)</p>
                            <h6>1 - 5 معلومات هامة يجب أن تعرفها عن رينو كليو قبل الشراء
                            </h6>
                            <p>- سعة المحرك (سي سي) - سعة المحرك (سي سي)</p>
                        </div>

                        <div class="share-box">
                            <span>
                                شارك علي
                            </span>
                            <ul>
                                <li>
                                    <a class="color-facebook" href="">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-youtube" href="">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-twitter" href="">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-google" href="">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-instagram" href="">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-pinterest" href="">
                                        <i class="fab fa-pinterest-p"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="box-gallery">
                            <span class="box-title">
                                <i class="fas fa-play-circle"></i>
                                أحدث الفيديوهات
                            </span>
                            <div class="box-content">
                                <ul>
                                    <li>
                                        <img src="assets/img/img-man.png" alt="..." class="rounded">
                                        <div class="list-info">
                                            <h5>عوامل السلامة و الامان داخل عربيتك</h5>
                                            <span>
                                                <i class="far fa-calendar-alt"></i>
                                                3-11-2018
                                            </span>
                                            <span>
                                                <i class="fas fa-eye"></i>12.808</span>
                                        </div>

                                    </li>
                                    <li>
                                        <img src="assets/img/img-man.png" alt="..." class="rounded">
                                        <div class="list-info">
                                            <h5>عوامل السلامة و الامان داخل عربيتك</h5>
                                            <span>
                                                <i class="far fa-calendar-alt"></i>
                                                3-11-2018
                                            </span>
                                            <span>
                                                <i class="fas fa-eye"></i>12.808</span>
                                        </div>

                                    </li>
                                    <li>
                                        <img src="assets/img/img-man.png" alt="..." class="rounded">
                                        <div class="list-info">
                                            <h5>عوامل السلامة و الامان داخل عربيتك</h5>
                                            <span>
                                                <i class="far fa-calendar-alt"></i>
                                                3-11-2018
                                            </span>
                                            <span>
                                                <i class="fas fa-eye"></i>12.808</span>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-gallery">
                            <span class="box-title">
                                <i class="fas fa-play-circle"></i>
                                أحدث المجالات
                            </span>
                            <div class="box-content">
                                <ul>
                                    <li>
                                        <img src="assets/img/img-man.png" alt="..." class="rounded">
                                        <div class="list-info">
                                            <h5>عوامل السلامة و الامان داخل عربيتك</h5>
                                            <span>
                                                <i class="far fa-calendar-alt"></i>
                                                3-11-2018
                                            </span>
                                            <span>
                                                <i class="fas fa-eye"></i>12.808</span>
                                        </div>

                                    </li>
                                    <li>
                                        <img src="assets/img/img-man.png" alt="..." class="rounded">
                                        <div class="list-info">
                                            <h5>عوامل السلامة و الامان داخل عربيتك</h5>
                                            <span>
                                                <i class="far fa-calendar-alt"></i>
                                                3-11-2018
                                            </span>
                                            <span>
                                                <i class="fas fa-eye"></i>12.808</span>
                                        </div>

                                    </li>
                                    <li>
                                        <img src="assets/img/img-man.png" alt="..." class="rounded">
                                        <div class="list-info">
                                            <h5>عوامل السلامة و الامان داخل عربيتك</h5>
                                            <span>
                                                <i class="far fa-calendar-alt"></i>
                                                3-11-2018
                                            </span>
                                            <span>
                                                <i class="fas fa-eye"></i>12.808</span>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </section>


@stop

@push("head")
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/product-showcase.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/edit-css.css") }}">
@endpush
