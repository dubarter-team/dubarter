@extends("layouts.master")

@section("content")
    <section id="add-post-page">
        <section class="category-search ad-post-banner pb-0"
                 style="background-image: url({{ assets('frontend') . '/assets/img/post-ad-banner.jpg' }});">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-md-9">
                        <h4 class="add-post mb-5 py-3 px-5" style="margin-bottom: 0 !important;"><span
                                    class="{{ app()->getLocale() == 'ar' ? 'ml-3' : 'mr-3' }}"><i
                                        class="far fa-user"></i></span>{{ trans('auth.verification_title') }}</h4>
                    </div>

                </div>
            </div>
        </section>

        <div class="container my-5" style="margin-top: 0 !important;">


            <form action="" method="post" id="verification_form">

                {{ csrf_field() }}

                <input type="hidden" name="user_id" value="{{ request("user_id", Request::old("user_id")) }}" />

                <div class="row justify-content-center">
                    <div class="col-md-6 mb-2">

                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                @if(count($errors->all()))
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $message)
                            <div>{{ $message }}</div>
                        @endforeach
                    </div>
                @endif
                    </div>

                </div>

                    <div class="row justify-content-center">
                        <div class="col-md-6 mb-2">

                        <label class="form-label">{{ trans('auth.code') }}</label> <span class="required">*</span>
                        <div class="custom-select-2">
                            <input type="text" autocomplete="off" name="code" maxlength="70" class="form-control"
                                   value="{{ Request::old("code") }}"
                                   placeholder="{{ trans('auth.code') }}" required>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center">
                    <div class="col-md-6 mb-2">
                        <label class="form-label">{{ trans('auth.password') }}</label> <span class="required">*</span>
                        <div class="custom-select-2">
                            <input type="password" name="password" autocomplete="off" name="code" maxlength="70" class="form-control"
                                   value=""
                                   placeholder="{{ trans('auth.password') }}" required>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center">
                    <div class="col-md-6 mb-2">
                        <label class="form-label">{{ trans('auth.repassword') }}</label> <span class="required">*</span>
                        <div class="custom-select-2">
                            <input type="password" name="repassword" autocomplete="off" name="code" maxlength="70" class="form-control"
                                   value=""
                                   placeholder="{{ trans('auth.repassword') }}" required>
                        </div>
                    </div>

                </div>

                <div class="text-center mt-5">
                    <button type="submit" class="btn btn-dark">{{ trans("auth.save") }}</button>
                </div>


        </div>
    </section>

@stop

@push('after_head')

@endpush

@push("head")


@endpush

@push("footer")


@endpush
