@extends("layouts.master")

@push('meta')
    <title>{{ $post->title . ' - ' . trans('common.dubarter') }}</title>

    <?php $seo = $post->seo; ?>
    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:type" content="{{ $post->format == 'article' ? 'article' : 'video.episode' }}"/>
    <meta name="author" content="{{option('site_name')}}">
    <meta name="twitter:site" content="{{'@'.option('site_name')  }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="robots" content="index, follow">
    <meta name="twitter:url" content="{{$post->url}}">
    <meta property="og:url" content="{{$post->url}}">
    <meta property="og:site_name" content="{{option('site_name')}}"/>


    @if($seo&&!empty($seo))
        <meta name="title" content="{{ $seo->meta_title or $post->title}}">
        <meta name="keywords"
              content="{{ !empty($seo->meta_keywords) ? $seo->meta_keywords : (count($post->tags) ? implode(',', $post->tags->pluck(['name'])->toArray()) : '') }}">
        <meta name="description" content="{{!empty($seo->meta_description)?$seo->meta_description:$post->excerpt}}"/>
        <meta property="og:title"
              content="{{ !empty($seo->facebook_title)?$seo->facebook_title:(!empty($seo->meta_title)? $seo->meta_title: $post->title)}}"/>
        <meta property="og:site_name" content="{{option('site_name')}}"/>
        <meta property="og:description"
              content="{{ !empty($seo->facebook_description)?$seo->facebook_description:(!empty($seo->meta_description)? $seo->meta_description: $post->excerpt)}}">
        <meta property="og:image"
              content="{{ !empty($seo->facebook)?uploads_url($seo->facebook->path):($post->image ? uploads_url($post->image->path) : '')}}">
        <meta name="twitter:title"
              content="{{!empty($seo->twitter_title)?$seo->twitter_title:(!empty($seo->meta_title)?$seo->meta_title: $post->title) }}">
        <meta name="twitter:description"
              content="{{ !empty( $seo->twitter_description)? $seo->twitter_description : ( !empty($seo->meta_description)?$seo->meta_description:$post->excerpt)}}">
        <meta name="twitter:image"
              content="{{ !empty($seo->twitter)?uploads_url($seo->twitter->path):($post->image ? uploads_url($post->image->path) : '')}}">
    @else
        <meta name="title" content="{{ $post->title}}">
        <meta name="keywords"
              content="{{ count($post->tags) ? implode(',', $post->tags->pluck(['name'])->toArray()) : '' }}">
        <meta name="description" content="{{ $post->excerpt}}"/>
        <meta property="og:locale" content="{{app()->getLocale()}}"/>
        <meta property="og:title" content="{{ $post->title }}"/>
        <meta property="og:description" content="{{ $post->excerpt}}">
        <meta property="og:image" content="{{ ($post->image ? uploads_url($post->image->path) : '') }}">
        <meta name="twitter:title" content="{{$post->title }}">
        <meta name="twitter:description" content="{{ $post->excerpt}}">
        <meta name="twitter:image"
              content="{{ ($post->image ? uploads_url($post->image->path) : '') }}">
    @endif
@endpush

@push('head')

    <style>

        .post-content-x * {
            white-space: normal !important;
            word-wrap: break-word !important;
        }

    </style>

@endpush

@section("content")
    <h1 style="display: none;">{{ $post->title }}</h1>

    <section id="cats-page">
        <section class="category-search pb-0"
                 style="{{ $main_category->banner ? "background-image: url($main_category->banner_image);" : '' }}">
            <div class="container">
                <div class="m-auto search box-filter">

                    @include('partials.category.category_search_bar', ['cities' => $cities, 'main_category' => $main_category])

                </div>
                <div class="text-center font-weight-bold mt-2 mb-4">
                    <span></span>
                </div>

                @include('partials.category.nav_cats', ['nav_cats' => $nav_categories, 'main_category' => $main_category])
            </div>
        </section>
        <section class="slider">
            <div class="container">

                <div class="ads-cat mt-4">

                    <!-- home page resposive ads -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9085617575772553"
                         data-ad-slot="8947623489"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                </div>
            </div>

            <div class="container">

                <br/>

                {{-- <div class="ads-cat my-3">
                     <img src="{{ assets("frontend") }}/assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">
                 </div>--}}
                <div class="row">
                    <div class="col-sm-8 slide-cont">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ asset('/') }}">{{trans('common.home')}}</a></li>
                                <li class="breadcrumb-item"><a
                                            href="{{ asset('/').($post->format=='article'?'blog':'video').'/'.$post->category->slug}}">{{trans('common.'.$post->format.'s')}}</a>
                                </li>
                            </ol>
                        </nav>
                        <h3 class="{{ dir_class($post->title) }}">{{ $post->title }}</h3>
                        <div class="slide-info">
                            <ul>
                                {{-- <li>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <span>{{ $post->date }}</span>
                                </li> --}}
                                <li>
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    <span>{{ $post->real_views }}</span>
                                </li>

                            </ul>
                        </div>
                        <div class="image-d mb-5">
                            @if($post->format == 'article')
                                <img src="{{ $post->image ? uploads_url($post->image->path) : '' }}"
                                     width="100%">

                                @if($post->media)
                                    <br/><br/>
                                    @if($post->media->provider == 'youtube')
                                        <iframe width="100%" height="400" src="{{ $post->media->path }}?rel=0"
                                                frameborder="0"
                                                allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    @else
                                        <video width="100%" height="400" controls>
                                            <source src="{{ uploads_url($post->media->path) }}">
                                            Your browser does not support the video tag.
                                        </video>
                                    @endif
                                @endif
                            @elseif($post->format == 'video' && $post->media)
                                @if($post->media->provider == 'youtube')
                                    <iframe width="100%" height="400" src="{{ $post->media->path }}?rel=0"
                                            frameborder="0"
                                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                @else
                                    <video width="100%" height="400" controls>
                                        <source src="{{ uploads_url($post->media->path) }}">
                                        Your browser does not support the video tag.
                                    </video>
                                @endif

                            @endif
                        </div>

                        <div class="post-content-x {{ dir_class($post->content) }}">{!! $post->content !!}</div>
                        <hr>

                        <div class="new-car">

                                    @if(count($post->tags))
                                        <div class="post-tags">
                                            <h4>{{trans('ad.related_links')}}</h4>
                                            <ul class="add-tags">
                                                @foreach($post->tags as $tag)
                                                    <li>
                                                        <a href="{{ route('tag.index', ['slug' => $tag->slug]) }}">{{ $tag->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if(count($related_ads))

                                        <div class="ads-cat">
                                            <ins class="adsbygoogle"
                                                 style="display:block"
                                                 data-ad-format="autorelaxed"
                                                 data-ad-client="ca-pub-9085617575772553"
                                                 data-ad-slot="8869907254"></ins>
                                            <script>
                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                            </script>
                                        </div>

                                        <div class="related-items">
                                            <h6 class="d-inline-block font-weight-bold">   {{trans('ad.similar_ads')}} </h6>

                                            <div id="item-page">
                                                <div class="ad-related-ads">
                                                    <div class="owl-carousel owl-theme">

                                                        @foreach($related_ads as $ad1)

                                                            @include('partials.ad.ad_card', ['ad' => $ad1])

                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    @endif
                        </div>

                        <hr>

                        <div class="ads-cat my-3">

                            <!-- responsive ads replaced 728*90 -->
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-9085617575772553"
                                 data-ad-slot="5498890495"
                                 data-ad-format="auto"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>

                        </div>

                        <hr>

                        @include('partials.post.comments', ['comments' => $comments,'posts'=>$post])
                    </div>
                    <div class="col-sm-4">

                        <div class="share-box">
                            <span>
                                {{trans('ad.share_on')}}
                            </span>
                            <ul>
                                <li>
                                    <a class="color-facebook shareBtn facebook" href="{{ $post->url }}">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-twitter shareBtn twitter" href="{{ $post->url }}">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-google shareBtn google" href="{{ $post->url }}">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>

                                <li>
                                    <a class="color-pinterest shareBtn pinterest" href="{{ $post->url }}">
                                        <i class="fab fa-pinterest-p"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        @if(config("country.code") == "eg")
                        @include('partials.common.latest_videos_sidebar', ['latest_videos' => $latest_videos, 'main_category' => $post->category])
                        @endif
                        @include('partials.common.latest_articles_sidebar', ['latest_articles' => $latest_articles, 'main_category' => $post->category])

                    </div>
                </div>
            </div>
        </section>


    </section>


@stop

@push("head")
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/product-showcase.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/edit-css.css") }}">
@endpush

@push('footer')

    <script type="application/ld+json">
        {!!  json_encode($post->schema)  !!}
    </script>

    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "{{ asset('/') }}",
      "name": "{{trans('common.home')}}"
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{ asset('/').($post->format=='article'?'blog':'video').'/'.$post->category->slug}}",
      "name": "{{trans('common.'.$post->format.'s')}}"
          }
  },{
    "@type": "ListItem",
    "position": 3,
    "item": {
      "@id": "{{$post->url}}",
      "name": "{{$post->title}}"
    }
  }]
}



    </script>
@endpush
