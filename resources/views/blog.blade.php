@extends("layouts.master")

@section("content")
    <!-- End navbar section -->
    <section id="cats-page">

        <!-- End navbar section -->
        <section id="cats-page">
            <section class="category-search pb-0">
                <div class="container">
                    <div class="m-auto search box-filter">

                        @if($category->id == \App\Models\Category::AUTOBARTER)

                            @include('partials.category.cars_search_bar', ['main_category' => $category, 'cars_categories' => $cars_categories, 'cities' => $cities])

                        @elseif($category->id == \App\Models\Category::AQARBARTETR)

                            @include('partials.category.realestate_search_bar', ['main_category' => $category, 'realestate_categories' => $realestate_categories, 'cities' => $cities])

                        @elseif($category->id == \App\Models\Category::STYLEBARTER)

                            @include('partials.category.style_search_bar', ['main_category' => $category, 'style_categories' => $style_categories, 'cities' => $cities])

                        @elseif($category->id == \App\Models\Category::FOODBARTER)

                            @include('partials.category.food_search_bar', ['main_category' => $category, 'food_categories' => $food_categories, 'cities' => $cities])

                        @elseif($category->id == \App\Models\Category::MOBAWABABARTER)

                            @include('partials.category.mobwaba_search_bar', ['main_category' => $category, 'mob_categories' => $mob_categories, 'cities' => $cities])

                        @endif

                    </div>
                    {{-- <div class="text-center font-weight-bold mt-2 mb-4">
                         <span>{{ trans('category.detailed_search') }}</span>
                     </div>--}}
                    <br/>
                    <div class="quick-search">

                        <div class="row justify-content-center text-center quick-filter">

                            @if(count($nav_categories))
                                @foreach($nav_categories as $cat)

                                    <div class="col-md-2 p-0">
                                        <a href="{{ $cat->url }}">{{ $cat->name }}</a>
                                    </div>

                                @endforeach
                            @endif

                            <div class="col-md-2 p-0">
                            <a href="{{ url("videos/". $category->slug) }}">{{ trans('category.videos') }}</a>
                            </div>

                            <div class="col-md-2 p-0">
                            <a href="{{ url("blog/". $category->slug) }}">{{ trans('category.articles') }}</a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

        <section class="slider">
            <div class="container">
                {{--<div class="ads-cat my-3">
                    <img src="{{ assets("frontend") }}/assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">
                </div>--}}

                <div class="slider-tabs">
                    <div class="row">

                        <div class="col-5">
                            <div class="list-group p-0" id="list-tab" role="tablist">

                                @if(count($featured_posts))
                                    @foreach($featured_posts as $post)

                                        <a class="list-group-item list-group-item-action {{ $loop->first ? 'active' : '' }}"
                                           id="news{{ $loop->index + 1 }}" data-toggle="list"
                                           href="#list-news{{ $loop->index + 1 }}" role="tab" aria-controls="home">
                                            <img src="{{ $post->image ? thumbnail($post->image->path) : '' }}" alt=""
                                                 class="img-fluid">
                                            <div>
                                                <h6 class="mb-0">{{ $post->title }}</h6>
                                                <br/>
                                                <small>{{ Str::words($post->excerpt, 20) }}</small>
                                            </div>
                                        </a>

                                    @endforeach
                                @endif

                                {{--<div class="next-news">--}}
                                {{--<a><img src="{{ assets("frontend") }}/assets/img/chevron-bottom.png" alt=""></a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="col-7 pr-0">
                            <div class="tab-content" id="nav-tabContent">
                                @if(count($featured_posts))
                                    @foreach($featured_posts as $post)

                                        <div class="tab-pane fade {{ $loop->first ? 'show active' : '' }}"
                                             id="list-news{{ $loop->index + 1 }}" role="tabpanel"
                                             aria-labelledby="list-news{{ $loop->index + 1 }}">
                                            <a href="">
                                                <img src="{{ $post->image ? uploads_url($post->image->path) : '' }}"
                                                     alt="" class="slider-img">
                                                <div class="slider-info">
                                                    <h6 class="mb-0">
                                                        <a href="{{ $post->url }}" class="color-white">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h6>
                                                    <small class="mb-0">{{ $post->excerpt }}</small>
                                                </div>
                                            </a>
                                        </div>

                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Start videos section -->
        @if($posts->count())
            <section id="videos" class="bg-white border-0 h-auto">
                <div class="container">
                    <h6 class="px-2 mb-0 py-4 d-inline-block text-auto-barter font-weight-bold"
                        style="{{ $category->color ? "color: $category->color !important;" : "" }}">{{ $format == 'article' ? trans('category.latest_articles') : trans('category.latest_videos') }}</h6>
                    {{-- <button class="btn follow-up"><span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                         <span>{{ trans('category.follow') }}</span></button>--}}

                    <div class="owl-carousel owl-theme">

                        @foreach($posts as $post)

                            <div class="item">
                                <div class="card viedo">
                                    @if($post->format == 'video')
                                        <div class="play-icon"><i class="fa fa-play" aria-hidden="true"></i></div>
                                    @endif

                                    <img src="{{ $post->image ? uploads_url($post->image->path) : '' }}"
                                         alt="{{ $post->title }}">

                                    <div class="video-details d-none">
                                        <div class="title text-center p-1 border-bottom">
                                            <a href="{{ $category->url }}" class="color-white">
                                               {{-- <span class="fa fa-car"></span> --}}<span>{{ $category->name }}</span>
                                            </a>
                                        </div>
                                        <div class="desc px-3 pt-2 pb-0 ">
                                            <a href="{{ $post->url }}" class="color-white">
                                                <span>{{ $post->title }}</span>
                                            </a>
                                        </div>
                                        <div class="info py-2 px-3">
                                            {{-- <div class="d-inline-block"><i class="fas fa-calendar-alt"></i> <span
                                                        class="date">{{ $post->date }}</span>
                                            </div> --}}
                                            <div class="d-inline-block float-right"><i class="fas fa-eye"></i> <span
                                                        class="views">{{ $post->views }}</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach


                    </div>
                    <div class="ads-cat my-3">
                        <img src="{{ assets("frontend") }}/assets/img/ads1.png" alt="" class="d-block img-fluid m-auto">
                    </div>
                </div>
            </section>
        @endif

    </section>
@stop
