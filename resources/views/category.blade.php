@extends("layouts.master")

@push('meta')

    <title>{{ $main_category->page_title . ' - ' . \App\Models\Place::getCurrentCountry()->name . ' - '.trans('common.dubarter') }}</title>

    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:type" content="website"/>
    <meta name="author" content="{{option('site_name')}}">
    <meta name="twitter:site" content="{{'@'.option('site_name')  }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="robots" content="index, follow">

    <meta name="keywords"
          content="{{ $main_category->key_words }}">
    <meta name="title" content="{{ $main_category->page_title. " - ". \App\Models\Place::getCurrentCountry()->name }}">
    <meta name="description" content="{{ $main_category->description }}"/>
    <meta property="og:title" content="{{ $main_category->page_title }}"/>
    <meta property="og:site_name" content="{{ option('site_name') }}"/>
    <meta property="og:url" content="{{ $main_category->url }}">
    <meta property="og:description" content="{{ $main_category->description}}">
    <meta property="og:image" content="{{ thumbnail($main_category->image->path, "medium") }}">
    <meta name="twitter:title" content="{{ $main_category->page_title }}">
    <meta name="twitter:description" content="{{ $main_category->description }}">
    <meta name="twitter:image" content="{{ thumbnail($main_category->image->path, "medium") }}">
    <meta name="twitter:url" content="{{ $main_category->url }}">
@endpush

@section("content")

    <h1 style="display: none;">{{ $main_category->h1 }}</h1>
    <h2 style="display: none;">{{ $main_category->h2 }}</h2>
    <!-- End navbar section -->
    <section id="cats-page">

        <section class="category-search pb-0"
                 style="{{ $main_category->banner ? "background-image: url($main_category->banner_image);" : '' }}">
            <div class="container">
                <div class="m-auto search box-filter">

                    @include('partials.category.category_search_bar', ['cities' => $cities, 'main_category' => $main_category])

                </div>
                <div class="text-center font-weight-bold mt-2 mb-4">
                    <span> </span>
                </div>

                @include('partials.category.nav_cats', ['nav_cats' => $nav_categories, 'main_category' => $main_category])
            </div>
        </section>
        <section class="slider category-main-off-mobile">

{{--            <div class="container">--}}
{{--                <div class="ads-cat mt-4">--}}

{{--                    <!-- home page resposive ads -->--}}
{{--                    <ins class="adsbygoogle"--}}
{{--                         style="display:block"--}}
{{--                         data-ad-client="ca-pub-9085617575772553"--}}
{{--                         data-ad-slot="8947623489"--}}
{{--                         data-ad-format="auto"></ins>--}}
{{--                    <script async>--}}
{{--                        (adsbygoogle = window.adsbygoogle || []).push({});--}}
{{--                    </script>--}}

{{--                </div>--}}
{{--            </div>--}}

            <div class="container">
                <h5 class="px-2 mb-0 d-inline-block text-auto-barter"
                    style="padding: 30px 0px 0px; font-weight: bold; {{ $main_category->color ? "color: $main_category->color !important;" : "" }}">{{trans('category.featured_ads')}}</h5>
                <div id="jslidernews2" class="lof-slidecontent">
                    <div class="preload">
                        <div></div>
                    </div>
                    <!-- MAIN CONTENT -->
                    <div class="main-slider-content">
                        <ul class="sliders-wrap-inner">


                            @if(count($featured_ads))
                                @foreach($featured_ads as $ad)

                                    <li>
                                        <a href="{{ $ad->url }}">
                                            <img src="{{ $ad->main_image ? thumbnail($ad->main_image, 'large') : '' }}"
                                                 title="{{ $ad->title }}">
                                            <div class="slider-description">
                                                <h4 class="{{ dir_class($ad->title) }}">{{ $ad->title }}</h4>
                                                <p class="{{ dir_class($ad->excerpt) }}">{{ $ad->excerpt }}
                                                </p>
                                            </div>
                                        </a>
                                    </li>

                                @endforeach
                            @endif

                        </ul>
                    </div>
                    <!-- END MAIN CONTENT -->
                    <!-- NAVIGATOR -->
                    <div class="navigator-content">
                        <div class="navigator-wrapper">
                            <ul class="navigator-wrap-inner">
                                @if(count($featured_ads))
                                    @foreach($featured_ads as $ad)

                                        <li>
                                            <div>
                                                <div class="cat-slider-thumb">
                                                    <img src="{{ $ad->main_image ? thumbnail($ad->main_image, "thumbnail") : '' }}"/>
                                                </div>
                                                <h5 class="{{ dir_class($ad->title) }}">{{ $ad->title }}</h5>
                                                <p class="{{ dir_class($ad->excerpt) }}">{{ $ad->excerpt }}</p>
                                            </div>
                                        </li>


                                    @endforeach
                                @endif
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section id="cars">
            @if(count($sub_categories))
                @foreach($sub_categories as $cat)

                    @php
                        $sub_cat_ads = ${'cat_' . $cat->id . '_ads'};
                    @endphp

                    @if(count($sub_cat_ads))

                        <div class="category-slider">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="px-2 mb-0 d-inline-block text-auto-barter"><a href="{{ $cat->url }}"
                                                                                                 style="{{ $main_category->color ? "color: $main_category->color !important;" : "" }}">{{ $cat->custom_name }}</a>
                                        </h5>
                                        {{--<button class="btn follow-up">--}}
                                        {{--<span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>--}}
                                        {{--<span>{{ trans('category.follow') }}</span>--}}
                                        {{--</button>--}}
                                        <div class="owl-carousel owl-theme">
                                            @foreach($sub_cat_ads as $ad)

                                                @include('partials.ad.ad_card', ['ad' => $ad])

                                            @endforeach


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif

                @endforeach
            @endif
            <div class="container">
                <div class="ads-cat mt-4">


                    <!-- home page resposive ads -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9085617575772553"
                         data-ad-slot="8947623489"
                         data-ad-format="auto"></ins>
                    <script async>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                </div>
            </div>
        </section>


    @if(config("country.code") == "eg")
        <!-- Start videos section -->
            @if(count($featured_videos))
                <section id="videos" class="bg-white border-0 h-auto">
                    <div class="container">
                        <h5 class="px-2 mb-0 d-inline-block">
                            <a href="{{ url("videos/". $main_category->slug) }}"
                               style="{{ $main_category->color ? "color: $main_category->color !important;" : "" }}">{{ trans('category.latest_videos') }}</a>
                        </h5>
                        {{-- <button class="btn follow-up"><span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                             <span>{{ trans('category.follow') }}</span></button>--}}

                        <div class="owl-carousel owl-theme">

                            @foreach($featured_videos as $post)

                                <div class="item">
                                    <div class="card viedo m-0">
                                        <div class="icon"><span class="play-icon-video"><i class="fa fa-play"
                                                                                           aria-hidden="true"></i></span>
                                        </div>
                                        <a href="{{ $post->url }}">
                                            <img src="{{ $post->image ? thumbnail($post->image->path, 'small') : '' }}"
                                        </a>
                                        <div class="video-details d-none">
                                            <div class="title text-center p-1 border-bottom">
                                                <a href="{{ $main_category->url }}" class="color-white">

                                                    <span>
                                                        @if($main_category->id == 1)
                                                            <img src="{{ assets("frontend") }}/assets/img/shape-five.png"
                                                                 alt=""
                                                                 style="width: 33px !important;height: 27px !important;"
                                                                 class="img-fluid">
                                                        @elseif($main_category->id == 2)
                                                            <img src="{{ assets("frontend") }}/assets/img/shape-four.png"
                                                                 alt=""
                                                                 style="width: 33px !important;height: 27px !important;"
                                                                 class="img-fluid">
                                                        @elseif($main_category->id == 3)
                                                            <img src="{{ assets("frontend") }}/assets/img/shape-three.png"
                                                                 alt=""
                                                                 style="width: 33px !important;height: 27px !important;"
                                                                 class="img-fluid">
                                                        @elseif($main_category->id == 4)
                                                            <img src="{{ assets("frontend") }}/assets/img/shape-one.png"
                                                                 alt=""
                                                                 style="width: 33px !important;height: 27px !important;"
                                                                 class="img-fluid">
                                                        @elseif($main_category->id == 5)
                                                            <img src="{{ assets("frontend") }}/assets/img/shape-two.png"
                                                                 alt=""
                                                                 style="width: 33px !important;height: 27px !important;"
                                                                 class="img-fluid">
                                                        @endif
                                                    </span>

                                                    <span>{{ $main_category->custom_name }}</span>
                                                </a>
                                            </div>
                                            <div class="desc px-3 pt-2 pb-0 ">
                                                <a href="{{ $post->url }}"
                                                   class="color-white {{ dir_class($post->title) }}">
                                                    <span>{{ $post->title }}</span>
                                                </a>
                                            </div>
                                            <div class="info py-2 px-3">
                                                {{-- <div class="d-inline-block"><i class="fas fa-calendar-alt"></i> <span
                                                            class="date">{{ date("Y-m-d", strtotime($post->created_at)) }}</span>
                                                </div> --}}
                                                <div class="d-inline-block float-right"><i class="fas fa-eye"></i> <span
                                                            class="views">{{ $post->views }}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach


                        </div>
{{--                        <div class="ads-cat mt-4">--}}


{{--                            <!-- responsive ads replaced 728*90 -->--}}
{{--                            <ins class="adsbygoogle"--}}
{{--                                 style="display:block"--}}
{{--                                 data-ad-client="ca-pub-9085617575772553"--}}
{{--                                 data-ad-slot="5498890495"--}}
{{--                                 data-ad-format="auto"></ins>--}}
{{--                            <script async>--}}
{{--                                (adsbygoogle = window.adsbygoogle || []).push({});--}}
{{--                            </script>--}}

{{--                        </div>--}}
                    </div>
                </section>
            @endif
        <!-- End videos section -->
        @endif
    <!-- Start artical-barter section -->
        @if(count($featured_articles))
            <section id="artical-barter" class="bg-white border-0  h-auto">
                <div class="container">
                    <h5 class="px-2 mb-0 d-inline-block"><a href="{{ url("blog/". $main_category->slug) }}"
                                                            style="{{ $main_category->color ? "color: $main_category->color !important;" : "" }}">{{ trans('category.latest_articles') }}</a>
                    </h5>
                    {{-- <button class="btn follow-up"><span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                         <span>{{ trans('category.follow') }}</span></button>--}}

                    <div class="owl-carousel owl-theme">

                        @foreach($featured_articles as $post)

                            <div class="item">
                                <div class="card custom-box m-0">
                                    <div class="card-img-header">
                                        <a href="{{ $post->url }}">
                                            <img src="{{ $post->image ? thumbnail($post->image->path, 'medium') : '' }}"
                                                 alt="" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        {{-- <div class="date">
                                            <img src="{{ assets("frontend") }}/assets/img/date-2-icon.png" alt=""
                                                 class="icon">
                                            <bdi>{{ date("Y-m-d", strtotime($post->created_at)) }}</bdi>
                                        </div> --}}

                                        <div class="title">
                                            <a href="{{ $post->url }}" class="{{ dir_class($post->title) }}">
                                                {{ $post->title }}
                                            </a>
                                        </div>
                                        <div class="desc">
                                            <p {{ dir_class($post->excerpt) }}>{{ $post->excerpt }}</p>
                                        </div>
                                        <a href="{{ $post->url }}"
                                           class="read-more float-right">{{ trans('common.read_article') }}</a>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>

                    <div class="ads-cat mt-4">

                        <!-- responsive ads replaced 728*90 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-9085617575772553"
                             data-ad-slot="5498890495"
                             data-ad-format="auto"></ins>
                        <script async>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>

                    </div>
                </div>

            </section>
        @endif
    <!-- Start End-barter section -->

        @if(count($topics))
            <section id="discover">
                <div class="container">
                    <div class="title"
                         style="{{ $main_category->color ? "border-color: $main_category->color !important;" : "" }}">
                        <span class="text-center mb-0"
                              style="{{ $main_category->color ? "color: $main_category->color !important;" : "" }}">{{ trans('category.discover_with') }} {{ $main_category->custom_name }}</span>
                    </div>
                    <!-- Nav pills -->
                    <ul class="nav nav-pills mt-3 mb-4">

                        @foreach($topics as $topic)

                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ $loop->first ? 'active' : '' }}" data-toggle="pill"
                                   href="#topic_{{ $topic->id }}">{{ $topic->custom_name }}</a>
                            </li>

                        @endforeach

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($topics as $topic)

                            <div class="tab-pane container {{ $loop->first ? 'active' : '' }}"
                                 id="topic_{{ $topic->id }}">
                                <div class="row">
                                    <ul class="list-unstyled discover-list">

                                        @if(count($topic->tags))
                                            @foreach($topic->tags as $tag)

                                                <li class="col-md-3 col-sm-6">
                                                    <a href="{{ route('tag.index', ['slug' => $tag->slug]) }}">{{ $tag->name }}
                                                        <bdi>({{ \App\Models\Tag::indexed_ads_count($tag->id) }})</bdi>
                                                    </a>
                                                </li>

                                            @endforeach
                                        @endif

                                    </ul>

                                </div>
                            </div>

                        @endforeach


                    </div>
                </div>
            </section>
        @endif

    </section>
@stop
