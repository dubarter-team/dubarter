<!DOCTYPE html>
<html>
   <head>
      <title>dubarter</title>
      <!-- Site Charset -->
      <meta charset="utf-8">
      <!-- Mobile Meta -->
      <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">
      <head>
      <body>
          <table cellpadding="0" cellspacing="0" style="width:100%; max-width:600px; margin:0 auto; background-color:#65b7cc; padding:40px;">
            <tr>
               <td style="padding:0; text-align: center;">
                  <img style="display: block; margin: 0 auto;" src="{{ assets("frontend") }}/assets/images/mail-pw.jpg">
               </td>
             </tr>
              <tr>
                <td>
                  <table cellpadding="0" cellspacing="0" style="background-color:#113543; padding:40px; width: 100%;">
                      <tr><td style="text-align: center; padding-top: 20px;"><img src="{{ assets("frontend") }}/assets/img/logo.png"></td></tr>
                       <tr><td style="text-align: center; padding-top: 20px; padding-bottom: 20px; font-family: Tahoma; color:#fff;"> <span>{{ trans('auth.new_password_mail_message') }}</span></td></tr>
                       <tr><td style="text-align: center; padding: 7px 10px; background: #fff; font-family: Tahoma; font-size: 18px;">{{ $new_password }}</td></tr>
                  </table>
                </td>
              </tr>
            </table>
       </body>
</html>
