<!DOCTYPE html>
<html>
   <head>
      <title>dubarter</title>
      <!-- Site Charset -->
      <meta charset="utf-8">
      <!-- Mobile Meta -->
      <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">
      <head>
      <body>
          <table cellpadding="0" cellspacing="0" style="width:100%; background: #8cad10; padding: 10px; max-width:600px; margin:0 auto; ">
               <th style=" background: #8cad10; height: 330px; padding: 0 60px;">
                  <img src="{{ assets("frontend") }}/assets/images/mail-ac.jpg">
               </th>
                  <tr style="background: #fff;">
                  <td style="text-align: center; padding-top: 20px;"><img src="{{ assets("frontend") }}/assets/img/logo.png"></td></tr>
                   <tr style="background: #fff;"><td style="text-align: center; padding-top: 20px; font-family: Tahoma;"> <span>{{trans('auth.registeration_confirm_mail_hello')}}</span> <span style="color: #8cad10;text-decoration-line: underline;">{{ $user->username }}</span></td></tr>

                   <tr style="background: #fff;">
                     <td style="text-align: center; padding-top: 20px; padding-bottom:40px; font-family: Tahoma;">
                      <span>{{trans('auth.registeration_confirm_mail_message')}}</span>
                      <a href="{{ url("activation/". $user->code) }}" style="background: #8cad10; padding: 7px 10px; color: #fff; text-decoration: none; border: none; font-size: 18px;">{{trans('auth.registeration_confirm_mail_activate')}}</a>
                  </td></tr>
            </table>
       </body>
</html>
