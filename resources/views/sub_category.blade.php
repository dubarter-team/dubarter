@extends("layouts.master")
@push('meta')
    <title>{{ $category->page_title . ' - ' . \App\Models\Place::getCurrentCountry()->name . " - " .  trans('common.dubarter')  }}</title>
    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:type" content="website"/>
    <meta name="author" content="{{option('site_name')}}">
    <meta name="twitter:site" content="{{'@'.option('site_name')  }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="robots" content="index, follow">
    <meta name="keywords"
          content="{{ $category->key_words }}">
    <meta name="title" content="{{ $category->page_title . " - ". \App\Models\Place::getCurrentCountry()->name }}">
    <meta name="description" content="{{ $category->description }}"/>
    <meta property="og:title" content="{{ $category->page_title }}"/>
    <meta property="og:site_name" content="{{ option('site_name') }}"/>
    <meta property="og:url" content="{{ $category->url }}">
    <meta property="og:description" content="{{ $category->description}}">
    <meta property="og:image" content="{{ thumbnail($category->image->path, "medium") }}">
    <meta name="twitter:title" content="{{ $category->page_title }}">
    <meta name="twitter:description" content="{{ $category->description }}">
    <meta name="twitter:image" content="{{ thumbnail($category->image->path, "medium") }}">
    <meta name="twitter:url" content="{{ $category->url }}">
@endpush

@section("content")
    <h1 style="display: none;">{{ $category->h1 }}</h1>
    <h2 style="display: none;">{{ $category->h2 }}</h2>
    <section id="cats-page">
        <section class="category-search pb-0"
                 style="{{ $main_category->banner ? "background-image: url($main_category->banner_image);" : '' }}">
            <div class="container">
                <div class="m-auto search box-filter">

                    @include('partials.category.category_search_bar', ['cities' => $cities, 'main_category' => $main_category])

                </div>
                <div class="text-center font-weight-bold mt-2 mb-4">
                    <span> </span>
                </div>
                @include('partials.category.nav_cats', ['nav_cats' => $nav_categories, 'main_category' => $main_category, 'selected_cat_id' => isset($categories_list[1]) ? $categories_list[1]->id : 0])
            </div>
        </section>

        <section class="cats-filter">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">

                        @foreach($categories_list as $cat)
                            <li class="breadcrumb-item"><a href="{{ $cat->url }}">{{ $cat->name }}</a></li>
                        @endforeach
                    </ol>
                </nav>
                {{-- <div class="ads-cat my-3">
                     <!-- home page resposive ads -->
                     <ins class="adsbygoogle"
                          style="display:block"
                          data-ad-client="ca-pub-9085617575772553"
                          data-ad-slot="8947623489"
                          data-ad-format="auto"></ins>
                     <script>
                         (adsbygoogle = window.adsbygoogle || []).push({});
                     </script>
                 </div>--}}
                <div class="row cat-wrapper">
                    <div class="col-lg-3">
                        <div class="side-bar-filter">
                            @if(!isMobile())
                                @include("sub_category_sidebar")
                            @endif
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <div class="img-logo">
                            <img src="{{ $category->logo ? thumbnail($category->logo->path, 'small') : '' }}" alt="">
                            <span>{{ $category->name }}</span>
                        </div>

                        <div id="sub-category-ads-listing">

                            @if(count($sponsored_ads))
                                <div class="sponsored_ads">
                                    <div class="row">
                                        @foreach($sponsored_ads as $ad)
                                            <div class="@if(count($sponsored_ads) == 1) col-md-12 col-sm-12 @else col-md-6 col-sm-6 @endif pl-0">
                                                @include('partials.ad.ad_card', ['ad' => $ad])
                                            </div>

                                        @endforeach
                                    </div>
                                </div>
                            @endif

                            @if($objects_count)

                                {!! $objects_html !!}

                            @else
                                @if($category->listing_type == 'ads')
                                    <div>{{ trans('category.no_ads') }}</div>
                                @else
                                    <div>{{ trans('category.no_subs') }}</div>
                                @endif

                            @endif
                        </div>

                        @if($objects_count == $per_page)
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center my-3" id="sub-category-load-more">
                                        <a class="load-more"
                                           style="{{ $main_category->color ? "color:#fff;background: $main_category->color;" : "" }}">{{ trans('common.loadmore') }}</a>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(isMobile())
                            <div class="side-bar-filter">
                                @include("sub_category_sidebar")
                            </div>
                        @endif


                    </div>

                    <div class="col-lg-3 latest-sec">

                        @if(config("country.code") == "eg")
                            @include('partials.common.latest_videos_sidebar', ['latest_videos' => $latest_videos, 'main_category' => $main_category])
                        @endif
                        <div class="ad mb-3">
                            <!-- side bar 336X280 -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:336px;height:280px"
                                 data-ad-client="ca-pub-9085617575772553"
                                 data-ad-slot="6145269080"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>

                        @include('partials.common.latest_articles_sidebar', ['latest_articles' => $latest_articles, 'main_category' => $main_category])

                        <div class="ad mb-3">

                            <!-- side bar 336X280 -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:336px;height:280px"
                                 data-ad-client="ca-pub-9085617575772553"
                                 data-ad-slot="6145269080"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>

                        </div>
                    </div>

                </div>


            </div>
        </section>


    </section>

@stop


@push("head")

    <style>

        #sub-category-ads-listing .row {
            margin: 0 !important;
            padding: 0 !important;
        }

        .sponsored_ads {
            padding: 5px 0 5px 0 !important;
            background: {{ $main_category->color }};
            text-align: center;
            border-radius: 5px;
        }

        .sponsored_ads .info, .sponsored_ads .info * {
            float: right;
        }

        .sponsored_ads .location {
            margin: 10px 0;
            width: 100%;
        }

        .sponsored_ads .date {
            width: 100%;
            margin-bottom: 9px;
        }

        .sponsored_ads .icon-box {
            margin: 10px 0;
        }

        .sponsored_ads img {
            margin: 0 4px !important;
        }

        .sponsored_ads .card {
            margin: 0 5px;
            /*min-height: 370px;*/
            /*max-height: 370px;*/
            border: none;
        }

        .sponsored_ads .card-img-header img {
            margin: 0 !important;
        }

        .sponsored_ads .icon-box {
            background: unset !important;
        }

        .sponsored_ads * {
            padding: 0;
        }


    </style>

@endpush


@push("footer")

    <script>

        var sc_paginate_url = '{{ route('category.sub_category_paginate') . (count(Request::except('offset')) ? '?' . http_build_query(Request::except('offset')) : '') }}';

        var sc_offset = {{ $objects_count }};

        var category_id = {{ $category->id }};

        var per_page = {{ $per_page }};

        var all_ads = {{ $all_ads ? '1' : '0' }};

        var sc_flag = true;

        var sc_load_more_idle = true;

        $(document).ready(function () {

            $(document).on('click', '#sub-category-load-more a', function () {

                if (sc_flag && sc_load_more_idle) {

                    sc_load_more_idle = false;

                    $('#sub-category-load-more a').html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

                    $.ajax({
                        url: sc_paginate_url,
                        data: {offset: sc_offset, category_id: category_id, all_ads: all_ads},
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            $('#sub-category-ads-listing').append(res.objects_html);

                            sc_offset += res.objects_count;

                            item_details_height();

                            images_lazy_load();

                            if (res.objects_count < per_page) {
                                sc_flag = false;

                                $('#sub-category-load-more').remove();
                            }
                        },
                        complete: function () {
                            sc_load_more_idle = true;

                            $('#sub-category-load-more a').html('{{ trans("search.more_ads") }}');
                        }
                    });

                }

            });

        });

    </script>

    <script type="application/ld+json">

        {

         "@context": "http://schema.org",

         "@type": "BreadcrumbList",

         "itemListElement":

         [

        @foreach($categories_list as $cat)

            @php

                $list = [];

                $list['@type'] = "ListItem";

                $list['position'] = $loop->index + 1;

                $list["item"]['@id'] = $cat->url;

                $list["item"]['name'] = $cat->name->{ app()->getLocale() };

            @endphp

            {!! json_encode($list) . ($loop->last ? '' : ',') !!}

        @endforeach

        ]

       }



    </script>

@endpush
