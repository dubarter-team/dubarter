@extends("layouts.master")

@section("content")
    <div class="text-center">

        {{ trans('common.videos')}} {{ $category ? $category->name: "" }}

        <br/>

        {{ $videos->total() }}

    </div>
@stop
