@extends("layouts.master")
@section("content")
    <!-- End navbar section -->
    <section id="cats-page">
        <section class="category-search pb-0"
                 style="{{ $category->banner ? "background-image: url($category->banner_image);" : '' }}">
            <div class="container">
                <div class="m-auto search box-filter">

                    @include('partials.category.category_search_bar', ['cities' => $cities, 'main_category' => $category])

                </div>
                <div class="text-center font-weight-bold mt-2 mb-4">
                    <span> </span>
                </div>

                @include('partials.category.nav_cats', ['nav_cats' => $nav_categories, 'main_category' => $category, 'posts_type' => $posts_type])
            </div>
        </section>

        @if(count($featured_posts))
            <section class="slider category-main-off-mobile">
                <div class="container">
                    <div class="ads-cat mt-4">

                        <!-- home page resposive ads -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-9085617575772553"
                             data-ad-slot="8947623489"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>

                    </div>
                </div>

                <div class="container">
                    <h5 class="px-2 mb-0 d-inline-block text-auto-barter"
                        style="padding: 30px 0px 0px; font-weight: bold; {{ $category->color ? "color: $category->color !important;" : "" }}">{{ $format == 'video' ? trans('category.featured_videos') : trans('category.featured_articles') }}</h5>

                    <div id="jslidernews2" class="lof-slidecontent">
                        <div class="preload">
                            <div></div>
                        </div>
                        <!-- MAIN CONTENT -->
                        <div class="main-slider-content">
                            <ul class="sliders-wrap-inner">

                                @foreach($featured_posts as $post)

                                    <li>
                                        <a href="{{ $post->url }}">
                                            <img src="{{ $post->image ? thumbnail($post->image->path, 'large') : '' }}"
                                                 title="{{ $post->title }}">
                                            <div class="slider-description">
                                                <h4 class="{{ dir_class($post->title) }}">{{ $post->title }}</h4>
                                                <p class="{{ dir_class($post->excerpt) }}">{{ $post->excerpt }}</p>
                                            </div>
                                        </a>
                                    </li>

                                @endforeach

                            </ul>
                        </div>
                        <!-- END MAIN CONTENT -->
                        <!-- NAVIGATOR -->
                        <div class="navigator-content">
                            <div class="navigator-wrapper">
                                <ul class="navigator-wrap-inner">

                                    @foreach($featured_posts as $post)

                                        <li>
                                            <div>
                                                <div class="cat-slider-thumb">
                                                    <img src="{{ $post->image ? thumbnail($post->image->path, "thumbnail") : '' }}"/>
                                                </div>
                                                <h5 class="{{ dir_class($post->title) }}">{{ $post->title }}</h5>
                                                <p class="{{ dir_class($post->excerpt) }}">{{ str_limit($post->excerpt, 80) }}</p>
                                            </div>
                                        </li>


                                    @endforeach

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="slider show-on-mobile-only-new">
                <div class="container">
                    <div class="category-slider ad-related-ads">
                        <div class="owl-carousel owl-theme">

                            @foreach($featured_posts as $post)
                                <div class="item">
                                    <div class="card custom-box">
                                        <div class="card-img-header">
                                            <a href="{{ $post->url }}">
                                                <img src="{{ $post->image ? thumbnail($post->image->path, 'large') : '' }}"
                                                     title="{{ $post->title }}" alt=""
                                                     class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="card-body"
                                             style="{{ $category->color ? "border-color: $category->color;" : "" }}">
                                             {{-- No date here --}}
                                            <div class="title">
                                                <h4 class="{{ dir_class($post->title) }}">
                                                  <a href="{{ $post->url }}">{{ $post->title }}</a>
                                                </h4>
                                            </div>
                                            <div class="desc">
                                                <p class="{{ dir_class($post->excerpt) }}">{{ $post->excerpt }}</p>
                                            </div>
                                            <a href="{{ $post->url }}" class="read-more float-right"
                                               style="{{ $category->color ? "background-color: $category->color;" : "" }}">
                                                @if($post->format == 'article')
                                                    {{ trans('common.read_article') }}
                                                @elseif($post->format == 'video')
                                                    {{ trans('common.watch_video') }}
                                                @endif
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </section>
    @endif

    <!-- Start videos section -->

        <section id="artical-barter" class="bg-white border-0 h-auto">
            <div class="container">
                <h6 class="px-2 mb-0 py-4 d-inline-block text-auto-barter font-weight-bold"
                    style="{{ $category->color ? "color: $category->color !important;" : "" }}">{{ $format == 'article' ? trans('category.latest_articles') : trans('category.latest_videos') }}</h6>
                {{-- <button class="btn follow-up"><span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                     <span>{{ trans('category.follow') }}</span></button>--}}
                <div class="row">
                    <div id="articles-listing">
                        @if($posts_count)
                            {!! $posts_html !!}
                        @else
                            @if($format == 'article')

                                <div class="col-12">
                                    <div class="text-center">{{ trans('common.no_articles') }}</div>
                                </div>
                            @else
                                <div class="col-12">
                                    <div class="text-center">{{ trans('common.no_videos') }}</div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                @if($posts_count == $per_page)
                    <div class="row">
                        <div class="col-12">
                            <div class="float-right my-3" id="articles-load-more">
                                <a class="load-more"
                                   style="{{ $category->color ? "background: $category->color !important;" : "" }}"><span>{{ trans('search.more_ads') }} </span></a>
                            </div>
                        </div>
                    </div>

                @endif
            </div>


            <div class="ads-cat my-3">


                <!-- home page resposive ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-9085617575772553"
                     data-ad-slot="8947623489"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

            </div>
        </section>

    </section>
@stop


@push("footer")

    <script>

        var posts_paginate_url = '{{ route('posts.paginate') }}';

        var posts_offset = {{ $posts_count }};

        var category_id = {{ $category->id }};

        var per_page = {{ $per_page }};

        var format = '{{ $format }}';

        var featured_ids = [{{ implode(',', $featured_posts->pluck('id')->toArray()) }}];

        var posts_flag = true;

        var posts_load_more_idle = true;

        $(document).ready(function () {

            $(document).on('click', '#articles-load-more a', function () {

                if (posts_flag && posts_load_more_idle) {

                    posts_load_more_idle = false;

                    $('#articles-load-more a').html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

                    $.ajax({
                        url: posts_paginate_url,
                        data: {
                            format: format,
                            offset: posts_offset,
                            category_id: category_id,
                            featured_ids: featured_ids
                        },
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            $('#articles-listing').append(res.posts_html);

                            posts_offset += res.posts_count;

                            if (res.posts_count < per_page) {
                                posts_flag = false;

                                $('#articles-load-more').remove();
                            }
                        },
                        complete: function () {
                            posts_load_more_idle = true;

                            $('#articles-load-more a').html('{{ trans("search.more_ads") }}');
                        }
                    });

                }

            });

        });

    </script>

@endpush
