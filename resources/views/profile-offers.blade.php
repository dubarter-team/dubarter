@extends("layouts.master")

@section("content")
    <section id="my-account-page">
        <section class="account-barter-search my-account-ads pb-0 category-search">
            <div class="container">
                <div class="m-auto search w-75">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="filter">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item">
                                            <h5><i class="my-account-ico my-account-offer-ico"></i>{{trans('profile.offers')}}  </h5>
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="quick-search mt-4">
                    <div class="row">
                        <div class="col p-0 text-center border-left">
                            <a href="{{ route('profile.edit.form') }}" class="item">{{trans('profile.my-account')}}</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="{{ route('profile.ads.index') }}" class="item">{{trans('profile.my-ads')}}</a>
                        </div>
                        <div class="col p-0 text-center  border-left">
                            <a href="{{ route('profile.offers.index') }}" class="active item">{{trans('profile.offers')}}   </a>
                        </div>
                        <div class="col p-0 text-center">
                            <a href="{{route('profile.ads.favourites')}}" class="item">{{trans('profile.favorites')}}  </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="result cats-filter my-account-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="mb-2"><i class="my-account-ico user-info-ico"></i>{{trans('profile.offers')}} </h5>
                    </div>

                    <div class="col-md-12">
                        <div class="my-account-table my-4">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">{{trans('profile.offer-name')}}</th>
                                        <th scope="col">{{trans('profile.price')}}</th>
                                        <th scope="col">{{trans('profile.history')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody id="search-ads-listing">
                                    @if($offers_count)
                                        @include('partials.offers.offers',['offers'=>$offers])
                                    @else
                                        <tr>
                                            <td colspan="4">
                                                <div class="no-content">{{ trans('category.not_found') }}</div>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                </table>
                            </div>
                        </div>
                    </div>


                    @if($offers_count == $per_page)
                        <div class="load-more-btn" id="search-load-more-ads">
                            <a class="btn btn-defult green-btn text-white"
                               @if($offers_count == 0) style="display: none" @endif>{{ trans('search.more_ads') }}</a>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="ads-cat mb-3">
                            <img src="{{ assets("frontend") }}/assets/img/ads1.png" alt=""
                                 class="d-block img-fluid m-auto">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>


@stop


@push("footer")

    <script>

        var offset = {{ $offset }};

        $(document).ready(function () {

            $(document).on('click', '#search-load-more-ads a', function () {


                $('#search-load-more-ads a').html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');

                $.ajax({
                    url: '{{route('profile.offers.index')}}',
                    data: {offset: offset},
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        $('#search-ads-listing').append(res.offers_html);

                        offset += res.offers_count;

                        if (res.offers_count < {{$per_page}}) {

                            $('#search-load-more-ads').remove();
                        }
                    },
                    complete: function () {
                        $('#search-load-more-ads a').html('{{ trans("search.more_ads") }}');
                    }
                });

            });

        });

    </script>

@endpush
