<h5 style="{{ $main_category->color ? "color:#fff;background: $main_category->color;" : "" }}">
                                <span class="car-icon">
                                    @if($main_category->id == 1)
                                        <img src="{{ assets("frontend") }}/assets/img/shape-five.png" alt=""
                                             class="img-fluid">
                                    @elseif($main_category->id == 2)
                                        <img src="{{ assets("frontend") }}/assets/img/shape-four.png" alt=""
                                             class="img-fluid">
                                    @elseif($main_category->id == 3)
                                        <img src="{{ assets("frontend") }}/assets/img/shape-three.png" alt=""
                                             class="img-fluid">
                                    @elseif($main_category->id == 4)
                                        <img src="{{ assets("frontend") }}/assets/img/shape-one.png" alt=""
                                             class="img-fluid">
                                    @elseif($main_category->id == 5)
                                        <img src="{{ assets("frontend") }}/assets/img/shape-two.png" alt=""
                                             class="img-fluid">
                                    @endif
                                </span>{{ $main_category->name }}</h5>

<div class="card">
    <div class="card-body">

        @if(count($main_sub_categories))

            @foreach($main_sub_categories as $m_sub)


                @if(!in_array($m_sub->id, [3, 6]) or config("country.code") == "eg")

                    <div class="filter-type mb-2">
                        <a class="collapsed-btn" data-toggle="collapse"
                           href="#msc-{{ $m_sub->id }}"
                           role="button"
                           aria-expanded="{{ (isset($categories_list[1]) && $categories_list[1]->id == $m_sub->id) ? 'false' : 'false' }}"
                           aria-controls="msc-{{ $m_sub->id }}">{{ $m_sub->custom_name }}<span
                                    class="count"> ( {{ $m_sub->ads_count }} )</span>
                            <span class="icons float-right">
                                                            <i class="far fa-plus-square"></i>
                                                            <i class="far fa-minus-square"></i>
                                                        </span>
                        </a>
                        <div class="collapse {{  (isset($categories_list[1]) && $categories_list[1]->id == $m_sub->id)  ? '_show' : '' }}"
                             id="msc-{{ $m_sub->id }}">

                            <ul class="list-unstyled p-0 mt-2">

                                <li>
                                    <a href="{{ $m_sub->all_url }}">
                                                                    <span class="angle-left-icon"><i
                                                                                class="fas fa-angle-{{ app()->getLocale() == 'ar' ? 'left' : 'right' }}"></i></span>{{ trans('category.all_link') }}
                                        <span class="count"> ( {{ $m_sub->ads_count }}
                                                                    )</span>
                                    </a>
                                </li>

                                @if(count($m_sub->categories))
                                    @foreach($m_sub->categories as $mss)

                                        <li class="{{ ($category->id ==  $mss->id) ? 'active' : '' }}">
                                            <a href="{{ $mss->url }}">
                                                                        <span class="angle-left-icon"><i
                                                                                    class="fas fa-angle-{{ app()->getLocale() == 'ar' ? 'left' : 'right' }}"></i></span>{{ $mss->custom_name }}
                                                <span class="count"> ( {{ $mss->ads_count }}
                                                                            )</span>
                                            </a>
                                        </li>

                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                @endif
            @endforeach
        @endif
    </div>
</div>
<style>
    .side-bar-filter .collapse a:hover, .side-bar-filter .collapse .active a {
        color: {{ $main_category->color}}   !important;
    }
</style>
