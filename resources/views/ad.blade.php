@extends("layouts.master")

@push('meta')
    <title>{{ $ad->title . ' - ' . trans('common.dubarter') }}</title>

    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:type"
          content="{{ $ad->check_category(\App\Models\Category::FOODBARTER) ? 'restaurant.restaurant' : 'product' }}"/>
    <meta name="author" content="{{option('site_name')}}">
    <meta name="twitter:site" content='@dubarter'>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="robots" content="index, follow">

    @if($seo && !empty($seo))
        <meta name="keywords"
              content="{{ !empty($seo->meta_keywords) ? $seo->meta_keywords : (count($ad->tags) ? implode(',', collect($ad->tags)->pluck(['name'])->toArray()) : '') }}">
        <meta name="title" content="{{ !empty($seo->meta_title) ? $seo->meta_title : $ad->title }} {{ $ad->_id }}">
        <meta name="description"
              content="{{ !empty($seo->meta_description) ? $seo->meta_description : $ad->seo_excerpt }}"/>
        <meta property="og:title"
              content="{{ !empty($seo->facebook_title) ? $seo->facebook_title: (!empty($seo->meta_title) ?$seo->meta_title : $ad->title) }}"/>
        <meta property="og:site_name" content="{{option('site_name')}}"/>
        <meta property="og:url" content="{{$ad->url}}">
        <meta property="og:description"
              content="{{ !empty($seo->facebook_description) ? $seo->facebook_description : (!empty($seo->meta_description) ? $seo->meta_description : $ad->seo_excerpt)}}">
        <meta property="og:image"
              content="{{ ($seo->facebook?uploads_url($seo->facebook->path) : thumbnail($ad->main_image, "medium"))  }}">
        <meta name="twitter:title" content="{{$seo->twitter_title  ? $seo->twitter_title : $seo->meta_title }}">
        <meta name="twitter:description"
              content="{{ !empty($seo->twitter_description) ? $seo->twitter_description : $ad->seo_excerpt}}">
        <meta name="twitter:image"
              content="{{ ($seo->twitter?uploads_url($seo->twitter->path) : thumbnail($ad->main_image, "medium")) }}">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:url" content="{{$ad->url}}">
    @else
        <meta name="keywords"
              content="{{ count($ad->tags) ? implode(',', collect($ad->tags)->pluck(['name'])->toArray()) : '' }}">
        <meta name="title" content="{{$ad->title}} {{ $ad->_id }}">
        <meta name="description" content="{{ str_limit(strip_tags($ad->seo_excerpt), 150) }}"/>
        <meta property="og:title" content="{{ $ad->title }}"/>
        <meta property="og:site_name" content="{{option('site_name')}}"/>
        <meta property="og:url" content="{{$ad->url}}">
        <meta property="og:description" content="{{ $ad->seo_excerpt}}">
        <meta property="og:image" content="{{thumbnail($ad->main_image, "medium")}}">
        <meta name="twitter:title" content="{{$ad->title }}">
        <meta name="twitter:description" content="{{ $ad->seo_excerpt}}">
        <meta name="twitter:image" content="{{thumbnail($ad->main_image, "medium")}}">
        <meta name="twitter:url" content="{{$ad->url}}">
    @endif
@endpush


@section("content")
    @php
        //dd($ad->files);
    @endphp
    <script type="application/ld+json">

        {

         "@context": "http://schema.org",

         "@type": "BreadcrumbList",

         "itemListElement":

         [
         <?php  $prevSlug = '/';$lastIndex = 0; ?>

        @foreach($ad->categories as $cat)
            @php

                $prevSlug.=$cat["slug"][app()->getLocale()].'/';

                $list = [];

                $list['@type'] = "ListItem";

                $list['position'] = $lastIndex=$loop->index + 1;

                $list["item"]['@id'] = url($prevSlug);

                $list["item"]['name'] = $cat['name'][app()->getLocale()];

            @endphp

            {!! json_encode($list) . ',' !!}

        @endforeach

        @php
            $list = [];
          $list['@type'] = "ListItem";

          $list['position'] = $lastIndex+1;

          $list["item"]['@id'] = $ad->url;

          $list["item"]['name'] = $ad->title;
        @endphp
        {!! json_encode($list)  !!}

        ]

       }










    </script>
    <h1 style="display: none;">{{ $ad->title }}</h1>
    <h2 style="display: none;">{{ str_limit($ad->seo_excerpt, 70) }}</h2>

    <section id="cats-page">
        <section class="category-search pb-0"
                 style="{{ $main_category->banner ? "background-image: url($main_category->banner_image);" : '' }}">
            <div class="container">
                <div class="m-auto search box-filter">

                    @include('partials.category.category_search_bar', ['cities' => $cities, 'main_category' => $main_category])

                </div>
                <div class="text-center font-weight-bold mt-2 mb-4">
                    <span> </span>
                </div>

                @include('partials.category.nav_cats', ['nav_cats' => $nav_categories, 'main_category' => $main_category])
            </div>
        </section>

        <section class="slider">
            <div class="container">

                <div class="ads-cat mt-4">

                    <!-- home page resposive ads -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9085617575772553"
                         data-ad-slot="8947623489"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                </div>
            </div>

            <div class="container">

                @if($ad->is_expired && !$ad->check_category(\App\Models\Category::FOODBARTER))
                    <div class="expired-ad">
                        <div class="expired-ad-icon"><img src="{{ assets("frontend") }}/assets/images/expired.png"/>
                        </div>
                        <div class="expired-ad-text">
                            <h4>{{trans('ad.this_ad_is_expired')}}</h4>
                            <h4>{{trans('ad.you_can_see_related_ads_from')}}
                                <a class="expired-ad-link">{{trans('ad.here')}}</a></h4>
                        </div>
                    </div>
                @endif

                <br/>

                <div class="row">
                    <div class="col-sm-8 slide-cont">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
								<?php $pervSlug = '/'; ?>
                                @foreach($ad->categories as $cat)
									<?php $pervSlug .= $cat["slug"][app()->getLocale()] . '/'; ?>
                                    <li class="breadcrumb-item"><a
                                                href="{{ url($pervSlug) }}">{{ $cat['name'][app()->getLocale()] }}</a>
                                    </li>
                                @endforeach
                            </ol>
                        </nav>
                        <h3 class="{{ dir_class($ad->title) }}"> {{ $ad->title }} </h3>
                        @if($ad->price)
                            <div class="box-salary price-mobile" style="color: {{ $main_category->color }}">
                                {{ (collect($ad->categories)->where('id', 6)->count() ? trans('ad.price_starts_from') : '') . number_format($ad->price) }} {{ $ad->currency }}

                            </div>
                        @endif
                        <div class="slide-info">
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span>
                                        {{ $ad->country_name }}
                                        {{ $ad->city_name ? "/". $ad->city_name : "" }}
                                        {{ $ad->region_name ? "/". $ad->region_name : "" }}
                                    </span>
                                </li>
                                @if($ad->check_category(\App\Models\Category::FOODBARTER))
                                    <li>
                                        <a href="tel:+{{ $ad->phone }}"><span>{{ $ad->phone }}</span></a>
                                        <img src="{{ assets("frontend") }}/assets/img/delivery-icon.png" alt=""/>
                                    </li>
                                @else
                                    {{-- <li>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <span>{{ $ad->date }}</span>
                                    </li> --}}
                                @endif
                            </ul>
                        </div>

                        @if(!$ad->check_category(\App\Models\Category::FOODBARTER))
                            @if(count($ad->images))
                                <div class="image-slider mb-4">
                                    <div class="product-showcase">

                                        @foreach($ad->files as $file)
                                            @php
                                                //dd($ad->images);
                                                $ext = substr($file['path'], -3)
                                            @endphp
                                            @if($ext && $ext == 'mp4')
                                                <video width="100%" height="400px" style="margin: 15px 0" controls>
                                                    <source src="https://dubarter.s3-eu-west-1.amazonaws.com/{{$file['path']}}"
                                                            type="video/mp4">
                                                    Your browser does not support the video tag.
                                                </video>
                                            @else
                                                <div class="ps-icon"
                                                     data-icon="{{ thumbnail($file['path'], "large")  }}"
                                                     data-image="{{ thumbnail($file['path'], "large")  }}"
                                                     data-button-link="#"
                                                     data-align="right"
                                                     @if ($loop->first) data-selected="true"
                                                     @endif data-text-highlight="true">
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @else
                            @if(count($ad->files))
                                <div class="ad-featuered-img">
                                    <img src="{{ thumbnail($ad->files[0]['path'], "large")  }}"/>
                                </div>
                            @endif
                        @endif

                        <div class="about-slide">
                            @if($ad->single_attributes->slice(0, 4)->count())
                                <ul class="mb-4">
                                    @foreach($ad->single_attributes->slice(0, 4) as $attribute)
                                        <li>
                                            <img src="{{ $attribute->small_icon_url }}"
                                                 title="{{ $attribute->attribute_title }}">
                                            <span> {{ $attribute->value_title }} </span>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif

                            @if($ad->single_attributes->slice(4)->count())
                                <div class="table-about">
                                    <table class="table table-light table-responsive">
                                        @foreach($ad->single_attributes->slice(4) as $attribute)
                                            <tr>
                                                <td class="" width="50%">{{ $attribute->attribute_title }}</td>
                                                <td>{{ $attribute->value_title }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @endif

                            @if(count($ad->multiple_attributes))
                                <div class="table-about">
                                    <table class="table table-light table-responsive">
                                        @foreach($ad->multiple_attributes as $attribute)
                                            <tr>
                                                <td class="" width="50%">{{ $attribute->attribute_title }}</td>
                                                <td>{{ implode(' - ', $attribute->value_title) }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @endif

                        </div>

                        @if($ad->content)
                            <h5> {{trans('ad.other_details')}} </h5>
                            <p class="{{ dir_class($ad->content) }}">
                                {!! $ad->content !!}
                            </p>
                        @endif


                        @if($ad->check_category(\App\Models\Category::FOODBARTER))
                            @if(count($ad->images))
                                <h4>{{trans('ad.restaurant_menu')}}</h4>
                                <ul class="ad-gallery">
                                    @foreach($ad->images as $file)
                                        <li class="col-md-3 col-sm-4 col-xs-6">
                                            <a href="{{ uploads_url($file['path'])}}" data-lightbox="example-set"><img
                                                        src="{{ thumbnail($file['path'], "medium")  }}"/></a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        @endif
                        @if(!$ad->check_category(\App\Models\Category::FOODBARTER) && !$ad->check_category(\App\Models\Category::NEW_CARS))
                            <div class="user-info p-3 price-mobile">


                                @if($ad_user)

                                    <img src="{{ $ad_user->photo ? thumbnail($ad_user->photo->path) : assets('frontend') . '/assets/img/userprofile.png' }}"
                                         alt=""
                                         class="rounded-circle p-4">

                                    <h5> {{ $ad_user->display_name }} </h5>
                                    <br/>
                                @endif
                                {{--<a href="" class="more-ads"> مشاهدة كل الاعلانات </a>--}}
                                {{--<ul class="d-flex b-button">--}}
                                {{--<li class="d-inline pt-2 pl-2 pb-2 "><a href="" class="btn btn-light">تفاوض على--}}
                                {{--السعر</a></li>--}}
                                {{--<li class="d-inline  pt-2 pr-2 pb-2"><a href="" class="btn btn-light">ارسال رساله</a>--}}
                                {{--</li>--}}
                                {{--</ul>--}}

                                @if($ad->phone)
                                    <div class="phone-info">
                                        <div class="show-phone">
                                            {{-- <span class="phone-title">إظهار الرقم</span>--}}
                                            <a href="tel:+{{ $ad->phone }}"><span
                                                        class="phone-nu">{{ $ad->phone }}</span></a>
                                        </div>
                                        <div class="phone-icon">
                                            <img src="{{ assets('frontend')}}/assets/img/phone-icon.png">
                                        </div>
                                    </div>
                                @endif
                            </div>

                        @endif
                        @include('partials.ad.branches', ['ad' => $ad])

                        <div class="share-box">
                            <span>
                                {{trans('ad.share_on')}}
                            </span>
                            <ul>
                                <li>
                                    <a class="color-facebook shareBtn facebook" href="{{ $ad->url }}">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-twitter shareBtn twitter" href="{{ $ad->url }}">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="color-google shareBtn google" href="{{ $ad->url }}">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>

                                <li>
                                    <a class="color-pinterest shareBtn pinterest" href="{{ $ad->url }}">
                                        <i class="fab fa-pinterest-p"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        @if(count($ad->tags))
                            <h4>{{trans('ad.related_links')}}</h4>
                            <ul class="add-tags">
                                @foreach($ad->tags as $tag)
                                    <li>
                                        <a href="{{ route('tag.index', ['slug' => $tag['slug']]) }}">{{ $tag['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif

                        @if(count($related_ads))
                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- responsive ads replaced 728*90 -->
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-9085617575772553"
                                 data-ad-slot="5498890495"
                                 data-ad-format="auto"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                            <div id="related-ads-item">
                                <h4>{{trans('ad.similar_ads')}}</h4>

                                <div id="item-page">
                                    <div class="ad-related-ads">
                                        <div class="owl-carousel owl-theme">
                                            @foreach($related_ads as $ad1)

                                                @include('partials.ad.ad_card', ['ad' => $ad1])

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- responsive ads replaced 728*90 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-9085617575772553"
                             data-ad-slot="5498890495"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>

                        @if(!$ad->check_category(\App\Models\Category::FOODBARTER) && !$ad->is_expired)

                            @if(Auth::guard('frontend')->check())
                                @if($isMyOwnAd)
                                    @include('partials.offers.my-offers',['offers'=>$offers,'ad_id'=>$ad->_id, 'mine' => true])
                                @else
                                    @include('partials.offers.ad-offers',['offer'=>$offer,'ad'=>$ad])
                                @endif
                            @endif

                        @endif

                    </div>

                    <div class="col-sm-4">

                        @if($ad->price)
                            <div class="box-salary" style="color: {{ $main_category->color }}">
                                {{ (collect($ad->categories)->where('id', 6)->count() ? trans('ad.price_starts_from') : '') . number_format($ad->price) }} {{ $ad->currency }}

                            </div>
                        @endif

                        @if(!$ad->check_category(\App\Models\Category::FOODBARTER) && !$ad->check_category(\App\Models\Category::NEW_CARS))
                            <div class="user-info p-3">


                                @if($ad_user)

                                    <img src="{{ $ad_user->photo ? thumbnail($ad_user->photo->path) : assets('frontend') . '/assets/img/userprofile.png' }}"
                                         alt=""
                                         class="rounded-circle p-4">

                                    <h5> {{ $ad_user->display_name }} </h5>
                                    <br/>
                                @endif
                                {{--<a href="" class="more-ads"> مشاهدة كل الاعلانات </a>--}}
                                {{--<ul class="d-flex b-button">--}}
                                {{--<li class="d-inline pt-2 pl-2 pb-2 "><a href="" class="btn btn-light">تفاوض على--}}
                                {{--السعر</a></li>--}}
                                {{--<li class="d-inline  pt-2 pr-2 pb-2"><a href="" class="btn btn-light">ارسال رساله</a>--}}
                                {{--</li>--}}
                                {{--</ul>--}}

                                @if($ad->phone)
                                    <div class="phone-info">
                                        <div class="show-phone">
                                            {{-- <span class="phone-title">إظهار الرقم</span>--}}
                                            <a href="tel:+{{ $ad->phone }}"><span
                                                        class="phone-nu">{{ $ad->phone }}</span></a>
                                        </div>
                                        <div class="phone-icon">
                                            <img src="{{ assets('frontend')}}/assets/img/phone-icon.png">
                                        </div>
                                    </div>
                                @endif
                            </div>

                        @endif


                        @if(config("country.code") == "eg")
                            @include('partials.common.latest_videos_sidebar', ['latest_videos' => $related_videos, 'main_category' => $main_category])
                        @endif

                        @include('partials.common.latest_articles_sidebar', ['latest_articles' => $related_articles, 'main_category' => $main_category])

                    </div>
                </div>

            </div>
        </section>


    </section>


@stop


@push('meta')



@endpush


@push("head")
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/product-showcase.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/edit-css.css") }}">
    <style>.ps-icon.selected {
            border-color: {{ $main_category->color }};
        }</style>
@endpush

@push("footer")



    <script src="{{ asset("frontend/assets/js/product-showcase.js") }}"></script>
    <script src="{{ assets("frontend/assets/js/lightbox.min.js") }}"></script>

    <script>
        $(document).ready(function () {
            @if(count($ad->images) && (!$ad->check_category(\App\Models\Category::FOODBARTER)))
            $(".product-showcase").productShowcase({

                // max height of the product viewer
                maxHeight: "550px",

                // width of the product viewer
                width: "100%",

                // size of the thumbnails
                iconSize: "90px"

            });
            @endif

        });
    </script>

@endpush
