<?php

return [
    'search_for' => 'Search for',
    'city' => 'City',
    'search' => 'Search',
    // main slider
    'videos_and_articles' => 'Video and articles',
    'sell_your_car' => 'Sell your car',
    'buy_a_car' => 'Buy a car',

    //cars section in home page
    'cars_title' => 'Car title',
    'cars_type' => 'Type',
    'cars_model' => 'Model',
    'used_cars' => 'Used cars',
    'new_cars' => 'New cars',
    //realestate section in home
    'realestate_title' => 'Realestate_ title',
    'realestate_all_purchase_methods' => 'Sale / Rent',
    'realestate_type' => 'Real estates Type',
    'realestate_featured_ads_title' => 'Real estate featured',
    //lifestyle section in home
    'style_title' => 'Lifestyle',
    'style_category' => 'Category',
    'style_shop_here' => 'shop here',
    'style_best_offers' => 'Best offers',
    'food_title' => 'Restaurants',


    // mobile
    'mobile_brand' => 'model',
    'all_cars' => 'All',
    'other_cars' => 'Other cars',
    'all_realestate' => 'All',
    'apartment_realestate' => 'Apartments',
    'villa_realestate' => 'Villas',
    'land_realestate' => 'Lands',
    'all-videos' => 'All',
    'cars-videos' => 'Cars',
    'food-videos' => 'Restaurants',
    'style-videos' => 'Life style',
    'all_mob' => 'All',
    'mob_mobiles' => 'Mobiles',
    'mob_electric' => 'Electrical devices',
    'mob_jobs' => 'Careers',
    'all-articles' => 'All',
    'cars-articles' => 'Cars',
    'food-articles' => 'Food',
    'style-articles' => 'Life style',
    'articles_title' => 'Most read articles',
    'videos_title' => 'Most viewed videos',
    'rests' => 'Restaurants',
    'today-rest' => 'Today\'s Restaurant',
    'rests-link' => 'More restaurants',
    'ls_fashion' => 'Fashion',
    'ls_beauty' => 'Beauty',
    'ls_decoration' => 'Home Decoration',
    'all_ls' => 'All',
    'meta-title' => 'Free classified ads in Egypt, Saudi Arabia and the UAE',
    'meta-description' => 'Free Classifieds Ads in Egypt, Saudi Arabia and United Arab Emirates Ali Dubartar The largest free classified ad site in the Arab World in all fields and for the first time Buy from the seller directly without an agent in the largest site Classifieds  Jobs, Real Estate, Restaurants and Used Cars Browse',


    'dubarter_videos'=>'Dubarter videos',
    'sell_your_property' => 'Sell your property',
    'rent_your_property' => 'Rent your property',
    'search_jobs' => 'Search jobs',
    'sell_your_mobile' => 'Sell your mobile',
    'dubarter_articles' => 'Dubarter articles',
    'cars' => 'Cars',
    'classifieds' => 'Classifieds',
];


