<?php

return [
    'all_categories' => 'All',
    'category' => 'Category',
    'all_attributes' => 'all',
    'search_for' => 'Search for',
    'city' => 'City',
    'order_by' => 'Order by',
    'per_page' => 'per page',
    'latest' => 'Latest',
    'oldest' => 'Oldest',
    'highest_price' => 'Highest price',
    'lowest_price' => 'Lowest price',
    'search_results_for' => 'Search results for',
    'no_ads' => 'No ads',
    'more_ads' => 'Load More ',
    'pages' => 'Pages',
    'ads' => 'Ads',
    'search' => 'Search',

];



