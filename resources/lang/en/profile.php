<?php


return [

    'favorites' => 'Favorites',
    'my-account'=>'My account',
    'my-ads'=>'My ads',
    'offers'=>'Offers',
    'my-favorites-ads'=>'My favorites ads',
    'ad-name'=>'Ad title',
    'history'=>'Date',
    'status'=>'Status',
    'edit'=>'Edit',
    'sold'=>'Sold',
    'offer-name'=>'Offer',
    'price'=>'Price',
	'preview' => 'preview ad',
	'no' => 'No'

];
