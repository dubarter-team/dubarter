<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'activation_mail_subject' => 'Activate your account on the dubarter',
    'account_created' => 'You have successfully registered and will receive an email to activate your account',
    'new_pass_mail_subject' => 'New password',
    'new_password_sent' => 'A new password has been sent to your email address',
    'invalid_username_password' => 'The login data is incorrect',
    'login' => 'Login',
    'email' => 'Email',
    'email_placeholder' => 'Email',
    'email_required' => 'Email field required',
    'send_new_pass' => 'Send new password',
    'username' => 'Username',
    'username_placeholder' => 'Username',
    'username_required' => 'user required',
    'password' => 'Password',
    'password_required' => 'Password',
    'repeat_password' => 'Repeat password',
    'login_button' => 'Login',
    'forget_password' => 'Forget password',
    'login_with' => 'Login with',
    'twitter' => 'twitter',
    'facebook' => 'facebook',
    'register_link' => 'Are you a new member? You can register from :link_tag here :link_end',
    'remember_password' => 'Remember password',
    'create_new_account' => 'create new Account',
    'terms_required' => 'You must agree to the terms and conditions',
    'create_account_button' => 'Create account',
    'you_agree' => 'By clicking on Create Account you agree to our',
    'terms_cond' => 'Terms and Conditions',
    'and_also' => 'And also on',
    'privacy' => ' Privacy policy',
    'account_activated_message' => 'Your account has been activated on dubarter',
    'registeration_confirm_mail_hello' => 'Hello',
    'registeration_confirm_mail_message' => 'You have been subscribed to dubarter',
    'registeration_confirm_mail_steps' => 'To complete the activation steps please click on the following link',
    'registeration_confirm_mail_activate' => 'Activate',
    'new_password_mail_message' => 'The password has been changed and this is the new password',
    'image' => 'Photo',
	'code' => 'Verification code',
	'repassword' => 'Password confirmation',
	'verification_title' => 'Verification process',
	'save' => 'Save',
	'incorrent_code' => 'Incorrent verification code',
	'verification_sent' => 'Verification code was sent to you mail. please check it.',
	'your_code_is' => 'Your verification code is',
	'hi' => 'Welcome',
	'dubarter_thanks' => 'Thank you for using Dubarter',

];
