<?php

return [
    'search_for' => 'أبحث عن',
    'city' => 'المدينة',
    'search' => 'بحث',
    // main slider
    'videos_and_articles' => 'فيديوهات و مقالات',
    'sell_your_car' => 'بيع سيارتك',
    'buy_a_car' => 'إشترى سيارة',

    //cars section in home page
    'cars_title' => 'سيارات',
    'cars_type' => 'النوع',
    'cars_model' => 'الموديل',
    'used_cars' => 'سيارات مستعملة',
    'new_cars' => 'سيارات جديدة',
    //realestate section in home
    'realestate_title' => 'عقارات',
    'realestate_all_purchase_methods' => 'بيع / إيجار',
    'realestate_type' => 'نوع العقار',
    'realestate_featured_ads_title' => 'لقطة',
    //lifestyle section in home
    'style_title' => 'لايف ستايل',
    'style_category' => 'القسم',
    'style_shop_here' => 'تسوق هنا',
    'style_best_offers' => 'أفضل العروض',
    'food_title' => 'مطاعم',


    // mobile
    'mobile_brand' => 'الموديل',
    'all_cars' => 'الكل',
    'used_cars' => 'سيارات مستعملة',
    'new_cars' => 'سيارات جديدة',
    'other_cars' => 'مركبات أخري',
    'all_realestate' => 'الكل',
    'apartment_realestate' => 'شقق',
    'villa_realestate' => 'فلل',
    'land_realestate' => 'اراضي',
    'all-videos' => 'الكل',
    'cars-videos' => 'سيارات',
    'food-videos' => 'مطاعم',
    'style-videos' => 'لايف ستايل',
    'all_mob' => 'الكل',
    'mob_mobiles' => 'موبيلات',
    'mob_electric' => 'أجهزه كهربائيه',
    'mob_jobs' => 'وظائف',
    'all-articles' => 'الكل',
    'cars-articles' => 'سيارات',
    'food-articles' => 'مطاعم',
    'style-articles' => 'لايف ستايل',
    'articles_title' => 'المقالات الاكثر قراءه',
    'videos_title' => 'الفيديوهات الأكثر مشاهدة',
    'rests' => 'المطاعم',
    'today-rest' => 'مطعم اليوم',
    'rests-link' => 'المزيد من المطاعم',
    'ls_fashion' => 'موضة',
    'ls_beauty' => 'تجميل',
    'ls_decoration' => 'ديكور منزلى',
    'all_ls' => 'الكل',
    'meta-title' => 'اعلانات مبوبة مجانية فى مصر و السعودية والامارات وظائف عقارات سيارات',
    'meta-description' => 'اعلانات مبوبة مجانية فى مصر والسعودية والامارات علي دوبارتر اكبر موقع اعلانات مجانية واعلانات مبوبة في الوطن العربي في كافة المجالات ولاول مرة اشتري من البائع مباشرة بدون وسيط في اكبر موقع اعلانات مجانية مبوبة اعلانات وظائف خالية وعقارات ومطاعم وسيارات مستعملة تصفح الان',

    'dubarter_videos'=>'فيديوهات دوبارتر',
    'sell_your_property' => 'بيع عقارك',
    'rent_your_property' => 'اجر عقارك',
    'search_jobs' => 'ابحث عن وظيفه',
    'sell_your_mobile' => 'بيع موبيلك',
    'dubarter_articles' => 'مقالات دوبارتر',
    'cars' => 'سيارات',
    'classifieds' => 'مبوبات',


    'dentistry' => 'طب اسنان',
    'fulltime' => 'دوام كانل',
    '' => '',
    '' => '',
    '' => '',
];


