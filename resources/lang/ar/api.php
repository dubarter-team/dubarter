<?php

return [
    'error' => 'حدث خطأ ما',
    'success' => 'تمت العمليه بنجاح',
    'usrRegSuccess' => 'تم تسجيل العضو بنجاح',
    'usrNtFnd' => 'العضو غير موجود',
    'usrAlreadyActive' => 'هذا العضو مفعل من قبل',
    'usrActivetedSuccessfully' => 'تم تفعيل العضو بنجاح',
    'emailActive' => 'البريد الإلكتروني مفعل',
    '!emailActive' => 'البريد الإلكتروني غير مفعل',
    'loginError' => 'خطأ اثناء تسجيل الدخول ، حاول مره اخري',
    'loginSuccess' => 'تم تسجيل الدخول بنجاح',
    'wrongPassword' => 'خطأ في كلمه المرور',
    'refreshTokenError' => 'حدث خطأ ما اثناء تعديل بيانات الدخول',
    'methodNtAlwd' => 'نوع الطلب غير مسموح , مسموح فقط بـ : ',
    'notFound' => 'الطلب غير موجود',
    'validationError' => 'حدث خطأ ، من فضلك راجع مدخلاتك',
    'noPlaces' => 'لا توجد اماكن للعرض',
    'noCategories' => 'لا توجد اقسام للعرض',
    'noVideos' => 'لا توجد فيديوهات للعرض',
    'noArticles' => 'لا توجد مقالات للعرض',
    'noAttr' => 'لا توجد خواص للعرض',
    'noAds' => 'لا توجد اعلانات للعرض',
    'adNtFnd' => 'الإعلان غير موجود',
    'postNtFnd' => 'المقال غير موجود',
    'mustlogdIn' => 'يجب ان تسجل دخولك اولاً',
    'ntPrntCat' => 'القسم المطلوب غير رئيسي',
    'cmntNtFnd' => 'التعليق غير موجود',
    'cmntPstdScss' => 'تم نشر التعليق بنجاح',
    'rplyPstdScss' => 'تم نشر الرد بنجاح',
    'cat_not_found' => 'القسم غير موجود',
    'user_not_activated' => 'يجب تفعيل حسـابك أولا, من فضلك راجع بريدك الإلكترونى.',
    'usr_edit_success' => 'تم تعديل البيانات بنجاح'
];



