<?php


return [

    'favorites' => 'المفضلة',
    'my-account'=>'حسابى',
    'my-ads'=>'اعلاناتى',
    'offers'=>'العروض',
    'my-favorites-ads'=>'إعلاناتى المفضلة',
    'ad-name'=>'اسم الاعلان',
    'history'=>'التاريخ',
    'status'=>'الحالة',
    'edit'=>'تعديل',
    'sold'=>'مباع',
    'offer-name'=>' العرض',
    'price'=>'السعر',
	'preview' => 'معاينة الإعلان',
	'no' => 'لا'
];
