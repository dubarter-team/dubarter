<?php

return [
    'no_ads' => 'لا يوجد إعلانات',
    'no_subs' => 'لا يوجد اقسام فرعيه',
    'all_link' => 'الكل',
    'follow' => 'متابعة',
    'latest_videos' => 'أحدث الفيديوهات',
    'latest_articles' => 'أحدث المقالات',
    'read_article' => 'قراءة المقال',
    'discover_with' => 'إكتشف مع',
    'videos' => 'الفيديوهات',
    'articles' => 'المقالات',
    'detailed_search' => 'بحث مفصل',
    'search' => 'بحث',
    'car_type' => 'حالة السيارة',
    'car_brand' => 'براند السيارة',
    'car_model' => 'الموديل',
    'realestate_all_purchase_methods' => 'بيع / إيجار',
    'realestate_type' => 'نوع العقار',
    'city' => 'المدينه',
    'style_category' => 'القسم',
    'food_kitchen_type' => 'نوع المطبخ',
    'mobwaba_all_subs' => 'الكل',
    'category' => 'القسم',
    'send' => 'إرسال',
    'send_to'=>'أرسلت الى ',
    'sended_form'=>'أرسلت من',
    'not_found'=>'لا يوجد محتوى',
    'view'=>'شاهد',
    'edit'=>'تعديل',
    'delete'=>'مسح',
    'more'=>'تحميل  المزيد',
    'status'=>[
        0 => 'غير مفعل',
        1 => 'مفعل',
        2 => 'بإنتظار المراجعه',
        3 => 'مرفوض',
        4 => 'مباع',
    ],
    'featured_ads'=>'الأعلانات المميزة',
    'price_form'=>'السعر من ',
    'price_to'=>'السعر الى ',

    'featured_videos'=>'الفيديوهات المميزة',
    'featured_articles'=>'المقالات المميزة',
    ''=>'',

];


