<?php

return [
    'all_categories' => 'الكل',
    'category' => 'القسم',
    'all_attributes' => 'الكل',
    'search_for' => 'أبحث عن',
    'city' => 'المدينة',
    'order_by' => 'رتب حسب',
    'per_page' => 'عدد الإعلانات',
    'latest' => 'الأحدث',
    'oldest' => 'الأقدم',
    'highest_price' => 'الأعلى سعرا',
    'lowest_price' => 'الأقل سعرا',
    'search_results_for' => 'نتائج البحث عن',
    'no_ads' => 'لا يوجد إعلانات',
    'more_ads' => 'عرض المزيد',
    'pages' => 'صفحات',
    'ads' => 'إعلان',
    'search' => 'بحث',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];



