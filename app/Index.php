<?php

namespace App;

use App\Indices\Ad as AdIndex;
use Dot\Ads\Jobs\CategoryJob;
use Dot\Ads\Models\Ad as AdModel;

/**
 * Class Index
 */
class Index
{

	/**
	 * Save to index
	 *
	 * @param bool $id
	 */
	public static function save($id = false)
	{

		$row = AdModel::with([
			"categories",
			"tags",
			"user",
			"country",
			"city",
			"region",
			"custom_attributes",
			"files",
			"rejection_reason",
			"branches"
		])
			->where("id", $id)
			->first();

		if ($row) {

			AdIndex::id($row->id)->ignore(404)->delete();

			$ad = new AdIndex();

			$ad->_id = $row->id;
			$ad->title = $row->title;
			$ad->slug = $row->slug;
			$ad->content = $row->content;
			$ad->created_at = (string)$row->created_at;
			$ad->updated_at = (string)$row->updated_at;
			$ad->published_at = $row->published_at ? (string)$row->published_at : (string)$row->created_at;
			$ad->expired_at = $row->expired_at ? (string)$row->expired_at : '2050-12-31 14:15:33';
			$ad->email = $row->email;
			$ad->phone = $row->phone;
			$ad->status = $row->status;
			$ad->price = $row->price;
			$ad->currency = json_decode($row->currency);
			$ad->parent = $row->parent;
			$ad->views = $row->views;
			$ad->mobile_views = $row->mobile_views;
			$ad->featured = $row->featured;
			$ad->sponsored = (int)$row->sponsored;
			$ad->country_id = $row->country_id;
			$ad->city_id = $row->city_id;
			$ad->region_id = $row->region_id;

			if ($row->user) {
				$ad->user = (object)[
					"id" => $row->user->id,
					"name" => $row->user->first_name,
					"phone" => $row->user->phone
				];
			}

			if ($row->country) {
				$ad->country = (object)[
					"id" => $row->country->id,
					"code" => $row->country->code,
					"name" => (object)$row->country->name->toArray()
				];
			}

			if ($row->city) {
				$ad->city = (object)[
					"id" => $row->city->id,
					"name" => (object)$row->city->name->toArray()
				];
			}

			if ($row->region) {
				$ad->region = (object)[
					"id" => $row->region->id,
					"name" => (object)$row->region->name->toArray()
				];
			}

			if ($row->lat and $row->lng) {
				$ad->location = [$row->lat, $row->lng];
			}

			if ($row->categories) {

				$categories = [];

				foreach ($row->categories as $category) {
					$categories[] = (object)[
						"id" => $category->id,
						"name" => (object)$category->name->toArray(),
						"slug" => (object)$category->slug->toArray(),
						"direct" => $category->pivot->direct
					];

				}

				$ad->categories = array_reverse($categories);
			}

			if ($row->tags) {

				$tags = [];

				foreach ($row->tags as $tag) {

					$tags[] = (object)[
						"id" => $tag->id,
						"slug" => trim($tag->slug),
						"name" => trim($tag->name)
					];

				}

				$ad->tags = $tags;
			}

			if ($row->custom_attributes) {

				$attrs = [];

				foreach ($row->custom_attributes as $attribute) {

					if ($attribute->type == "multiple") {
						$attrs[$attribute->slug] = $attribute->pivot->value;
					} else {
						if (is_numeric($attribute->pivot->value)) {
							$attrs[$attribute->slug] = (int)$attribute->pivot->value;
						} else {
							$tmp = @json_decode($attribute->pivot->value, 1);

							if ($tmp && is_array($tmp)) {
								$attrs[$attribute->slug] = $tmp[0];
							} else {
								$attrs[$attribute->slug] = $attribute->pivot->value;
							}
						}
					}

				}

				$ad->attrs = (object)$attrs;
			}


			if ($row->files) {

				$files = [];

				foreach ($row->files as $file) {

					$files[] = (object)[
						"id" => $file->id,
						"type" => $file->type,
						'provider' => $file->provider,
						'provider_image' => $file->provider_image,
						"path" => $file->path,
						"title" => $file->title ? $file->title : $row->title
					];

				}

				$ad->files = $files;
			}

			if ($row->rejection_reason) {
				$ad->rejection_reason = (object)[
					"id" => $row->rejection_reason->id,
					"title" => $row->rejection_reason->title->toArray(),
					"message" => $row->rejection_reason_message
				];
			}

			if ($row->branches) {
				$branches = [];

				foreach ($row->branches as $br) {
					$branches[] = (object)[
						"id" => $br->id,
						"title" => $br->title,
						"address" => $br->address,
						"phone" => $br->phone,
						"lat" => $br->lat,
						"lng" => $br->lng,
						"region_id" => $br->region_id,
						"city_id" => $br->city_id,
						"country_id" => $br->country_id,
						"available_from" => $br->available_from,
						"available_to" => $br->available_to,
						"status" => $br->status
					];
				}

				$ad->branches = $branches;
			}

			$ad->save();

		} else {

			// Ad not found in database. delete it from index.

			AdIndex::id($id)->ignore(404)->delete();
		}

	}


	/**
	 * Delete from index
	 *
	 * @param bool $id
	 */
	public static function delete($id = false)
	{
		AdIndex::id($id)->ignore(404)->delete();
	}

}
