<?php

namespace App;

use App\Models\Ad;
use App\Models\Media;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $appends = [
        "display_name", "medium_photo"
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $creatingRules = [
        'username' => 'required|unique:users',
        "email" => "required|email|unique:users",
    ];

    /**
     * @var array
     */
    protected $updatingRules = [
        "username" => "required|unique:users,username,[id],id",
        "email" => "required|email|unique:users,email,[id],id",
    ];

    function setPasswordAttribute($password)
    {
        if (trim($password) != "") {
            $this->attributes["password"] = Hash::make($password);
        } else {
            unset($this->attributes["password"]);
        }
    }

    /**
     * liked_ads relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function liked_ads()
    {
        return $this->belongsToMany(Ad::class, 'ads_likes', 'user_id', 'ad_id');
    }

    /**
     * photo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function photo()
    {
        return $this->hasOne(Media::class, 'id', 'photo_id');
    }

    /**
     * photo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Media::class, 'id', 'photo_id');
    }

    function getDisplayNameAttribute()
    {
        $display_name = $this->username;

        if ($this->first_name) {
            $display_name = $this->first_name . ($this->last_name ? ' ' . $this->last_name : '');
        }

        return $display_name;
    }

    function getMediumPhotoAttribute()
    {
        if ($this->photo) {
            return thumbnail($this->photo->path, "medium");
        } else {
            return "";
        }
    }
}
