<?php

namespace App\Indices;

use App\Models\Attribute;
use App\Models\Category;
use Basemkhirat\Elasticsearch\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\Place;
use Illuminate\Support\Facades\Request;

/**
 * Class Ad
 *
 * @package App\Indices
 */
class Ad extends Model
{

	/**
	 * @var string
	 */
	protected $index = "dubarter";

	/**
	 * @var string
	 */
	protected $type = "ads";

	/**
	 * @var array
	 */
	protected $appends = [
		"url",
		"main_category_id",
		"main_category_name",
		"main_category_slug",
		"main_category_url",
		"date",
		"main_image",
		"country_name",
		"city_name",
		"region_name",
		"excerpt",
		"custom_attributes",
		"liked",
		"images",
		"seo_excerpt",
		"real_views",
		"single_attributes",
		"multiple_attributes",
		"is_expired"
	];

	/**
	 * @return string
	 */
	function getUrlAttribute()
	{
		return url("ads/" . $this->slug);
	}

	/**
	 * @param        $query
	 * @param string $column
	 * @param string $order
	 */
	public function scopeLatest($query, $column = "created_at", $order = "DESC")
	{
		$query->orderBy($column, $order);
	}

	/**
	 * @param $query
	 */
	public function scopeFeatured($query)
	{
		$query->where("featured", 1);
	}

	public static function escape($term)
	{
		$result = $term;

		$chars = ['\\', '+', '-', '&&', '||', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '/', '<', '>'];

		foreach ($chars as $char) {
			$result = str_replace($char, '\\' . $char, $result);
		}

		return trim($result);
	}

	/**
	 * @param $query
	 * @param $category
	 */
	function scopeInCategory($query, $category)
	{
		$query->byCategory($category->id);
	}

	/**
	 * @param $query
	 * @param $category_id
	 */
	public function scopeByCategory($query, $category_id)
	{
		$query->where('categories.id', $category_id);

		if ($category_id == Category::FOODBARTER || $category_id == 8) {
			$query->withoutGlobalScopes()->where("status", 1)->byCountry();
		}
	}

	function getImagesAttribute()
	{
		$images = collect([]);

		if (count($this->files)) {
			$images = collect($this->files)->where('type', 'image');
		}

		return $images;
	}

	/**
	 * @return mixed
	 */
	function getCountryNameAttribute()
	{
		return isset($this->attributes["country"]["name"]) ? $this->attributes["country"]["name"][app()->getLocale()] : NULL;
	}

	/**
	 * @return null
	 */
	function getCityNameAttribute()
	{
		return isset($this->attributes["city"]["name"]) ? $this->attributes["city"]["name"][app()->getLocale()] : NULL;
	}

	/**
	 * @return null
	 */
	function getRegionNameAttribute()
	{
		return isset($this->attributes["region"]["name"]) ? $this->attributes["region"]["name"][app()->getLocale()] : NULL;
	}

	/**
	 * @return null
	 */
	function getExcerptAttribute()
	{
		return str_limit(strip_tags($this->content), 80);
	}

	function getSeoExcerptAttribute()
	{
		$seo_excerpt = str_limit(strip_tags($this->content), 280);

		if (empty($seo_excerpt)) {
			$seo_excerpt = $this->title;
		}

		return $seo_excerpt;
	}

	/**
	 * @return null
	 */
	function getMainImageAttribute()
	{
		$image = null;

		if (count($this->files)) {
			foreach ($this->files as $file) {
				if ($file['type'] == 'image') {
					$image = $file['path'];
					break;
				}

			}
		}

		if (!$image) {
			$image = '2018/03/2398247304400053075.png';
		}

		return $image;
	}

	public function getLikedAttribute()
	{
		$liked = false;

		if (auth('frontend')->user()) {
			$check = DB::table('ads_likes')->where('ad_id', $this->_id)->where('user_id', auth('frontend')->user()->id)->first();

			$liked = $check ? true : false;
		}

		return $liked;
	}

	public function getRealViewsAttribute()
	{
		$ad = DB::table('ads')->where('id', $this->_id)->select('views')->first();

		return $ad ? $ad->views : 0;
	}

	/**
	 * @return mixed
	 */
	function getCurrencyAttribute()
	{
		$curr = $this->attributes["currency"][app()->getLocale()];

		if (empty($curr)) {
			$curr = @json_decode(config('country.currency'), 1)[app()->getLocale()];
		}

		return $curr;
	}

	/**
	 * @return mixed
	 */
	function getMainCategoryNameAttribute()
	{
		if (count($this->categories)) {
			return $this->categories[0]["name"][app()->getLocale()];
		}
	}

	function getIsExpiredAttribute()
	{
		$flag = false;

		if ($this->published_at && $this->published_at < '2018-01-01 00:00:00') {
			$flag = true;
		} else if ($this->created_at && $this->created_at < '2018-01-01 00:00:00') {
			$flag = true;
		}

		return $flag;
	}

	/**
	 * @return mixed
	 */
	function getMainCategoryIdAttribute()
	{
		if (count($this->categories)) {
			return $this->categories[0]["id"];
		}
	}

	/**
	 * @return mixed
	 */
	function getMainCategorySlugAttribute()
	{
		if (count($this->categories)) {
			return $this->categories[0]["slug"][app()->getLocale()];
		}
	}

	/**
	 * @return mixed
	 */
	function getMainCategoryUrlAttribute()
	{
		if (count($this->categories)) {
			return $this->categories[0]["slug"][app()->getLocale()];
		}
	}

	/**
	 * @return false|string
	 */
	function getDateAttribute()
	{
		return (isset($this->attributes["published_at"]) && $this->attributes["published_at"]) ? date("Y-m-d", strtotime($this->attributes["created_at"])) : date("Y-m-d", strtotime($this->attributes["published_at"]));
	}

	/**
	 * @param string $size
	 *
	 * @return string
	 */
	function image($size = "medium")
	{
		if (count($this->files)) {
			foreach ($this->files as $file) {
				if ($file["type"] == "image") {
					return thumbnail($file['path'], $size);
				}
			}
		}
	}

	/**
	 * @param $query
	 */
	function scopeAutobarter($query)
	{
		$query->where("categories.id", Category::AUTOBARTER);
	}

	/**
	 * @param $query
	 */
	function scopeAqarbarter($query)
	{
		$query->where("categories.id", Category::AQARBARTETR);
	}

	/**
	 * @param $query
	 */
	function scopeStylebarter($query)
	{
		$query->where("categories.id", Category::STYLEBARTER);
	}

	/**
	 * @param $query
	 */
	function scopeFoodbarter($query)
	{
		$query->where("categories.id", Category::FOODBARTER);
	}

	/**
	 * @param $query
	 */
	function scopeMobawababarter($query)
	{
		$query->where("categories.id", Category::MOBAWABABARTER);
	}

	/**
	 * @param $query
	 */
	function scopeMobiles($query)
	{
		$query->where("categories.id", Category::MOBILES);
	}

	/**
	 * @param $query
	 */
	function scopeJobs($query)
	{
		$query->where("categories.id", Category::JOBS);
	}

	/**
	 * @param $query
	 */
	public function boot($query)
	{
		$query->where('published_at', ">=", '2018-01-01 00:00:00')->where("status", 1);

		if($country = Place::getCurrentCountry()){
			$query->where('country.id', $country->id);
		}

		if (Request::filled("country_id")) {
			$query->where('country.id', request("country_id"));
		}
	}

	function scopeByCountry($query)
	{
		$query->where('country.id', config("country.id"));
	}

	/**
	 *
	 */
	public function getCustomAttributesAttribute()
	{

		$attributes = [];

		$attrs_slugs = array_keys($this->attributes["attrs"]);

		if (count($attrs_slugs)) {
			$custom_attributes = [];

			foreach ($attrs_slugs as $slug) {
				$attr = Cache::remember('attribute-' . $slug, 5000, function () use ($slug) {
					return Attribute::with("big_icon", "small_icon")
						->where("slug", $slug)->where('visible', 1)
						->orderBy('priority', 'desc')->first();
				});

				$custom_attributes[] = $attr;
			}


			if (count($custom_attributes)) {

				foreach ($custom_attributes as $attribute) {

					if ($attribute) {

						$options = array_combine(array_keys($attribute->options), array_values($attribute->options));

						$value_title = '';

						$val = isset($this->attributes["attrs"][$attribute->slug]) ? $this->attributes["attrs"][$attribute->slug] : '';

						if (is_array($val) && count($val) > 0 && $attribute->type == 'single') {
							$val = $val[0];
						}

						if (count($options)) {

							if (is_array($val)) {
								$value_title = [];

								foreach ($val as $v) {
									$value_title[$v] = array_key_exists($v, $options) ? $options[$v] : '';
								}

							} else {
								if (array_key_exists($val, $options)) {
									$value_title = $options[$val];
								}

								if (array_key_exists(snake_case($val), $options)) {
									$value_title = $options[snake_case($val)];
								}
							}

						} else {
							$value_title = $val;
						}

						$attributes[] = (object)[
							"attribute_slug" => $attribute->slug,
							"attribute_title" => (string)$attribute->name,
							"value_slug" => $val,
							"attribute_type" => $attribute->type,
							"value_title" => $value_title,
							"big_icon_url" => $attribute->big_icon ? uploads_url($attribute->big_icon->path) : assets("frontend/assets/img/icon1.png"),
							"small_icon_url" => $attribute->small_icon ? uploads_url($attribute->small_icon->path) : assets("frontend/assets/img/icon1.png"),
						];

					}

				}
			}
		}

		return $attributes;

	}

	function getSingleAttributesAttribute()
	{
		return collect($this->custom_attributes)->where('attribute_type', 'single');
	}

	function getMultipleAttributesAttribute()
	{
		return collect($this->custom_attributes)->where('attribute_type', 'multiple');
	}

	public function check_category($id)
	{
		$res = false;

		$cats = collect($this->categories);

		if ($cats->count()) {
			$check = $cats->where('id', $id)->count();

			if ($check) {
				$res = true;
			}
		}

		return $res;
	}

	public static function featured_by_category($cat_id, $cache_id)
	{
		return Cache::remember($cache_id, 5, function () use ($cat_id) {
			$q = Ad::where('featured', 1);

			if (is_array($cat_id)) {
				$q->whereIn('categories.id', $cat_id);
			} else if ($cat_id) {
				$q->byCategory($cat_id);
			}

			return $q->take(8)->orderBy('published_at', 'desc')->get();
		});
	}

}
