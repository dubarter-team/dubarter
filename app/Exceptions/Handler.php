<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if ($request->segment(1) == 'api') {
            App()->setLocale($request->segment(2));

            if ($exception instanceof MethodNotAllowedHttpException) {
                return response()->json(['scode' => 405, 'message' => __('api.error'), 'data' => null, 'errors' => [__('api.methodNtAlwd') . $exception->getHeaders()['Allow']]]);
            }

            if ($exception instanceof NotFoundHttpException) {
                return response()->json(['scode' => 404, 'message' => __('api.error'), 'data' => null, 'errors' => [($exception->getMessage() == '') ? __('api.notFound') : $exception->getMessage()]]);
            }

            if ($exception instanceof ValidationException) {
                return response()->json(['scode' => 400, 'message' => __('api.validationError'), 'data' => null, 'errors' => array_flatten($exception->errors())]);
            }

        }

        return parent::render($request, $exception);
    }
}
