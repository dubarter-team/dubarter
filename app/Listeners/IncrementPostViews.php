<?php

namespace App\Listeners;

use App\Events\PostViewed;
use Illuminate\Support\Facades\DB;
use Request;

class IncrementPostViews
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostViewed $event
     * @return void
     */
    public function handle(PostViewed $event)
    {
        $check = DB::table('posts_views')
            ->where('post_id', $event->post->id)
            ->where('user_ip', Request::ip())
            ->where('agent', Request::header('User-Agent'))
            ->first();

        if (!$check) {
            DB::table('posts_views')->insert([
                'post_id' => $event->post->id,
                'user_ip' => Request::ip(),
                'agent' => Request::header('User-Agent')
            ]);

            DB::table('posts')->whereId($event->post->id)->increment('views');
        }
    }
}