<?php

namespace App\Listeners;

use App\Events\AdViewed;
use Illuminate\Support\Facades\DB;
use Request;

class IncrementAdViews
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdViewed $event
     * @return void
     */
    public function handle(AdViewed $event)
    {
        $check = DB::table('ads_views')
            ->where('ad_id', $event->ad->_id)
            ->where('user_ip', Request::ip())
            ->where('agent', Request::header('User-Agent'))
            ->first();

        if (!$check) {
            DB::table('ads_views')->insert([
                'ad_id' => $event->ad->_id,
                'user_ip' => Request::ip(),
                'agent' => Request::header('User-Agent')
            ]);

            DB::table('ads')->whereId($event->ad->_id)->increment('views');
        }
    }
}