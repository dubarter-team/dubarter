<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;

class Place extends \Dot\I18n\Models\Place
{

	function getLocalNameAttribute($name)
	{
		return $this->name->{app()->getLocale()};
	}

	static function get_local_cities()
	{
		return Cache::remember('cities-' . config("country.id"), 200, function () {
			return Place::where('parent', config("country.id"))->get();
		});
	}

	/**
	 *  Global scopes
	 */
	static function boot()
	{
		static::addGlobalScope(function ($query) {
			$query->where("status", 1);
		});
	}

	public static function getCurrentCountry()
	{

		if (!config('country')) {
			return false;
		}

		$country = (object)config('country');

		$country->name = json_decode($country->name)->{app()->getLocale()};
		$country->currency = json_decode($country->currency)->{app()->getLocale()};

		return $country;

	}
}
