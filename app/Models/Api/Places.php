<?php

namespace App\Models\Api;

use Dot\I18n\Models\Place;

class Places extends Place
{

	protected $hidden = ["image"];
	protected $appends = ["logo"];

	protected function getLogoAttribute()
	{
		return $this->image ? uploads_url($this->image->path) : NULL;
	}

    public function scopeActive($query,$status=true)
    {
        $query->where('status',$status);
    }

/*    public function getNameAttribute($name)
    {
        return $this->translate($name);
    }*/
/*    public function getCurrencyAttribute($currency)
    {
        return $this->translate($currency);
    }*/

    private function translate($string)
    {
        $lang = app()->getLocale();
        return $this->decode($string)->$lang;
    }

    private function decode($json)
    {
        return json_decode($json);
    }

}
