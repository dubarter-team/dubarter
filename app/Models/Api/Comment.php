<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['object_type','object_id','user_id','parent','content','status'];

    public function replies()
    {
        return $this->hasMany(Comment::class,'parent');
    }

    public function user()
    {
        return $this->belongsTo(UserApi::class,'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class,'object_id');
    }
}
