<?php

namespace App\Models\Api;

use App\Models\Category;
use Dot\Attributes\Models\Attribute;
use Dot\Media\Models\Media;
use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    protected $appends =['big_icon','small_icon'];

    public function bIcon()
    {
        return $this->hasOne(Media::class, "id", "big_icon_id");
    }

    public function sIcon()
    {
        return $this->hasOne(Media::class, "id", "small_icon_id");
    }

    function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_attributes', 'attribute_id', 'category_id');
    }

    public function getBigIconAttribute()
    {
        if($this->bIcon){
            return thumbnail($this->bIcon->path,'medium');
        }
        return "";
    }

    public function getSmallIconAttribute()
    {
        if($this->sIcon){
            return thumbnail($this->sIcon->path,'medium');
        }
        return "";
    }
    public function getTypeAttribute($type)
    {
        return ($this->template == "dropdown")?$type:"";
    }


    /************* Json Translate *************/
    public function getNameAttribute($name)
    {
        return $this->decode($name);
    }

    public function getOptionsAttribute($options)
    {
        $translatedOptions = /*$this->decode($options);//*/$this->translate($options);
        if($translatedOptions){
            $i=0;
            foreach ($translatedOptions as $slug => $value){
                $arr[$i]['slug']=$slug;
                $arr[$i]['value']=$value;
                $i++;
            }
            return $arr;
        }
        return null;
    }

    private function translate($string)
    {
        return ($string)?$this->decode($string)->{app()->getLocale()}:null;
    }

    private function decode($json)
    {
        return json_decode($json);
    }
}
