<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class AdsLikes extends Model
{
    protected $table    = 'ads_likes';
    protected $fillable = ['ad_id','user_id'];

    public function ad()
    {
        return $this->belongsTo(Ads::class,'ad_id');
    }

    public function user()
    {
        return $this->belongsTo(UserApi::class,'user_id');
    }

}