<?php

namespace App\Models\Api;

use App\Models\Media;
use Basemkhirat\Elasticsearch\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Ad
 *
 * @package App\Indices
 */
class Ads extends Model
{

    /**
     * @var string
     */
    protected $index = "dubarter";

    /**
     * @var string
     */
    protected $type = "ads";

    /**
     * @var array
     */
    protected $casts = ['currency' => 'json'];
    protected $appends = ["real_views", 'isLiked'];


    public function adsLikes()
    {
        return $this->belongsToMany(Ads::class, 'ads_likes', 'user_id', 'ad_id');
    }

    public function scopeLatest($query, $column = "created_at", $order = "DESC")
    {
        $query->orderBy($column, $order);
    }

    public function scopeFeatured($query)
    {
        $query->where("featured", 1);
    }

    public function getRealViewsAttribute()
    {
        $ad = DB::table('ads')->where('id', $this->_id)->select('views')->first();

        return $ad ? $ad->views : 0;
    }

    function getMainImageAttribute()
    {
        $image = null;

        if (count($this->files)) {
            if ($this->files[0]['type'] == 'image') {
                $image = $this->files[0]['path'];
            } else {
                return "";
            }
        }

        return $image;
    }

    public function getFilesAttribute($files)
    {
        $arr = [];

        foreach ($files as $k => $file) {

            $temp = [];

            if ($file['type'] == 'image') {

                $temp['id'] = $file['id'];
                $temp['type'] = $file['type'];
                $temp['medium'] = thumbnail($file['path'], 'medium');
                $temp['thumb'] = thumbnail($file['path'], 'thumbnail');

            } else if ($file['type'] == 'video') {

                $temp['id'] = $file['id'];
                $temp['type'] = $file['type'];

                $temp['provider'] = $file['provider'];

                if ($file['provider'] == 'youtube') {
                    $temp['path'] = $file['path'];
                } else {
                    $temp['path'] = uploads_url($file['path']);
                }

                $temp['provider_image'] = $file['provider_image'];

            }

            if (count($temp)) {
                $arr[] = $temp;
            }
        }

        if (!count($arr)) {
            $media = Media::where('id', 0)->first();

            if ($media) {
                $temp = [];

                $temp['id'] = $media->id;
                $temp['type'] = $media->type;
                $temp['medium'] = thumbnail($media->path, 'medium');
                $temp['thumb'] = thumbnail($media->path, 'thumbnail');

                $arr[] = $temp;
            }
        }

        return $arr;
    }

    public function getCountryAttribute($country)
    {
        $arr = [];
        $arr['id'] = $country['id'];
        $arr['code'] = $country['code'];
        $arr['name'] = $country['name'];
        return $arr;
    }

    public function getCityAttribute($city)
    {
        $arr['id'] = $city['id'];
        $arr['name'] = $city['name'];
        return $arr;
    }

    public function getCategoriesAttribute($categories)
    {
        $arr = [];
        foreach ($categories as $k => $category) {
            $arr[$k]['id'] = $category['id'];
            $arr[$k]['name'] = $category['name'];
            $arr[$k]['slug'] = $category['slug'];
            $arr[$k]['direct'] = $category['direct'];
        }
        return $arr;
    }

    public function getAttrsAttribute($attributes)
    {
        $i = 0;
        $arr = [];
        foreach ($attributes as $k => $attribute) {
            $temp = $this->getAttrib($k);

            $arr[$i]['slug'] = $k;
            $arr[$i]['name'] = $temp ? $temp->name : null;
            $arr[$i]['small_icon'] = $temp ? $temp->small_icon : "";
            $arr[$i]['big_icon'] = $temp ? $temp->big_icon : "";
            $arr[$i]['big_icon_id'] = $temp ? $temp->big_icon_id : 0;
            $arr[$i]['small_icon_id'] = $temp ? $temp->small_icon_id : 0;

            $arr[$i]['value'] = null;
            $arr[$i]['options'] = [];

            if (is_array($attribute) && count($attribute)) {
                $arr[$i]['value'] = $attribute[0];

                foreach ($attribute as $atv) {
                    if ($temp->options && count($temp->options)) {

                        foreach ($temp->options as $op) {
                            if (isset($op['slug']) && $op['slug'] == $atv) {
                                $arr[$i]['options'][] = $op;
                            }
                        }
                    }
                }
            }

            $i++;
        }
        return $arr;
    }

    public function getAttrib($slug)
    {
        return Attributes::where('slug', $slug)->first();
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->timestamp;
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->timestamp;
    }

    public function getPublishedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->timestamp;
    }

    public function getExpiredAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->timestamp;
    }


    public function getIsLikedAttribute()
    {
        $auth = auth()->guard('mobile-api');
        if (!$auth->check()) {
            return false;
        }
        $liked = AdsLikes::where('ad_id', $this->_id)->where('user_id', $auth->user()->id)->first();
        return ($liked) ? true : false;
    }

    public function likes()
    {
        return $this->hasMany(AdsLikes::class, 'ad_id', '_id');
    }

    /************* Json Translate *************/
    /*public function getCurrencyAttribute($currency)
    {
        return $this->translate($currency);
    }*/

    private function decode($string)
    {
        return ($string) ? json_decode($string) : null;
    }

    private function translate($source)
    {
        return ($source) ? $source[app()->getLocale()] : null;
    }

    public function boot($query)
    {
        //$query->where("status", 1);
        $query->where('published_at', ">=", '2018-01-01 00:00:00');
    }

    function scopeByCountry($query, $id)
    {
        $query->where(function ($query) use ($id) {
            $query->where('country_id', $id)
                ->orWhere('country_id', 0);
        });
    }

}
