<?php

namespace App\Models\Api;

use \Dot\Media\Models\Media;
use Dot\Tags\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Post extends Model
{

    protected $appends = ['image','media'];

    public function img()
    {
        return $this->hasOne(Media::class, 'id', 'image_id');
    }
    public function med()
    {
        return $this->hasOne(Media::class, 'id', 'media_id');
    }

    public function getTypeAttribute()
    {
        if($this->format =="post" || $this->format == "article")
        {
            return "article";
        }
        return "video";
    }

    public function getImageAttribute()
    {
        if ($this->img) {
            return thumbnail($this->img->path);
        }
        return "";
    }

    public function getMediaAttribute()
    {
        if ($this->med) {
            if($this->med->provider == "" || $this->med->provider == null){
                return thumbnail($this->med->path);
            }
            return $this->med->path;
        } else {
            return "";
        }
        return "";
    }

    public function scopeVideos($query)
    {
        $query->where('format','video');
    }

    public function scopeArticles($query)
    {
        $query->where('format','post')->orWhere('format','article');
    }

    public function scopeFeatured($query)
    {
        $query->where('featured',1);
    }


    public function scopeByLanguage($query)
    {
        $query->where('lang',request()->segment(2));
    }

    public function scopePublished($query)
    {
        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $query->where('published_at','<=',$now);
    }

    public function scopeActive($query)
    {
        $query->where('status','1');
    }

    /**
     * Categories relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Categories::class, "posts_categories", "post_id", "category_id");
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, "posts_tags", "post_id", "tag_id");
    }

	static function boot()
	{

		static::addGlobalScope(function ($query) {

			$query->latest()->where("status", 1);

			if (Request::filled("country_id")) {
				$query->whereIn('country_id', [
					0,
					request("country_id")
				]);
			}

		});
	}

}
