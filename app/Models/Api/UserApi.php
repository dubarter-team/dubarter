<?php

namespace App\Models\Api;

use Dot\Users\Models\User;
use Laravel\Passport\HasApiTokens;

class UserApi extends User
{
    use HasApiTokens;

    protected $hidden = ['password'];
    protected $appends = ['medium_photo_url'];

    public function findForPassport($identifier) {
        return $this->Where('email', $identifier)->whereNull('provider')->first();
    }

    public function getThumbnailPhotoUrlAttribute()
    {
        if ($this->photo) {
            return thumbnail($this->photo->path, "thumbnail");
        } else {
            return "";
        }
    }

    public function getMediumPhotoUrlAttribute()
    {
        if ($this->photo) {
            return thumbnail($this->photo->path, "medium");
        } else {
            return "";
        }
    }

    public function likes()
    {
        return $this->hasMany(AdsLikes::class,'user_id');
    }

    public function adsLikes()
    {
        return $this->belongsToMany(Ads::class,'ads_likes','user_id','ad_id');
    }
}
