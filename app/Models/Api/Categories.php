<?php

namespace App\Models\Api;

use Dot\Categories\Models\Category;
use Dot\Media\Models\Media;
use Illuminate\Database\Eloquent\Builder;

class Categories extends Category
{
	protected $appends = ['image', 'logo', 'ads_count', 'banner_image'];

	/**************** RelationShips *****************/
	public function img()
	{
		return $this->hasOne(Media::class, 'id', 'image_id');
	}

	public function log()
	{
		return $this->hasOne(Media::class, 'id', 'logo_id');
	}

	public function banner()
	{
		return $this->hasOne(Media::class, 'id', 'banner_id');
	}

	function categories()
	{
		return $this->hasMany(Categories::class, 'parent');
	}

	/**************** Accessors *****************/
	public function getListingTypeAttribute($type)
	{
		return ($type == "") ? 'ads' : $type;
	}

	public function getLogoAttribute()
	{
		if ($this->log) {
			return thumbnail($this->log->path, 'medium');
		} else {
			return "";
		}
	}

	public function getImageAttribute()
	{
		if ($this->img) {
			return thumbnail($this->img->path, 'medium');
		} else {
			return "";
		}
	}

	public function getBannerImageAttribute()
	{
		if ($this->banner) {
			return uploads_url($this->banner->path);
		} else {
			return "";
		}
	}

	public function getAdsCountAttribute()
	{
		return $this->adsCount();
	}

	/************* Scopes *************/
	public function scopeParent($query, $parent = 0)
	{
		$query->where('parent', $parent);
	}

	public function scopeActive($query, $status = true)
	{
		$query->where('status', $status);
	}

	public function adsCount()
	{
		$id = $this->id;

		$q = Ads::where('status', 1);

		$q->where('categories.id', $id);

		if (request('country_code', 0)) {
			$q->where(function ($query) {
				$query->where('country.code', request('country_code'))
					->orWhere('country_id', 0);
			});
		}

		return $q->count();
	}

	/************* Json Translate *************/
	/*  public function getNameAttribute($name)
	  {
		  return $this->translate($name);
	  }*/
	public function getSlugAttribute($slug)
	{
		return $this->decode($slug);
	}

	private function translate($string)
	{
		$lang = app()->getLocale();
		return $this->decode($string)->$lang;
	}

	private function decode($json)
	{
		return json_decode($json);
	}

	static function get_parents_objects($id)
	{
		$parents = [];

		while (true) {
			$cat = self::with(['parent_category'])->where('id', $id)->first();

			$parents[] = $cat;

			if ($cat->parent) {
				$id = $cat->parent;
			} else {
				break;
			}
		}

		$parents = collect(array_reverse($parents));

		return $parents;
	}

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(function (Builder $builder) {
			if (request("country_id") == 10) {
				$builder->whereNotIn('id', [
					3,    // lifestyle,
					6    // new cars
				]);
			}
		});
	}

}
