<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Category
 *
 * @package App\Models
 */
class Category extends \Dot\Categories\Models\Category
{

	// Main categories

	const AUTOBARTER = 1;
	const AQARBARTETR = 2;
	const STYLEBARTER = 3;
	const FOODBARTER = 4;
	const MOBAWABABARTER = 5;

	// Most used categories

	const KITCHENS = 10;
	const RESTAURANTS = 8;
	const RESTAURANTS_OFFERS = 9;
	const JOBS = 46;
	const MOBILES = 47;
	const ELECTRONICS = 48;
	const COMPUTERS = 50;
	const AQARBARTER_TYPES = 32;
	const USED_CARS = 7;
	const NEW_CARS = 6;
	const REALESTATE_SALE = 32;
	const REALESTATE_RENT = 33;
	const FASHION = 17;
	const BEAUTY = 18;
	const WEDDING = 19;
	const OTHER_VEHICLES = 69;

	/**
	 * @var array
	 */
	protected $appends = ["url", 'all_url', 'url_slugs', 'banner_image', 'custom_name'];

	/**
	 * Category URL attribute
	 *
	 * @return string
	 */
	function getUrlAttribute()
	{
		return url($this->url_slugs);
	}

	function getAllUrlAttribute()
	{
		return url('all-ads/' . $this->url_slugs);
	}

	function getUrlSlugsAttribute()
	{
		if ($this->parent == 0) {
			return $this->slug->{app()->getLocale()};
		} else {
			return $this->getRecursiveSlug($this->parent);
		}
	}

	function getCustomNameAttribute()
	{
		$name = ucfirst((string)$this->name);

		return $name;
	}

	/**
	 * ads relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	function ads()
	{
		return $this->belongsToMany(Ad::class, 'ads_categories', 'category_id', 'ad_id')
//            ->where(function ($query) {
//                $query->where('country_id', config("country.id"))
//                    ->orWhere('country_id', 0);
//            })
			->where('ads.status', 1)->where('ads.published_at', ">=", '2018-01-01 00:00:00');
	}

	/**
	 * categories relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	function categories()
	{
		return $this->hasMany(Category::class, 'parent')->where('status', 1);
	}

	/**
	 * videos relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	function videos()
	{
		return $this->belongsToMany(Post::class, 'posts_categories', 'category_id', 'post_id')->where('posts.status', 1)->where('posts.format', 'video');
	}

	/**
	 * videos relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	function articles()
	{
		return $this->belongsToMany(Post::class, 'posts_categories', 'category_id', 'post_id')->where('posts.status', 1)->where('posts.format', 'article');
	}

	/**
	 * Generate recursive category url
	 *
	 * @param      $parent
	 * @param null $slug
	 *
	 * @return string
	 */
	function getRecursiveSlug($parent, $slug = NULL)
	{

		if ($parent) {
			$parent_category = Cache::remember('link-category-' . $parent, 300, function () use ($parent) {
				return Category::where("id", $parent)->first();
			});

			if ($parent_category) {

				$slug = $parent_category->slug->{app()->getLocale()} . "/" . $slug;

				return $this->getRecursiveSlug($parent_category->parent, $slug);
			}
		}

		return $slug . $this->slug->{app()->getLocale()};
	}

	public function getBannerImageAttribute()
	{
		if ($this->banner) {
			return uploads_url($this->banner->path);
		} else {
			return "";
		}
	}

	/**
	 * @param $query
	 * @param $slug
	 */
	function scopeSlug($query, $slug)
	{
		$query->where(DB::raw('JSON_EXTRACT(`slug`, "$.' . app()->getLocale() . '")'), '=', $slug);
		//$query->where("slug->" . app()->getLocale(), $slug);
	}

	/**
	 * @param $query
	 * @param $category
	 */
	function scopeInCategory($query, $category)
	{
		$query->where("parent", $category->id);
	}

	/**
	 * @param $query
	 */
	function scopeInLandingNav($query)
	{
		$query->where("in_landing_nav", 1);
	}

	/**
	 * List of all restaurants types
	 *
	 * @param $query
	 */
	function scopeKitchen($query)
	{
		$query->where("parent", self::KITCHENS);
	}


	/**
	 * Scope to get only kitchen category
	 *
	 * @param $query
	 */
	function scopeIsKitchen($query)
	{
		$query->where("id", self::KITCHENS);
	}

	/**
	 * List of all restaurants
	 *
	 * @param $query
	 */
	function scopeRestaurants($query)
	{
		$query->whereIn("parent", self::where("parent", self::KITCHENS)->get()->pluck('id'));
	}

	/**
	 * Job types
	 *
	 * @param $query
	 */
	function scopeJobs($query)
	{
		$query->where("parent", self::JOBS);
	}

	/**
	 * List of mobile brands
	 *
	 * @param $query
	 */
	function scopeMobiles($query)
	{
		$query->where("parent", self::MOBILES);
	}

	/**
	 * List of all electronic brands
	 *
	 * @param $query
	 */
	function scopeElectronics($query)
	{
		$query->where("parent", self::ELECTRONICS);
	}

	/**
	 * List of all computer brands
	 *
	 * @param $query
	 */
	function scopeComputers($query)
	{
		$query->where("parent", self::COMPUTERS);
	}

	/**
	 * Featured filter scope
	 *
	 * @param $query
	 */
	function scopeFeatured($query)
	{
		$query->where("featured", 1);
	}

	/**
	 * Lastest filter scope
	 *
	 * @param $query
	 */
	function scopeLatest($query)
	{
		$query->orderBy("id", "DESC");
	}


	static function get_parents_objects($id)
	{
		$parents = [];

		while (true) {
			$cat = self::with(['parent_category'])->where('id', $id)->first();

			$parents[] = $cat;

			if ($cat->parent) {
				$id = $cat->parent;
			} else {
				break;
			}
		}

		$parents = collect(array_reverse($parents));

		return $parents;
	}

	public static function get_sub_categories($id)
	{
		return Cache::remember('sub-categories-' . $id, 200, function () use ($id) {
			return Category::where("parent", $id)
				->take(3)->get();
		});
	}

	public static function get_nav_sub_categories($id)
	{
		return Cache::remember('nav-categories-' . $id, 200, function () use ($id) {
			return Category::where("parent", $id)
				->inLandingNav()->take(3)->get();
		});
	}

	/**
	 *  Global scopes
	 */
	static function boot()
	{
		static::addGlobalScope(function ($query) {
			$query->where("categories.status", 1);
		});
	}

}
