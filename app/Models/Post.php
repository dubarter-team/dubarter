<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

/**
 * Class Post
 *
 * @package App\Models
 */
class Post extends \Dot\Posts\Models\Post
{

	/**
	 * @var string
	 */
	protected $table = "posts";

	/**
	 * @var array
	 */
	protected $appends = [
		"url",
		"category",
		"date",
		"real_views",
		"country"
	];

	/**
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	function getUrlAttribute()
	{

		if ($this->categories) {

			$main_category_slug = $this->categories->first() ? $this->categories->first()->slug : '';

			$country_code = $this->country ? $this->country->code : "eg";

			if ($this->format == "video") {
				return asset("ar-" . $country_code . "/videos/" . $main_category_slug . "/" . $this->slug);
			}

			if ($this->format == "article") {
				return asset("ar-" . $country_code . "/blog/" . $main_category_slug . "/" . $this->slug);
			}
		}

	}

	public function country()
	{
		return $this->hasOne(Place::class, "id", "country_id");
	}

	/**
	 * @return false|string
	 */
	function getDateAttribute()
	{
		return date("Y-m-d", strtotime($this->attributes["created_at"]));
	}

	/**
	 * @param $query
	 */
	function scopeAutobarter($query)
	{
		$query->whereHas("categories", function ($query) {
			$query->where("categories.id", Category::AUTOBARTER);
		});
	}

	function getSchemaAttribute()
	{
		$schema = [];

		if ($this->format == 'article') {
			$schema["@context"] = "http://schema.org";

			$schema["@type"] = "NewsArticle";

			$schema["mainEntityOfPage"]["@type"] = "WebPage";

			$schema["mainEntityOfPage"]["@id"] = $this->url;

			$schema["headline"] = $this->title;

			$schema["image"]["@type"] = "ImageObject";

			$schema["image"]["url"] = thumbnail($this->image->path, 'medium');

			$schema["image"]["height"] = "500";

			$schema["image"]["width"] = "300";

			$schema["datePublished"] = date("Y-m-d", strtotime($this->created_at));

			$schema["dateModified"] = date("Y-m-d", strtotime($this->updated_at));

			$schema["author"]["@type"] = "Person";

			$schema["author"]["name"] = "dubarter articles";

			$schema["publisher"]["@type"] = "Organization";

			$schema["publisher"]["name"] = "Dubarter";


			$schema["publisher"]["logo"]["@type"] = "ImageObject";

			$schema["publisher"]["logo"]["url"] = assets("frontend") . '/assets/img/logo.png';

			$schema["publisher"]["logo"]["width"] = "115";

			$schema["publisher"]["logo"]["height"] = "29";

			$schema["description"] = $this->excerpt;

			$schema["articleBody"] = strip_tags($this->content);

		} else if ($this->format == 'video') {

			$schema["@context"] = "http://schema.org";
			$schema["@type"] = "VideoObject";
			$schema["name"] = $this->title;
			$schema["description"] = $this->excerpt;
			$schema["uploadDate"] = (string)$this->created_at;
			$schema["duration"] = $this->media->length;
			$schema["publisher"]["@type"] = "Organization";
			$schema["publisher"]["name"] = "Dubarter";
			$schema["publisher"]["logo"]["@type"] = "ImageObject";
			$schema["publisher"]["logo"]["url"] = assets("frontend") . '/assets/img/logo.png';
			$schema["publisher"]["logo"]["width"] = 115;
			$schema["publisher"]["logo"]["height"] = 29;

			if ($this->media->provider == 'youtube') {
				$schema["thumbnailUrl"] = $this->media->provider_image;
				$schema["embedUrl"] = $this->media->path;
			} else {
				$schema["contentUrl"] = uploads_url($this->media->path);
				$schema["thumbnailUrl"] = 'https://dubarter.s3-eu-west-1.amazonaws.com/2018/03/medium-2398247304400053075.png';
			}

			$schema["interactionCount"] = $this->views + 300;

		}

		return $schema;
	}


	public function getRealViewsAttribute()
	{
		$post = DB::table('posts')->where('id', $this->id)->select('views')->first();

		return $post ? $post->views : 0;
	}

	/**
	 * @param $query
	 */
	function scopeAqarbarter($query)
	{
		$query->whereHas("categories", function ($query) {
			$query->where("categories.id", Category::AQARBARTETR);
		});
	}

	/**
	 * @param $query
	 */
	function scopeStylebarter($query)
	{
		$query->whereHas("categories", function ($query) {
			$query->where("categories.id", Category::STYLEBARTER);
		});
	}

	/**
	 * @param $query
	 */
	function scopeFoodbarter($query)
	{
		$query->whereHas("categories", function ($query) {
			$query->where("categories.id", Category::FOODBARTER);
		});
	}

	/**
	 * @param $query
	 * @param $slug
	 */
	function scopeSlug($query, $slug)
	{
		$query->where("slug", $slug);
	}

	/**
	 * @param $query
	 */
	function scopeMobawababarter($query)
	{
		$query->whereHas("categories", function ($query) {
			$query->where("categories.id", Category::MOBAWABABARTER);
		});
	}

	/**
	 * Categories relation
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function categories()
	{
		return $this->belongsToMany(Category::class, "posts_categories", "post_id", "category_id");
	}

	/**
	 * @return mixed
	 */
	function getCategoryAttribute()
	{

		return $this->categories->last();
	}

	/**
	 * @param $query
	 * @param $category
	 */
	function scopeInCategory($query, $category)
	{
		$query->whereHas("categories", function ($query) use ($category) {
			$query->where("categories.id", $category->id);
		});
	}

	/**
	 * @param $query
	 */
	function scopeFeatured($query)
	{
		$query->where("featured", 1);
	}

	/**
	 * Comments relation
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function comments()
	{
		return $this->hasMany(Comment::class, 'object_id', 'id');
	}

	/**
	 *  Global scopes
	 */
	static function boot()
	{


		static::addGlobalScope(function ($query) {

			$query->latest()->where("status", 1);

			if (Request::filled("country_id")) {

				$query->whereIn('country_id', [
					0,
					request("country_id")
				]);

			} else {

				if(config("country.id") == 21){
					$codes = [21, 0];
				}

				if(config("country.id") == 10){
					$codes = [10];
				}

				$query->whereIn("country_id", $codes);
			}

		});
	}


}
