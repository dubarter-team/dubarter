<?php

namespace App\Models;

class Tag extends \Dot\Tags\Models\Tag
{

    /**
     * ads relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function ads()
    {
        return $this->belongsToMany(Ad::class, 'ads_tags', 'tag_id', 'ad_id');
    }

    function posts()
    {
        return $this->belongsToMany(\App\Models\Api\Post::class, 'ads_tags', 'tag_id', 'post_id');
    }

    public static function indexed_ads_count($id)
    {
        $query = \App\Indices\Ad::where(function ($query) {
            $query->where('country.id', config("country.id"))
                ->orWhere('country.id', 0);
        });

        $count = $query->where('tags.id', $id)->count();

        return $count;
    }
}
