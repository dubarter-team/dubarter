<?php

namespace App\Models;

class Topic extends \Dot\Topics\Models\Topic
{

    protected $appends = ['custom_name'];


    function getCustomNameAttribute()
    {
        $name = ucfirst(strtolower((string)$this->name));

        return $name;
    }

    /**
     *  Global scopes
     */
    static function boot()
    {
        static::addGlobalScope(function ($query) {
            $query->where("status", 1);
        });
    }

}
