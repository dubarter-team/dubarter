<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class LiveComment extends Model
{

    protected $table = 'livestream_comments';

    /**
     * user relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

}
