<?php

namespace App\Models;

class Comment extends \Dot\Comments\Models\Comment
{

    /**
     * @var array
     */
    protected $appends = [
        "date"
    ];

    /**
     * @return false|string
     */
    function getDateAttribute()
    {
        return date("Y-m-d", strtotime($this->attributes["created_at"]));
    }

    protected $creatingRules = [
        'content' => 'required|min:2'
    ];
}
