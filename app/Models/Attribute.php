<?php

namespace App\Models;

use Dot\Media\Models\Media;

/**
 * Class Attribute
 *
 * @package App\Models
 */
class Attribute extends \Dot\Attributes\Models\Attribute
{

    protected $translatable = [
        "name",
    ];

    protected $casts = [
        "name" => "json"
    ];

    protected $appends = [
        "options"
    ];

    function scopeSlug($query, $field)
    {
        $query->where("slug", $field);
    }

    function getOptionsAttribute()
    {
        $options = json_decode($this->attributes["options"]);

        if($options){
            return (array) $options->{app()->getLocale()};
        }

        return [];
    }

    /**
     * big icon relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function big_icon()
    {
        return $this->hasOne(Media::class, "id", "big_icon_id");
    }

    /**
     * small icon relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function small_icon()
    {
        return $this->hasOne(Media::class, "id", "small_icon_id");
    }

}
