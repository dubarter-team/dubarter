<?php

namespace App\Models;

class Page extends \Dot\Pages\Models\Page
{

    public static function get_page($slug)
    {
        return self::where('slug', $slug)->where('status', 1)->where('lang', app()->getLocale())->first();
    }

}
