<?php

namespace App\Models;

use App\User;
use Illuminate\Support\Facades\Request;
use App\Models\Place;
use Illuminate\Database\Eloquent\Builder;

class Ad extends \Dot\Ads\Models\Ad
{

	/**
	 * liked_ads relation
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	function liked_by()
	{
		return $this->belongsToMany(User::class, 'ads_likes', 'ad_id', 'user_id');
	}

	/**
	 * @return string
	 */
	function getUrlAttribute()
	{
		return url("ads/" . $this->slug);
	}


	protected static function boot()
	{
		static::addGlobalScope(function (Builder $builder) {

			if($country = Place::getCurrentCountry()){
				$builder->where('country_id', $country->id);
			}

			if (Request::filled("country_id")) {
				$builder->where('country_id', request("country_id"));
			}

			$builder->where('status', 1);

		});
	}


}
