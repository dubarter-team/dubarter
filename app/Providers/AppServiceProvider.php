<?php

namespace App\Providers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use App\Models\Category;
use App\Models\Place;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        URL::forceScheme('https');

        $cats = Cache::remember('main-categories', 300, function () {
            return Category::where("parent", 0)->where('status', 1)->get();
        });

        $places = Cache::remember('all-countries', 300, function () {
            return place::where("parent", 0)->whereIn("id", [10, 21])->where('status', 1)->get();
        });

        view()->composer("*", function($view) use ($places, $cats){
            $view->with("_countries", $places);
            $view->with("_categories", $cats);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
