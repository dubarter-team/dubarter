<?php

function resizeRatio($image_source,$path,$sizes=[])
{
    $upload = $image_source->store('uploads/'.$path);
    foreach ($sizes as $size)
    {
        $width  = ($size[0])?$size[0]:"250";
        $height = ($size[1])?$size[1]:"250";
        if (!file_exists('uploads/'.$path.'/thumbnails/'.$width.'x'.$height)) {
            mkdir('uploads/'.$path.'/thumbnails/'.$width.'x'.$height, 0777, true);
        }
        $image_name = explode('/',$upload);

        //$canvas = Image::canvas($width, $height);
        $image  = Image::make($upload)->resize($width, $height, function($constraint)
        {
            $constraint->aspectRatio();
        });

        //$canvas -> insert($image, 'center');
        $image ->save('uploads/'.$path.'/thumbnails/'.$width.'x'.$height.'/'.end($image_name));
    }
    return $upload;
}
