<?php
/**
 * Created by PhpStorm.
 * User: elbardeny
 * Date: 5/22/18
 * Time: 12:47 PM
 */

function check_gdpr()
{
    $show = false;

    if (!isset($_COOKIE['gpdr_bar'])) {
        setcookie('gpdr_bar', 'true', time() + (10 * 365 * 24 * 60 * 60));

        $show = true;
    }

    return $show;
}

function is_rtl($string)
{
    $rtl_chars_pattern = '/[\x{0590}-\x{05ff}\x{0600}-\x{06ff}]/u';

    return preg_match($rtl_chars_pattern, $string);
}

function dir_class($string){
    return is_rtl($string) ? 'rtl-cont' : 'ltr-cont';
}