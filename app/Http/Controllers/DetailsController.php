<?php

namespace App\Http\Controllers;

use App\Events\AdViewed;
use App\Events\PostViewed;
use App\Indices\Ad;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Offer;
use App\Models\Page;
use App\Models\Place;
use App\Models\Post;
use App\User;
use Auth;
use Dot\Ads\Models\AdOldSlugs;
use Dot\Posts\Models\PostOldSlugs;
use Dot\Seo\Models\SEO;
use Illuminate\Support\Facades\Cache;
use Request;

/**
 * Class DetailsController
 *
 * @package App\Http\Controllers
 */
class DetailsController extends Controller
{
	/**
	 * @var array
	 */
	public $data = [];

	/**
	 * @param $slug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function ad($slug)
	{

		$query = Ad::withoutGlobalScopes()->byCountry()->where("slug", $slug);

		if (!Auth::guard('frontend')->check()) {
			$query->where("status", 1);
		}

		$ad = $query->first();

		if (!$ad) {
			$ad_old_slug = AdOldSlugs::where('slug', $slug)->first();

			if ($ad_old_slug) {
				$temp = \App\Models\Ad::where("id", $ad_old_slug->ad_id)->first();

				if ($temp) {
					return redirect($temp->url, 301)->send();
				}
			}
		}

		if (!$ad) {
			return app()->abort(404);
		}

		$this->data["ad"] = $ad;

		$this->data["seo"] = SEO::where('object_id', $ad->_id)->where('type', '2')->first();

		$this->data["ad_user"] = User::where("id", $ad->user["id"])->first();

		$this->data["main_category"] = $main_category = Category::where("id", $ad->main_category_id)->with(['banner'])
			//->withCount('videos')->withCount('articles')
			->first();

		$cat = collect($ad->categories)->where('direct', 1)->first();

		if ($cat) {
			$did = $cat['id'];
		} else {
			$did = $main_category->id;
		}

		$this->data['cat'] = $cat;

		$this->data["related_ads"] = Ad::byCategory($did)->whereNot('slug', $ad->slug)->take(8)->orderBy('published_at', 'desc')->get();

		$this->data["related_articles"] = Cache::remember('related-articles-' . config("country.id") . '-' . $main_category->id, 20, function () use ($main_category) {
			return Post::format("article")->with(['image'])->inCategory($main_category)->orderBy('published_at', 'desc')->take(4)->get();
		});

		$this->data["related_videos"] = Cache::remember('related-videos-' . config("country.id") . '-' . $main_category->id, 20, function () use ($main_category) {
			return Post::format("video")->with(['image'])->inCategory($main_category)->take(4)->orderBy('published_at', 'desc')->get();
		});

		$this->data["nav_categories"] = Category::get_nav_sub_categories($main_category->id);

		$this->search_bar($main_category);

		$this->data['offer'] = null;

		if (Auth::guard('frontend')->check()) {

			$this->AddOfferData($ad);
		}

		event(new AdViewed($ad));

		return view("ad", $this->data);
	}

	/**
	 * @param $category_slug
	 * @param $post_slug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function video($category_slug, $post_slug)
	{

		$this->data["main_category"] = $main_category = Category::slug($category_slug)->with(['banner'])
			//->withCount('videos')->withCount('articles')
			->first();

		$this->data["post"] = $post = Post::where("slug", $post_slug)->with(['image', 'media', 'categories', 'tags'])->first();

		if (!$post) {
			$post_old_slug = PostOldSlugs::where('slug', $post_slug)->first();

			if ($post_old_slug) {
				$temp = Post::where("id", $post_old_slug->post_id)->first();

				if ($temp && $main_category) {
					return redirect($temp->url, 301)->send();
				}
			}
		}

		if (!$post || !$main_category) {
			return app()->abort(404);
		}

		$this->data['comments'] = Comment::where('status', 1)->where('parent', 0)->where('object_id', $post->id)
			->with(['replies', 'user', 'user.photo', 'replies.user', 'replies.user.photo'])
			->take(5)->get();

		$this->data["nav_categories"] = Category::get_nav_sub_categories($main_category->id);

		$this->data['latest_videos'] = Cache::remember('related-videos-' . $main_category->id, 20, function () use ($main_category) {
			return Post::format("video")->with(['image'])->inCategory($main_category)->orderBy('published_at', 'desc')->take(4)->get();
		});

		$this->data['latest_articles'] = Cache::remember('related-articles-' . $main_category->id, 20, function () use ($main_category) {
			return Post::format("article")->with(['image'])->inCategory($main_category)->orderBy('published_at', 'desc')->take(4)->get();
		});

		$this->data['related_ads'] = Cache::remember('related-ads-' . $main_category->id, 20, function () use ($post) {
			return Ad::inCategory($post->category)->take(8)->orderBy('published_at', 'desc')->get();
		});

		$this->search_bar($main_category);

		event(new PostViewed($post));

		return view("article", $this->data);
	}

	/**
	 * @param $category_slug
	 * @param $post_slug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function article($category_slug, $post_slug)
	{

		$this->data["main_category"] = $main_category = Category::slug($category_slug)->with(['banner'])
			//->withCount('videos')->withCount('articles')
			->first();

		$this->data["post"] = $post = Post::where("slug", $post_slug)->with(['image', 'media', 'categories', 'tags'])->first();

		if (!$post) {
			$post_old_slug = PostOldSlugs::where('slug', $post_slug)->first();

			if ($post_old_slug) {
				$temp = Post::where("id", $post_old_slug->post_id)->first();

				if ($temp && $main_category) {
					return redirect($temp->url, 301)->send();
				}
			}
		}

		if (!$post || !$main_category) {
			return app()->abort(404);
		}

		// GEMI Comment this
		$this->data['comments'] = Comment::where('status', 1)->where('parent', 0)->where('object_id', $post->id)
			->with(['user', 'user.photo'])
			->orderBy('created_at', 'DESC')
			->take(5)
			->get();

		$this->data["nav_categories"] = Category::get_nav_sub_categories($main_category->id);

		$this->data['latest_videos'] = Cache::remember('related-videos-' . $main_category->id, 20, function () use ($main_category) {
			return Post::format("video")->with(['image'])->inCategory($main_category)->orderBy('published_at', 'desc')->take(4)->get();
		});

		$this->data['latest_articles'] = Cache::remember('related-articles-' . $main_category->id, 20, function () use ($main_category) {
			return Post::format("article")->with(['image'])->inCategory($main_category)->orderBy('published_at', 'desc')->take(4)->get();
		});

		$this->data['related_ads'] = Cache::remember('related-ads-' . $main_category->id, 20, function () use ($post) {
			return Ad::inCategory($post->category)->take(8)->orderBy('published_at', 'desc')->get();
		});

		$this->search_bar($main_category);

		event(new PostViewed($post));

		return view("article", $this->data);
	}

	/**
	 * Add Offer data to data array
	 * @param $ad
	 */
	private function addOfferData($ad)
	{
		$user = (Auth::guard('frontend'))->user();
		$this->data['isMyOwnAd'] = $isMyOwnAd = ($ad->user['id'] == $user->id);
		if ($isMyOwnAd) {
			$this->data['offers'] = Offer::with('receiver', 'sender')->where([
				'receiver_id' => (Auth::guard('frontend'))->user()->id,
				'parent' => 0,
				'ad_id' => $ad->_id,
			])->take(5)->get();
		} else {
			$this->data['offer'] = Offer::with(['newestReplies' => function ($query) {
				$query->take(5);
			}, 'newestReplies.sender', 'newestReplies.receiver'])->where([
				'sender_id' => (Auth::guard('frontend'))->user()->id,
				'parent' => 0,
				'ad_id' => $ad->_id,
			])->first();
		}

	}

	public function page($slug)
	{
		$this->data['page'] = $page = Page::get_page($slug);

		if (!$page) {
			abort(404);
		}

		return view('page', $this->data);
	}

	public function search_bar($main_category)
	{
		$this->data['cities'] = Place::get_local_cities();
	}

}
