<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use App\Models\Place;
use App\User;
use Dot\Ads\Models\AdOldSlugs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Index;

/**
 * Class AdsController
 *
 * @package App\Http\Controllers
 */
class AdsController extends Controller
{

	/**
	 * AdsController constructor.
	 */
	/*public function __construct()
	{

		$this->middleware(function ($request, $next) {

			if (Auth::guard('frontend')->check()) {
				return $next($request);
			} else {
				return redirect()->route('login', ['redirect' => Request::url()]);
			}

		});
	}*/

	/**
	 * @var array
	 */
	public $data = [];


	public function verification($user_id = false)
	{

		$user = User::where("id", $user_id)->first();

		if (!$user) {
			return abort(404);
		}

		if (Request::isMethod("post")) {

			// Code verification

			$code = request("code");

			$member = User::where("code", $code)->where("id", $user_id)->first();

			if (!$member) {
				return redirect()->back()
					->withErrors(trans("auth.incorrent_code"))
					->withInput(Request::all());
			}

			// Password validation

			$validator = Validator::make(Request::all(), [
				'password' => 'required|min:7',
				'repassword' => 'required|same:password',
			]);

			if ($validator->fails()) {
				return redirect()->back()
					->withErrors($validator)
					->withInput(Request::all());
			} else {

				// Activating user

				$user = User::where("code", $code)->first();

				$user->code = NULL;
				$user->password = request("password");
				$user->status = 1;
				$user->save();

				// Creating session

				Auth::guard('frontend')->login($user, true);

				return redirect()->route("profile.ads.index");
			}

		}

		return view("verification");

	}

	/**
	 * @param $slug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
	 */
	public function create()
	{
		$ad = new Ad();

		if (Request::isMethod("post")) {

			if (Auth::guard('frontend')->check()) {

				$user = Auth::guard('frontend')->user();

			} else {

				$email = request()->get("email");

				$user = User::where("email", $email)->first();

				$verification_code = Str::random(30);

				if (!$user) {

					// user is not exist. we should create a virtual user.

					$user = new User();

					$user->status = 0;
					$user->first_name = explode("@", $email)[0];
					$user->username = explode("@", $email)[0] . "-" . rand(1000, 10000);
					$user->email = $email;
					$user->country_id = request('country_id');
					$user->city_id = request('country_id');
					$user->phone = request('phone');
					$user->code = $verification_code;

					$user->save();

				} else {

					$user->code = $verification_code;
					$user->save();
				}


			}

			$category = Category::where('id', request('category_id'))->with(['custom_attributes', 'parent_category'])->first();

			$validator = Validator::make(Request::all(), [
				'title' => 'required',
				"category_id" => "required|min:1",
				"country_id" => "required|min:1",
				'email' => 'required|email',
				'phone' => 'required',
			]);

			$validator->after(function ($validator) use ($category) {
				if ($category && $category->listing_type != 'ads') {
					$validator->errors()->add('category_id', trans('ad.cat_not_allowed'));
				}
			});

			if (!$category) {
				abort(404);
			}

			$custom_attributes = ($category->listing_type == 'ads') ? Category::get_attributes($category) : collect([]);

			//validating attributes

			$valid_attrs = true;

			if ($custom_attributes->count()) {
				$attrs_values = Request::get('attrs');

				foreach ($custom_attributes as $attr) {

					if ($attr->required) {
						if (!isset($attrs_values[$attr->slug])) {
							$valid_attrs = false;
						} else if ($attr->data_type == 'text' && $attr->template == 'input_text' && trim($attrs_values[$attr->slug]) == '') {
							$valid_attrs = false;
						} else if ($attr->data_type == 'number' && $attr->template == 'input_text' && !is_numeric($attrs_values[$attr->slug])) {
							$valid_attrs = false;
						}
					}

				}

			}

			$validator->after(function ($validator) use ($valid_attrs) {
				if (!$valid_attrs) {
					$validator->errors()->add('category_id', trans('ad.incorrect_data'));
				}
			});

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput(Request::all());
			}

			$ad->title = request('title');
			$ad->price = request('price');
			$ad->content = request('content');
			$ad->country_id = request('country_id');
			$ad->city_id = request('city_id');
			$ad->region_id = request('region_id');
			$ad->email = request('email');
			$ad->phone = request('phone');
			$ad->featured = request('featured');
			$ad->status = 2;
			$ad->user_id = $user->id;
			$ad->lat = request('lat', '');
			$ad->lng = request('lng', '');

			$ad->source = 'frontend';

			$ad->expired_at = request('expire_at');

			$country = Place::where('parent', 0)->where('id', Request::get('country_id'))->where('status', 1)->first();

			$ad->currency = $country->currency->toArray();

			$ad->save();

			//saving categories
			$cat = $category;

			while (true) {
				$ad->categories()->attach($cat->id, ['direct' => ($cat->id == $category->id ? 1 : 0)]);

				if ($cat->parent == 0) {
					break;
				} else {
					$cat = $cat->parent_category;
				}
			}

			//saving media
			$media_ids = request('media_ids', []);

			if (count($media_ids)) {
				$i = 0;

				foreach ($media_ids as $media_id) {
					$ad->media()->attach($media_id, ['order' => $i]);

					$i++;
				}
			}

			//saving attributes
			$custom_attributes = Category::get_attributes($category);

			$ad->custom_attributes()->detach();

			if ($custom_attributes->count()) {
				$attrs_values = request('attrs');

				foreach ($custom_attributes as $attr) {
					$value = '';

					if (isset($attrs_values[$attr->slug])) {
						if ($attr->type == 'single') {
							$value = [$attrs_values[$attr->slug]];
						} elseif ($attr->type == 'multiple') {
							$value = $attrs_values[$attr->slug];
						}

						$ad->custom_attributes()->attach($attr->id, ['value' => $value]);
					}

				}

			}

			Index::save($ad->id);

			if (Auth::guard('frontend')->check()) {
				return redirect()->route('profile.ads.index')->with('status', trans('ad.ad_created'));
			} else {

				// Send verification code

				$payload = [
					'user' => $user,
					'code' => $verification_code
				];

				Mail::send('emails.verification', $payload, function ($m) use ($user) {
					$m->from('info@dubarter.com', 'Dubarter');
					$m->to($user->email, $user->first_name)
						->subject(trans("auth.verification_title"));
				});

				return redirect("verification/".$user->id)->with('message', trans('auth.verification_sent'));
			}

		}

		$this->data["ad"] = $ad;

		$this->data['categories'] = Category::where('parent', 0)->where('frontend_user_enabled', 1)->get();

		$this->data['attributes_html'] = '';

		$this->data["ad_tags"] = [];

		$this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

		$this->data["cities"] = [];

		$this->data["regions"] = [];

		return view('create_ad', $this->data);
	}

	public function edit($id)
	{

		$ad = Ad::where('id', $id)->where('user_id', Auth::guard('frontend')->user()->id)->with(['categories', 'tags', 'media', 'user', 'custom_attributes'])->first();

		if (!$ad) {
			abort(404);
		}

		$direct_category = $ad->categories->where('pivot.direct', 1)->first();

		$custom_attributes = Category::get_attributes($direct_category);


		if (Request::isMethod("post")) {
			$old_title = $ad->title;
			$old_slug = $ad->slug;

			$revision = false;

			$old_ad = null;

			$cat_changed = false;

			$validator = Validator::make(Request::all(), [
				'title' => 'required',
				"category_id" => "required|min:1",
				"country_id" => "required|min:1",
				'email' => 'required|email',
				'phone' => 'required',
			]);

			//validating attributes

			$category = null;

			if (request('category_id', 0) != $direct_category->id) {
				$cat_changed = true;

				$category = Category::where('id', request('category_id'))->with(['custom_attributes', 'parent_category'])->first();

				$custom_attributes = ($category->listing_type == 'ads') ? Category::get_attributes($category) : collect([]);
			}

			$validator->after(function ($validator) use ($category) {
				if ($category && $category->listing_type != 'ads') {
					$validator->errors()->add('category_id', trans('ad.cat_not_allowed'));
				}
			});

			$valid_attrs = true;

			if ($custom_attributes->count()) {
				$attrs_values = Request::get('attrs');

				foreach ($custom_attributes as $attr) {

					if ($attr->required) {
						if (!isset($attrs_values[$attr->slug])) {
							$valid_attrs = false;
						} else if ($attr->data_type == 'text' && $attr->template == 'input_text' && trim($attrs_values[$attr->slug]) == '') {
							$valid_attrs = false;
						} else if ($attr->data_type == 'number' && $attr->template == 'input_text' && !is_numeric($attrs_values[$attr->slug])) {
							$valid_attrs = false;
						}
					}

				}

			}

			$validator->after(function ($validator) use ($valid_attrs) {
				if (!$valid_attrs) {
					$validator->errors()->add('category_id', trans('ad.incorrect_data'));
				}
			});

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput(Request::all());
			}

			if ($ad->status == 3) {
				$revision = true;

				$old_ad = $ad;

				$ad = new Ad();

				$ad->user_id = $old_ad->user_id;
			}

			$ad->title = request('title');
			$ad->price = request('price');
			$ad->content = request('content');
			$ad->country_id = request('country_id');
			$ad->city_id = request('city_id');
			$ad->region_id = request('region_id');
			$ad->email = request('email');
			$ad->phone = request('phone');
			$ad->featured = request('featured');
			$ad->status = 2;
			$ad->lat = request('lat', '');
			$ad->lng = request('lng', '');

			$ad->expired_at = request('expire_at');

			if (!$ad->validate()) {
				return Redirect::back()->withErrors($ad->errors())->withInput(Request::all());
			}

			if ($old_title != Request::get('title')) {
				$new_slug = $ad->new_slug(str_slug_utf8(Request::get('title')), 'slug');

				$ad_old_slug = new AdOldSlugs();

				$ad_old_slug->ad_id = $ad->id;

				$ad_old_slug->slug = $old_slug;

				$ad_old_slug->save();

				$ad->slug = $new_slug;
			}

			$ad->save();

			if ($revision) {
				$old_ad->parent = $ad->id;

				$old_ad->save();
			}

			//saving categories
			if (request('category_id', 0) != $direct_category->id || $revision) {

				if (!$cat_changed) {
					$category = Category::where('id', request('category_id'))->with(['custom_attributes', 'parent_category'])->first();

					$custom_attributes = Category::get_attributes($category);
				}

				$ad->categories()->detach();

				$cat = $category;

				while (true) {
					$ad->categories()->attach($cat->id, ['direct' => ($cat->id == $category->id ? 1 : 0)]);

					if ($cat->parent == 0) {
						break;
					} else {
						$cat = $cat->parent_category;
					}
				}
			}

			//saving media
			$ad->media()->detach();

			$media_ids = request('media_ids', []);

			if (count($media_ids)) {
				$i = 0;

				foreach ($media_ids as $media_id) {
					$ad->media()->attach($media_id, ['order' => $i]);

					$i++;
				}
			}

			//saving attributes
			$ad->custom_attributes()->detach();

			if ($custom_attributes->count()) {
				$attrs_values = request('attrs');

				foreach ($custom_attributes as $attr) {
					$value = '';

					if (isset($attrs_values[$attr->slug])) {
						if ($attr->type == 'single') {
							$value = [$attrs_values[$attr->slug]];
						} elseif ($attr->type == 'multiple') {
							$value = $attrs_values[$attr->slug];
						}

						$ad->custom_attributes()->attach($attr->id, ['value' => $value]);
					}

				}

			}

			return redirect()->route('ads.edit', ['id' => $ad->id])->with('message', trans('ad.ad_updated'));
		}


		$this->data["ad"] = $ad;

		$this->data["direct_category"] = $direct_category;

		$this->data["categories"] = Category::where('parent', 0)->where('status', 1)->get();;

		$this->data["attributes"] = $custom_attributes;

		$this->data['attributes_html'] = view('partials.ad.custom_attributes', ['attributes' => $custom_attributes, 'custom_attributes' => $ad->custom_attributes])->render();

		$this->data["ad_tags"] = $ad->tags->pluck("name")->toArray();

		$this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

		$this->data["cities"] = $ad->country_id ? Place::where('parent', $ad->country_id)->where('status', 1)->get() : [];

		$this->data["regions"] = $ad->city_id ? Place::where('parent', $ad->city_id)->where('status', 1)->get() : [];


		return view('create_ad', $this->data);
	}

	public function sold($id)
	{
		Ad::where('id', $id)->update(array('status' => 4));

		return Redirect::back();
	}

	/**
	 * GET /profile/ads
	 * @routeName profile.ads.index
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
	 * @throws \Throwable
	 */
	public function myAds()
	{

		$this->data['per_page'] = $per_page = 10;

		$this->data['offset'] = $offset = Request::get('offset', 0);

		$user = Auth::guard('frontend')->user();

		$this->data['ads'] = Ad::withoutGlobalScopes()->take($per_page)
			->where('user_id', $user->id)
//			->where(function ($query) {
////				$query->where('country_id', config("country.id"))
////					->orWhere('country_id', 0);
////			})
			->where('parent', 0)
			->skip($offset)
			->orderBy('updated_at', 'desc')
			->get();

		$this->data['ads_count'] = count($this->data['ads']);

		if (Request::ajax()) {
			$this->data['ads_html'] = view('partials.ad.ads-list', $this->data)->render();

			return response()->json($this->data);
		}


		return view('profile-ads', $this->data);
	}

	/**
	 * GET /profile/ads
	 * @routeName profile.ads.index
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
	 * @throws \Throwable
	 */
	public function my_favourites()
	{

		$this->data['per_page'] = $per_page = 10;

		$this->data['offset'] = $offset = Request::get('offset', 0);

		$user = Auth::guard('frontend')->user();

		$this->data['ads'] = Ad::take($per_page)
			->whereHas('liked_by', function ($query) use ($user) {
				$query->where('user_id', $user->id);
			})
			->skip($offset)
			->get();

		$this->data['ads_count'] = count($this->data['ads']);

		if (Request::ajax()) {
			$this->data['ads_html'] = view('partials.ad.favourites_list', $this->data)->render();

			return response()->json($this->data);
		}


		return view('favourites', $this->data);
	}
}
