<?php

namespace App\Http\Controllers\Profile;

use App\Models\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    /**
     * @var array
     */
    public $data = [];

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (auth('frontend')->check()) {
                return $next($request);
            } else {
                return redirect()->route('login');
            }

        });
    }

    /**
     * GET/POST  profile/edit
     * @routeName profile.edit.form
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profileEdit(Request $request)
    {
        $this->data['user'] = $user = auth('frontend')->user();
        if ($request->isMethod("post")) {

            $validator = Validator::make($request->all(), [
                "first_name" => "required|alpha",
                "last_name" => "required|alpha",
            ], trans('validation.profile-validation'));

            $validator->sometimes('password', 'same:repassword|min:6', function () use ($request) {
                return $request->filled('password') || $request->filled('repassword');
            });

            $validator->sometimes('username', ['required', 'min:2', 'max:50', 'regex:/^[a-zA-Z]([._-]?[a-zA-Z0-9]+)*$/', Rule::unique('users')->ignore($user->id)], function () use ($request, $user) {
                return ((trim($request->get('username')) != trim($user->username)) && (!$user->provider));
            });

            $validator->sometimes('photo', 'required|image', function ($input) use ($request, $user) {
                return $request->filled('photo');
            });

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');

            if (!$user->provider) {
                $user->username = $request->get('username');
            }
            $user->phone = $request->get('phone');

            if ($request->filled('password')) {
                $user->password = $request->get('password');
            }

            if ($request->hasFile('photo')) {
                $media = new Media();
                $user->photo_id = $media->saveFile($request->photo);
            }

            $user->edited = 1;

            $user->save();

            return redirect()->route('profile.edit.form')->with(['message' => trans('common.profile-updated')]);
        }
        $this->data['user'] = auth('frontend')->user();
        return view('profile-edit', $this->data);
    }
}
