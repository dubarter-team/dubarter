<?php

namespace App\Http\Controllers\Profile;

use App\Models\Ad;
use App\Models\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class OffersController extends Controller
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * OffersController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (Auth::guard('frontend')->check()) {
                return $next($request);
            } else {
                return redirect()->route('login', ['redirect' => Request::url()]);
            }

        });
    }

    /**
     * GET profile/offers
     * @routeName profile.offers.index
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    function index(Request $request)
    {

        $this->data['per_page'] = $per_page = 10;

        $this->data['offset'] = $offset = $request->get('offset', 0);

        $user = Auth::guard('frontend')->user();

        $this->data['offers'] = Offer::where('parent', 0)->with(['ad'])
            ->where(function ($query) use ($user) {

                $query->Where('sender_id', $user->id);

            })
            ->offset($offset)
            ->take($per_page)
            ->orderBy('created_at', 'desc')
            ->get();

        $this->data['offers_count'] = count($this->data['offers']);

        if ($request->ajax()) {

            $this->data['offers_html'] = view('partials.offers.offers', $this->data)->render();

            return response()->json($this->data);

        }

        return view('profile-offers', $this->data);
    }

    /**
     * POST profile/offers
     * @routeName profile.offers.add
     * AJAX only
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addOffer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|min:1',
            'price' => 'required|numeric',
            'type' => 'required|in:bid,bart'
        ], trans('validation.offer-validation'));

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()->all()]);
        }

        $ad = Ad::where('slug', $request->get('ad_slug'))->first();

        $offer = new Offer();
        $offer->sender_id = Auth::guard('frontend')->user()->id;
        $offer->price = $request->get('price');
        $offer->comment = $request->get('comment');
        $offer->type = $request->get('type');
        $offer->ad_id = $ad->id;
        $offer->receiver_id = $ad->user_id;
        $offer->status = 1;

        $offer->save();

        $offer_html = view('partials.offers.my-offers',['offers'=>collect([$offer]),'ad_id'=>$ad->id, 'mine' => false])->render();

        return response()->json(['status' => true, 'offer_html' => $offer_html]);
    }


    /**
     * POST profile/offers/reply
     * @routeName profile.offers.reply
     *  AJAX only
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addReply(Request $request)
    {
        $parent_offer = Offer::where('id', $request->get('offer_id'))->firstOrFail();

        $user = Auth::guard('frontend')->user();

        $ad = Ad::findOrFail($parent_offer->ad_id);

        // is My Own Ad

        $isMyOwnAd = ($ad->user_id == $user->id);
        $offer = new Offer();
        $offer->sender_id = Auth::guard('frontend')->user()->id;
        $offer->price = $parent_offer->price;
        $offer->comment = $request->get('comment');
        $offer->type = $parent_offer->type;
        $offer->ad_id = $parent_offer->ad_id;
        $offer->receiver_id = $isMyOwnAd ? $parent_offer->sender_id : $parent_offer->receiver_id;
        $offer->parent = $parent_offer->id;
        $offer->status = 1;

        $offer->save();

        $this->data['repliesHtml'] = view('partials.offers.replies-list', ['replies' => [$offer]])->render();

        $this->data['status'] = true;

        return response()->json($this->data);
    }

    /**
     * GET profile/offers/replies
     * @routeName profile.offers.replies
     *  AJAX only
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getReplies(Request $request)
    {
        $this->data['per_page'] = $per_page = $request->get('limit',5);

        $this->data['offset'] = $offset = $request->get('offset', 0);

        $user = Auth::guard('frontend')->user();

        $this->data['offers'] = $offers = Offer::with(['receiver', 'sender'])
            ->where(function ($query) use ($user) {
                $query->Where('receiver_id', $user->id);
                $query->OrWhere('sender_id', $user->id);
            })
            ->where('parent', $request->get('offer_id'))
            ->orderBy('created_at', 'desc')
            ->offset($offset)
            ->take($per_page)
            ->get();

        $this->data['repliesHtml'] = view('partials.offers.replies-list', ['replies' => $offers])->render();

        $this->data['count'] = count($this->data['offers']);

        $this->data['status'] = true;

        return response()->json($this->data);
    }


    /**
     * GET profile/offers/json
     * @routeName profile.offers.json
     * Ajax only
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getOffersJson(Request $request)
    {
        $this->data['per_page'] = $per_page = 5;
        $this->data['offset'] = $offset = $request->get('offset', 0);

        $user = Auth::guard('frontend')->user();

        $query = Offer::with(['receiver', 'sender'])
            ->where('parent', 0)
            ->orderBy('created_at', 'desc');

        $query->where('ad_id', $request->get('ad_id', '0'));

        $this->data['offers'] = $offers = $query->offset($offset)
            ->take($per_page)
            ->get();

        $this->data['offersHtml'] = view('partials.offers.offers-list', ['offers' => $offers])->render();

        $this->data['count'] = count($this->data['offers']);

        $this->data['status'] = true;

        return response()->json($this->data);
    }
}
