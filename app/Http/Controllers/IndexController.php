<?php

namespace App\Http\Controllers;

use App\Index;
use App\Models\Ad as AdModel;
use Request;

/**
 * Class IndexController
 *
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * @var int
     */
    public $limit = 100;

    /**
     * @param int $offset
     *
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function index($offset = 0)
    {

        $ads = AdModel::withoutGlobalScopes()->select("id")->orderBy("id", "DESC")->take($this->limit)->skip($offset)->get();

        if (count($ads)) {

            foreach ($ads as $ad) {
                Index::save($ad->id);
            }

            $offset += $this->limit;

            return redirect()->route("index.cron", ["offset" => $offset]);

        } else {
            return "done";
        }
    }

}
