<?php

namespace App\Http\Controllers;

use App\Indices\Ad;
use App\Models\Place;
use App\Models\Tag;
use Request;

class TagsController extends Controller
{


    /**
     * @var array
     */
    public $data = [];

    /**
     * GET /tag/{slug}
     * @routeName tags.index
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @throws \Throwable
     */
    public function index($slug)
    {

        $this->data['cities'] = Place::where('parent', config("country.id"))->get();

        $this->data['regions'] = Request::filled('city_id') ? Place::where('parent', Request::get('city_id'))->get() : collect([]);

        $this->data['tag'] = $tag = Tag::where('slug', $slug)->firstOrFail();

        $this->data['per_page'] = $per_page = 24;

        $ads = $this->getAds($tag);

        $this->data['ads_html'] = view('partials.search.ads_listing', ['ads' => $ads, 'half_count' => $per_page / 2])->render();

        $this->data['ads_count'] = count($ads);

        $this->data['ads_total'] = $ads->total;

        $this->data['offset'] = ((request('offset', 0) % $per_page == 0) ? request('offset', 0) : 0) + count($ads);

        return view("tag", $this->data);
    }

    /**
     * GET /tag/{slug}/paginate
     * @routeName tags.paginate
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function tags_paginate($slug)
    {
        $tag = Tag::where('slug', $slug)->first();

        // Get ads
        $ads = $this->getAds($tag);
        $per_page = request('per_page', 24);

        $res['ads_html'] = view('partials.search.ads_listing', ['ads' => $ads, 'half_count' => $per_page / 2])->render();

        $res['ads_count'] = count($ads);

        return response()->json($res);
    }

    /**
     * Ads Query Prepare
     * @param $tag
     * @return mixed
     */
    private function getAds($tag)
    {
        $per_page = 24;

        $offset = (request('offset', 0) % $per_page == 0) ? request('offset', 0) : 0;

        $query = Ad::where(function ($query) {
            $query->where('country.id', config("country.id"))
                ->orWhere('country.id', 0);
        });

        if (Request::filled('city_id')) {
            $query->where("city_id", Request::get('city_id'));

            if (Request::filled('region_id')) {
                $query->where("region_id", Request::get('region_id'));
            }
        }

        // Tags
        $query->where('tags.id', $tag->id);

        $ads = $query->take($per_page)->skip($offset)->get();

        return $ads;
    }

}
