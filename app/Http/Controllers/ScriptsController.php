<?php

namespace App\Http\Controllers;

use App\Index;
use Illuminate\Support\Facades\DB;
use Request;

/**
 * Class ScriptsController
 *
 * @package App\Http\Controllers
 */
class ScriptsController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * @var int
     */
    public $limit = 100;

    /**
     * @param int $offset
     *
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function ads_shift($offset = 0)
    {

        $ads = DB::table("ads")
            ->where("id", ">=", 55506)
            ->where("id", "<", 100000)
            ->orderBy("id", "ASC")
            ->take($this->limit)
            ->skip($offset)
            ->get();

        if (count($ads)) {

            foreach ($ads as $ad) {

                DB::table("ads")->where("id", $ad->id)->update([
                    "id" => $this->get_new_id($ad->id)
                ]);

                DB::table("ads_categories")->where("ad_id", $ad->id)->update([
                    "ad_id" => $this->get_new_id($ad->id)
                ]);

                DB::table("ads_tags")->where("ad_id", $ad->id)->update([
                    "ad_id" => $this->get_new_id($ad->id)
                ]);

                DB::table("ads_media")->where("ad_id", $ad->id)->update([
                    "ad_id" => $this->get_new_id($ad->id)
                ]);

                DB::table("ads_attributes_values")->where("ad_id", $ad->id)->update([
                    "ad_id" => $this->get_new_id($ad->id)
                ]);

                DB::table("offers")->where("ad_id", $ad->id)->update([
                    "ad_id" => $this->get_new_id($ad->id)
                ]);

                Index::delete($ad->id);
                Index::save($this->get_new_id($ad->id));
            }

            $offset += $this->limit;

            return redirect()->route("scripts.ads_shift", ["offset" => $offset]);

        } else {

            return "done";

        }
    }

    function get_new_id($old_id)
    {
        return $old_id + 100000;
    }

}
