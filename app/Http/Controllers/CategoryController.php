<?php

namespace App\Http\Controllers;

use App\Indices\Ad;
use App\Models\Category;
use App\Models\Place;
use App\Models\Post;
use App\Models\Topic;
use Illuminate\Support\Facades\Cache;
use Request;

/**
 * Class CategoryController
 *
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
	/**
	 * @var array
	 */
	public $data = [];

	/**
	 * @param $path
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index($path)
	{

		$all_ads = false;

		$slugs = explode("/", $path);

		if (count($slugs) && $slugs[0] == 'all-ads') {
			$all_ads = true;

			unset($slugs[0]);
		}

		if (count($slugs) == 1) {
			if ($all_ads) {
				abort(404);
			}

			return $this->main_category($slugs);

		} else if (count($slugs) > 1) {

			return $this->sub_category($slugs, $all_ads);

		} else {

			abort(404);

		}

	}

	/**
	 * @param $path
	 */
	public function main_category($slugs)
	{

		$cache_key = "data:". $slugs[0] . "-" . config("country.code") . "-" . app()->getLocale();

		if (Cache::has($cache_key)) {

			$data = Cache::get($cache_key);

			return response($data)->send();
		}

		$this->data["main_category"] = $main_category = Category::slug($slugs[0])
			->with(['custom_attributes', 'custom_attributes.small_icon', 'banner', 'image'])
			//->withCount('videos')->withCount('articles')
			->leftJoin("categories_meta", function ($query) {
				$query->on("categories_meta.category_id", "=", "categories.id")
					->where("categories_meta.country_id", config("country.id"));
			})
			->where('parent', 0)
			->first();


		if (!$main_category) {
			app()->abort(404);
		}

		if ($main_category->missing_parent) {
			return redirect('السيارات', 301);
		}


		//  dd($main_category);
		// search bar

		$this->data['cities'] = Place::get_local_cities();

		// Sections appeared in header navigation menu

		$this->data["nav_categories"] = Category::get_nav_sub_categories($main_category->id);

		// Main slider ads

		$this->data["featured_ads"] = Cache::remember('category-featured-ads-' . $main_category->id, 10, function () use ($main_category) {
			$query = Ad::inCategory($main_category);

			if ($main_category->id == Category::FOODBARTER) {
				$query->where("categories.id", '!=', 9);
			}

			return $query->featured()->take(6)->orderBy('published_at', 'desc')->get();
		});

		// get direct sub categories

		$this->data["sub_categories"] = Category::get_sub_categories($main_category->id);

		// get sub categories ads

		if (count($this->data["sub_categories"])) {
			foreach ($this->data["sub_categories"] as $sub_cat) {

				$this->data['cat_' . $sub_cat->id . '_ads'] = Cache::remember('sub-categories-ads-' . $sub_cat->id, 5, function () use ($sub_cat) {
					return Ad::inCategory($sub_cat)
						->featured()->take(6)->orderBy('published_at', 'desc')->get();
				});

			}
		}

		// Featured videos

		$this->data["featured_videos"] = Cache::remember('categories-videos-' . $main_category->id, 10, function () use ($main_category) {
			return Post::inCategory($main_category)
				->featured()->format("video")->take(6)->orderBy('published_at', 'desc')->get();
		});

		// Featured articles

		$this->data["featured_articles"] = Cache::remember('categories-articles-' . $main_category->id, 10, function () use ($main_category) {
			return Post::inCategory($main_category)
				->featured()->format("article")->take(6)->orderBy('published_at', 'desc')->get();
		});

		// Topics

		$this->data['topics'] = Cache::remember('categories-topics-' . $main_category->id, 10, function () use ($main_category) {
			return Topic::whereHas("categories", function ($query) use ($main_category) {
				$query->where("categories.id", $main_category->id);
			})->with(['tags'])->get();
		});


		$html = view("category", $this->data)->render();

		Cache::put($cache_key, $html, 1000);

		return $html;
	}

	public function sub_category($slugs, $all_ads = false)
	{

		$temp = 0;

		$i = 0;

		$main_category = null;

		$category = null;

		$categories_list = [];

		foreach ($slugs as $sl) {

			$cat = Cache::remember('sub-categories-slugs-' . implode('-', $slugs) . '-' . $sl . '-' . $i, 100, function () use ($slugs, $sl, $i, $temp) {
				$q = Category::where('parent', $temp)->with(['image']);

				if ($i == (count($slugs) - 1)) {
					$q->withCount(['categories' => function ($query) {
						$query->has('ads');
					}]);
				}

				if ($i == 0) {
					$q->with(['banner'])
						//->withCount('videos')
						//->withCount('articles')
						->where('parent', 0);
				}

				return $q->slug($sl)->first();
			});

			if ($cat) {

				if ($i == 0) {
					$main_category = $cat;
				} else if ($i == (count($slugs) - 1)) {
					$category = $cat;
				}

				$temp = $cat->id;

				$categories_list[] = $cat;

			} else {
				abort(404);
			}

			$i++;
		}

		if ($main_category->missing_parent) {
			return redirect('السيارات', 301);
		}

		$this->data["main_category"] = $main_category;

		$this->data["category"] = $category;

		$this->data["categories_list"] = $categories_list;

		// Sections appeared in header navigation menu

		$this->data["nav_categories"] = Category::get_nav_sub_categories($main_category->id);

		// sidebar tree

		$this->data['main_sub_categories'] = $main_subs = Cache::remember('sub-categories-with-ads-count-' . $main_category->id, 10, function () use ($main_category) {
			return Category::where('parent', $main_category->id)
				->leftJoin("categories_meta", function ($query) {
					$query->on("categories_meta.category_id", "=", "categories.id")
						->where("categories_meta.country_id", config("country.id"));
				})
				->with(['categories' => function ($query) {
					$query
						->withCount('ads')->has('ads')
						->orderBy('id', 'desc');
				}])
				->withCount('ads')->where('status', 1)->get();
		});

		$this->data['cities'] = Place::get_local_cities();

		// videos and articles

		$this->data['latest_videos'] = Cache::remember('sub-categories-videos-' . $main_category->id, 10, function () use ($main_category) {
			return Post::format("video")->inCategory($main_category)->take(3)->get();
		});

		$this->data['latest_articles'] = Cache::remember('sub-categories-articles-' . $main_category->id, 10, function () use ($main_category) {
			return Post::format("article")->inCategory($main_category)->take(3)->get();
		});

		// objects

		$this->data['all_ads'] = $all_ads;

		if ($category->listing_type == 'ads' || $category->categories_count == 0 || $all_ads) {

			$per_page = 12;

			$ads = $this->get_sub_category_ads($category->id, $per_page);

			$this->data['objects_count'] = count($ads);

			$this->data['objects_html'] = view('partials.category.sub_category_ads', ['ads' => $ads, 'per_page' => $per_page])->render();

			$this->data['per_page'] = $per_page;

		} else if ($category->listing_type == 'categories_1') {

			$per_page = 30;

			$subs = $this->get_sub_category_subs($category->id, $per_page);

			$this->data['objects_count'] = count($subs);

			$this->data['objects_html'] = view('partials.category.sub_category_subs_1', ['cats' => $subs, 'per_page' => $per_page])->render();

			$this->data['per_page'] = $per_page;

		} else if ($category->listing_type == 'categories_2') {

			$per_page = 24;

			$subs = $this->get_sub_category_subs($category->id, $per_page);

			$this->data['objects_count'] = count($subs);

			$this->data['objects_html'] = view('partials.category.sub_category_subs_2', ['cats' => $subs, 'per_page' => $per_page])->render();

			$this->data['per_page'] = $per_page;

		}


//		$query = AdModel::where("sponsored", 1);
//
//		$query->whereHas("categories", function ($query) use($categories_list){
//			$query->where("categories.id", end($categories_list)->id);
//		});
//
//		$sponsored = $query->take(2)->orderBy('published_at', 'desc')->get();
//
//
//		if(count($sponsored)){
//
//			$sponsored_ids = $sponsored->pluck("id")->toArray();
//
//			//$this->data["sponsored_ads"] = Ad::whereIn('id', $sponsored_ids)->orderBy('published_at', 'desc')->get();
//			$this->data["sponsored_ads"] = Ad::take(2)->orderBy('published_at', 'desc')->get();
//		}else{
//			$this->data["sponsored_ads"] = [];
//		}
//


		$this->data["sponsored_ads"] = Ad::byCategory(end($categories_list)->id)
			->where("sponsored", 1)
			->orderBy('created_at', 'desc')
			->take(2)
			->get();


		return view("sub_category", $this->data);
	}

	public function get_sub_category_ads($id, $limit = 12)
	{

		$offset = request('offset', 0);

		$query = Ad::byCategory($id);

		$query->orderBy('created_at', 'desc');

		$ads = $query->take($limit)->skip($offset)->get();

		return $ads;

	}

	public function get_sub_category_subs($id, $limit = 30)
	{
		$offset = request('offset', 0);

		$query = Category::where("parent", $id);

		//$query->orderBy('ads_count', 'desc');

		$query->with(['logo']);

		$query->withCount('ads')->has('ads');

		$ads = $query->take($limit)->skip($offset)->get();

		return $ads;

	}

	public function sub_category_paginate()
	{
		$res = ['status' => false, 'objects_html' => '', 'objects_count' => 0];

		$id = request('category_id', 0);

		$all_ads = request('all_ads', 0);

		$category = Category::where('id', $id)->withCount('categories')->first();

		if ($category) {

			$res['status'] = true;

			if ($category->listing_type == 'ads' || $category->categories_count == 0 || $all_ads) {
				$ads = $this->get_sub_category_ads($category->id, 12);

				$res['objects_count'] = count($ads);

				$res['objects_html'] = view('partials.category.sub_category_ads', ['ads' => $ads, 'per_page' => 12])->render();

			} else if ($category->listing_type == 'categories_1') {
				$subs = $this->get_sub_category_subs($category->id, 30);

				$res['objects_count'] = count($subs);

				$res['objects_html'] = view('partials.category.sub_category_subs_1', ['cats' => $subs, 'per_page' => 32])->render();

			} else if ($category->listing_type == 'categories_2') {
				$subs = $this->get_sub_category_subs($category->id, 24);

				$res['objects_count'] = count($subs);

				$res['objects_html'] = view('partials.category.sub_category_subs_2', ['cats' => $subs, 'per_page' => 24])->render();

			}
		}

		return response()->json($res);
	}
}
