<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Dot\Media\Models\Media;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;

class MediaRegenerator extends Controller
{


    function resize($id = 0)
    {

        $images = Media::where("type", "image")
            ->where('id', $id)
            //->where("regenerated", 0)
            // ->inRandomOrder()
            //->orderBy("created_at", "DESC")
            ->get();

        if (count($images)) {

            foreach ($images as $image) {

                $this->regenerate($image->path);


                unset($image);
            }


            dd("done");

        } else {

            dd("not found");

        }
    }

    function regenerate_media($offset = 0)
    {

        Config::set("media.sizes", [
            'video_thumbnail' => [248, 300]
        ]);
        //$posts = Post::where('thumbnail_id', '!=', 0)->get();

        //$media_ids = $posts->pluck(['thumbnail_id']);

//        dd($media_ids);

        $images = Media::where("type", "image")
            //->whereIn('id', $media_ids)
//            ->where("regenerated", 0)
//            ->inRandomOrder()
            //->where("id", 78225)
            //->orderBy("created_at", "ASC") wget --max-redirect 99999999 https://dubarter.com/ar-eg/media/regenerate
            ->take(5)->skip($offset)->get();

        if (count($images)) {

            foreach ($images as $image) {

                $this->regenerate($image->path);

                Media::where("id", $image->id)->update([
                    "regenerated" => 1
                ]);
            }

            return response()->json([
                "regenerated" => 1
            ]);

            $offset = $offset + 5;

            return redirect()->route("media.cron", ["offset" => $offset]);

        } else {

            return response()->json([
                "regenerated" => 1
            ]);

        }
    }

    function regenerate($path)
    {

        $file_directory = storage_path();

        list($year, $month, $filename) = @explode("/", $path);

        file_put_contents($file_directory . "/" . $filename, fopen("http://dubarter.s3-eu-west-1.amazonaws.com/" . $path, 'r'));

        if (file_exists($file_directory . "/" . $filename)) {

            $has_transperancy = $this->hasAlpha($file_directory . "/" . $filename);

            $sizes = Config::get("media.sizes");

            $width = Image::make($file_directory . "/" . $filename)->width();
            $height = Image::make($file_directory . "/" . $filename)->height();

            foreach ($sizes as $size => $dimensions) {

                if ($width > $height) {
                    $new_width = $dimensions[0];
                    $new_height = null;
                } else {
                    $new_height = $dimensions[1];
                    $new_width = null;
                }

                if (in_array($size, ["large", "small", "medium", "thumbnail"])) {

                    if($has_transperancy){

                        $background = Image::canvas($dimensions[0], $dimensions[1], "#ffffff");

                        $image = Image::make($file_directory . "/" . $filename)
                            ->resize($new_width, $new_height, function ($constraint) {
                                $constraint->aspectRatio();
                            });

                        $background->insert($image, 'center');
                        $background->save($file_directory . "/" . $size . "-" . $filename);

                        unset($background);
                        unset($image);

                    }else{

                        $background = Image::make($file_directory . "/" . $filename)
                            ->fit($dimensions[0], $dimensions[1])
                            ->blur(100);

                        $image = Image::make($file_directory . "/" . $filename)
                            ->resize($new_width, $new_height, function ($constraint) {
                                $constraint->aspectRatio();
                            });

                        $background->insert($image, 'center');
                        $background->save($file_directory . "/" . $size . "-" . $filename);

                    }

                } elseif ($size == "free") {

                    Image::make($file_directory . "/" . $filename)
                        ->resize($new_width, $new_height, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->save($file_directory . "/" . $size . "-" . $filename);

                } else {

                    Image::make($file_directory . "/" . $filename)
                        ->fit($dimensions[0], $dimensions[1])
                        ->save($file_directory . "/" . $size . "-" . $filename);

                }

                $s3 = app()->make('aws')->get('s3');

                $s3->putObject(array(
                    'Bucket' => config("media.s3.bucket"),
                    'Key' => $year . "/" . $month . "/" . $size . "-" . $filename,
                    'SourceFile' => $file_directory . "/" . $size . "-" . $filename,
                    'ACL' => 'public-read',
                    'Expires' => date("D, j M Y", time() + (2000 * 24 * 60 * 60)) . " 02:00:00 GMT",
                    'CacheControl' => 'max-age=2592000,public',
                ));

                unset($s3);

                @unlink($file_directory . "/" . $size . "-" . $filename);
            }

            @unlink($file_directory . "/" . $filename);
        }

        unset($file_directory);
        unset($month);
        unset($filename);
        unset($year);
    }

    function hasAlpha($path)
    {
        //return true;
        return (ord(@file_get_contents($path, NULL, NULL, 25, 1)) == 6);
    }
}
