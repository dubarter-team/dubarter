<?php

namespace App\Http\Controllers;

use App\Indices\Ad;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Place;
use Illuminate\Support\Facades\DB;
use Request;

/**
 * Class SearchController
 *
 * @package App\Http\Controllers
 */
class SearchController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    public $order_by = ['latest', 'oldest', 'lowest_price', 'highest_price'];

    public $per_page_options = [24, 32];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index()
    {

        $this->data['cities'] = Place::where('parent', config("country.id"))->get();

        $this->data['regions'] = Request::filled('city_id') ? Place::where('parent', Request::get('city_id'))->get() : collect([]);

        $this->data['categories'] = Category::where('parent', 0)->get();

        $this->data['order_by'] = $this->order_by;

        $this->data['per_page_options'] = $this->per_page_options;

        $this->data['per_page'] = $per_page = in_array(request('per_page'), $this->data['per_page_options']) ? request('per_page') : 24;

        $ads = $this->search_ads();

        $this->data['ads_html'] = view('partials.search.ads_listing', ['ads' => $ads, 'half_count' => $per_page / 2])->render();

        $this->data['ads_count'] = count($ads);

        $this->data['ads_total'] = $ads->total;

        $this->data['offset'] = ((request('offset', 0) % $per_page == 0) ? request('offset', 0) : 0) + count($ads);

        $this->data['attributes_html'] = '';

        if (Request::filled('category_id')) {
            $category = Category::where('id', Request::get('category_id'))->with(['custom_attributes'])->first();

            if ($category && $category->listing_type == 'ads') {
                $this->data['attributes_html'] = view('partials.search.categories_attributes', ['attributes' => Category::get_attributes($category, true)->where('searchable', 1)->where('template', 'dropdown')])->render();
            }


        }

        return view("search", $this->data);
    }

    public function search_paginate()
    {
        $ads = $this->search_ads();

        $per_page = request('per_page', 24);

        $res['ads_html'] = view('partials.search.ads_listing', ['ads' => $ads, 'half_count' => $per_page / 2])->render();

        $res['ads_count'] = count($ads);

        return response()->json($res);
    }

    function search_ads()
    {
        $per_page = in_array(request('per_page'), $this->per_page_options) ? request('per_page') : 24;

        $offset = (request('offset', 0) % $per_page == 0) ? request('offset', 0) : 0;

        $query = Ad::take($per_page);

        if (Request::filled('category_id') && Request::get('category_id')) {
            $query->byCategory(Request::get('category_id'));
        }

        if (Request::filled('attrs')) {
            $attrs = Request::input('attrs');

            if (count($attrs)) {
                foreach ($attrs as $k => $v) {

                    if (is_array($v) && count($v)) {

                        $query->whereIn("attrs." . $k, array_map('strtolower', $v));

                    }
                }
            }

        }

        if (Request::filled('city_id')) {
            $query->where("city_id", Request::get('city_id'));

            if (Request::filled('region_id')) {
                $query->where("region_id", Request::get('region_id'));
            }
        }

        if (Request::filled('q')) {
            $query->search(Request::get('q'));
        }

        if (Request::filled('price_from')) {
            $query->where('price', '>=', Request::get('price_from'));
        }

        if (Request::filled('price_to')) {
            $query->where('price', '<=', Request::get('price_to'));
        }

        switch (Request::get('order_by')) {

            case 'latest' :
                $query->orderBy('published_at', 'desc');

                break;

            case 'oldest' :
                $query->orderBy('published_at', 'asc');

                break;

            case 'lowest_price' :
                $query->orderBy('price', 'asc');

                break;

            case 'highest_price' :
                $query->orderBy('price', 'desc');

                break;

            default :
                $query->orderBy('created_at', 'desc');

                break;
        }

        $ads = $query->skip($offset)->get();

        return $ads;
    }


}
