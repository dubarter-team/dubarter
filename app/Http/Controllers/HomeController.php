<?php

namespace App\Http\Controllers;

use App\Indices\Ad;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Place;
use App\Models\Post;
use App\Models\Tag;
use App\Models\Topic;
use Illuminate\Support\Facades\Cache;
use Request;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $this->data['cities'] = Place::get_local_cities();

        $this->home_main_slider_section();


//        $this->home_cars_section();
//
//        $this->home_realestate_section();
//
//        $this->home_style_section();
//
//        $this->home_food_section();
//
//        $this->home_mobile_section();


        return view('home', $this->data);
    }

    /**
     * Cars section
     */
    function home_cars_section()
    {
        $cars_ads = [];

        $cars_ads['all_cars'] = Ad::featured_by_category(Category::AUTOBARTER, 'home-cars-all');

        $cars_ads['used_cars'] = Ad::featured_by_category(Category::USED_CARS, 'home-cars-used');

        $cars_ads['new_cars'] = Ad::featured_by_category(Category::NEW_CARS, 'home-cars-new');

        $cars_ads['other_cars'] = Ad::featured_by_category(Category::OTHER_VEHICLES, 'home-cars-other');

        $this->data['featured_cars'] = $cars_ads;
    }

    /**
     *  Realestates section
     */
    function home_realestate_section()
    {
        $realestate_ads = [];

        $realestate_ads['all_realestate'] = Ad::featured_by_category(Category::AQARBARTETR, 'home-realestate-all');

        $realestate_ads['apartment_realestate'] = Ad::featured_by_category([34, 40], 'home-realestate-apartment');

        $realestate_ads['villa_realestate'] = Ad::featured_by_category([35, 41], 'home-realestate-villa');

        $realestate_ads['land_realestate'] = Ad::featured_by_category([37, 43], 'home-realestate-land');

        $this->data['featured_realestate'] = $realestate_ads;
    }

    /**
     * Style section
     */
    function home_style_section()
    {

        $lfs_ads = [];

        $lfs_ads['all_ls'] = Ad::featured_by_category(Category::STYLEBARTER, 'home-all-ls');

        $lfs_ads['ls_fashion'] = Ad::featured_by_category(17, 'home-ls-fashion');

        $lfs_ads['ls_beauty'] = Ad::featured_by_category(18, 'home-ls-beauty');

        $lfs_ads['ls_decoration'] = Ad::featured_by_category(3752, 'home-ls-decoration');

        $this->data['featured_lfs'] = $lfs_ads;
    }

    /**
     *  Videos section
     */
    function home_videos_section()
    {

        $videos = [];

        $videos["all-videos"] = Cache::remember('home-videos-all', 10, function () {
            return Post::where("featured", 1)->format("video")
                ->with(['categories', 'image', 'thumbnail'])->take(8)->get();
        });

        $videos["cars-videos"] = Cache::remember('home-videos-cars', 10, function () {
            return Post::where("featured", 1)->format("video")
                ->with(['categories', 'image', 'thumbnail'])->autobarter()->take(8)->get();
        });

        $videos["food-videos"] = Cache::remember('home-videos-food', 10, function () {
            return Post::where("featured", 1)->format("video")
                ->with(['categories', 'image', 'thumbnail'])->foodbarter()->take(8)->get();
        });

        $videos["style-videos"] = Cache::remember('home-videos-style', 10, function () {
            return Post::where("featured", 1)->format("video")
                ->with(['categories', 'image', 'thumbnail'])->stylebarter()->take(8)->get();
        });

        $this->data["featured_videos"] = $videos;
    }

    /**
     * Food section
     */
    function home_food_section()
    {

        $this->data["day_restaurant"] = $today_rest = Cache::remember('home-today-restaurant', 10, function () {
            return Ad::byCategory(Category::FOODBARTER)->where("attrs.today-restaurant", 'yes')->orderBy('published_at', 'desc')->first();
        });

        $this->data["latest_restaurants"] = Cache::remember('home-latest-restaurants', 100, function () use ($today_rest) {
            return Ad::byCategory(Category::FOODBARTER)->orderBy('published_at', 'desc')
                ->where('_id', '!=', $today_rest ? $today_rest->_id : 0)
                ->where("categories.id", '!=', 9)
                ->where('featured', '!=', 1)->take(4)->get();
        });

        $this->data["featured_restaurants"] = Cache::remember('home-featured-restaurants', 10, function () use ($today_rest) {
            return Ad::byCategory(Category::FOODBARTER)->orderBy('published_at', 'desc')
                ->where('_id', '!=', $today_rest ? $today_rest->_id : 0)
                ->where("categories.id", '!=', 9)
                ->where('featured', 1)->take(8)->get();
        });

        $this->data["kitchen_category"] = Cache::remember('home-restaurant-category', 100, function () {
            return Category::where('id', Category::FOODBARTER)->first();
        });
    }

    /**
     * mobile section
     */
    function home_mobile_section()
    {

        $mob_ads = [];

        $mob_ads['all_mob'] = Ad::featured_by_category(Category::MOBAWABABARTER, 'home-mob-all');

        $mob_ads['mob_mobiles'] = Ad::featured_by_category(47, 'home-mob-mobiles');

        $mob_ads['mob_electric'] = Ad::featured_by_category(48, 'home-mob-electric');

        $mob_ads['mob_jobs'] = Ad::featured_by_category(46, 'home-mob-jobs');

        $this->data['featured_mobs'] = $mob_ads;
    }

    /**
     *  Articles section
     */
    function home_articles_section()
    {

        $articles = [];

        $articles["all-articles"] = Cache::remember('home-articles-all', 10, function () {
            return Post::where("featured", 1)->format("article")
                ->with(['categories', 'image', 'thumbnail'])->take(8)->get();
        });

        $articles["cars-articles"] = Cache::remember('home-articles-' . Category::AUTOBARTER, 10, function () {
            return Post::where("featured", 1)->format("article")->autobarter()
                ->with(['categories', 'image', 'thumbnail'])->take(8)->get();
        });

        $articles["food-articles"] = Cache::remember('home-articles-' . Category::FOODBARTER, 10, function () {
            return Post::where("featured", 1)->format("article")->foodbarter()
                ->with(['categories', 'image', 'thumbnail'])->take(8)->get();
        });

        $articles["style-articles"] = Cache::remember('home-articles-' . Category::STYLEBARTER, 10, function () {
            return Post::where("featured", 1)->format("article")->stylebarter()
                ->with(['categories', 'image', 'thumbnail'])->take(8)->get();
        });

        $this->data["featured_articles"] = $articles;
    }

    function home_main_slider_section()
    {

        $this->data['ms_main_cats'] = Cache::remember('home-slider-main-categories', 100, function () {
            return Category::with(['categories'])->where('parent', 0)->with(['image'])->get();
        });

        // cars slide

        $this->data['ms_cars'] = $this->data['ms_main_cats']->where('id', Category::AUTOBARTER)->first();

//        $this->data['ms_cars_main_sub_cats'] = Cache::remember('home-slider-cars-subs', 100, function () {
//            return Category::whereIn('id', [Category::USED_CARS, Category::NEW_CARS])->get();
//        });
//
//        $this->data['ms_cars_sub_cats_' . Category::USED_CARS] = Cache::remember('home-slider-subs-' . Category::USED_CARS, 100, function () {
//            return Category::where('parent', Category::USED_CARS)->with(['logo'])->withCount(['ads'])
//                ->take(4)->where('id', '!=', 3308)->orderBy('ads_count', 'desc')->get();
//        });
//
//        $this->data['ms_cars_sub_cats_' . Category::NEW_CARS] = Cache::remember('home-slider-subs-' . Category::NEW_CARS, 100, function () {
//            return Category::where('parent', Category::NEW_CARS)->with(['logo'])->withCount(['ads'])->take(9)->orderBy('ads_count', 'desc')->get();
//        });

        // realestate slide

        $this->data['ms_realestate'] = $this->data['ms_main_cats']->where('id', Category::AQARBARTETR)->first();

//        $this->data['ms_realestate_main_sub_cats'] = Cache::remember('home-slider-realestate-subs', 100, function () {
//            return Category::whereIn('id', [Category::REALESTATE_SALE, Category::REALESTATE_RENT])->get();
//        });
//
//        $this->data['ms_realestate_sub_cats_' . Category::REALESTATE_SALE] = Cache::remember('home-slider-subs-' . Category::REALESTATE_SALE, 100, function () {
//            return Category::where('parent', Category::REALESTATE_SALE)->take(5)->get();
//        });
//
//        $this->data['ms_realestate_sub_cats_' . Category::REALESTATE_RENT] = Cache::remember('home-slider-subs-' . Category::REALESTATE_RENT, 100, function () {
//            return Category::where('parent', Category::REALESTATE_RENT)->take(5)->get();
//        });

        // style slide

        $this->data['ms_style'] = $this->data['ms_main_cats']->where('id', Category::STYLEBARTER)->first();

//        $this->data['ms_style_main_sub_cats'] = Cache::remember('home-slider-subs-' . Category::STYLEBARTER, 100, function () {
//            return Category::where('parent', Category::STYLEBARTER)->get();
//        });
//
//        foreach ($this->data['ms_style_main_sub_cats'] as $stc) {
//            $this->data['ms_style_sub_cats_' . $stc->id] = Cache::remember('home-slider-subs-' . $stc->id, 100, function () use ($stc) {
//                return Category::where('parent', $stc->id)->with(['logo'])->take(2)->get();
//            });
//        }

        // mobawba slide

        $this->data['ms_mobawba'] = $this->data['ms_main_cats']->where('id', Category::MOBAWABABARTER)->first();

//        $this->data['ms_mobawba_main_sub_cats'] = Cache::remember('home-slider-mobawba-subs' . Category::REALESTATE_SALE, 100, function () {
//            return Category::whereIn('id', [Category::JOBS, Category::MOBILES])->get();
//        });
//
//        $this->data['ms_mobawba_sub_cats_' . Category::JOBS] = Cache::remember('home-slider-subs-' . Category::JOBS, 100, function () {
//            return Category::where('parent', Category::JOBS)->take(5)->get();
//        });
//
//        $this->data['ms_mobawba_sub_cats_' . Category::MOBILES] = Cache::remember('home-slider-subs-' . Category::MOBILES, 100, function () {
//            return Category::where('parent', Category::MOBILES)->take(5)->get();
//        });

        // food slide

        $this->data['ms_food'] = $this->data['ms_main_cats']->where('id', Category::FOODBARTER)->first();

//        $this->data['ms_food_main_topics'] = Cache::remember('ms_food_main_topics', 100, function () {
//            return Topic::whereIn('id', [11, 12])->orderBy('id', 'desc')->get();
//        });
//
//        $this->data['ms_food_tags_' . 11] = Cache::remember('ms_food_tags_' . 11, 100, function () {
//            return Tag::whereHas('topics', function ($query) {
//                $query->where('id', 11);
//            })->take(4)->get();
//        });
//
//        $this->data['ms_food_tags_' . 12] = Cache::remember('ms_food_tags_' . 12, 100, function () {
//            return Tag::whereHas('topics', function ($query) {
//                $query->where('id', 12);
//            })->take(4)->get();
//        });
    }

    public function contact_us()
    {
        if (Request::method() == "POST") {
            if(!request('name') || !request('email')){
                return redirect()->back()->withErrors(trans('common.cnus_emr'));
            }

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <' . request('email') . '>' . "\r\n";

            $message = "<div>name : " . request('name') . "</div>";
            $message .= "<div>email : " . request('email') . "</div>";
            $message .= "<div>phone : " . request('phone') . "</div>";
            $message .= "<div>message : " . request('message') . "</div>";

            mail('mostafa.elbardeny@gmail.com', 'contact us message', $message, $headers);

            return redirect()->back()->with('status', trans('common.cnus_msnt'));
        }

        return view('contact_us');
    }
}
