<?php

namespace App\Http\Controllers;

use App\Indices\Ad;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Log;
use URL;

class SitemapController extends Controller
{

	const SITEMAP_LIMIT = 5000;


	const SITEMAP_URL = "https://dubarter.s3-eu-west-1.amazonaws.com/sitemaps";


	const SITEMAP_ROUTERS = [
		'ads' => 'sitemap.update.ads',
		'tags' => 'sitemap.update.tags',
		'categories' => 'sitemap.update.categories',
		'articles' => 'sitemap.update.articles',
		'videos' => 'sitemap.update.videos'
	];

	/**
	 * Waring this only for print in index sitemap
	 */
	const SITEMAPS_NAME = [
		'ads' => 'sitemap-ads-[code].xml.gz',
		'categories' => 'sitemap-categories-[code].xml.gz',
		'tags' => 'sitemap-tags-[code].xml.gz',
		'videos' => 'sitemap-videos-[code].xml.gz',
		'articles' => 'sitemap-articles-[code].xml.gz',
	];

	protected $country_ids = [];

	function __construct()
	{

		$country_code = config("country.code");

		if ($country_code == "eg") {
			$this->country_ids = [0, 21];
		}

		if ($country_code == "iq") {
			$this->country_ids = [10];
		}

	}

	/**
	 * @route sitemap.index
	 * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
	 */
	public function index()
	{

		if (file_exists(public_path($this->getSitemapIndexName() . '.xml'))) {
			return response()->file(public_path($this->getSitemapIndexName() . '.xml'), [
					'Content-type' => 'text/xml'
			]);
		}
		return response('sitemap not found', 404);
	}


	/**
	 * @routeName sitemap.update.tags
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */
	public function tagsUpdate()
	{
		$sitemap = App::make("sitemap");
		$tags = Tag::get(['id', 'created_at', 'slug']);
		foreach ($tags as $tag) {
			$sitemap->add(url('tag/' . $tag->slug), (string)$tag->created_at, "0.9", "monthly");
			unset($tag);
		}
		$sitemap->store('xml', 'sitemap-tags-' . config("country.code"));
		$url = $this->compress("sitemap-tags-" . config("country.code") . ".xml");
		return $url;
	}


	/**
	 * @routeName sitemap.update.categories
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */
	public function categoriesUpdate()
	{

		$sitemap = App::make("sitemap");

		$query = Category::where(['status' => 1])
			->whereHas('countries', function ($query) {
				$query->whereIn('id', $this->country_ids);
			});


		$offset = 0;

		while ($offset < $query->count()) {

			$categories = $query->take(100)->skip($offset)
				->get(['id', 'parent', 'created_at', 'slug']);

			foreach ($categories as $category) {
				$sitemap->add($category->url, (string)$category->created_at, "0.9", "monthly");
				unset($category);
			}


			$offset = $offset + 100;
		}


		$sitemap->store('xml', 'sitemap-categories-' . config("country.code"));

		$url = $this->compress("sitemap-categories-" . config("country.code") . ".xml");

		return $url;
	}

	/**
	 * @routeName sitemap.update.videos
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */
	public function videosUpdate()
	{

		// create sitemap index
		$sitemap = App::make("sitemap");

		$posts = Post::withoutGlobalScopes()->where(['status' => 1, 'format' => 'video'])->with(['media', "country"])
			->whereIn("country_id", $this->country_ids)
			->get();

		foreach ($posts as $post) {

			$videos_in = [];

			if ($post->media) {
				$videos_in['title'] = $post->title;
				$videos_in['description'] = $post->excerpt;

				$videos_in["duration"] = $post->media->length;

				$videos_in["view_count"] = $post->views;

				if ($post->media->provider == 'youtube') {
					$videos_in["thumbnail_loc"] = $post->media->provider_image;
					$videos_in["content_loc"] = $post->media->path;
				} else {
					$videos_in["content_loc"] = uploads_url($post->media->path);
					$videos_in["thumbnail_loc"] = 'https://dubarter.s3-eu-west-1.amazonaws.com/2018/03/medium-2398247304400053075.png';
				}
			}

			$sitemap->add($post->url, $post->date, "0.9", "monthly", [], null, [], [$videos_in]);

			unset($post);
		}

		$sitemap->store('xml', 'sitemap-videos-' . config("country.code"));

		$url = $this->compress("sitemap-videos-" . config("country.code") . ".xml");

		return $url;
	}

	/**
	 * @routeName sitemap.update.articles
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */

	public function articlesUpdate()
	{

		// create sitemap index
		$sitemap = App::make("sitemap");

		$posts = Post::withoutGlobalScopes()->with('image', 'thumbnail', "country")
			->where(['status' => 1, 'format' => 'article'])
			->whereIn("country_id", $this->country_ids)
			->get();

		foreach ($posts as $post) {
			$image_in = [];
			// Add images
			if ($post->image_id != 0) {
				$image_in[] = ['url' => uploads_url($post->image->path), 'title' => ($post->image->title)];
			}

			// Add thumbnail
			if ($post->thumbnail_id != 0) {
				$image_in[] = ['url' => uploads_url($post->thumbnail->path), 'title' => ($post->thumbnail->title)];
			}
			$sitemap->add($post->url, $post->date, "0.9", "monthly", $image_in);
			unset($post);
		}
		$sitemap->store('xml', 'sitemap-articles-' . config("country.code"));
		$url = $this->compress("sitemap-articles-" . config("country.code") . ".xml");
		return $url;
	}

	/**
	 * @routeName sitemap.update.index
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Illuminate\Container\EntryNotFoundException
	 * @throws \Throwable
	 */

	public function indexUpdate(Request $request)
	{

		$messages = [];

		$sitemap = App::make("sitemap");

		foreach (self::SITEMAPS_NAME as $value) {
			$value = str_replace("[code]", config("country.code"), $value);
			$sitemap->addSitemap(self::SITEMAP_URL . '/' . $value, date('c', time()), "0.9", "monthly");
		}

		$sitemap->store('sitemapindex', $this->getSitemapIndexName());

		if ($request->filled('hard')) {
			$messages = $this->UpdateAll();
		}

		return response()->json(['status' => true, 'notes' => $messages]);
	}

	/**
	 * @routeName sitemap.update.ads
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */

	public function adsUpdate()
	{
		// create sitemap index
		$sitemap = App::make("sitemap");
		$this->addAdsSitemaps($sitemap);
		$sitemap->store('sitemapindex', 'sitemap-ads-' . config("country.code"));
		$url = $this->compress("sitemap-ads-" . config("country.code") . ".xml");
		return $url;
	}

	/**
	 * Add All ads sitemaps
	 * @param $sitemap
	 * @return mixed
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */

	protected function addAdsSitemaps($sitemap)
	{
		$offset = 0;

		$index = 1;

		$scroll_id = null;

		while (true) {

			$name = 'sitemap-ad-' . config("country.code") . "-" . $index;

			$q = Ad::withoutGlobalScopes()->whereIn("country.id", $this->country_ids)->where("status", 1);

			$q->skip($offset)->take(self::SITEMAP_LIMIT);

			if ($scroll_id) {
				$q->scrollID($scroll_id);
			}

			$ads = $q->scroll("2m")->get();

			$url = $this->addAdsSitemap($name, $ads);

			$offset += self::SITEMAP_LIMIT;

			$sitemap->addSitemap($url, date('c', time()), '0.9', 'daily');

			if (count($ads) < self::SITEMAP_LIMIT) {
				break;
			}

			$index++;

			$scroll_id = $ads->scroll_id;

			unset($ads);
		}

		return $sitemap;
	}

	/**
	 * Add Sitemap for LIMIT ads
	 * @param $offset
	 * @param $name
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */
	protected function addAdsSitemap($name, $ads)
	{
		// create sitemap
		$sitemap_ads = App::make("sitemap");
		// add items
		foreach ($ads as $ad) {

			// Create Images structure
			$images_in = [];

			if (count($ad->images)) {
				foreach ($ad->images as $image) {
					$images_in[] = ['url' => uploads_url($image['path']), 'title' => $image['title']];
				}
			}
			// Add url to site maps
			$sitemap_ads->add($ad->url, $ad->date, "0.9", "monthly", $images_in);

			unset($image);
		}
		// create file $name.xml in your public folder (format, filename)
		$sitemap_ads->store('xml', $name);

		return $this->compress(("$name.xml"));
	}


	/**
	 * @param $path
	 * @return string
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */
	protected function compress($path)
	{
		if (file_exists(public_path($path)) && filesize(public_path($path))) {

			$content = \File::get(public_path($path));

			$gzdata = gzencode($content);

			$gz_file_name = $path . '.gz';

			// write
			File::put(public_path($gz_file_name), $gzdata);


			// upload
			@s3_delete('sitemaps/' . $gz_file_name);

			$this->s3Save(public_path($gz_file_name), 'sitemaps/' . $gz_file_name);


			// delete
			@unlink(public_path($path));
			@unlink(public_path($gz_file_name));
			unset($content);
			unset($gzdata);
			return self::SITEMAP_URL . "/$gz_file_name";
		}
		abort(500);
	}


	/**
	 * Saving
	 * @param $file_directory
	 * @param $filename
	 * @return mixed
	 * @throws \Illuminate\Container\EntryNotFoundException
	 */
	protected function s3Save($file_directory, $filename)
	{
		$s3 = app('aws')->get('s3');
		$op = $s3->putObject(array(
			'Bucket' => Config::get("media.s3.bucket"),
			'Key' => $filename,
			'SourceFile' => $file_directory,
			'ACL' => 'public-read',
			'Expires' => date("D, j M Y", time() + (2000 * 24 * 60 * 60)) . " 02:00:00 GMT",
			'CacheControl' => 'max-age=2592000,public',
		));
		return $op;
	}

	/**
	 *  Get sitemaps index name with country id as prefix
	 * @return string
	 */
	protected function getSitemapIndexName()
	{
		return config("country.code") . '_sitemap';
	}

	/**
	 * Send an asynchronous request for all sitemaps linkes.
	 * @return array
	 * @throws \Throwable
	 */
	protected function UpdateAll()
	{
		// Send an asynchronous request.
		$client = new \GuzzleHttp\Client();
		$promises = [];
		$messages = [];
		foreach (self::SITEMAP_ROUTERS as $routeName) {
			$promises[] = $client->requestAsync('GET', route($routeName))->then(function ($response) use ($messages) {
				$messages[] = 'I completed! ' . $response->getBody();
			});

		}
		\DB::table('options')->where('name', 'sitemap_last_update')->update([
			'value' => Carbon::now()->getTimestamp()
		]);
		\GuzzleHttp\Promise\unwrap($promises);
		return $messages;
	}
}
