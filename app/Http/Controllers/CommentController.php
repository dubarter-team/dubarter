<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{


    /**
     * @param $id
     *
     */
    public $data = [];

    /**
     * POST /comments/{id}/add
     * @routeName comments.add
     * @param Request $request
     * @param $id
     * @return void
     * @throws \Throwable
     */
    public function addComment(Request $request, $id)
    {
        if (!$request->ajax()) {
            return abort(404);
        }
        $post = Post::findOrFail($id);
        $comment = new Comment();
        $comment->object_type = $post->format;
        $comment->object_id = $id;
        $comment->user_id = Auth::guard('frontend')->user()->id;
        $comment->parent = 0;
        $comment->content = $request->get('content');
        $comment->status = 1;
        if (!$comment->validate()) {
            // send bad request
            return abort(400);
        }
        $comment->save();

        $this->data['commentsHtml'] = view('partials.post.comment-list', ['comments' => [$comment]])->render();
        return response()->json($this->data, 200);
    }

    /**
     * POST /comments/replies/add
     * @routeName comments.replies.add
     * @param Request $request
     * @return void
     * @throws \Throwable
     */
    public function addReply(Request $request)
    {
        if (!$request->ajax()) {
            return abort(404);
        }
        $comment = Comment::findOrFail($request->get('comment_id'));
        $reply = new Comment();
        $reply->object_type = $comment->object_type;
        $reply->object_id = $comment->object_id;
        $reply->user_id = Auth::guard('frontend')->user()->id;
        $reply->parent = $comment->id;
        $reply->content = $request->get('content');
        $reply->status = 1;
        if (!$reply->validate()) {
            // send bad request
            return abort(400);
        }
        $reply->save();

        $this->data['repliesHtml'] = view('partials.post.replies-list', ['replies' => [$reply]])->render();
        return response()->json($this->data, 200);
    }

    /**
     * GET  /comments/{id}'
     * @routeName comments.index
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function comments(Request $request, $id)
    {
        if (!$request->ajax()) {
            return abort(404);
        }

        $comments = Comment::where('status', 1)
            ->where('parent', 0)
            ->where('object_id', $id)
            ->with(['user', 'user.photo'])
            ->orderBy('created_at', 'DESC')
            ->offset($request->get('offset', 5))
            ->take(5)
            ->get();

        $this->data['comments_count'] = count($comments);
        $this->data['commentsHtml'] = view('partials.post.comment-list', ['comments' => $comments])->render();

        return response()->json($this->data, 200);

    }

    /**
     * GET  /comments/replies
     * @routeName comments.replies.index
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function replies(Request $request)
    {
        if (!$request->ajax()) {
            return abort(404);
        }

        $replies = Comment::where('status', 1)
            ->where('parent', $request->get('parent', -1))
            ->with(['user', 'user.photo'])
            ->orderBy('created_at', 'DESC')
            ->offset($request->get('offset', 5))
            ->take($request->get('limit', 4))
            ->get();

        $this->data['replies_count'] = count($replies);

        $this->data['repliesHtml'] = view('partials.post.replies-list', ['replies' => $replies])->render();
        return response()->json($this->data, 200);

    }
}
