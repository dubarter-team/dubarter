<?php

namespace App\Http\Controllers;

use App\Indices\Ad;
use App\Models\Media;
use App\Models\Place;
use App\User;
use App\Models\Category;
use Request;
use Validator;

/**
 * Class AjaxController
 *
 * @package App\Http\Controllers
 */
class AjaxController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function search_ads()
    {
        $res = [];

        if (Request::filled('q')) {
            $q = trim(urldecode(Request::get("q")));

            $ads = Ad::search($q, function ($search) {
                $search->boost(2)->fields(["title" => 2, "slug" => 1]);
            })->take(10)->get();

            if ($ads->count()) {

                foreach ($ads as $ad) {

                    $res[] = $ad->title;

                }

            }

        }

        return response()->json($res);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function search_cities()
    {

        $res = [];

        if (Request::filled('q')) {

            $q = trim(urldecode(Request::get("q")));

            $cities = Place::search($q)->where('parent', config("country.id"))->take(10)->get();

            if ($cities->count()) {

                foreach ($cities as $city) {

                    $res[] = ['id' => $city->id, 'name' => $city->local_name];

                }

            }

        }

        return response()->json($res);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_sub_places()
    {

        $res = ['count' => 0, 'places' => ''];

        if (Request::filled('id')) {

            $places = Place::where('parent', request('id'))->get();

            if ($places->count()) {

                $res['count'] = $places->count();

                $res['places'] = view('partials.ad.places_options', ['places' => $places])->render();

            }

        }

        return response()->json($res);

    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_regions()
    {

        $res = ['count' => 0, 'places' => ''];

        if (Request::filled('id')) {

            $places = Place::where('parent', request('id'))->get();

            $res['count'] = $places->count();

            $res['places'] = view('partials.common.regions_options', ['places' => $places])->render();

        }

        return response()->json($res);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function like()
    {

        $res = ['status' => false, 'liked' => false];

        if (auth('frontend')->check()) {

            $user = auth('frontend')->user();

            $id = Request::get('id', 0);

            if ($id) {
                $res['status'] = true;

                $liked_ad = $user->liked_ads()->where('ad_id', $id)->first();

                if ($liked_ad) {
                    $user->liked_ads()->detach($id);

                    $res['liked'] = false;
                } else {
                    $user->liked_ads()->attach($id, ['created_at' => date('Y-m-d H:i:s')]);

                    $res['liked'] = true;
                }

            }
        }

        return response()->json($res);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function get_sub_categories()
    {

        $res = ['status' => false, 'count' => 0, 'data' => ''];

        if (Request::filled('cat_id')) {

            $cats = Category::where('parent', Request::get('cat_id'))->orderBy('id', 'asc')->get();

            $res['status'] = true;

            $res['count'] = $cats->count();

            $res['data'] = view('partials.category.sub_categories_options', ['cats' => $cats])->render();

        }

        return response()->json($res);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function mob_get_sub_categories()
    {

        $res = ['status' => false, 'count' => 0, 'data' => ''];

        if (Request::filled('cat_id')) {

            $cats = Category::where('parent', Request::get('cat_id'))->orderBy('id', 'asc')->get();

            $res['status'] = true;

            $res['count'] = $cats->count();

            $res['data'] = view('partials.category.mob_sub_categories_options', ['cats' => $cats])->render();

        }

        return response()->json($res);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function search_get_sub_categories()
    {

        $res = ['status' => false, 'count' => 0, 'categories_html' => '', 'attributes_html' => ''];

        if (Request::filled('cat_id')) {

            $cat = Category::where('id', Request::get('cat_id'))->with(['categories', 'custom_attributes'])->orderBy('id', 'asc')->first();

            if ($cat) {
                $res['status'] = true;

                $res['count'] = $cat->categories->count();

                $res['categories_html'] = view('partials.search.categories_dropdown', ['categories' => $cat->categories, 'label' => 'all_categories'])->render();

                if ($cat->listing_type == 'ads') {
                    $attributes = Category::get_attributes($cat, true)->where('searchable', 1)->where('template', 'dropdown');

                    $res['attributes_html'] = view('partials.search.categories_attributes', ['attributes' => $attributes])->render();
                }

            }
        }

        return response()->json($res);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function ca_get_sub_categories()
    {

        $res = ['status' => false, 'count' => 0, 'categories_html' => '', 'attributes_html' => ''];

        if (Request::filled('cat_id')) {

            $cat = Category::where('id', Request::get('cat_id'))->with(['categories', 'custom_attributes'])->orderBy('id', 'asc')->first();

            if ($cat) {
                $res['status'] = true;

                $res['count'] = $cat->categories->where('frontend_user_enabled', 1)->count();

                $res['categories_html'] = view('partials.ad.categories_dropdown', ['categories' => $cat->categories->where('frontend_user_enabled', 1), 'label' => 'all_categories'])->render();

                if ($cat->listing_type == 'ads') {
                    $attributes = Category::get_attributes($cat, true);

                    $res['attributes_html'] = view('partials.ad.custom_attributes', ['attributes' => $attributes])->render();
                }

            }
        }

        return response()->json($res);

    }

    public function image_upload()
    {
        $res = ['count' => 0, 'media' => [], 'media_html' => '', 'message' => ''];

        $files = Request::file('files');

        $uploaded_ids = request('uploaded_ids', []);

        $i = 0;

        if (count($files)) {

            foreach ($files as $file) {
                $validator = $this->validateFile($i, ['jpg', 'png', 'jpeg']);

                if ($validator->fails()) {

                    if (count($files) > 1) {
                        $res['message'] = trans('common.inc_files');
                    } else {
                        $res['message'] = trans('common.inc_file');
                    }

                } else {

                    $media = new Media();

                    $id = $media->saveFile($file);

                    if (!in_array($id, $uploaded_ids)) {

                        $res['media'][] = Media::where("id", $id)->first();

                    }

                }

                $i++;

            }

            $res['count'] = count($res['media']);

            $res['media_html'] = view('partials.ad.uploaded_images', ['images' => $res['media']])->render();

        }

        return response()->json($res);
    }

    public function video_upload()
    {
        $res = ['status' => false, 'media' => null, 'media_html' => '', 'message' => ''];

        $file = Request::file('file');

        $validator = $this->validateFile(0, ['mp4'], 30072);

        if ($validator->fails()) {

            $res['message'] = trans('common.inc_file');

        } else {

            $media = new Media();

            $id = $media->saveFile($file);

            $res['media'] = Media::where("id", $id)->first();

            $res['status'] = true;

        }

        $res['media_html'] = view('partials.ad.uploaded_video', ['video' => $res['media']])->render();


        return response()->json($res);
    }

    public function validateFile($index, $types = [], $max_size = 0)
    {

        if (count($types)) {
            $allowed_types = $types;
        } else {
            $allowed_types = Config("media.allowed_file_types");
        }

        if (!$max_size) {
            $max_size = Config("media.max_file_size");
        }

        if (is_array($allowed_types)) {
            $allowed_types = join(",", $allowed_types);
        }

        $rules = array(
            'file' => "mimes:" . $allowed_types . "|max:" . $max_size,
        );

        $validator = Validator::make(Request::all(), $rules);

        $validator->setAttributeNames(array(
            'files.' . $index => "file"
        ));

        return $validator;
    }

}
