<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use \Laravel\Socialite\Facades\Socialite as Socialize;
use Validator;
use Illuminate\Validation\Rule;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{

    /**
     * @var string
     */
    protected $guard = "frontend";

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (Auth::guard($this->guard)->check()) {
                return $next($request);
            } else {
                return redirect("/");
            }

        })->only(["logout"]);
    }

    /**
     * Local registration
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    function register()
    {
        if (Auth::guard($this->guard)->check()) {
            return redirect("/");
        }

        if (Request::method() == "POST") {

            $validator = Validator::make(Request::all(), [
                'username' => 'required|min:2|regex:/^[a-zA-Z]([._-]?[a-zA-Z0-9]+)*$/|max:50|unique:users',
                "email" => [
                    "required",
                    "email",
                    Rule::unique('users')->where(function ($query) {
                        return $query->whereNull('provider');
                    }),
                ],
                'password' => 'required|confirmed|min:6',
                'privacy_check' => 'required',
            ]);

            $validator->sometimes('photo', 'required|image', function ($input) {
                return Request::filled('photo');
            });

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $user = new User();

            $user->username = Request::get("username");
            $user->email = Request::get("email");
            $user->provider = NULL;
            $user->password = Request::get("password");
            $user->code = str_random(32);
            $user->status = 0;

            if (Request::hasFile('photo')) {
                $media = new Media();
                $user->photo_id = $media->saveFile(Request::file('photo'));
            }

            $user->save();

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <info@dubarter.com>' . "\r\n";

            mail($user->email, trans('auth.activation_mail_subject'), view('emails.activation', ['user' => $user])->render(), $headers);

//            Mail::send('emails.activation', ["user" => $user], function ($message) use ($user) {
//                $message->to($user->email, $user->first_name)->subject('تفعيل الحساب');
//            });

            return redirect('register')->with('status', trans('auth.account_created'));
        }

        return view("auth.register");
    }

    public function forget_password()
    {

        if (Auth::guard($this->guard)->check()) {
            return redirect("/");
        }

        if (Request::method() == "POST") {

            Request::validate([
                "email" => "required|email",
            ]);

            $user = User::where('email', request('email'))->whereNull('provider')->first();

            if ($user) {

                $new_pass = str_random(8);

                $user->password = $new_pass;

                $user->save();

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: <info@dubarter.com>' . "\r\n";

                mail($user->email, trans('auth.new_pass_mail_subject'), view('emails.new_password', ['new_password' => $new_pass])->render(), $headers);

                return redirect('forget_password')->with('status', trans('auth.new_password_sent'));
            } else {
                return redirect('forget_password')->with('error', trans('common.email_ntfnd'));
            }

        }

        return view("auth.forget_password");
    }

    /**
     * Auth activation
     *
     * @param null $code
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activation($code = null)
    {

        $user = User::where("code", $code)->where("code", "!=", null)->first();

        if (!$user) {
            abort(404);
        }

        $user->code = NULL;
        $user->status = 1;

        $user->save();

        Auth::guard($this->guard)->login($user);

        $this->data["user"] = $user;

        return redirect()->route('profile.edit.form')->with('status', trans('common.act_activated'));
    }

    /**
     * Local login
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    function login()
    {

        if (Auth::guard($this->guard)->check()) {
            return redirect("/");
        }

        if (!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }

        if (Request::method() == "POST") {

            $username = Request::get("username");
            $password = Request::get("password");

            $user = User::where(function ($query) use ($username) {
                $query->where('username', $username)
                    ->orWhere('email', $username);
            })->whereNull('provider')->first();

            if ($user) {

                if (Hash::check($password, $user->password)) {

                    if ($user->status === 1) {
                        Auth::guard($this->guard)->login($user, Request::has("remember"));

                        return redirect()->intended();
                    } else {
                        return redirect()->back()->withErrors(trans('common.act_nt_actv'));
                    }

                } else {
                    return redirect()->back()->withErrors(trans("auth.invalid_username_password"))->withInput(Request::all());
                }

            } else {
                return redirect()->back()->withErrors(trans("auth.invalid_username_password"))->withInput(Request::all());
            }
        }

        return view("auth.login");
    }

    /**
     * Facebook login
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function facebook()
    {

        $row = Socialize::driver('facebook')->user();

        $user = User::where("provider", "facebook")->where('provider_id', $row->id)->first();

        if (!$user) {
            $user = User::where("provider", "facebook")->where('username', $row->name)->first();
        }

        if (!$user) {

            $photo = new Media();

            $avatar_link = '';

            if (isset($row->avatar_original)) {
                $avatar_link = $row->avatar_original;
            } else if (isset($row->avatar)) {
                $avatar_link = $row->avatar;
            }

            $photo_id = $photo->saveLink($avatar_link, "frontend")->id;

            $user = new User();
            $user->username = "user_facebook_" . $row->id;
            $user->provider = "facebook";
            $user->provider_id = $row->id;
            $user->first_name = $row->name;
            $user->last_name = "";
            $user->photo_id = $photo_id;
            $user->email = $row->email;
            $user->status = 1;
            $user->save();
        } else {

            if (!$user->edited) {
                $photo = new Media();

                $avatar_link = '';

                if (isset($row->avatar_original)) {
                    $avatar_link = $row->avatar_original;
                } else if (isset($row->avatar)) {
                    $avatar_link = $row->avatar;
                }

                $photo_id = $photo->saveLink($avatar_link, "frontend")->id;

                $user->photo_id = $photo_id;

                $user->first_name = $row->name;
            }

            if (!$user->provider_id) {
                $user->provider = "facebook";
                $user->provider_id = $row->id;
            }

            $user->email = $row->email;

            $user->save();
        }

        Auth::guard($this->guard)->login($user);

        return redirect()->intended();
    }

    /**
     * Twitter login
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function twitter()
    {

        $row = Socialize::driver('twitter')->user();

        $user = User::where("provider", "twitter")->where('provider_id', $row->id)->first();

        if (!$user && isset($row->email)) {
            $user = User::where("provider", "twitter")->where('email', $row->email)->first();
        }

        if (!$user) {

            $photo = new Media();

            $avatar_link = '';

            if (isset($row->avatar_original)) {
                $avatar_link = $row->avatar_original;
            } else if (isset($row->avatar)) {
                $avatar_link = $row->avatar;
            }

            $photo_id = $photo->saveLink($avatar_link, "frontend")->id;

            $user = new User();
            $user->provider = "twitter";
            $user->provider_id = $row->id;
            $user->username = "user_twitter_" . $row->id;
            $user->email = isset($row->email) ? $row->email : $row->id . "@twitter.com";
            $user->first_name = $row->name;
            $user->last_name = "";
            $user->photo_id = $photo_id;
            $user->status = 1;
            $user->save();
        } else {
            if (!$user->edited) {
                $photo = new Media();

                $avatar_link = '';

                if (isset($row->avatar_original)) {
                    $avatar_link = $row->avatar_original;
                } else if (isset($row->avatar)) {
                    $avatar_link = $row->avatar;
                }

                $photo_id = $photo->saveLink($avatar_link, "frontend")->id;

                $user->photo_id = $photo_id;

                $user->first_name = $row->name;
            }

            $user->email = isset($row->email) ? $row->email : $row->id . "@twitter.com";

            if (!$user->provider_id) {
                $user->provider = "twitter";
                $user->provider_id = $row->id;
            }

            $user->save();
        }

        Auth::guard($this->guard)->login($user);

        return redirect()->intended();
    }

    /**
     * Google login
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function google()
    {

        $row = Socialize::driver('google')->user();

        $user = User::provider("google")->providerId($row->id)->first();

        if (!$user) {

            $photo = new \Media();
            $photo_id = $photo->saveLink($row->avatar, "frontend")->id;

            $user = new User();
            $user->provider = "google";
            $user->provider_id = $row->id;
            $user->username = "user_google_" . $row->id;
            $user->first_name = $row->user["name"]["givenName"];
            $user->last_name = $row->user["name"]["familyName"];
            $user->photo_id = $photo_id;
            $user->email = $row->email;
            $user->status = 1;
            $user->save();
        }

        Auth::guard("")->login($user);

        return redirect("/");
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function logout()
    {

        Auth::guard($this->guard)->logout();

        return redirect("/");
    }


}
