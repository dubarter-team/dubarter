<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Ads;
use App\Models\Api\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoriesController extends BaseApiController
{
	public function listCategories(Request $request)
	{

		$country_code = "eg";

		if ($request->filled("country_code")) {
			$country_code = $request->get("country_code");
		}

		$categories = Categories::active()->parent()
			->select('id', 'name', 'slug', 'listing_type', 'template', 'color', 'logo_id', 'image_id', 'banner_id', 'frontend_user_enabled')
			->whereHas('countries', function ($query) use ($country_code) {
				$query->where('code', '=', $country_code);
			})
			->get();

		if ($categories->count() == 0) {
			return $this->response([], __('api.noCategories'));
		}
		$categories->makeHidden(['logo_id', 'image_id', 'banner_id', 'banner', 'img', 'log']);
		$categories->makeVisible(['ads_count']);
		return $this->response($categories, __('api.success'));
	}

	public function getCategories(Request $request)
	{

		$parent_id = 0;

		if ($request->filled("parent_id")) {
			$parent_id = $request->get("parent_id");
		}

		$country_code = "eg";

		if ($request->filled("country_code")) {
			$country_code = $request->get("country_code");
		}

		$categories = Categories::active()
			->whereHas('countries', function ($query) use ($country_code) {
				$query->where('code', '=', $country_code);
			})->parent($parent_id)
			->select('id', 'name', 'slug', 'listing_type', 'template', 'color', 'logo_id', 'image_id', 'banner_id', 'frontend_user_enabled')->get();

		if ($categories->count() == 0) {
			return $this->response([], __('api.noCategories'));
		}

		$categories->makeHidden(['logo_id', 'image_id', 'banner_id', 'img', 'log', 'banner_image']);

		$categories->makeVisible(['ads_count']);

		return $this->response($categories);
	}

	public function getMainCategory(Request $request)
	{

		$this->validate(['category_id' => 'required',/*'country_code'=>'required'*/]);


		$country_code = "eg";

		if ($request->filled("country_code")) {
			$country_code = $request->get("country_code");
		}

		$category = Categories::where("id", $request->category_id)
			->whereHas('countries', function ($query) use ($country_code) {
				$query->where('code', '=', $country_code);
			})->first();

		if (!$category) {
			return $this->response([], __('api.noCategories'));
		}

		if ($category->parent > 0) {
			return $this->response([], __('api.ntPrntCat'));
		}

		$response = Cache::remember('mob-app-maincategory-' . $request->country_code . '-' . $category->id, 5, function () use ($country_code, $category, $request) {

			$res = [];

			$categories = Categories::where("parent", $category->id)
				->whereHas('countries', function ($query) use ($country_code) {
					$query->where('code', '=', $country_code);
				})->get();

			foreach ($categories as $k => $subcategory) {

				$c = $this->returnItemsCountOf($subcategory, ($subcategory->id == 6) ? "categories" : 'ads', $request->country_code, $request->get('limit'));

				if($c != 0){
					$res['categories'][$k]['id'] = $subcategory->id;
					$res['categories'][$k]['name'] = $subcategory->name;
					$res['categories'][$k]['ads_count'] = $c;
					$res['categories'][$k]['template'] = $subcategory->template;
					$res['categories'][$k]['listing_type'] = ($subcategory->id == 6) ? "categories" : 'ads';
					$res['categories'][$k][($subcategory->id == 6) ? "categories" : 'ads'] = $this->returnItemsOf($subcategory, ($subcategory->id == 6) ? "categories" : 'ads', $request->country_code, $request->get('limit'));
				}

			}

			$posts = new PostsController();

			$articles = $posts->getPostsCollection("articles", $category->id, false, $request->filled('limit') ? $request->get('limit') : 10, 'get');

			$videos = $posts->getPostsCollection("videos", $category->id, false, $request->filled('limit') ? $request->get('limit') : 10, 'get');

			$res['articles'] = ($articles) ? $articles : [];

			$res['videos'] = ($videos and $country_code == "eg") ? $videos : [];

			return $res;
		});

		return $this->response($response, __('api.success'));
	}

	protected function returnItemsOf($categories, $type, $country_code, $limit = 10)
	{
		if ($limit == null) {
			$limit = 10;
		}

		if ($type == 'ads') {
			$ads = Ads::where('categories.id', $categories->id)->latest()->featured()
				->where(function ($query) use ($country_code) {
					$query->where('country.code', $country_code)
						->orWhere('country_id', 0);
				})->where("status", 1)->take($limit)->get()->toArray();

			return $ads;
		}

		$type = $categories->{$type}->take($limit)->makeHidden('img', 'log');

		return $type;
	}

	protected function returnItemsCountOf($categories, $type, $country_code, $limit = 10)
	{
		$ads = Ads::where('categories.id', $categories->id)
			->where(function ($query) use ($country_code) {
				$query->where('country.code', $country_code)
					->orWhere('country_id', 0);
			})->where("status", 1)->count();

		return $ads == 1 ? 0 : $ads;
	}

	public function get_parents()
	{
		$category = Categories::where('id', request('category_id', 0))->where('status', 1)->first();

		if (!$category) {
			return $this->response([], __('api.error'), [__('api.cat_not_found')]);
		}

		$parents = Categories::get_parents_objects($category->id);

		return $this->response($parents, __('api.success'));
	}
}
