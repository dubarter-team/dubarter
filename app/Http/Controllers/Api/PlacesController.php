<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Places;
use Illuminate\Http\Request;


class PlacesController extends BaseApiController
{
    public function listPlaces(Request $request,$locale)
    {
        $places = Places::active()->where('parent',0)->get();
        if(count($places) == 0)
        {
            return $this->response($places,__('api.noPlaces'));
        }
        return $this->response($places,__('api.success'));
    }

    public function getPlacesByParentUId(Request $request)
    {
        $this->validate(['parent_id'=>'required']);
        $places = Places::active()->where('parent',$request->parent_id)->select('id','code',"name",'currency','lat','lng')->get();
        if(count($places) == 0)
        {
            return $this->response($places,__('api.noPlaces'));
        }
        return $this->response($places,__('api.success'));
    }

}
