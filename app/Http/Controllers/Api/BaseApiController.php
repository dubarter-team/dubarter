<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class BaseApiController extends Controller
{
    public function validate(array $rules, array $messages = [], array $customAttributes = [])
    {
        $request = request();
        $validator = \Validator::make($request->all(), $rules, $messages, $customAttributes);
        if ($validator->fails()) {
            throw new ValidationException($validator);
            return $this->response([], '', $validator->errors(), 400);
        }
    }

    public function response($data, $message = '', $errors = [], $code = 200)
    {
        if (!$data || $data && $data == [] && count($data)) {
            $data = null;
        }
        return response()->json(['scode' => $code, 'message' => $message, 'data' => $data, 'errors' => $errors]);
    }

    protected function userObject()
    {
        $columns = ['id', 'first_name', 'last_name', 'email', 'username', 'phone', 'lang', 'country_id', 'city_id', 'address', 'facebook', 'twitter', 'linked_in', 'google_plus'];
        $user = auth()->guard('mobile-api')->user();
        $data = [
        	'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'email' => $user->email,
            'thumbnail_photo' => $user->thumbnail_photo_url,
            'medium_photo' => $user->medium_photo_url,
            'phone' => $user->phone,
            'photo_id' => $user->photo_id,
        ];
        return $data;
    }
}
