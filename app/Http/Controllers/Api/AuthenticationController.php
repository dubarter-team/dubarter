<?php

namespace App\Http\Controllers\Api;


use App\Mail\UserActivationMail;
use App\Models\Api\UserApi;
use Dot\Media\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends PassportHandler
{
	public function register(Request $request)
	{

		$rules = [
			'username' => 'required|min:2|regex:/^[a-zA-Z]([._-]?[a-zA-Z0-9]+)*$/|max:50|unique:users',
			"email" => [
				"required",
				"email",
				Rule::unique('users')->where(function ($query) {
					return $query->whereNull('provider');
				}),
			],
			'password' => 'required|min:6'
		];

		if ($request->filled('first_name')) {
			$rules['first_name'] = 'alpha';
		}

		if ($request->filled('last_name')) {
			$rules['last_name'] = 'alpha';
		}

		$this->validate($rules);

		$user = new UserApi();

		if ($request->filled('photo')) {
			$media = new Media();
			$user->photo_id = $media->saveContent($request->photo)->id;
		} else {
			$user->photo_id = 0;
		}

		$user->email = $request->email;
		$user->password = $request->password;
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->username = $request->username;
		$user->phone = $request->phone;

		$user->mobile_token = $request->mobile_token;

		$user->code = str_random(32);

		$user->save();

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <info@dubarter.com>' . "\r\n";

		mail($user->email, 'dubarter activation link', view('emails.activation', ['user' => $user])->render(), $headers);

		//$login = $this->generateTokenByCredentials($request->email,$request->password);

//        Mail::send('emails.activation', ["user" => $user], function ($message) use ($user) {
//            $message->to($user->email, $user->first_name)->subject('تفعيل الحساب');
//        });

		$data = [
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'username' => $user->username,
			'email' => $user->email,
			'thumbnail_photo' => $user->thumbnail_photo_url,
			'medium_photo' => $user->medium_photo_url,
			'phone' => $user->phone,
			'photo_id' => $user->photo_id
		];

		return $this->response($data, __('api.usrRegSuccess'));
	}

	public function social_register(Request $request)
	{
		$user_data = $this->get_user_from_tokens($request->provider);

		if ($user_data) {

			$user = UserApi::where("provider", $request->provider)->where('provider_id', $user_data->id)->first();

			if (!$user && $request->provider == 'facebook') {
				$user = UserApi::where("provider", $request->provider)->where('username', $user_data->name)->first();
			}

			if (!$user && $request->provider == 'twitter') {
				$user = UserApi::where("provider", $request->provider)->where('email', $user_data->email)->first();
			}

			if (!$user) {
				$photo = new Media();

				$avatar_link = '';

				if (isset($user_data->avatar_original)) {
					$avatar_link = $user_data->avatar_original;
				} else if (isset($user_data->avatar)) {
					$avatar_link = $user_data->avatar;
				}

				$photo_id = !empty($avatar_link) ? $photo->saveLink($avatar_link, "frontend")->id : 0;

				$user = new UserApi();

				$user->username = "user_" . $request->provider . "_" . $user_data->id;
				$user->provider = $request->provider;
				$user->provider_id = $user_data->id;
				$user->first_name = $user_data->name;
				$user->last_name = "";
				$user->photo_id = $photo_id;

				if (isset($user_data->email) && !empty($user_data->email)) {
					$user->email = $user_data->email;
					$user->status = 1;
				} else {
					$user->email = '';
					$user->status = 2;
				}

				$user->save();
			} else {

				if (!$user->edited) {
					$photo = new Media();

					$avatar_link = '';

					if (isset($user_data->avatar_original)) {
						$avatar_link = $user_data->avatar_original;
					} else if (isset($user_data->avatar)) {
						$avatar_link = $user_data->avatar;
					}

					$photo_id = !empty($avatar_link) ? $photo->saveLink($avatar_link, "frontend")->id : 0;

					$user->photo_id = $photo_id;

					$user->first_name = $user_data->name;
				}

				if (!$user->provider_id) {
					$user->provider = $request->provider;
					$user->provider_id = $user_data->id;
				}

				$user->save();
			}

			$data = [
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'username' => $user->username,
				'email' => $user->email,
				'thumbnail_photo' => $user->thumbnail_photo_url,
				'medium_photo' => $user->medium_photo_url,
				'photo_id' => $user->photo_id,
				'phone' => $user->phone,
				'status' => $user->status,
				'user_token' => null,
				'provider' => $request->provider,
				'provider_id' => $user_data->id
			];

			if ($user->status == 1) {
				$user_token = $user->token() ? $user->token() : $user->createToken('socialLogin');

				$data['user_token']['token_type'] = '';
				$data['user_token']['expires_in'] = '';
				$data['user_token']['access_token'] = @$user_token->accessToken;
				$data['user_token']['refresh_token'] = '';
			}

			return $this->response($data, __('api.usrRegSuccess'));
		} else {
			return $this->response(null, __('api.error'), [__('api.loginError')], 400);
		}
	}

	private function get_user_from_tokens($provider)
	{
		$user = null;

		if ($provider == 'facebook') {
			$user = Socialite::driver('facebook')->userFromToken(request('access_token'));

		} else if ($provider == 'twitter') {

			if (request("platform") == "ios") {
				Config::set("services.twitter", [
					'client_id' => 'ISKZ1tbwLwWbxsD2x5giGqVhg',
					'client_secret' => 'UHuVwTJ24C4aPdwWRFBndZvKOQcrzzushwv7OwfxlgdhhtHXtC',
					'redirect' => env("APP_URL") . "/ar-eg/login/twitter"
				]);
			}

			$user = Socialite::driver('twitter')->userFromTokenAndSecret(request('token'), request('secret'));
		}

		return $user;
	}

	private function _sendActivationMail()
	{

		\Mail::to(request()->get('email'))->send(new UserActivationMail());

	}

	public function activateMail(Request $request)
	{
		$this->validate([
			'email' => 'required|email',
			'code' => 'required|min:6|max:6',
		]);

		$user = UserApi::where('email', $request->email)->where('code', $request->code)->first();

		if (!$user) {
			return $this->response([], __('api.error'), [__('api.usrNtFnd')], 404);
		}

		if ($user->status == 1) {

			return $this->response([], __('api.error'), [__('api.usrAlreadyActive')]);

		} else if ($user->status == 0) {

			$user->status = 1;
			$user->code = null;
			$user->save();

			return $this->response([], __('api.usrActivetedSuccessfully'), []);
		}

		return $this->response([], __('api.error'), [], 400);
	}

	public function login()
	{
		$this->validate([
			'email' => 'required|email',
			'password' => 'required|min:6',

		]);

		$check_user = $this->checkForCredentials(request('email'), request('password'));

		if (!$check_user instanceof UserApi) {
			return $this->response(null, __('api.error'), [$check_user], 400);
		}

		$tokens = $this->generateTokenByCredentials(request('email'), request('password'));

		if (!$tokens) {
			return $this->response(null, __('api.error'), [__('api.loginError')], 400);
		}

		$data = [
			'id' => $check_user->id,
			'first_name' => $check_user->first_name,
			'last_name' => $check_user->last_name,
			'username' => $check_user->username,
			'email' => $check_user->email,
			'thumbnail_photo' => $check_user->thumbnail_photo_url,
			'medium_photo' => $check_user->medium_photo_url,
			'photo_id' => $check_user->photo_id,
			'phone' => $check_user->phone,
			'user_token' => [
				'token_type' => $tokens->token_type,
				'expires_in' => $tokens->expires_in,
				'access_token' => $tokens->access_token,
				'refresh_token' => $tokens->refresh_token,
			]
		];

		return $this->response($data, __('api.loginSuccess'));
	}

	private function checkForCredentials($email, $password)
	{
		$user = UserApi::where('email', $email)->whereNull('provider')->first();

		if (!$user) {
			return __('api.usrNtFnd');
		}

		if (!\Hash::check($password, $user->password)) {
			return __('api.wrongPassword');
		}

		if ($user->status != 1) {
			return __('api.user_not_activated');
		}

		return $user;
	}

	public function refresh_token(Request $request)
	{
		$this->validate([
			'refresh_token' => 'required',
		]);

		$request = $this->refreshToken($request->refresh_token);

		if (!$request) {
			return $this->response([], __('api.error'), [__('api.refreshTokenError')], 400);
		}

		return $this->response($request, __('api.success'));
	}

	public function check_status()
	{
		$email = request('email');

		if ($email) {

			$q = UserApi::where('email', $email);

			if (request('provider') && request('provider_id')) {
				$q->where("provider", request('provider'))->where('provider_id', request('provider_id'));
			} else {
				$q->whereNull('provider');
			}

			$user = $q->first();

			if ($user) {

				return $this->response(['status' => $user->status], '');

			}

		}

		return $this->response(null, __('api.error'), [__('api.usrNtFnd')], 401);
	}

	public function resend_activation()
	{
		$email = request('email');

		$provider = request('provider');

		$provider_id = request('provider_id');

		if ($email && !$provider) {

			$user = UserApi::where('email', $email)->whereNull('provider')->where('status', '!=', 1)->first();

			if ($user) {

				$user->code = str_random(32);

				$user->status = 0;

				$user->save();

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: <info@dubarter.com>' . "\r\n";

				mail($user->email, 'dubarter activation link', view('emails.activation', ['user' => $user])->render(), $headers);

				return $this->response(['status' => true], '');
			}

		} else if ($provider && $provider_id && $email) {
			$user = UserApi::where("provider", $provider)->where('provider_id', $provider_id)->first();

			if ($user) {

				$user->code = str_random(32);

				$user->email = $email;

				$user->status = 0;

				$user->save();

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: <info@dubarter.com>' . "\r\n";

				mail($user->email, 'dubarter activation link', view('emails.activation', ['user' => $user])->render(), $headers);

				return $this->response(['status' => true], '');
			}
		}

		return $this->response(null, __('api.error'), [__('api.usrNtFnd')], 401);
	}

	public function forget_password()
	{

		$this->validate([
			"email" => "required|email|exists:users",
		]);

		$user = UserApi::where('email', request('email'))->whereNull('provider')->first();

		if ($user) {

			$new_pass = str_random(8);

			$user->password = $new_pass;

			$user->save();

			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: <info@dubarter.com>' . "\r\n";

			mail($user->email, trans('auth.new_pass_mail_subject'), view('emails.new_password', ['new_password' => $new_pass])->render(), $headers);

			return $this->response(['status' => true], '');
		}

		return $this->response(null, __('api.error'), [__('api.usrNtFnd')], 400);
	}

}
