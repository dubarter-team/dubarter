<?php

namespace App\Http\Controllers\Api;


use App\Models\Api\Ads;
use App\Models\Api\AdsLikes;
use Illuminate\Http\Request;
use App\Models\Api\UserApi;
use Dot\Media\Models\Media;
use Illuminate\Validation\Rule;

class UsersController extends PassportHandler
{
    public function getProfile()
    {
        $user = $this->userObject();
        return $this->response($user);
    }

    public function updateProfile(Request $request)
    {


        $user = auth()->guard('mobile-api')->user();

        $rules = [
            "first_name" => "required",
            "last_name" => "required",
        ];

        if ((trim($request->get('username')) != trim($user->username)) && (!$user->provider)) {
            $rules['username'] = ['required', 'min:2', 'max:50', 'regex:/^[a-zA-Z]([._-]?[a-zA-Z0-9]+)*$/', Rule::unique('users')->ignore($user->id)];
        }

        $this->validate($rules);

        if (request('username') && !$user->provider) {
            $user->username = request('username');
        }

        if (request('first_name')) {
            $user->first_name = request('first_name');
        }

        if (request('last_name')) {
            $user->last_name = request('last_name');
        }

        if (request('phone')) {
            $user->phone = request('phone');
        }

        if (request('mobile_token')) {
            $user->mobile_token = request('mobile_token');
        }

        if (request('password') && !$user->provider) {
            $user->password = request('password');
        }

        if ($request->filled('photo')) {
            $media = new Media();

            $user->photo_id = $media->saveContent($request->photo)->id;
        }

        $user->edited = 1;

        $user->save();

        $data = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'email' => $user->email,
            'thumbnail_photo' => $user->thumbnail_photo_url,
            'medium_photo' => $user->medium_photo_url,
            'phone' => $user->phone,
            'photo_id' => $user->photo_id,
            'provider' => $user->provider,
            'provider_id' => $user->provider_id
        ];

        return $this->response($data, __('api.usr_edit_success'));
    }

    public function update_mobile_token(Request $request)
    {
        $res = ['status' => false];

        if ($request->get('mobile_token')) {
            $user = auth()->guard('mobile-api')->user();
            $user->mobile_token = $request->mobile_token;
            $user->save();

            $res['status'] = true;
        }

        return $this->response($res);
    }

    public function verifyEmail(Request $request)
    {
        $this->validate([
            'email' => 'email|required',
        ]);
        $user = UserApi::where('email', $request->email)->first();
        if (!$user) {
            return $this->response([], __('api.error'), [__('api.usrNtFnd')], 404);
        }
        if ($user->status == 1) {
            return $this->response([], __('api.emailActive'));
        }
        return $this->response([], __('api.!emailActive'));
    }

    public function likeDislikeAd(Request $request)
    {
        $this->validate([
            'ad_id' => 'required|integer',
        ]);
        $like = AdsLikes::where('ad_id', $request->ad_id)->where('user_id', auth()->guard('mobile-api')->user()->id)->first();
        if ($like) {
            $like->delete();
            return $this->response(['ad_id' => $request->ad_id, 'status' => 'disliked']);
        }
        $like = new AdsLikes();
        $like->ad_id = $request->ad_id;
        $like->user_id = auth()->guard('mobile-api')->user()->id;
        $like->save();
        return $this->response(['ad_id' => $request->ad_id, 'status' => 'liked']);
    }

    public function userAdsLikes(Request $request)
    {
        $adsArray = auth()->guard('mobile-api')->user()->likes->pluck('ad_id')->toArray();
        $ads = Ads::whereIn('_id', $adsArray)->get()->toArray();
        return $this->response($ads, 'api_success');
    }

    public function userLikesCount(Request $request)
    {
        $likesCount = AdsLikes::where('user_id', auth()->guard('mobile-api')->user()->id)->count();
        return $this->response(['user_like_count' => $likesCount], 'api_success');
    }

}
