<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Ads;
use App\Models\Api\Post;
use App\Models\LiveComment;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Dot\Options\Facades\Option;

class PostsController extends BaseApiController
{

    /**
     * @param string $perPage
     * @return \Illuminate\Http\JsonResponse
     */
    public function listArticles(Request $request)
    {
        $perPage = ($request->has('perPage')) ? intval($request->perPage) : 10;
        $articles = $this->getPostsCollection('articles', request('category_id', 0), $request->has('featured'), $perPage);
        if ($articles == false) {
            return $this->response([], __('api.noArticles'));
        }
        return $this->response($articles, __('api.success'));
    }


    /**
     * @param string $perPage
     * @return \Illuminate\Http\JsonResponse
     */
    public function listVideos(Request $request)
    {
        $perPage = ($request->has('perPage')) ? intval($request->perPage) : 10;
        $videos = $this->getPostsCollection('videos', request('category_id', 0), $request->has('featured'), $perPage);
        if ($videos == false) {
            return $this->response([], __('api.noVideos'));
        }
        return $this->response($videos, __('api.success'));
    }

    public function list_livestream_comments(Request $request)
    {
        $comments = LiveComment::with(['user'])->get();

        return $this->response($comments, __('api.success'));
    }

    public function add_livestream_comment(Request $request)
    {
        $res = ['status' => false];

        if ($request->get('comment')) {
            $lc = new LiveComment();

            $lc->user_id = auth()->guard("mobile-api")->user()->id;
            $lc->comment = $request->get('comment');

            $lc->save();

            $res['status'] = true;
        }

        return $this->response($res, __('api.success'));
    }

    public function livestream_options(Request $request)
    {
        $res = [
            'status' => !!Option::get('livestream_status'),

            'url' => Option::get('livestream_url'),

            'title' => Option::get('livestream_title'),

            'message' => Option::get('livestream_message'),
        ];

        return $this->response($res, __('api.success'));
    }

    /**
     * @param string $article_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getArticle(Request $request)
    {
        $this->validate([
            'article_id' => 'required|integer'
        ]);
        $video = $this->getPostsObject($request->article_id, ['article', 'post']);
        return $this->response($video, __('api.success'));
    }

    /**
     * @param string $video_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVideo(Request $request)
    {
        $this->validate([
            'video_id' => 'required|integer'
        ]);
        $video = $this->getPostsObject($request->video_id, ['video']);
        return $this->response($video, __('api.success'));
    }


    public function relatedAds(Request $request)
    {
        $this->validate([
            'post_id' => 'required|integer'
        ]);

        $limit = $request->filled('limit') ? $request->limit : 5;

        $article = Post::find($request->post_id);
        if (!$article || $article->status != 1) {
            throw new NotFoundHttpException(__('api.postNtFnd'));
        }
        $tagsArray = $article->tags->pluck('id')->toArray();
        $ads = Ads::whereIn('tags.id', $tagsArray)->take($limit)->orderBy('published_at', 'DESC')->get()->toArray();
        return $this->response($ads, __('api.success'));
    }

    public function getPostsCollection($type = "articles", $category_id = 0, $featured = false, $perPage = 10, $fetch = "paginate")
    {
        $query = Post::$type()->active()->latest();

        if ($featured) {
            $query->featured();
        }

        if ($category_id) {
            $query->whereHas("categories", function ($query) use ($category_id) {
                $query->where("categories.id", $category_id);
            });
        }

        if ($fetch == "paginate") {
            $articles = $query->with('img', 'med')->paginate($perPage)->appends(request()->all());
            $articles->makeHidden(['img']);
            if ($articles->total() == 0) {
                return false;
            }
        } else {
            $articles = $query->with('img', 'med')->take($perPage)->get();
            $articles->makeHidden(['img']);
            if ($articles->count() == 0) {
                return false;
            }
        }
        return $articles;
    }

    /**
     * @param $id
     * @param $type
     * @return mixed
     */
    public function getPostsObject($id, $type)
    {
        $article = Post::find($id);

        if (!$article || $article->status != 1) {
            throw new NotFoundHttpException(__('api.postNtFnd'));
        }

        if (!in_array($article->format, $type)) {
            throw new NotFoundHttpException(__('api.rongEndpoint'));
        }

        $article->makeHidden(['img', 'image_id', 'media_id', 'user_id', 'status', 'format', 'lang', 'created_at', 'updated_at', 'published_at', 'country_id', 'city_id']);

        $article->increment('views', 1);

        return $article;
    }

}
