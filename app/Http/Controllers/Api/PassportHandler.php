<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PassportHandler extends BaseApiController
{
    protected function generateTokenByCredentials($username,$password = null)
    {
        $credentials = [
            'username'=>$username,
            'grant_type' => 'password',
        ];

        if($password){
            $credentials['password'] = $password;
        }
        return $this->makeRequest(url('api/oauth/token'),$credentials);
    }

    protected function refreshToken($refresh_token)
    {
        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
        ];
        return $this->makeRequest(url('api/oauth/token'),$params);
    }

    private function makeRequest($url,$params)
    {
        $client   = new \GuzzleHttp\Client();
        $originalParams = [
            'form_params' => [
                'client_id'=>$this->passportClient('password')['client_id'],
                'client_secret'=>$this->passportClient('password')['client_secret'],
                'scope' => '*',
            ]
        ];
        $postData = ['form_params'=>array_merge($originalParams['form_params'],$params)];
        try{
            $response = $client->post($url,$postData );
            $authData = \GuzzleHttp\json_decode($response->getBody());
            return $authData;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function passportClient($type='password')
    {
           return [
               'client_id'      =>config("services.passport.$type.client_id"),
               'client_secret'  =>config("services.passport.$type.client_secret"),
           ];
    }


}
