<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Attributes;
use App\Models\Api\Categories;

class AttributesController extends BaseApiController
{

    public function getAttributesByCateogryId()
    {
        $this->validate([
            'category_id' => 'required',
        ]);

        $cats = Categories::get_parents(request('category_id', 0));

        if (!count($cats)) {
            return $this->response([], __('api.error'), [__('api.cat_not_found')]);
        }

        $attributes = Attributes::whereHas('categories', function ($query) use ($cats) {
            return $query->whereIn('id', $cats);
        })->with(['bIcon', 'sIcon'])->select('id', 'template', 'type', 'name', 'options', 'default', 'slug', 'data_type', 'big_icon_id', 'small_icon_id', 'required', 'searchable')->get();

        $attributes->makeHidden(['bIcon', 'sIcon']);

        if (!count($attributes)) {
            return $this->response([], __('api.error'), [__('api.noAttr')]);
        }

        return $this->response($attributes, __('api.success'));
    }
}
