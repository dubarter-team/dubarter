<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Comment;
use App\Models\Api\Post;
use App\Models\Api\UserApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentsController extends BaseApiController
{
    public function getArticlesComments(Request $request)
    {
        $perPage = ($request->has('perPage'))?intval($request->perPage):10;
        $this->validate([
            'article_id'=>'required'
        ]);
        $post = Post::find($request->article_id);
        if(!$post){
            throw new NotFoundHttpException(__('api.postNtFnd'));
        }
        if($post->type != "article"){
            throw new NotFoundHttpException(__('api.rongEndpoint'));
        }
        return $this->response($this->getTypeCommentsWithReplies('article',$post->id,$perPage),__('api.success'));
    }

    public function getVideosComments(Request $request)
    {
        $perPage = ($request->has('perPage'))?intval($request->perPage):10;
        $this->validate([
            'video_id'=>'required'
        ]);
        $post = Post::find($request->video_id);
        if(!$post){
            throw new NotFoundHttpException(__('api.postNtFnd'));
        }
        if($post->type != "video"){
            throw new NotFoundHttpException(__('api.rongEndpoint'));
        }
        return $this->response($this->getTypeCommentsWithReplies('video',$post->id,$perPage),__('api.success'));
    }

    public function getCommentReplies(Request $request)
    {
        $perPage = ($request->has('perPage'))?intval($request->perPage):10;
        $this->validate([
            'comment_id'=>'required'
        ]);

        $comment =  Comment::find($request->comment_id);
        if(!$comment){
            throw new NotFoundHttpException(__('api.cmntNtFnd'));
        }
        $replies =  Comment::where('parent',$request->comment_id)->paginate($perPage);
        $commentsArray['main_comment']['id'] = $comment->id;
        $commentsArray['main_comment']['id'] = $comment->id;
        $commentsArray['main_comment']['id'] = $comment->id;
        $commentsArray['main_comment']['user']['id'] = $comment->user->id;
        $commentsArray['main_comment']['user']['name'] = $comment->user->first_name . " " . $comment->user->last_name;
        $commentsArray['main_comment']['user']['photo'] = $comment->user->medium_photo_url;

        foreach($replies as $k => $reply) {
            $commentsArray['replies'][$k]['id'] = $reply->id;
            $commentsArray['replies'][$k]['content'] = $reply->content;
            $commentsArray['replies'][$k]['created_at'] = $reply->created_at;
            $commentsArray['replies'][$k]['user']['id'] = $reply->user->id;
            $commentsArray['replies'][$k]['user']['name'] = $reply->user->first_name . " " . $reply->user->last_name;
            $commentsArray['replies'][$k]['user']['photo'] = $reply->user->medium_photo_url;
        }
        return $this->response($commentsArray,__('api.success'));
    }


    private function getTypeCommentsWithReplies($type,$type_id,$perPage)
    {
        if(!in_array($type,['article', 'video'])){
            return $this->response([],__('api.error'),[__('api.rongEndpoint')]);
        }

        $comments = Comment::where('object_type',$type)->where('object_id',$type_id)->where('parent',0)->latest()->paginate($perPage);

        foreach($comments as $k => $comment){
            $commentsArray[$k]['id']            = $comment->id;
            $commentsArray[$k]['content']       = $comment->content;
            $commentsArray[$k]['created_at']    = $comment->created_at;
            $commentsArray[$k]['user']['id']    = $comment->user->id;
            $commentsArray[$k]['user']['name']  = $comment->user->first_name ." ". $comment->user->last_name ;
            $commentsArray[$k]['user']['photo'] = $comment->user->medium_photo_url;
            foreach ($comment->replies as $i => $reply){
                $commentsArray[$k]['replies'][$i]['id']             = $reply->id;
                $commentsArray[$k]['replies'][$i]['content']        = $reply->content;
                $commentsArray[$k]['replies'][$i]['created_at']     = $reply->created_at;
                $commentsArray[$k]['replies'][$i]['user']['id']     = $reply->user->id;
                $commentsArray[$k]['replies'][$i]['user']['name']   = $reply->user->first_name ." ". $reply->user->last_name ;
                $commentsArray[$k]['replies'][$i]['user']['photo']  = $reply->user->medium_photo_url;
            }

        }
        return $commentsArray;
    }

    public function postComment(Request $request)
    {
        $this->validate([
            'post_id'=>'required',
            'content'=>'required',
        ]);

        $post = Post::find($request->post_id);
        if(!$post)
        {
            throw new NotFoundHttpException(__('api.postNtFnd'));
        }
        $comment = new Comment();
        $comment->object_type   = $post->type;
        $comment->object_id     = $post->id;
        $comment->user_id       = auth()->guard('mobile-api')->user()->id;
        $comment->parent        = 0;
        $comment->content       = $request->get('content');
        $comment->status        = 1;
        $comment->save();
        return $this->response([],__('api.cmntPstdScss'));
    }

    public function postReply(Request $request)
    {
        $this->validate([
            'comment_id'=>'required',
            'content'=>'required',
        ]);

        $comment = Comment::find($request->comment_id);
        if(!$comment)
        {
            throw new NotFoundHttpException(__('api.cmntNtFnd'));
        }
        $reply = new Comment();
        $reply->object_type   = $comment->post->type;
        $reply->object_id     = $comment->post->id;
        $reply->user_id       = auth()->guard('mobile-api')->user()->id;
        $reply->parent        = $comment->id;
        $reply->content       = $request->get('content');
        $reply->status        = 1;
        $reply->save();
        return $this->response([],__('api.rplyPstdScss'));
    }
}
