<?php

namespace App\Http\Controllers\Api;

use App\Index;
use App\Models\Ad;
use App\Models\Api\Ads;
use App\Models\Api\Categories;
use Illuminate\Http\Request;
use Dot\Media\Models\Media;
use App\Events\AdViewed;

class AdsController extends BaseApiController
{
    public function listAds(Request $request)
    {

        $perPage = ($request->filled('perPage')) ? $request->perPage : 10;

        $order_by = in_array(request('order_by'), ['published_at', 'price']) ? request('order_by') : 'published_at';

        $order_type = in_array(request('order_type'), ['asc', 'desc']) ? request('order_type') : 'desc';

        $available_status = [-1, 0, 1, 2, 3, 4];

        $query = Ads::orderBy($order_by, $order_type);

        if ($request->filled('status') && in_array($request->get('status'), $available_status)) {
            if ($request->get('status') != -1) {
                $query->where('status', $request->get('status'));
            }
        } else {
            $query->where('status', 1);
        }

        if ($request->filled('category_id')) {
            $query->where('categories.id', $request->category_id);
        }

        if ($request->filled('country_id')) {
            $query->where('country.id', $request->country_id);
        }

        if ($request->filled('city_id')) {
            $query->where('city.id', $request->city_id);
        }

        if ($request->filled('tag_id')) {
            $query->where('tags.id', $request->tag_id);
        }

        if ($request->filled('featured')) {
            $query->where('featured', 1);
        }

        if ($request->filled('price_from')) {
            $price_from = number_format($request->price_from, 2, '.', "");

            $query->where('price', '>=', $price_from);
        }

        if ($request->filled('price_to')) {
            $price_to = number_format($request->price_to, 2, '.', "");

            $query->where('price', '<=', $price_to);
        }

        if ($request->filled('attributes')) {

        	if(is_array($request->get('attributes'))){
				$attributes = $request->get('attributes');
			}else{
				$attributes = @json_decode($request->get('attributes'), 1);
			}

            if ($attributes && count($attributes)) {

                foreach ($attributes as $attribute => $v) {
                    if (count($v)) {

                        for ($i = 0; $i < count($v); $i++) {
                            $v[$i] = strtolower($v[$i]);

                            //$v[$i] = str_replace('-', '\-', $v[$i]);
                        }

                        $query->whereIn('attrs.' . $attribute, $v);
                    }
                }

            }
        }

        if (request('list_type') == 'my_ads') {
            if (auth()->guard("mobile-api")->check()) {
                $query->where('user.id', auth()->guard("mobile-api")->user()->id);
            }
        }

        if (request('user_id', 0)) {
            $query->where('user.id', request('user_id'));
        }

        if ($request->filled('query')) {
            $query->search(request('query'));
        }

        $ads = $query->paginate($perPage)->appends($request->all());

        if ($ads->total() === 0) {
            return $this->response([], __('api.error'), [__('api.noAds')]);
        }

        return $this->response($ads, __('api.success'));
    }

    public function getAd(Request $request)
    {
        $ad = null;

        if ($request->ad_id) {
            $ad = Ads::find($request->ad_id);
        } else if ($request->slug) {
            $ad = Ads::where("slug", $request->slug)->first();
        }

        if (!$ad) {
            return $this->response([], __('api.error'), [__('api.adNtFnd')]);
        }

        return $this->response($ad->toArray(), __('api.success'));
    }

    public function getRelatedAds(Request $request)
    {
        $this->validate(['ad_id' => 'required']);

        $ad = Ads::find($request->ad_id);

        if (!$ad) {
            return $this->response([], __('api.error'), [__('api.adNtFnd')]);
        }

        $categories = collect($ad->categories)->where('direct', 1)->first();

        $relatedAds = Ads::where("country.id", $ad->country["id"])->where('categories.id', $categories['id'])->whereNot('slug', $ad->slug)->where("status", 1)->latest()->take(6)->get();


        return $this->response($relatedAds->toArray(), __('api.success'));
    }

    public function createAdd(Request $request)
    {
        $this->validate([
            'title' => 'required',
            'content' => 'required',
            'images.*' => 'image|max:5000',
            'category_id' => 'required',
            //'attributes'    => 'required',
            'country_id' => 'required',
            //'city_id'       => 'required',
            //'region_id'     => 'required',
        ]);

        $ad = new Ad();
        $ad->title = $request->get('title');
        $ad->price = $request->get('price');
        $ad->content = $request->get('content');
        $ad->country_id = $request->get('country_id');
        $ad->city_id = $request->get('city_id');
        $ad->region_id = $request->get('region_id');
        $ad->email = auth()->guard("mobile-api")->user()->email;
        $ad->phone = $request->get('phone');
        $ad->status = 2;
        $ad->user_id = auth()->guard("mobile-api")->user()->id;

        $ad->source = 'mobile';
        $ad->save();

        $category = Categories::where('id', $request->get('category_id'))->with(['parent_category'])->first();

        if ($category) {
            $cat = $category;
            while (true) {
                $ad->categories()->attach($cat->id, ['direct' => ($cat->id == $request->get('category_id') ? 1 : 0)]);

                if ($cat->parent == 0) {
                    break;
                } else {
                    $cat = $cat->parent_category;
                }
            }
        }

        $custom_attributes = Categories::get_attributes($category);

        if (count($custom_attributes)) {


        	if(is_array(request('attributes', ''))){
				$attrs_values = request('attributes', '');
			}else{
				$attrs_values = @json_decode(request('attributes', ''), 1);
			}

			//return $this->response($attrs_values, __('api.success'));

            if ($attrs_values && count($attrs_values)) {
                foreach ($custom_attributes as $attr) {

                    $value = '';

                    if (isset($attrs_values[$attr->slug])) {
                        if ($attr->type == 'single') {
                            $value = [$attrs_values[$attr->slug]];
                        } elseif ($attr->type == 'multiple') {
                            $value = $attrs_values[$attr->slug];
                        }

                        $ad->custom_attributes()->attach($attr->id, ['value' => $value]);
                    }

                }
            }

        }

        Index::save($ad->id);

        return $this->response($ad, __('api.success'));
    }

    public function store_images(Request $request)
    {

        $ad = Ad::withoutGlobalScopes()->where('id', request('ad_id', 0))->first();

        if (!$ad) {
            return $this->response([], __('api.error'), [__('api.adNtFnd')]);
        }

        $images_ids = [];

        if (count(request('images', []))) {

            $i = 0;

            foreach ($request->images as $image) {

                $media = new Media();

                $images_ids[] = $media_id = $media->saveContent($image)->id;

                $ad->media()->attach($media_id, ['order' => $i]);

                $i++;
            }

            Index::save($ad->id);

        }

        return $this->response(['success' => true, 'media_ids' => $images_ids], __('api.success'));
    }
}
