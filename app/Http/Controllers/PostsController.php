<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Place;
use Illuminate\Support\Facades\Cache;

/**
 * Class BlogController
 *
 * @package App\Http\Controllers
 */
class PostsController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    public $articles_per_page = 8;

    /**
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blog($slug = false)
    {

        $category = null;

        if ($slug) {

            $category = Category::slug($slug)
					//->withCount('videos')->withCount('articles')
					->first();

            if (!$category) {
                return app()->abort(404);
            }
        }

        $this->data["category"] = $category;

        $this->data['format'] = 'article';

        $this->data['per_page'] = $this->articles_per_page;

        $this->data["featured_posts"] = $featured_posts = Cache::remember('category-featured-articles-' . $slug, 60, function () use ($category) {

            $query = Post::latest()->format("article");

            if ($category) {
                $query->inCategory($category);
            }

            return $query->with(['image'])->orderBy('published_at', 'desc')->take(8)->get();
        });

        $posts = $this->get_posts('article', $category ? $category->id : 0, $featured_posts->pluck('id')->toArray());

        $this->data["posts_count"] = count($posts);

        $this->data['posts_html'] = view('partials.posts.posts_listing', ['posts' => $posts])->render();

        $this->data["posts_type"] = 'articles';

        // Sections appeared in header navigation menu

        $this->data["nav_categories"] = Category::inCategory($category)
            ->inLandingNav()->take(3)->get();

        $this->searchBox($category);

        return view("posts", $this->data);
    }


    /**
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function videos($slug = false)
    {

        $category = null;

        if ($slug) {

            $category = Category::slug($slug)
				//->withCount('videos')->withCount('articles')
				->first();

            if (!$category) {
                return app()->abort(404);
            }
        }

        $this->data["category"] = $category;

        $this->data['format'] = 'video';

        $this->data['per_page'] = $this->articles_per_page;

        $this->data["featured_posts"] = $featured_posts = Cache::remember('category-featured-videos-' . $slug, 60, function () use ($category) {
            $query = Post::latest()->format("video");

            if ($category) {
                $query->inCategory($category);
            }

            return $query->with(['image'])->orderBy('published_at', 'desc')->take(8)->get();
        });

        $posts = $this->get_posts('video', $category ? $category->id : 0, $featured_posts->pluck('id')->toArray());

        $this->data["posts_count"] = count($posts);

        $this->data['posts_html'] = view('partials.posts.posts_listing', ['posts' => $posts])->render();

        $this->data["posts_type"] = 'videos';

        // Sections appeared in header navigation menu

        $this->data["nav_categories"] = Category::inCategory($category)
            ->inLandingNav()->take(3)->get();

        $this->searchBox($category);

        return view("posts", $this->data);
    }

    public function posts_paginate(){

        $res = ['posts_count' => 0, 'posts_html' => ''];

        $category_id = request('category_id', 0);

        $not_in = is_array(request('featured_ids')) ? request('featured_ids') : [];

        $format = in_array(request('format'), ['article', 'video']) ? request('format') : null;

        if($format){

            $posts = $this->get_posts($format, $category_id, $not_in);

            $res["posts_count"] = count($posts);

            $res['posts_html'] = view('partials.posts.posts_listing', ['posts' => $posts])->render();


        }

        return response()->json($res);
    }

    public function get_posts( $format, $category_id, $not_in)
    {
        $offset = request('offset', 0);

        $query = Post::latest()->format($format)->with(['image']);

        if ($category_id) {

            $query->whereHas("categories", function ($query) use ($category_id) {
                $query->where("categories.id",$category_id);
            });

        }

        if(count($not_in)){
            $query->whereNotIn('id', $not_in);
        }

        $posts = $query->take($this->articles_per_page)->offset($offset)->get();

        return $posts;
    }

    function searchBox($main_category)
    {


        // search bar

        $this->data['cities'] = Place::get_local_cities();

    }

}
