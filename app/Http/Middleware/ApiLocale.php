<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Foundation\Application;
class ApiLocale
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array($request->segment(2), ['ar','en'])){
            return response()->json(['scode'=>400,'message'=>__('api.error'),'data'=>[],'errors'=>['wrong lang code']]);
        }

        \Carbon\Carbon::setLocale($request->segment(2));

        app()->setlocale($request->segment(2));

        return $next($request);
    }
}
