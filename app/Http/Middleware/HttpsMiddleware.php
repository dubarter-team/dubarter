<?php

namespace App\Http\Middleware;

use App\Models\Place;
use Closure;
use Illuminate\Support\Facades\Request;

class HttpsMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->isSecure()) {
            //$url = str_replace("//ar-eg", "", url($request->getRequestUri(), $request->getQueryString(), true));

            //dd($url);
            //header("Location: $url", 301);

            //return redirect($url, 301);
        }

        return $next($request);
    }

}
