<?php

namespace App\Events;

use App\Indices\Ad;
use Illuminate\Queue\SerializesModels;

class AdViewed
{
    use SerializesModels;

    public $ad;

    /**
     * Create a new event instance.
     *
     * @param  Ad  $ad
     * @return void
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }
}