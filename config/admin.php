<?php

return [

    /*
     * Admin prefix (the admin url segment)
     * For security concerns, prefix should be hashed.
     * @var string
     */

    'prefix' => env("ADMIN_PREFIX", "backend"),

    /*
     * Default Page after login
     * without admin prefix
     * @var string
     */

    'default_path' => env("DEFAULT_PATH", "dashboard"),

    /*
     * API prefix
     * example (api/v1)
     *
     * @var string
     */

    'api' => env("API_PREFIX", "api"),

    /*
     * Dot plugins
     *
     * @var array
     */

    'plugins' => [
        "admin" => Dot\Platform\System::class,
        "i18n" => Dot\I18n\I18n::class,
        "ads" => Dot\Ads\Ads::class,
        "offers" => Dot\Offers\Offers::class,
        "categories" => Dot\Categories\Categories::class,
        "attributes" => Dot\Attributes\Attributes::class,
        "paid_ads" => Dot\PaidAds\PaidAds::class,
        "posts" => Dot\Posts\Posts::class,
        "comments" => Dot\Comments\Comments::class,
        "tags" => Dot\Tags\Tags::class,
        "topics" => Dot\Topics\Topics::class,
        "pages" => Dot\Pages\Pages::class,
        "migration" => Migration\Migration::class,
        "media" => Dot\Media\Media::class,
        "seo" => Dot\Seo\Seo::class,
        "branches" => Dot\Branches\Branches::class,
        "questions" => Dot\Questions\Questions::class,

    ]
];
