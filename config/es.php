<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Elasticsearch Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the Elasticsearch connections below you wish
    | to use as your default connection for all work. Of course.
    |
    */

    'default' => env('ELASTIC_CONNECTION', 'default'),

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the Elasticsearch connections setup for your application.
    | Of course, examples of configuring each Elasticsearch platform.
    |
    */

    'connections' => [

        'default' => [

            'servers' => [

                [
                    "host" => env("ELASTIC_HOST", "127.0.0.1"),
                    "port" => env("ELASTIC_PORT", 9200),
                    'user' => env('ELASTIC_USER', ''),
                    'pass' => env('ELASTIC_PASS', ''),
                    'scheme' => env('ELASTIC_SCHEME', 'http'),
                ]

            ],

            'index' => env('ELASTIC_INDEX', 'dubarter'),

            // Elasticsearch handlers
            // 'handler' => new MyCustomHandler(),
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Indices
    |--------------------------------------------------------------------------
    |
    | Here you can define your indices, with separate settings and mappings.
    | Edit settings and mappings and run 'php artisan es:index:update' to update
    | indices on elasticsearch server.
    |
    | 'my_index' is just for test. Replace it with a real index name.
    |
    */

    'indices' => [

		'dubarter_v3' => [

			"aliases" => [
				"dubarter"
			],

			'settings' => [
				"number_of_shards" => 1,
				"number_of_replicas" => 0,
				"index.mapping.ignore_malformed" => true
			],

			'mappings' => [

				'ads' => [

					"properties" => [

						"slug" => [
							"type" => "keyword"
						],

						"status" => [
							"type" => "byte"
						],

						"featured" => [
							"type" => "byte"
						],


						"price" => [
							"type" => "double"
						],

						"categories" => [
							"properties" => [
								"id" => [
									"type" => "long"
								],
								"slug" => [
									"type" => "object"
								],
								"name" => [
									"type" => "object"
								],
								"direct" => [
									"type" => "byte"
								]
							]
						],

						"tags" => [
							"properties" => [
								"id" => [
									"type" => "long"
								],
								"slug" => [
									"type" => "keyword"
								],
								"name" => [
									"type" => "keyword"
								]
							]
						],

						"files" => [
							"properties" => [
								"id" => [
									"type" => "long"
								],
								"type" => [
									"type" => "keyword"
								],
								"path" => [
									"type" => "text"
								],
								"title" => [
									"type" => "text"
								],
							]
						],

						"currency" => [
							"type" => "object"
						],

						"country" => [
							"type" => "object"
						],

						"city" => [
							"type" => "object"
						],

						"region" => [
							"type" => "object"
						],

						"location" => [
							"type" => "geo_point"
						],

						'created_at' => [
							'type' => 'date',
							"format" => "yyyy-MM-dd HH:mm:ss"
						],

						'updated_at' => [
							'type' => 'date',
							"format" => "yyyy-MM-dd HH:mm:ss"
						],

						'published_at' => [
							'type' => 'date',
							"format" => "yyyy-MM-dd HH:mm:ss"
						],

						'expired_at' => [
							'type' => 'date',
							"format" => "yyyy-MM-dd HH:mm:ss"
						]
					]

				]
			]

		]

    ]

];
