<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN', 'stg.dubarter.com'),
        'secret' => env('MAILGUN_SECRET', 'key-666af41bf473e5ce8925df71554e5e8a'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'passport'=>[
        'personal'=>[
            'client_id'    =>env('PASSPORT_PERSONAL_CLIENT_ID','3'),
            'client_secret'=>env('PASSPORT_PERSONAL_CLIENT_SECRET','tvmBXCVBLouP793MwjSiOKuAVn1IKfexXN52nalN'),
        ],
        'password'=>[
            'client_id'    =>env('PASSPORT_PASSWORD_CLIENT_ID','4'),
            'client_secret'=>env('PASSPORT_PASSWORD_CLIENT_SECRET','Nt7aU47LDE5XguSzWmi4injzARKamh9RpMdTe48W'),
        ]
    ],

    'facebook' => [
        'client_id' => '938361302990496',
        'client_secret' => '67344f9ee2ea0efeffeac21be753f264',
        'redirect' => env("APP_URL") . "/login/facebook",
    ],

    'twitter' => [
        'client_id' => 'azv6WVuvY18xfcsNw353EcIbs',
        'client_secret' => 'v2govyk5YJGq7K3VDZvS1gdZvPe3cbvIksbCucYAnbBvxjxbxy',
        'redirect' => env("APP_URL") . "/ar-eg/login/twitter",
    ],

    'google' => [
        'client_id' => '988315590475-vudlo4if93fvde30t1mfen50rdibtda0.apps.googleusercontent.com',
        'client_secret' => 'mfqU5Tuin996grocGiGy2u7s',
        'redirect' => env("APP_URL") . "/login/google",
    ]
];
