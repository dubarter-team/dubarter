<?php

namespace Dot\I18n\Middlewares;

use Closure;
use Dot\I18n\Models\Place;
use Illuminate\Support\Facades\Config;

class I18nMiddleware
{
    public function handle($request, Closure $next)
    {

        // setting default locale

        if (config("i18n.driver") == "url") {

            if (isset(config()->get("i18n.locales")[app()->getLocale()]["direction"])) {
                define("DIRECTION", config()->get("i18n.locales")[app()->getLocale()]["direction"]);
            }

        } else {

            try {

                if (session()->has('locale')) {
                    app()->setLocale(session()->get('locale'));
                } else {
                    app()->setLocale(config()->get('app.locale'));
                }

                define("DIRECTION", config()->get("i18n.locales")[app()->getLocale()]["direction"]);

            } catch (Exception $error) {
                abort(500, "System locales is not configured successfully");
            }

        }

        return $next($request);
    }
}
