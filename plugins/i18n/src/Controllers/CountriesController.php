<?php

namespace Dot\I18n\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\Request;

/*
 * Class LocalesController
 * @package Dot\Platform\Controllers
 */

class CountriesController extends Controller
{

    /*
     * Switch between system locales
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function index()
    {
        $old_hreflang = app()->getLocale() . "-" . config("country.code");
        $hreflang = app()->getLocale() . "-" . Request::get("country");
        $url = str_replace($old_hreflang, $hreflang, url()->previous());
        return Request::get("redirect_url") ? redirect(Request::get("redirect_url")) : redirect($url);
    }

}
