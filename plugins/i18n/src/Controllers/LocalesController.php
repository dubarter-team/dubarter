<?php

namespace Dot\I18n\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\Request;

/*
 * Class LocalesController
 * @package Dot\Platform\Controllers
 */

class LocalesController extends Controller
{

    /*
     * Switch between system locales
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function index()
    {
        $old_hreflang = app()->getLocale() . "-" . config("country.code");
        $hreflang = Request::get("lang") . "-" . config("country.code");
        $url = str_replace($old_hreflang, $hreflang, url()->previous());

        //dd($url);

        return Request::get("redirect_url") ? redirect(Request::get("redirect_url")) : redirect($url);
    }

}
