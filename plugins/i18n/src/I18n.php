<?php

namespace Dot\I18n;

use Action;
use Dot\I18n\Classes\DotUrlGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Navigation;


class I18n extends \Dot\Platform\Plugin
{

    protected $middlewares = [
        Middlewares\I18nMiddleware::class
    ];

    protected $permissions = [
        'manage_places'
    ];

    protected $facades = [
        //'I18n' => Facades\I18n::class
    ];

    function boot()
    {

        parent::boot();


        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("i18n.manage_places")) {
                $menu->item('places', trans("i18n::places.places"), route("admin.places.show"))
                    ->order(5.5)
                    ->icon("fa-map-marker");
            }

        });

        Navigation::menu("topnav", function ($menu) {
            $menu->make("i18n::locales");
        });

        Navigation::menu("topnav", function ($menu) {
            $menu->make("i18n::countries");
        });

        if (!$this->app->runningInConsole()) {


            $request = $this->app->make('request');

            if (in_array($request->segment(1), ["api", "_debugbar", "horizon", "country"])) {
                return;
            }


            if (!($request->segment(1) and preg_match("/[a-zA-Z]{2}\-[a-zA-Z]{2}/i", $request->segment(1)))) {

                $country = $this->getCountry(request()->getClientIp());

                if (!in_array($country, ["eg", "iq"])) {
                    $country = "eg";
                }

               // $this->old_links_redirect($request, false);

                $url = "ar-" . $country . "/" . implode('/', $request->segments());

                if ($request->getQueryString()) {
                    $url .= "?" . $request->getQueryString();
                }

                redirect($url, 301)->send();
            }

        }


    }

    function getCountry($ip){
        return trim(file_get_contents("https://dubarter.com/ar-eg/country/current?ip=" . $ip));
    }

    function register()
    {

        parent::register();

        if (!$this->app->runningInConsole()) {

            app()->bind('url', function () {
                return new DotUrlGenerator(
                    app()->make('router')->getRoutes(),
                    app()->make('request')
                );
            });

            $request = $this->app->make('request');

            if (in_array($request->segment(1), ["api", "_debugbar", "horizon"])) {
                return;
            }


            if ($request->segment(1) and preg_match("/[a-zA-Z]{2}\-[a-zA-Z]{2}/i", $request->segment(1))) {

                //$this->old_links_redirect($request, true);

                list($lang_code, $country_code) = explode("-", $request->segment(1));

                $lang = array_key_exists($lang_code, config("i18n.locales"));
                $country = DB::connection("mysql")->table("places")->where("code", $country_code)->first();

                if ($lang and $country) {
                    Config::set("app.locale", $lang_code);
                    Config::set("country", (array)$country);
                    Config::set("hreflang", $lang_code . "-" . $country_code);
                    Config::set("admin.prefix", $lang_code . "-" . $country_code . "/" . config("admin.prefix"));
                    Config::set('cache.prefix', 'dubarter_cache_' . $country_code);
                }

            }


        }
    }

    public function old_links_redirect($request, $has_lang_seg)
    {
        $segments = $request->segments();

        $cat_slugs_mapping = [
            'عقارات' => 'عقارات',
            'مركبات' => 'السيارات',
            'مبوبات' => 'مبوبات',
            'وظائف' => 'مبوبات/وظائف',
            'سيارات-للبيع' => 'السيارات/سيارات-مستعملة-للبيع',
            'شقق-للبيع' => 'عقارات/عقارات-للبيع/شقق-للبيع',
            'أراضي-للبيع' => 'عقارات/عقارات-للبيع/اراضي-للبيع',
            'محلات تجارية' => 'عقارات/عقارات-للبيع/اخري',
            'محلات-تجارية' => 'عقارات/عقارات-للبيع/اخري',
            'شاليهات-للبيع' => 'عقارات/عقارات-للبيع/شاليهات-للبيع',
            'فلل-للبيع' => 'عقارات/عقارات-للبيع/فلل-للبيع',
            'مكاتب-للبيع' => 'عقارات/عقارات-للبيع/مكاتب-للبيع',
            'اتوبيسات-عربيات-نقل' => 'السيارات/مركبات-أخري',
            'ركن المرأة' => 'لايف-ستايل',
            'ركن-المرأة' => 'لايف-ستايل',
            'فساتين-زفاف-وسهرة' => 'لايف-ستايل',
            'ملابس-وملحقاتها' => 'مبوبات',
            'عربيات-اطفال' => 'السيارات/مركبات-أخري',
            'موتسكلات-سكوتر-للبيع' => 'السيارات/مركبات-أخري',
            'دراجات-رياضية-للبيع' => 'السيارات/مركبات-أخري',
            'اتوبيسات-عربيات-نقل' => 'السيارات/مركبات-أخري',
            'عقارات-للايجار' => 'عقارات/عقارات-للايجار',
            'شقق-للايجار' => 'عقارات/عقارات-للايجار/شقق-للايجار',
            'أراضي-للايجار' => 'عقارات/عقارات-للايجار/اخري',
            'فلل-للايجار' => 'عقارات/عقارات-للايجار/فلل-للايجار',
            'مكاتب-للايجار' => 'عقارات/عقارات-للايجار/مكاتب-للايجار',
            'محلات-للايجار' => 'عقارات/عقارات-للايجار/اخري',
            'شاليهات-للايجار' => 'عقارات/عقارات-للايجار/اخري',
            'وظائف-تعليم-وتدريس' => 'مبوبات/وظائف/وظائف-تعليم-وتدريس',
            'وظائف-تكنولوجيا-المعلومات-اتصالات' => 'مبوبات/وظائف/وظائف-اتصالات-تكنولوجيا',
            'وظائف-سكرتارية' => 'مبوبات/وظائف/وظائف-سكرتارية',
            'وظائف-عمارة-مهندسين' => 'مبوبات/وظائف/وظائف-مهندسين',
            'وظائف-محاسبة' => 'مبوبات/وظائف/وظائف-محاسبين',
            'وظائف-اداريه-عمل-اداري' => 'مبوبات/وظائف/وظائف-ادارية',
            'وظائف-فن-تصميم' => 'مبوبات/وظائف/وظائف-فن-تصميم',
            'السيارات' => 'السيارات',
            'العقارات' => 'عقارات',
            'سيارات' => 'السيارات',
            '' => '',
            '' => '',
            '' => '',
        ];

        $blog_cat_slugs_mapping = [
            'السيارات' => 'السيارات',
            'العقارات' => 'عقارات',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
        ];

        $cats_ids = [
            '232' => 'السيارات',
            '233' => 'السيارات/سيارات-مستعملة-للبيع',
            '234' => 'السيارات/سيارات-مستعملة-للبيع',
            '235' => 'السيارات/سيارات-مستعملة-للبيع',
        ];

        if (count($segments)) {

            $chs_index = $has_lang_seg ? 1 : 0;
            $c_count = $has_lang_seg ? 2 : 1;

            if (strtolower($segments[0]) == 'ar' || strtolower($segments[0]) == 'default') {
                redirect('ar-eg', 301)->send();
            } else if (count($segments) >= $c_count) {
                if (strtolower($segments[$chs_index]) == 'viewad' || strtolower($segments[$chs_index]) == 'viewad.aspx') {

                    if ($request->ID) {
                        $ad = DB::connection("mysql")->table('ads')->where('id', $request->ID)->first();
                    } else {
                        $ad = DB::connection("mysql")->table('ads')->where('slug', $segments[count($segments) - 1])->first();
                    }

                    if (!$ad) {
                        redirect('ar-eg', 301)->send();
                    }

                    $code = 'eg';

                    if ($ad) {
                        $country = DB::connection("mysql")->table('places')->where('id', $ad->country_id)->first();

                        $code = $country->code;
                    }

                    $url = "ar-" . $code . "/ads/" . $ad->slug;

                    redirect($url, 301)->send();

                } else if (strtolower($segments[$chs_index]) == 'searchresult' || strtolower($segments[$chs_index]) == 'searchresult.aspx') {

                    if (count($segments) >= 4) {

                        $old_cat_slug = urldecode($segments[count($segments) - 1]);

                        $country_code = 'eg';

                        $country = DB::connection("mysql")->table('places')->where('code', $segments[1])->first();

                        if ($country) {
                            $country_code = $country->code;
                        }

                        if (isset($cat_slugs_mapping[$old_cat_slug])) {
                            redirect("ar-" . $country_code . "/" . $cat_slugs_mapping[$old_cat_slug], 301)->send();
                        }

                    } else if ($request->CategoryId) {

                        $cat_id = $request->SubCategoryId ? $request->SubCategoryId : $request->CategoryId;

                        if (isset($cats_ids[$cat_id])) {
                            redirect("ar-eg/" . $cats_ids[$cat_id], 301)->send();
                        }
                    }

                    redirect('ar-eg', 301)->send();
                } else if (strtolower($segments[$chs_index]) == 'blog') {

                    $cat_slug_seg_index = 1;

                    if (count($segments) > 1 && strtolower($segments[1]) == 'blog') {
                        $cat_slug_seg_index = 2;
                    }

                    if (isset($segments[$cat_slug_seg_index])) {

                        $cat_slug = $segments[$cat_slug_seg_index];

                        $cat = DB::connection("mysql")->table('categories')->where(function ($query) use ($cat_slug) {
                            $query->where('slug->ar', $cat_slug)
                                ->orWhere('slug->en', $cat_slug);
                        })->first();

                        if (!$cat) {
                            $old_cat_slug = $cat_slug;

                            if (isset($blog_cat_slugs_mapping[$old_cat_slug])) {
                                redirect("ar-eg/blog/" . $blog_cat_slugs_mapping[$old_cat_slug], 301)->send();
                            }

                            redirect('ar-eg', 301)->send();
                        }

                    }

                    if ($segments[$chs_index] == 'Blog') {
                        redirect('ar-eg', 301)->send();
                    }
                } else if ($has_lang_seg && count($segments) > 1) {
                    if ($segments[$chs_index] !== 'register') {
                        $url = urldecode($request->fullUrl());

                        $url = str_replace('stg.', '', $url);

                        $link = DB::table('redirects')->where('from_url', $url)->first();

                        if ($link) {
                            redirect()->to($link->to_url, 301)->send();
                        }
                    }
                }

            }

        }
    }
}
