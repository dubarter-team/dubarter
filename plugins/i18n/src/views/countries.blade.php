<li class="dropdown">

    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
        {{ strtoupper(json_decode(config("country.name"))->{app()->getLocale()}) }}
    </a>

    <ul class="dropdown-menu dropdown-alerts dropdown-locales">
        <div class="aro"></div>

        @foreach (\Dot\I18n\Models\Place::where("parent", 0)->whereIn("code", ["eg", "iq"])->get() as $country)

                <li>
                    <a href="{{ url(app()->getLocale()."-". config("country.code"). "/country?country=" . $country->code) }}">
                        {{ $country->name }}
                    </a>
                </li>

        @endforeach

    </ul>
</li>

