<option value="">{{ $all ? trans("i18n::places.all_" . $type) : trans("i18n::places.choose_" . $type) }}</option>

@if(count($places))
    @foreach($places as $place)
        <option {{ $id == $place->id ? 'selected' : '' }} value="{{ $place->id }}">{{ $place->name->{app()->getLocale()} }}</option>
    @endforeach
@endif