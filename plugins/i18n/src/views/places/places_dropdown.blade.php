<div class="{{ $only_selects ? '' : 'form-group' }}">

    @if(!$only_selects)
        <label for="input-country">{{ trans("i18n::places.country") }}</label>
    @endif

    <select name="country_id" class="form-control chosen-select chosen-rtl" id="input-country">

        @include('i18n::places.places_options', ['places' => $countries, 'id' => $country_id, 'type' => 'country', 'all' => $all])

    </select>
</div>

<div class="{{ $only_selects ? '' : 'form-group' }}">

    @if(!$only_selects)
        <label for="input-city">{{ trans("i18n::places.city") }}</label>
    @endif

    <select name="city_id" class="form-control chosen-select chosen-rtl" id="input-city">

        @include('i18n::places.places_options', ['places' => $cities, 'id' => $city_id, 'type' => 'city', 'all' => $all])

    </select>
</div>

<div class="{{ $only_selects ? '' : 'form-group' }}">

    @if(!$only_selects)
        <label for="input-region">{{ trans("i18n::places.region") }}</label>
    @endif

    <select name="region_id" class="form-control chosen-select chosen-rtl"
            id="input-region">

        @include('i18n::places.places_options', ['places' => $regions, 'id' => $region_id, 'type' => 'region', 'all' => $all])

    </select>
</div>

@push("footer")

    <script>

        $(document).ready(function () {

            $('#input-country').change(function () {
                var country_id = $(this).val();

                $('#input-city').html('<option value="0">{{ trans("i18n::places." . ($all ? 'all' : 'choose') . "_city") }}</option>');
                $('#input-region').html('<option value="0">{{ trans("i18n::places." . ($all ? 'all' : 'choose') . "_region") }}</option>');

                $('#input-city').trigger("chosen:updated");
                $('#input-region').trigger("chosen:updated");

                if (country_id != 0) {
                    place_change(country_id, 'city', '#input-city');
                }
            });

            $('#input-city').change(function () {
                var city_id = $(this).val();

                $('#input-region').html('<option value="0">{{ trans("i18n::places." . ($all ? 'all' : 'choose') . "_region") }}</option>');

                $('#input-region').trigger("chosen:updated");

                if (city_id != 0) {
                    place_change(city_id, 'region', '#input-region');
                }
            });

            function place_change(parent_id, type, child_id) {
                $.ajax({
                    url: "{{ route("admin.places.get_places") }}",
                    data: { parent_id: parent_id, type: type, all: '{{ $all ? 1 : 0 }}' },
                    type: 'get',
                    dataType: "json",
                    success: function (data) {
                        if (data.status == true) {
                            if (child_id != undefined && $(child_id).size() > 0) {
                                $(child_id).html(data.html);

                                $('#input-city').trigger("chosen:updated");
                                $('#input-region').trigger("chosen:updated");
                            }
                        }
                    }
                });
            }

        });

    </script>

@endpush