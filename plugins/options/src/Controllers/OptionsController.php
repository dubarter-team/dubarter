<?php

namespace Dot\Options\Controllers;

use Dot;
use Dot\Options\Facades\Option;
use Dot\Platform\Controller;
use Dot\Platform\Facades\Action;
use File;
use Gate;
use Redirect;
use Request;
use Session;
use View;
use Dot\Options\Models\Option as OptionModel;

/**
 * Class OptionsController
 * @package Dot\Options\Controllers
 */
class OptionsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];


    /**
     * Render the option page
     * @param $page
     * @return mixed
     */
    function index($page = false)
    {

        if (!$page) return redirect()->route("admin.options", ["page" => "general"]);

        if (Request::isMethod("post")) {

            foreach (Request::get("option") as $name => $value) {


                if(in_array($name, ["site_title", "site_description", "site_author", "site_keywords"])){

                    OptionModel::where("name", $name)
                        ->where("lang", app()->getLocale())
                        ->where("country", config("country.code"))
                        ->delete();

                    $option = new OptionModel();

                    $option->name = $name;
                    $option->value = $value;
                    $option->lang = app()->getLocale();
                    $option->country = config("country.code");

                    $option->save();

                }else{

                    // Fire saving action

                    Action::fire("option.saving", $name, $value);

                    Option::set($name, $value);

                    // Fire saved action

                    Action::fire("option.saved", $name, $value);

                }


            }

            return redirect()->back()
                ->with("message", trans("options::options.events.saved", [], "messages", option("site_locale")));
        }

        $this->data["option_pages"] = Option::pages();
        $this->data["option_page"] = Option::getPage($page);

        return View::make("options::show", $this->data);
    }
}
