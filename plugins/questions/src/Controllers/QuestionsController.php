<?php

namespace Dot\Questions\Controllers;

use Action;
use Dot\Categories\Models\Category;
use Illuminate\Support\Facades\Auth;
use Dot\Platform\Controller;
use Dot\Questions\Models\Question;
use Redirect;
use Request;
use View;


/**
 * Class QuestionsController
 * @package Dot\Posts\Controllers
 */
class QuestionsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];


    /**
     * Show all Questions
     * @return mixed
     */
    function index()
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Question::with('user')->orderBy($this->data["sort"], $this->data["order"]);
        if (Request::filled("category_id") and Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });
        }


        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }

        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("status")) {
            $query->where("status", Request::get("status"));
        }

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }
        $this->data["questions"] = $query->paginate($this->data['per_page']);

        return View::make("questions::show", $this->data);
    }

    /**
     * Delete question by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $question = Question::findOrFail($ID);

            // Fire deleting action

            Action::fire("question.deleting", $question);

            $question->categories()->detach();

            $question->delete();

            // Fire deleted action

            Action::fire("question.deleted", $question);
        }

        return Redirect::back()->with("message", trans("questions::questions.events.deleted"));
    }

    /**
     * Activating / Deactivating question by id
     * @param $status
     * @return mixed
     */
    public function status($status)
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $question = Question::findOrFail($id);

            // Fire saving action
            Action::fire("question.saving", $question);

            $question->status = $status;
            $question->save();

            // Fire saved action

            Action::fire("question.saved", $question);
        }

        if ($status) {
            $message = trans("questions::questions.events.activated");
        } else {
            $message = trans("questions::questions.events.deactivated");
        }

        return Redirect::back()->with("message", $message);
    }

    /**
     * Create a new question
     * @return mixed
     */
    public function create()
    {

        $question = new Question();

        if (Request::isMethod("post")) {

            $question->title = Request::get('title');
            $question->content = Request::get('content');
            $question->user_id = Auth::user()->id;
            $question->status = Request::get("status", 0);
            $question->featured = Request::get("featured", 0);

            // Fire saving action

            Action::fire("question.saving", $question);

            if (!$question->validate()) {
                return Redirect::back()->withErrors($question->errors())->withInput(Request::all());
            }

            $question->save();
//            $question->categories()->sync(Request::get("categories", []));


            // Saving post meta

            // Fire saved action

            Action::fire("question.saved", $question);

            return Redirect::route("admin.questions.edit", array("id" => $question->id))
                ->with("message", trans("questions::questions.events.created"));
        }

        $this->data["question_categories"] = collect([]);
        $this->data["question"] = $question;

        return View::make("questions::edit", $this->data);
    }

    /**
     * Edit question by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $question = Question::findOrFail($id);

        $direct_category = $question->categories()->where('direct', 1)->first();
        if (Request::isMethod("post")) {

            $question->title = Request::get('title');
            $question->content = Request::get('content');
            $question->status = Request::get("status", 0);
            $question->featured = Request::get("featured", 0);

            // Fire saving action

            Action::fire("question.saving", $question);

            if (!$question->validate()) {
                return Redirect::back()->withErrors($question->errors())->withInput(Request::all());
            }

            $question->save();
//            $question->categories()->sync(Request::get("categories", []));

            if (Request::filled('category_id')) {
                $category = Category::where('id', Request::get('category_id'))->with(['custom_attributes', 'parent_category'])->first();

                if ($category) {
                    $question->categories()->detach();

                    $cat = $category;

                    while (true) {
                        $question->categories()->attach($cat->id, ['direct' => ($cat->id == Request::get('category_id') ? 1 : 0)]);

                        if ($cat->parent == 0) {
                            break;
                        } else {
                            $cat = $cat->parent_category;
                        }
                    }
                }
            }
            // Fire saved action

            Action::fire("question.saved", $question);

            return Redirect::route("admin.questions.edit", array("id" => $id))->with("message", trans("questions::questions.events.updated"));
        }

        $this->data["categories"] = Category::where('parent', '0')->get();
        $this->data["direct_category"] = $direct_category;
        $this->data["question"] = $question;
        return View::make("questions::edit", $this->data);
    }

}
