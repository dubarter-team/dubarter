<?php

namespace Dot\Questions;

use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class Questions extends \Dot\Platform\Plugin
{

    /*
     * @var array
     */
    protected $dependencies = [
        "categories" => \Dot\Categories\Categories::class,
    ];

    /**
     * @var array
     */
    protected $permissions = [
        "manage"
    ];

    /**
     *  initialize plugin
     */
    function boot()
    {
        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("questions.manage")) {

                $menu->item('questions', trans("questions::questions.questions"), route('admin.questions.show'))
                    ->order(1.4)
                    ->icon("fa-question-circle");
            }
        });

    }
}
