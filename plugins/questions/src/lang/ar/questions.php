<?php
return [

    'questions' => 'أسئلة',
    'question' => 'سؤال',
    'add_new' => 'أضف سؤال جديد',
    'edit' => 'تعديل السؤال',
    'back_to_questions' => 'العودة للأخبار',
    'no_records' => 'لا يوجد سؤال',
    'save_question' => 'حفظ',
    'search' => 'بحث',
    'search_questions' => 'بحث عن السؤال',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'اختر أمر',
    'delete' => 'حذف',
    'apply' => 'حفظ',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب بــ',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'تعديل',
    'filter' => 'عرض',
    'question_status' => 'حالة السؤال',
    'activate' => 'تفعيل',
    'activated' => 'مفعل',
    'all' => 'الكل',
    'deactivate' => 'غير مفعل',
    'deactivated' => 'غير مفعل',
    'sure_activate' => "هل تريد تفعيل السؤال؟",
    'sure_deactivate' => "هل تريد إلغاء تفعيل السؤال",
    'sure_delete' => 'هل تريد حذف السؤال؟',
    'add_category' => "أضف إلي قسم",

    'add_image' => 'أضف صورة',
    'change_image' => 'تغيير الصورة',

    'add_thumbnail' => 'أضف صورة رمزية',
    'change_thumbnail' => 'تغيير الصورة الرمزية',

    'change_media' => 'تغيير ',
    'add_media' => 'أضف فيديو',

    'all_categories' => "كل التصنيفات",
    'all_formats' => "كل أنواع الأخبار",
    'all_blocks' => "كل أماكن الأخبار",
    'not_image_file' => 'ملف غير مسموح به',
    'not_media_file' => 'ملف غير مسموح به',

    'all_questions' => 'الكل',

    'from' => "من",
    'to' => "إلي",

    'user' => 'الكاتب',
    'tags' => 'الوسوم',
    'add_tag' => 'أضف وسوم',

    "add_fields" => "أضف بيانات أخري",
    "custom_name" => "الإسم",
    "custom_value" => "القيمة",
    "sure_delete_field" => "هل أنت متأكد من الحذف ؟",


    "add_gallery" => "أضف ألبوم",
    "no_galleries_found" => "لا توجد ألبومات",
    'comments' => 'التعليقات',

    'attributes' => [
        'title' => 'العنوان',
        'excerpt' => 'المقتطف',
        'content' => 'الاجابة',
        'created_at' => 'تاريخ الإضافة',
        'updated_at' => 'تاريخ التعديل',
        'published_at' => 'تاريخ الحدث',
        'status' => 'الحالة',
        'featured' => 'مميز',
        'default' => 'إفتراضى',
    ],

    "events" => [
        'created' => 'تمت الإضافة بنجاح',
        'updated' => 'تم التحديث بنجاح',
        'deleted' => 'تم الحذف بنجاح',
        'activated' => 'تم التفعيل بنجاح',
        'deactivated' => 'تم إلغاء التفعيل بنجاح'
    ],

    "permissions" => [
        "manage" => "التحكم السؤال"
    ]

];
