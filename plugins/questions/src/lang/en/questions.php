<?php
return [

    'module' => 'questions',

    'questions' => 'FAQ',
    'add_new' => 'Add New Question',
    'edit' => 'Edit Question',
    'back_to_questions' => 'Back To questions',
    'no_records' => 'No questions Found',
    'save_question' => 'Save question',
    'search' => 'search',
    'search_questions' => 'Search questions',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',

    'all_questions' => 'All',

    'from' => "From",
    'to' => "To",

    'question_status' => 'question status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate question ?",
    'sure_deactivate' => "Are you sure to deactivate question ?",
    'sure_delete' => 'Are you sure to delete ?',

    'add_image' => 'Add image',
    'change_image' => 'change image',

    'add_thumbnail' => 'Add thumbnail',
    'change_thumbnail' => 'Change thumbnail',

    'change_media' => 'change media',

    'add_media' => "Add media",

    'not_image_file' => 'Please select a valid image file',
    'not_media_file' => 'Please select a valid video file',

    'user' => 'User',
    'tags' => 'Tags',
    'add_tag' => 'Add tags',
    'templates' => 'Templates',

    'all_categories' => "All Categories",
    'all_blocks' => "All Blocks",
    'all_formats' => "All Formats",
    'add_category' => "Add To Category",

    "add_fields" => "Add custom fields",
    "custom_name" => "Name",
    "custom_value" => "Value",
    "sure_delete_field" => "Are you sure to delete custom field ?",

    "add_gallery" => "Add gallery",
    "no_galleries_found" => "No Galleries Found",
    'comments' => 'Comments',

    'attributes' => [
        'title' => 'Title',
        'content' => 'Answer',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',
        'published_at' => 'Event date',
        'status' => 'Status',
        'featured' => 'Featured',
        'template' => 'Template',
        'default' => 'Default',
    ],

    "events" => [
        'created' => 'question created successfully',
        'updated' => 'question updated successfully',
        'deleted' => 'question deleted successfully',
        'activated' => 'question activated successfully',
        'deactivated' => 'question deactivated successfully'
    ],

    "permissions" => [
        "manage" => "Manage questions"
    ]

];
