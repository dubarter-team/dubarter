<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:questions.manage"],
    "namespace" => "Dot\\Questions\\Controllers"
], function ($route) {
    $route->group(["prefix" => "questions"], function ($route) {
        $route->any('/', ["as" => "admin.questions.show", "uses" => "QuestionsController@index"]);
        $route->any('/create', ["as" => "admin.questions.create", "uses" => "QuestionsController@create"]);
        $route->any('/{id}/edit', ["as" => "admin.questions.edit", "uses" => "QuestionsController@edit"]);
        $route->any('/delete', ["as" => "admin.questions.delete", "uses" => "QuestionsController@delete"]);
        $route->any('/{status}/status', ["as" => "admin.questions.status", "uses" => "QuestionsController@status"]);
    });
});
