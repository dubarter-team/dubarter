<?php
/**
 * Created by PhpStorm.
 * User: elbardeny
 * Date: 5/8/18
 * Time: 11:24 AM
 */

namespace Dot\Users\Classes;

use Dot\Users\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Request;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return ['id', 'username', 'first_name', 'last_name', 'email', 'status', 'provider', 'provider_id', 'backend', 'created_at'];
    }

    public function collection()
    {

        $sort = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $order = (Request::filled("order")) ? Request::get("order") : "desc";

        $query = User::with("role", "photo")->orderBy($sort, $order);

        if (Request::filled("q")) {
            $q = urldecode(Request::get("q"));

            $query->search($q);
        }

        if (Request::filled("type")) {
            $query->where('backend', Request::get("type"));
        }

        if (Request::filled("backend") and Request::get("backend") == 1) {
            $query->where("role_id", "!=", 0);
        }

        if (Request::filled("status")) {
            $query->where("status", Request::get("status"));
        }

        if (Request::filled("role_id")) {
            $query->where("role_id", Request::get("role_id"));
        }

        if (Request::filled("created_from")) {
            $query->where("created_at", ">=", Request::get("created_from"));
        }

        if (Request::filled("created_to")) {
            $query->where("created_at", "<=", Request::get("created_to"));
        }

        $query->select(['id', 'username', 'first_name', 'last_name', 'email', 'status', 'provider', 'provider_id', 'backend', 'created_at']);

        $users = $query->get();

        return $users;
    }
}