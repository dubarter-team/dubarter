<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 *
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ads")) {
            Schema::create('ads', function (Blueprint $table) {
                $table->increments('id');
                $table->string('slug', 255)->nullable()->default('');
                $table->text('title')->nullable();
                $table->longText('content')->nullable();
                $table->nullableTimestamps();
                $table->timestamp('published_at')->nullable();
                $table->timestamp('expired_at')->nullable();
                $table->integer('country_id')->default(0);
                $table->integer('city_id')->default(0);
                $table->integer('region_id')->default(0);
                $table->integer('data_entry_id')->default(0);
                $table->string('lat', 100)->nullable();
                $table->string('lng', 100)->nullable();
                $table->integer('user_id')->nullable();
                $table->string('email', 100)->nullable();
                $table->text('phone')->nullable();
                $table->tinyInteger('status')->default(0);
                $table->integer('parent')->default(0);
                $table->integer('rejection_reason_id')->default(0);
                $table->text('rejection_reason_message')->nullable();
                $table->json('currency')->nullable();
                $table->double('price')->default(0);
                $table->integer('views')->default(0);
                $table->integer('mobile_views')->default(0);
                $table->integer('featured')->default(0);
                $table->enum('source', ['frontend', 'backend', 'mobile'])->nullable();

                $table->index('slug', 'slug');
                $table->index('updated_at', 'updated_at');
                $table->index('updated_at', 'updated_at_2');
                $table->index('published_at', 'published_at');
                $table->index('expired_at', 'expired_at');
                $table->index('data_entry_id', 'data_entry_id_index');
                $table->index('country_id', 'country_id');
                $table->index('city_id', 'city_id');
                $table->index('region_id', 'region_id');
                $table->index('user_id', 'user_id');
                $table->index('email', 'email');
                $table->index('status', 'status');
                $table->index('parent', 'parent');
                $table->index('source', 'source');
                $table->index('rejection_reason_id', 'rejection_reason_id');
                $table->index('featured', 'featured');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
