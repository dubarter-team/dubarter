@extends("admin::layouts.master")

@section("content")

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-folder"></i>
                {{ trans("ads::ads.title") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.ads.show") }}">{{ trans("ads::ads.title") }} ({{ $ads->total() }})</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

            <a target="_blank" href="{{ route("admin.ads.export", Request::all()) }}"
               class="btn btn-flat btn-danger btn-main">
                <span class="btn-label icon fa fa-plus"></span>
                {{ trans("ads::ads.export_to_excel") }}
            </a>

        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">

        <div id="content-wrapper">

            @include("admin::partials.messages")

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-9" style="height: 32px;line-height: 32px;">
                            <i class="fa fa-th-large"></i>
                            {{ trans("ads::ads.filters") }}
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:void(0)" class="btn btn-primary pull-right" id="expand-filters">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body hidden" id="filters-panel">

                    <form action="" method="get" class="filter-form">

                        <input type="hidden" name="user_id" value="{{ Request::get('user_id') }}"/>

                        <div class="row">

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">

                                    <label for="input-q">{{ trans("ads::ads.search_for") }}</label>
                                    <input name="q" id="input-q" value="{{ Request::get("q") }}" type="text"
                                           class=" form-control"
                                           placeholder="{{ trans("ads::ads.search_ads") }} ...">

                                </div>

                                <div class="form-group">
                                        <label for="input-owner">{{ trans("ads::ads.owner") }}</label>
                                        <input name="username" id="input-owner" value="{{ Request::get("username") }}" type="text"
                                               class=" form-control"
                                               placeholder="{{ trans("ads::ads.owner") }} ...">

                                    </div>
                                <div class="form-group">
                                    <label for="input-order-by">{{ trans("ads::ads.order_by") }}</label>

                                    <select name="sort" id="input-order-by"
                                            class="form-control chosen-select chosen-rtl">
                                        <option
                                                value="title" {{ $sort == "title" ? "selected='selected'" : '' }}>{{ trans("ads::ads.attributes.title") }}</option>
                                        <option
                                                value="created_at" {{ $sort == "created_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.creation_date") }}</option>
                                        <option
                                                value="published_at" {{ $sort == "published_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.publish_date") }}</option>
                                        <option
                                                value="expired_at" {{ $sort == "expired_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.expire_date") }}</option>
                                    </select>

                                    <select name="order" class="form-control chosen-select chosen-rtl">
                                        <option value="DESC"
                                                @if($order == "DESC") selected='selected' @endif>{{ trans("ads::ads.desc") }}</option>
                                        <option value="ASC"
                                                @if($order == "ASC") selected='selected' @endif>{{ trans("ads::ads.asc") }}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="input-per-page">{{ trans("ads::ads.per_page") }}</label>

                                    <select name="per_page" id="input-per-page"
                                            class="form-control chosen-select chosen-rtl">

                                        <option {{ $per_page == 10 ? "selected='selected'" : '' }} value="10">10
                                        </option>
                                        <option {{ $per_page == 20 ? "selected='selected'" : '' }} value="20">20
                                        </option>
                                        <option {{ $per_page == 30 ? "selected='selected'" : '' }} value="30">30
                                        </option>
                                        <option {{ $per_page == 40 ? "selected='selected'" : '' }} value="40">40
                                        </option>
                                        <option {{ $per_page == 60 ? "selected='selected'" : '' }} value="60">60
                                        </option>
                                        <option {{ $per_page == 80 ? "selected='selected'" : '' }} value="80">80
                                        </option>

                                    </select>

                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">

                                @include('categories::partials.choose_category', ['categories' => $categories, 'type' => 'choose_category', 'cat_id' => Request::get('category_id', 0), 'show_label' => true, 'has_form_group' => false])


                                <div class="form-group">
                                    <label for="input-order-by">{{ trans("ads::ads.filter_date_by") }}</label>

                                    <select name="date_type" id="input-date_type"
                                            class="form-control chosen-select chosen-rtl">
                                        <option value="created_at" {{ request('date_type') == "created_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.creation_date") }}</option>
                                        <option value="updated_at" {{ request('date_type') == "updated_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.updated_date") }}</option>
                                        <option value="published_at" {{ request('date_type') == "published_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.publish_date") }}</option>
                                        <option value="expired_at" {{ request('date_type') == "expired_at" ? "selected='selected'" : '' }}>{{ trans("ads::ads.expire_date") }}</option>
                                    </select>
                                </div>

                                <div class="form-group">

                                    <div class="input-group date datetimepick col-sm-6 pull-left"
                                         style="margin-top: 5px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input name="from" type="text"
                                               value="{{ @Request::get("from") }}"
                                               class="form-control" id="input-from"
                                               placeholder="{{ trans("ads::ads.from") }}">
                                    </div>

                                    <div class="input-group date datetimepick col-sm-6 pull-left"
                                         style="margin-top: 5px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input name="to" type="text"
                                               value="{{ @Request::get("to") }}"
                                               class="form-control" id="input-to"
                                               placeholder="{{ trans("ads::ads.to") }}">
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">

                                    <label for="input-status">{{ trans("ads::ads.status") }}</label>

                                    <select name="status" id="input-status"
                                            class="form-control chosen-select chosen-rtl">
                                        <option value="">{{ trans("ads::ads.all") }}</option>
                                        <option
                                                {{ Request::get("status") == "1" ? "selected='selected'" : '' }} value="1">{{ trans("ads::ads.activated") }}</option>
                                        <option
                                                {{ Request::get("status") == "0" ? "selected='selected'" : '' }} value="0">{{ trans("ads::ads.deactivated") }}</option>
                                        <option
                                                {{ Request::get("status") == "2" ? "selected='selected'" : '' }} value="2">{{ trans("ads::ads.pending") }}</option>
                                        <option
                                                {{ Request::get("status") == "3" ? "selected='selected'" : '' }} value="3">{{ trans("ads::ads.rejected") }}</option>
                                        <option
                                                {{ Request::get("status") == "4" ? "selected='selected'" : '' }} value="4">{{ trans("ads::ads.sold") }}</option>
                                        <option
                                                {{ Request::get("status") == "5" ? "selected='selected'" : '' }} value="5">{{ trans("ads::ads.corrected") }}</option>
                                    </select>

                                </div>

                                <div class="form-group">

                                    <label for="input-source">{{ trans("ads::ads.source") }}</label>

                                    <select name="source" id="input-source"
                                            class="form-control chosen-select chosen-rtl">
                                        <option value="">{{ trans("ads::ads.all") }}</option>
                                        <option
                                                {{ Request::get("source") == "frontend" ? "selected='selected'" : '' }} value="frontend">{{ trans("ads::ads.frontend") }}</option>
                                        <option
                                                {{ Request::get("source") == "backend" ? "selected='selected'" : '' }} value="backend">{{ trans("ads::ads.backend") }}</option>
                                        <option
                                                {{ Request::get("source") == "mobile" ? "selected='selected'" : '' }} value="mobile">{{ trans("ads::ads.mobile") }}</option>
                                    </select>

                                </div>

                                <div class="form-group">

                                    <label for="input-status">{{ trans("ads::ads.type") }}</label>

                                    <select name="type" id="input-type"
                                            class="form-control chosen-select chosen-rtl">
                                        <option value="">{{ trans("ads::ads.all") }}</option>
                                        <option {{ Request::get("type") == "featured" ? "selected='selected'" : '' }} value="featured">{{ trans("ads::ads.featured") }}</option>
                                        <option {{ Request::get("type") == "sponsored" ? "selected='selected'" : '' }} value="sponsored">{{ trans("ads::ads.sponsored") }}</option>
                                    </select>

                                </div>

                                <div class="form-group">

                                    <label for="input-status">{{ trans("ads::ads.attributes.rejection_reason") }}</label>

                                    <select name="rejection_reason" id="input-rejection_reason"
                                            class="form-control chosen-select chosen-rtl">
                                        <option value="">{{ trans("ads::ads.choose_rejection_reason") }}</option>

                                        @if(count($rejection_reasons))
                                            @foreach($rejection_reasons as $rj)
                                                <option {{ Request::get("rejection_reason") == $rj->id ? "selected='selected'" : '' }} value="{{ $rj->id }}">{{ $rj->title }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label for="input-country">{{ trans("ads::ads.place") }}</label>

                                    @include('i18n::places.places_dropdown', ['countries' => $countries, 'cities' => $cities, 'regions' => $regions, 'country_id' => Request::get('country_id', 0), 'city_id' => Request::get('city_id', 0), 'region_id' => Request::get('region_id', 0), 'all' => true, 'only_selects' => true])

                                </div>

                            </div>
                        </div>

                        @if($category && $attributes->count())

                            <div class="row">

                                @foreach($attributes as $attr)

                                    @if(($loop->index) % 3 == 0)

                                        {!!  '</div><div class="row">' !!}

                                    @endif

                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-group">

                                            @if($attr->template == 'input_text')

                                                <div class="form-group">
                                                    <label for="input-{{ $attr->slug }}">{{ $attr->name }}</label>
                                                    <input name="attrs[{{ $attr->slug }}]" type="text"
                                                           class="form-control" id="input-{{ $attr->slug }}"
                                                           value="{{ Request::input("attrs." . $attr->slug, '') }}"
                                                           placeholder="{{ $attr->name }}">
                                                </div>

                                            @elseif($attr->template == 'dropdown')

                                                <div class="form-group">
                                                    <label for="input-{{ $attr->slug }}">{{ $attr->name }}</label>
                                                    <select name="attrs[{{ $attr->slug }}]"
                                                            class="form-control chosen-select chosen-rtl"
                                                            id="input-{{ $attr->slug }}">

                                                        <option
                                                                value="">{{ trans("ads::ads.choose_attribute_value") }}</option>

                                                        @if(isset($attr->options[app()->getLocale()]) && count($attr->options[app()->getLocale()]))
                                                            @foreach($attr->options[app()->getLocale()] as $slug => $option)

                                                                <option
                                                                        {{ Request::input("attrs." . $attr->slug, '') == $slug ? 'selected' : '' }} value="{{ $slug }}">{{ $option }}</option>

                                                            @endforeach
                                                        @endif

                                                    </select>
                                                </div>

                                            @endif

                                        </div>
                                    </div>

                                @endforeach

                            </div>

                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">{{ trans("ads::ads.filter") }}</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <form action="" method="post" class="action_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <i class="fa fa-folder"></i>
                            {{ trans("ads::ads.title") }} <span id="selected-count"></span>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        @if(count($ads))
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 action-box">

                                    <select name="action" class="form-control pull-left">
                                        <option value="-1"
                                                selected="selected">{{ trans("ads::ads.bulk_actions") }}</option>
                                        <option value="delete">{{ trans("ads::ads.delete") }}</option>
                                        <option value="unfeature">{{ trans("ads::ads.unfeature") }}</option>
                                        <option value="unsponsore">{{ trans("ads::ads.unsponsore") }}</option>
                                    </select>

                                    <button type="submit"
                                            class="btn btn-primary pull-right">{{ trans("ads::ads.apply") }}</button>

                                </div>

                                <div class="col-lg-6 col-md-4 hidden-sm hidden-xs"></div>
                            </div>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0"
                                       class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:35px">
                                            <input type="checkbox" class="i-checks check_all" name="ids[]"/>
                                        </th>
                                        <th>{{ trans("ads::ads.attributes.title") }}</th>
                                        <th>{{ trans("ads::ads.user") }}</th>
                                        <th>{{ trans("ads::ads.category") }}</th>
                                        <th>{{ trans("ads::ads.source") }}</th>
                                        <th>{{ trans("ads::ads.offers") }}</th>
                                        <th>{{ trans("ads::ads.revisions_count") }}</th>
                                        <th>{{ trans("ads::ads.creation_date") }}</th>
                                        <th>{{ trans("ads::ads.updated_date") }}</th>
                                        <th>{{ trans("ads::ads.actions") }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($ads as $ad)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="i-checks count-check-box" name="id[]"
                                                       value="{{ $ad->id }}"/>
                                            </td>

                                            <td>
                                                <a class="text-navy"
                                                   href="{{ route("admin.ads.edit", array("id" => $ad->id)) }}">
                                                    <strong>{{ $ad->title }}</strong>
                                                </a>
                                            </td>

                                            <td>
                                                @if($ad->user)
                                                    <a class="text-navy"
                                                       href="{{ route("admin.users.edit", array("id" => $ad->user_id)) }}">
                                                        <strong>{{ $ad->user->username }}</strong>
                                                    </a>
                                                @else
                                                    <strong>--</strong>
                                                @endif
                                            </td>

                                            <td>
                                                <?php
                                                $cat = $ad->categories->firstWhere('pivot.direct', 1);
                                                ?>

                                                @if($cat)
                                                    <a class="text-navy"
                                                       href="{{ route("admin.ads.show", ["category_id" => $cat->id]) }}">
                                                        {{ $cat->name }}
                                                    </a>
                                                @endif
                                            </td>

                                            <td>
                                                <small>{{ $ad->source }}</small>
                                            </td>

                                            <td>
                                                <a href="{{ route("admin.offers.show", array("ad_id" => $ad->id)) }}">
                                                    <small>{{ $ad->offers->count() }}</small>
                                                </a>
                                            </td>

                                            <td>
                                                <small>{{ $ad->revisions->count() }}</small>
                                            </td>

                                            <td>
                                                <small>{{ $ad->created_at }}</small>
                                            </td>

                                            <td>
                                                <small>{{ $ad->updated_at->render() }}</small>
                                            </td>

                                            <td class="center">

                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("ads::ads.edit") }}"
                                                   href="{{ route("admin.ads.edit", array("id" => $ad->id)) }}">
                                                    <i class="fa fa-pencil text-navy"></i>
                                                </a>

                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("ads::ads.delete") }}"
                                                   class="delete_user ask"
                                                   message="{{ trans("ads::ads.sure_delete") }}"
                                                   href="{{ URL::route("admin.ads.delete", array("id" => $ad->id)) }}">
                                                    <i class="fa fa-times text-navy"></i>
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    {{ trans("ads::ads.page") }} {{ $ads->currentPage() }} {{ trans("ads::ads.of") }} {{ $ads->lastPage() }}
                                </div>
                                <div class="col-lg-12 text-center">
                                    {{ $ads->appends(Request::all())->render() }}
                                </div>
                            </div>
                        @else
                            {{ trans("ads::ads.no_records") }}
                        @endif
                    </div>
                </div>
            </form>
        </div>

    </div>

@stop

@section("head")

    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet" type="text/css">

@stop

@section("footer")
    <script type="text/javascript" src="{{ assets('admin::js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>

        $(document).ready(function () {

            $('[data-toggle="tooltip"]').tooltip();

            $('.datetimepick').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.count-check-box').on('ifChecked ifUnchecked', function (event) {
                $(event.target).change();
            });

            $('.count-check-box').on('change', function () {
                var html = '';

                var count = $('.count-check-box:checked').length;

                if (count > 0) {
                    html = '( ' + count + ' )';
                }

                $('#selected-count').html(html);
            });

            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });

            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });

            $('#expand-filters').click(function () {
                if ($('#filters-panel').hasClass('hidden')) {
                    $('#filters-panel').removeClass('hidden');
                    $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
                    $.cookie("show_filters", 1);
                } else {
                    $('#filters-panel').addClass('hidden');
                    $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
                    $.cookie("show_filters", 0);
                }
            });

            if ($.cookie("show_filters") == 1) {
                $('#filters-panel').removeClass('hidden');
                $('#expand-filters').find('i').removeClass('fa-plus').addClass('fa-minus');
            }
        });

    </script>

@stop

