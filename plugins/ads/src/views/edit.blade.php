@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="category-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $ad->id ? trans("ads::ads.edit") : trans("ads::ads.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.ads.show") }}">{{ trans("ads::ads.title") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $ad->id ? trans("ads::ads.edit") : trans("ads::ads.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("ads::ads.save_ad") }}
                </button>

                @if($ad->id)
                    @if(auth()->user()->can("offers.manage"))
                        <a href="{{ route("admin.offers.show", ['ad_id' => $ad->id]) }}"
                           class="btn btn-primary btn-labeled btn-main"> <span
                                    class="btn-label icon fa fa-dollar"></span>
                            {{ trans("ads::ads.offers") }}</a>
                    @endif

                    @if(auth()->user()->can("branches.manage"))
                        <a href="{{ route("admin.branches.create", ['ad_id' => $ad->id]) }}"
                           class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-map"></span>
                            {{ trans("ads::ads.add_branches") }}</a>

                        <a href="{{ route("admin.branches.show", ['ad_id' => $ad->id]) }}"
                           class="btn btn-primary btn-labeled btn-main"> <span class="btn-label icon fa fa-map"></span>
                            {{ trans("ads::ads.show_branches") }}</a>
                    @endif
                @endif

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-title">{{ trans("ads::ads.attributes.title") }}</label>
                                <input name="title" type="text"
                                       value="{{ @Request::old("title", $ad->title) }}"
                                       class="form-control" id="input-title"
                                       placeholder="{{ trans("ads::ads.attributes.title") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-price">{{ trans("ads::ads.attributes.price") }}</label>
                                <input name="price" type="number"
                                       value="{{ @Request::old("price", $ad->price) }}"
                                       class="form-control" id="input-price"
                                       placeholder="{{ trans("ads::ads.attributes.price") }}">
                            </div>

                            <div class="form-group">
                                @include("admin::partials.editor", ["name" => "content", "id" => "ad-content", "value" => $ad->content])
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-pencil"></i>
                            {{ trans("ads::ads.category_attributes") }}
                        </div>
                        <div class="panel-body">

                            @if(count($attributes))

                                @foreach($attributes as $attr)

                                    <?php
                                    $value = (count($ad->custom_attributes) && $ad->custom_attributes->firstWhere('id', $attr->id)) ? $ad->custom_attributes->firstWhere('id', $attr->id)->pivot->value : [];
                                    ?>

                                    @if($attr->template == 'input_text')

                                        <div class="form-group">
                                            <label for="input-{{ $attr->slug }}">{!! $attr->required ? '<span style="color: red;">*</span> ' : '' !!}{{ $attr->name }}</label>
                                            <input name="attrs[{{ $attr->slug }}]" type="text"
                                                   class="form-control" id="input-{{ $attr->slug }}"
                                                   value="{{ @Request::old("attrs." . $attr->slug, (count($value) && isset($value[0])) ? $value[0] : '') }}"
                                                   placeholder="{{ $attr->name }}">
                                        </div>

                                    @elseif($attr->template == 'dropdown')

                                        @if($attr->type == 'single')

                                            <div class="form-group">
                                                <label for="input-{{ $attr->slug }}">{!! $attr->required ? '<span style="color: red;">*</span> ' : '' !!}{{ $attr->name }}</label>
                                                <select name="attrs[{{ $attr->slug }}]"
                                                        class="form-control chosen-select chosen-rtl"
                                                        id="input-{{ $attr->slug }}">

                                                    <option value="">{{ trans("ads::ads.choose_attribute_value") }}</option>

                                                    @if(isset($attr->options[app()->getLocale()]) && count($attr->options[app()->getLocale()]))
                                                        @foreach($attr->options[app()->getLocale()] as $slug => $option)

                                                            <option {{ (($ad->id && count($value) && isset($value[0]) && ($value[0] == $slug)) || (@Request::old("attrs." . $attr->slug) === $slug)) ? 'selected' : '' }} value="{{ $slug }}">{{ $option }}</option>

                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>

                                        @elseif($attr->type == 'multiple')

                                            <div class="form-group">
                                                <label>{!! $attr->required ? '<span style="color: red;">*</span> ' : '' !!}{{ $attr->name }}</label>
                                                <ul class='tree-views'>

                                                    @if(isset($attr->options[app()->getLocale()]) && count($attr->options[app()->getLocale()]))
                                                        @foreach($attr->options[app()->getLocale()] as $slug => $option)

                                                            <li>
                                                                <div class='tree-row checkbox i-checks'>
                                                                    <label>
                                                                        <input type='checkbox'
                                                                               {{ ($ad and in_array($slug, $value)) ? 'checked="checked"' : '' }}
                                                                               name='attrs[{{ $attr->slug }}][]'
                                                                               value='{{ $slug }}'>
                                                                        &nbsp; {{ $option }}
                                                                    </label>
                                                                </div>
                                                            </li>

                                                        @endforeach
                                                    @endif

                                                </ul>
                                            </div>

                                        @endif

                                    @endif


                                @endforeach

                            @endif

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-9" style="height: 32px;line-height: 32px;">
                                    <i class="fa fa-picture-o"></i>
                                    {{ trans("ads::ads.media") }}
                                </div>
                                <div class="col-md-3">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-labeled pull-right"
                                       id="add_media">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        {{ trans("ads::ads.add_media") }}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="dd">

                                <ul class="dd-list" id="sortable">

                                    @if($ad->id && $ad->media->count())
                                        @foreach($ad->media as $media)

                                            <li class="dd-item">
                                                <div class="dd-handle">

                                                    <div class="row" style="margin: 5px;">
                                                        <a class="col-md-3 media-prev-link"
                                                           href="{{ uploads_url($media->path) }}" target="_blank">
                                                            <img width="100%" height="90px" class="media-prev"
                                                                 src="{{ thumbnail($media->path) }}"/>
                                                        </a>

                                                        <span class="col-md-9 media-prev-title">{{ $media->title }}</span>
                                                    </div>
                                                </div>

                                                <input type="hidden" class="dd-hidden" name="media_ids[]"
                                                       value="{{ $media->id }}"/>
                                                <a href="javascript:void(0)"
                                                   class="pull-right remove-item remove-media"> <i
                                                            class="fa fa-times"></i></a>
                                            </li>

                                        @endforeach
                                    @endif

                                </ul>

                                @if(!$ad->id || $ad->media->count() == 0)
                                    <div id="no-media-files">{{ trans("ads::ads.no_media_files") }}</div>
                                @endif

                            </div>

                        </div>
                    </div>
                    @foreach(Action::fire("ads.form.featured", $ad) as $output)
                        {!!  $output !!}
                    @endforeach

                </div>

                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-cogs"></i>
                            {{ trans("ads::ads.attributes.settings") }}
                        </div>
                        <div class="panel-body">

                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-sponsored">{{ trans("ads::ads.attributes.sponsored") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("sponsored", $ad->sponsored)) checked="checked" @endif
                                    type="checkbox" id="input-sponsored" name="sponsored" value="1"
                                           class="sponsored-switcher switcher-sm">
                                </div>
                            </div>

                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-featured">{{ trans("ads::ads.attributes.featured") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("featured", $ad->featured)) checked="checked" @endif
                                    type="checkbox" id="input-featured" name="featured" value="1"
                                           class="featured-switcher switcher-sm">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input-status">{{ trans("ads::ads.attributes.status") }}</label>
                                <select name="status" class="form-control chosen-select chosen-rtl" id="input-status">

                                    <option {{ $ad->status == 0 ? 'selected="selected"' : '' }} value="0">{{ trans("ads::ads.deactivated") }}</option>
                                    <option {{ $ad->status == 1 ? 'selected="selected"' : '' }} value="1">{{ trans("ads::ads.activated") }}</option>
                                    <option {{ $ad->status == 2 ? 'selected="selected"' : '' }} value="2">{{ trans("ads::ads.pending") }}</option>

                                    @if($ad->id)
                                        <option {{ $ad->status == 3 ? 'selected="selected"' : '' }} value="3">{{ trans("ads::ads.rejected") }}</option>
                                        <option {{ $ad->status == 4 ? 'selected="selected"' : '' }} value="4">{{ trans("ads::ads.sold") }}</option>
                                    @endif

                                </select>
                            </div>

                            @if($ad->id)
                                <div class="form-group rejected-div {{ $ad->status == 3 ? '' : 'hidden' }}">
                                    <label for="input-rejection-id">{{ trans("ads::ads.attributes.rejection_reason") }}</label>
                                    <select name="rejection_reason_id" class="form-control chosen-select chosen-rtl"
                                            id="input-rejection-id">

                                        @if(count($rejection_reasons))
                                            @foreach($rejection_reasons as $rjr)

                                                <option {{ $ad->rejection_reason_id == $rjr->id ? 'selected' : '' }} value="{{ $rjr->id }}">{{ $rjr->title }}</option>

                                            @endforeach
                                        @endif

                                    </select>
                                </div>

                                <div class="form-group rejected-div {{ $ad->status == 3 ? '' : 'hidden' }}">
                                    <label for="input-rejection-message">{{ trans("ads::ads.attributes.rejection_reason_message") }}</label>
                                    <textarea name="rejection_reason_message"
                                              class="form-control">{{ $ad->rejection_reason_message }}</textarea>
                                </div>
                            @endif

                        </div>
                    </div>


                    @if($ad->id)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-folder"></i>
                                {{ trans("ads::ads.category") }}
                            </div>
                            <div class="panel-body">

                                @include('categories::partials.choose_category', ['categories' => $categories, 'type' => 'choose_category', 'cat_id' => $direct_category ? $direct_category->id : 0, 'show_label' => false, 'has_form_group' => true])

                                <input type="hidden" name="confirm_category_change" id="confirm_category_change"
                                       value=""/>

                                <button type="submit" id="change-category" class="btn btn-flat btn-danger">
                                    {{ trans("ads::ads.change_category") }}
                                </button>
                            </div>
                        </div>
                    @endif


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar-times-o"></i>
                            {{ trans("ads::ads.attributes.expire_at") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group format-area event-format-area">
                                <div class="input-group date datetimepick">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="expire_at" type="text"
                                           value="{{ (!$ad->id) ? "" : @Request::old('expire', $ad->expired_at) }}"
                                           class="form-control" id="input-expire-at"
                                           placeholder="{{ trans("ads::ads.attributes.expire_at") }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-map-marker"></i>
                            {{ trans("ads::ads.place") }}
                        </div>
                        <div class="panel-body">

                            @include('i18n::places.places_dropdown', ['countries' => $countries, 'cities' => $cities, 'regions' => $regions, 'country_id' => $ad->country_id, 'city_id' => $ad->city_id, 'region_id' => $ad->region_id, 'all' => false, 'only_selects' => false])

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-user"></i>
                            {{ trans("ads::ads.owner") }}
                        </div>
                        <div class="panel-body">

                            <input type="hidden" id="user-id-hid" name="user_id" value="{{ $ad ? $ad->user_id : '' }}"/>

                            <div class="form-group">
                                <label for="input-user">{{ trans("ads::ads.attributes.user_first_name") }}</label>
                                <input type="text" name="first_name"
                                       value="{{ $ad->user ? $ad->user->first_name : '' }}"
                                       class="form-control" id="input-user"
                                       placeholder="{{ trans("ads::ads.attributes.user_first_name") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-email">{{ trans("ads::ads.attributes.email") }}</label>
                                <input name="email" type="text"
                                       value="{{ @Request::old("email", $ad->email) }}"
                                       class="form-control" id="input-email"
                                       placeholder="{{ trans("ads::ads.attributes.email") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-phone">{{ trans("ads::ads.attributes.phone") }}</label>
                                <input name="phone" type="text"
                                       value="{{ @Request::old("phone", $ad->phone) }}"
                                       class="form-control" id="input-phone"
                                       placeholder="{{ trans("ads::ads.attributes.phone") }}">
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-tags"></i>
                            {{ trans("ads::ads.add_tag") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group" style="position:relative">
                                <input type="hidden" name="tags" id="tags_names"
                                       value="{{ join(",", $ad_tags) }}">
                                <ul id="mytags"></ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </form>

    <div style="display: none;" id="dummy-sortable">
        <li class="dd-item">
            <div class="dd-handle">

                <div class="row" style="margin: 5px;">
                    <a href="" target="_blank" class="col-md-3 media-prev-link">
                        <img width="100%" height="90px" class="media-prev"
                             src="{{ assets("admin::default/image.png") }}"/>
                    </a>

                    <span class="col-md-9 media-prev-title"></span>
                </div>
            </div>

            <input type="hidden" class="dd-hidden" name="media_ids[]" value=""/>
            <a href="javascript:void(0)" class="pull-right remove-item remove-media"> <i
                        class="fa fa-times"></i></a>
        </li>
    </div>
@stop


@push("head")

    <link href="{{ assets("admin::tagit") }}/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{{ assets("admin::tagit") }}/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"
          type="text/css">


    <link href="{{ assets('admin::css/plugins/nestable/nestable.ltr.css') }}" rel="stylesheet"/>

    <style>
        .autocomplete-suggestions {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border: 1px solid #999;
            background: #FFF;
            cursor: default;
            overflow: auto;
            -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
            -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
            box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
        }

        .autocomplete-suggestion {
            padding: 2px 5px;
            white-space: nowrap;
            overflow: hidden;
        }

        .autocomplete-no-suggestion {
            padding: 2px 5px;
        }

        .autocomplete-selected {
            background: #F0F0F0;
        }

        .autocomplete-suggestions strong {
            font-weight: bold;
            color: #000;
        }

        .autocomplete-group {
            padding: 2px 5px;
            font-weight: bold;
            font-size: 16px;
            color: #000;
            display: block;
            border-bottom: 1px solid #000;
        }

        .media-prev {
            padding: 5px;
            background-color: #ffffff;
        }

        .media-prev-title {
            font-weight: normal !important;
        }
    </style>
@endpush


@push("footer")

    <script type="text/javascript" src="{{ assets("admin::tagit") }}/tag-it.js"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript" src="{{ assets('ads::js/jquery.autocomplete.js') }}"></script>

    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.featured-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.sponsored-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        $(document).ready(function () {

            $('.datetimepick').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });

            $('#change-category').click(function (e) {

                bootbox.dialog({
                    message: "{{ trans('ads::ads.confirm_change_category') }}",
                    buttons: {
                        success: {
                            label: "{{ trans('ads::ads.accept') }}",
                            className: "btn-primary",
                            callback: function () {
                                $('#confirm_category_change').val('1');

                                $('#category-form').submit();
                            }
                        },
                        danger: {
                            label: "{{ trans('ads::ads.cancel') }}",
                            className: "btn-default",
                            callback: function () {
                                e.preventDefault();
                            }
                        },
                    },
                    className: "bootbox-sm"
                });

                return false;
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $("#mytags").tagit({
                singleField: true,
                singleFieldNode: $('#tags_names'),
                allowSpaces: true,
                minLength: 2,
                placeholderText: "",
                removeConfirmation: true,
                tagSource: function (request, response) {
                    $.ajax({
                        url: "{{ route("admin.tags.search") }}",
                        data: {q: request.term},
                        dataType: "json",
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.name
                                }
                            }));
                        }
                    });
                },
                beforeTagAdded: function (event, ui) {
                    $("#metakeywords").tagit("createTag", ui.tagLabel);
                }
            });


            $('#input-user').devbridgeAutocomplete({
                serviceUrl: '{{ route("admin.ads.get_users") }}',
                minChars: 2,
                onSelect: function (suggestion) {
                    $('#user-id-hid').val(suggestion.data.id);

                    $('#input-email').val(suggestion.data.email);

                    $('#input-phone').val(suggestion.data.phone);
                }
            });


            $('#input-user').off('focus.autocomplete');

            $("#sortable").sortable();

            $("#add_media").filemanager({
                panel: "media",
                types: "image|video",
                done: function (result, base) {
                    if (result.length) {
                        $('#no-media-files').hide();

                        for (var i = 0; i < result.length; i++) {
                            var existed = $('#sortable').find('.dd-hidden[value="' + result[i].id + '"]');

                            if (existed.size() == 0) {

                                var sortable_li = $('#dummy-sortable li').clone();

                                sortable_li.find('.dd-hidden').val(result[i].id);

                                sortable_li.find('.media-prev').attr('src', result[i].thumbnail);

                                if (result[i].type == 'image') {
                                    sortable_li.find('.media-prev-link').attr('href', result[i].url);
                                } else {
                                    sortable_li.find('.media-prev-link').attr('href', '#');
                                }

                                sortable_li.find('.media-prev-title').html(result[i].title);

                                $('#sortable').append(sortable_li);
                            }
                        }
                    }
                },
                error: function (media_path) {
                    alert_box(media_path + "{{ trans("ads::ads.file_not_media") }}");
                }
            });

            $(document).on('click', '.remove-media', function () {
                $(this).parents('.dd-item').remove();

                if ($('#sortable li').size() == 0) {
                    $('#no-media-files').show();
                }
            });

            $('#input-status').change(function () {
                var status = $(this).val();

                if (status == 3) {
                    $('.rejected-div').removeClass('hidden');
                } else {
                    $('.rejected-div').addClass('hidden');
                }
            });

        });
    </script>

@endpush
