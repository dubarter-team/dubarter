<li>
    <a href="javascript:void(0)" class="open-media-popup">
        <span class="fa fa-camera"></span>
    </a>
</li>

@push("footer")

<script>
    $(document).ready(function () {
        $(".open-media-popup").filemanager({
            panel: "media"
        })
    })
</script>

@endpush
