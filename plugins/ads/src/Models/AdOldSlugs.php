<?php

namespace Dot\Ads\Models;

use Dot\Platform\Model;

/**
 * Class AdOldSlugs
 * @package Dot\Ads\Models
 */
class AdOldSlugs extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $table = "ads_old_slugs";

}
