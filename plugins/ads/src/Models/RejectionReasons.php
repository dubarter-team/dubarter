<?php

namespace Dot\Ads\Models;

use DB;
use Dot\Platform\Model;

/**
 * Class rejectionreasons
 * @package Dot\Topics\Models
 */
class RejectionReasons extends Model
{

    /**
     * @var string
     */
    protected $module = 'ads';

    /**
     * @var string
     */
    protected $table = 'rejection_reasons';

    public $timestamps = false;
    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    /*
     * lang fields
     */
    protected $translatable = [
        'title'
    ];

    protected $casts = [
        'title' => 'json'
    ];
}
