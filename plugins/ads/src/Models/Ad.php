<?php

namespace Dot\Ads\Models;

use DB;
use Dot\Attributes\Models\Attribute;
use Dot\Branches\Models\Branch;
use Dot\Categories\Models\Category;
use Dot\Offers\Models\Offer;
use Dot\Platform\Model;
use Dot\Media\Models\Media;
use Dot\Seo\Models\SEO;
use Dot\Tags\Models\Tag;
use Dot\Users\Models\User;
use Dot\I18n\Models\Place;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ad
 * @package Dot\Topics\Models
 */
class Ad extends Model
{

    /**
     * @var string
     */
    protected $module = 'ads';

    /**
     * @var string
     */
    protected $table = 'ads';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;
    /*
         * lang fields
         */
    protected $translatable = [
        'currency'
    ];

    /*
     * Casts
     * @var array
     */
    protected $casts = [
        'currency' => 'json'
    ];
    /*
     * @var array
     */
    protected $searchable = ['title', 'slug'];

    /*
     * @var array
     */
    protected $sluggable = [
        'slug' => 'title',
    ];

    /*
     * @var array
     */
    protected $creatingRules = [
        "title" => "required",
        "slug" => "unique:ads,slug",
        'country_id' => 'required|numeric'
    ];

    /*
     * @var array
     */
    protected $updatingRules = [
        "title" => "required",
        "slug" => "unique:ads,slug,[id],id",
        'country_id' => 'required|numeric'
    ];

    /**
     * user relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * country relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function country()
    {
        return $this->hasOne(Place::class, "id", "country_id");
    }

    /**
     * offers relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function offers()
    {
        return $this->hasMany(Offer::class, 'ad_id')->where('parent', 0);
    }

    /**
     * branches relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function branches()
    {
        return $this->hasMany(Branch::class, 'ad_id');
    }

    /**
     * city relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function city()
    {
        return $this->hasOne(Place::class, "id", "city_id");
    }

    /**
     * region relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function region()
    {
        return $this->hasOne(Place::class, "id", "region_id");
    }

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function custom_attributes()
    {
        return $this->belongsToMany(Attribute::class, 'ads_attributes_values', 'ad_id', 'attribute_id')->using('Dot\Ads\Models\AdsAttributes')->withPivot('value');
    }

    /**
     * media relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function media()
    {
        return $this->belongsToMany(Media::class, 'ads_media', 'ad_id', 'media_id')->orderBy('order', 'asc');
    }

    /**
     * images relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function images()
    {
        return $this->belongsToMany(Media::class, 'ads_media', 'ad_id', 'media_id')->where('type', 'image')->orderBy('order', 'asc');
    }

    /**
     * filees relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function files()
    {
        return $this->belongsToMany(Media::class, 'ads_media', 'ad_id', 'media_id')->orderBy('order', 'asc');
    }

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function categories()
    {
        return $this->belongsToMany(Category::class, 'ads_categories', 'ad_id', 'category_id')->withPivot('direct');
    }

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function tags()
    {
        return $this->belongsToMany(Tag::class, 'ads_tags', 'ad_id', 'tag_id');
    }

    /**
     * user relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function rejection_reason()
    {
        return $this->hasOne(RejectionReasons::class, "id", "rejection_reason_id");
    }

    /**
     * revisions relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function revisions()
    {
        return $this->hasMany(Ad::class, 'parent', 'id');
    }

    /**
     * Sync tags
     * @param $tags
     */
    public function sync_tags($tags)
    {
        $tag_ids = array();

        if ($tags = @explode(",", $tags)) {
            $tags = array_filter($tags);
            $tag_ids = Tag::saveNames($tags);
        }

        $this->tags()->sync($tag_ids);
    }

    public function new_slug($slug, $to, $index = 1)
    {

        if ($index == 1) {
            $sluggy = $slug;
        } else {
            $sluggy = $slug . "-" . $index;
        }

        if (DB::table($this->table)->where($to, $sluggy)->where('id', '!=', $this->id)->count() > 0) {
            $index = $index + 1;
            return $this->new_slug($slug, $to, $index);
        }

        return $sluggy;
    }

    /**
     * Seo relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function seo()
    {
        return $this->hasOne(SEO::class, 'object_id')->where('type', 2);
    }

//    protected static function boot()
//    {
//        parent::boot();
//
//        static::addGlobalScope(function (Builder $builder) {
//            $builder->where("country_id", config("country.id"));
//        });
//    }


    public function scopeCountry($query, $code){

        if($code == "eg"){
            $query->whereIn("country_id", [0, 21]);
        }elseif($code == "iq"){
            $query->where("country_id", 10);
        }

    }
}
