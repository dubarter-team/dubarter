<?php

namespace Dot\Ads\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AdsAttributes extends Pivot
{
    protected $casts = [
        'value' => 'json'
    ];
}