<?php

namespace Dot\Ads\Controllers;

use Action;
use Dot\Ads\Classes\AdsExport;
use Dot\Ads\Models\AdOldSlugs;
use Dot\Offers\Models\Offer;
use Illuminate\Support\Facades\Auth;
use Dot\Ads\Jobs\AdJob;
use Dot\Ads\Models\Ad;
use Dot\Ads\Models\RejectionReasons;
use Dot\Categories\Models\Category;
use Dot\I18n\Models\Place;
use Dot\Platform\Controller;
use Dot\Users\Models\User;
use Redirect;
use Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Indices\Ad as AdIndex;
use App\Index;

class AdsController extends Controller
{

    use DispatchesJobs;

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all ads
     * @return mixed
     */
    function index()
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();

                    case "unfeature":
                        return $this->unfeature();

                    case "unsponsore":
                        return $this->unsponsore();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : 'created_at';
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : 20;

        $this->data['category'] = $category = Request::get('category_id') ? Category::where('id', Request::get('category_id'))->with(['custom_attributes'])->first() : null;

        $this->data['attributes'] = $attributes = Category::get_attributes($this->data['category']);

        $query = Ad::orderBy($this->data["sort"], $this->data["order"])->country(config("country.code"));
        // $query->whereHas("user", function ($query) {
        //     // $query->where("username", Request::get("username"));
        //     $query->where("users.username", 'like', '%' . Request::get("username"). '%');
        // })->get();
        // dd($query);


        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        if (Request::filled("category_id") && Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });

            if (count($attributes)) {
                foreach ($attributes as $attr) {
                    $post_attrs = Request::get('attrs');

                    if (isset($post_attrs[$attr->slug])) {
                        $val = $post_attrs[$attr->slug];
                        $id = $attr->id;

                        $query->whereHas("custom_attributes", function ($query) use ($id, $val) {
                            $query->where('id', $id);
                            $query->whereRaw('JSON_CONTAINS(value, \'"' . $val . '"\')');
                        });

                    }

                }
            }
        }

        if (Request::filled("tag_id") && Request::get("tag_id") != 0) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        if (Request::filled("username")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.username", Request::get("username"));
            });
        }

        if (Request::filled("user_id") && Request::get("user_id") != 0 ) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("source")) {
            $query->where('source', Request::get("source"));
        }

        if (Request::filled("status")) {
            if (in_array(request('status'), [0, 1, 2, 3, 4])) {
                $query->where("status", Request::get("status"));
            } else if (request('status') == 5) {
                $query->has('revisions');
            }
        }

        if (Request::filled('date_type')) {

            if (Request::filled("from")) {
                $query->where(request('date_type'), ">=", Request::get("from"));
            }

            if (Request::filled("to")) {
                $query->where(request('date_type'), "<=", Request::get("to"));
            }
        }

        if (Request::filled("type") && Request::get("type") == 'featured') {
            $query->where("featured", 1);
        }


        if (Request::filled("type") && Request::get("type") == 'sponsored') {
            $query->where("sponsored", 1);
        }

        if (Request::filled("country_id") && Request::get("country_id") != 0) {
            $query->where("country_id", Request::get("country_id"));
        }

        if (Request::filled("city_id") && Request::get("city_id") != 0) {
            $query->where("city_id", Request::get("city_id"));
        }

        if (Request::filled("rejection_reason")) {
            $query->where("rejection_reason_id", Request::get("rejection_reason"));
        }

        if (Request::filled("region_id") && Request::get("region_id") != 0) {
            $query->where("region_id", Request::get("region_id"));
        }


        $this->data["ads"] = $query->with(['categories', 'offers', 'user', 'revisions'])->where('parent', 0)->paginate($this->data['per_page']);

        $this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

        $this->data["categories"] = Category::where('parent', 0)->where('status', 1)->get();

        $this->data["cities"] = Request::get("country_id", 0) ? Place::where('parent', Request::get("country_id"))->orderBy('order', 'asc')->where('status', 1)->get() : [];

        $this->data["regions"] = Request::get("city_id", 0) ? Place::where('parent', Request::get("city_id"))->orderBy('order', 'asc')->where('status', 1)->get() : [];

        $this->data["rejection_reasons"] = RejectionReasons::all();

        return view("ads::show", $this->data);
    }

    public function export()
    {
        $category = Request::get('category_id') ? Category::where('id', Request::get('category_id'))->with(['custom_attributes'])->first() : null;

        return \Excel::download(new AdsExport(), 'ads-' . ($category ? (string)$category->slug . '-' : '') . date('Y-m-d') . '.xlsx');

    }

    /**
     * Delete ad by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $ad = Ad::where('id', $id)->with(['revisions'])->first();

            if (count($ad->revisions)) {
                foreach ($ad->revisions as $rev) {
                    $this->ad_delete($rev);
                }
            }

            $this->ad_delete($ad);

        }

        return Redirect::back()->with("message", trans("ads::ads.events.deleted"));
    }

    /**
     * Delete ad by id
     * @return mixed
     */
    public function unfeature()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        $ads = Ad::whereIn('id', $ids)->get();

        foreach ($ads as $ad) {
            $ad->featured = 0;
            $ad->save();

            Index::save($ad->id);
        }

        return Redirect::back()->with("message", trans("ads::ads.events.unfeatured"));
    }

    /**
     * Delete ad by id
     * @return mixed
     */
    public function unsponsore()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        $ads = Ad::whereIn('id', $ids)->get();

        foreach ($ads as $ad) {
            $ad->sponsored = 0;
            $ad->save();

            Index::save($ad->id);
        }

        return Redirect::back()->with("message", trans("ads::ads.events.unsponsored"));
    }

    public function ad_delete($ad)
    {
        $id = $ad->id;

        $ad->categories()->detach();

        $ad->tags()->detach();

        $ad->media()->detach();

        Offer::where('ad_id', $ad->id)->delete();

        $ad->custom_attributes()->detach();

        AdOldSlugs::where('ad_id', $ad->id)->delete();

        $ad->delete();

        Index::save($id);
    }

    /**
     * Create a new ad
     * @return mixed
     */
    public function create($cat_id)
    {
        $category = Category::where('id', $cat_id)->with(['custom_attributes', 'parent_category'])->first();

        if (!$category) {
            abort(404);
        }

        $custom_attributes = Category::get_attributes($category);

        $ad = new Ad();

        if (Request::isMethod("post")) {

            $ad->title = Request::get('title');
            $ad->price = Request::get('price');
            $ad->content = Request::get('content');
            $ad->country_id = Request::get('country_id');
            $ad->city_id = Request::get('city_id');
            $ad->region_id = Request::get('region_id');
            $ad->email = Request::get('email');
            $ad->phone = Request::get('phone');
            $ad->sponsored = Request::get('sponsored');
            $ad->featured = Request::get('featured');
            $ad->status = Request::get('status');
            $ad->user_id = request('user_id', Auth::user()->id);
            $ad->data_entry_id = Auth::user()->id;

            $ad->source = 'backend';

            $ad->expired_at = Request::get('expire_at');

            if (in_array($ad->expired_at, [NULL, ""])) {
                $ad->expired_at = NULL;
            }

            if (Request::get('status') == 1) {
                $ad->published_at = date("Y-m-d H:i:s");
            }


            //validating attributes

            $valid_attrs = true;

            if ($custom_attributes->count()) {
                $attrs_values = Request::get('attrs');

                foreach ($custom_attributes as $attr) {

                    if ($attr->required) {
                        if (!isset($attrs_values[$attr->slug])) {
                            $valid_attrs = false;
                        } else if ($attr->data_type == 'text' && $attr->template == 'input_text' && trim($attrs_values[$attr->slug]) == '') {
                            $valid_attrs = false;
                        } else if ($attr->data_type == 'number' && $attr->template == 'input_text' && !is_numeric($attrs_values[$attr->slug])) {
                            $valid_attrs = false;
                        }
                    }

                }

            }

            if (!$ad->validate() || !$valid_attrs) {
                $attrs_errors = '';

                if (!$valid_attrs) {
                    $attrs_errors = trans("ads::ads.missing_attrs");
                }

                return Redirect::back()->withErrors($ad->errors() ? $ad->errors()->add('attrs', $attrs_errors) : ['attrs' => $attrs_errors])->withInput(Request::all());
            }

            $country = Place::where('parent', 0)->where('id', Request::get('country_id'))->where('status', 1)->first();

            $ad->currency = $country->currency->toArray();

            $ad->save();

            //saving categories
            $cat = $category;

            while (true) {
                $ad->categories()->attach($cat->id, ['direct' => ($cat->id == $cat_id ? 1 : 0)]);

                if ($cat->parent == 0) {
                    break;
                } else {
                    $cat = $cat->parent_category;
                }
            }

            //saving attributes
            $ad->custom_attributes()->detach();

            if ($custom_attributes->count()) {
                $attrs_values = Request::get('attrs');

                foreach ($custom_attributes as $attr) {
                    $value = '';

                    if (isset($attrs_values[$attr->slug])) {
                        if ($attr->type == 'single') {
                            $value = [$attrs_values[$attr->slug]];
                        } elseif ($attr->type == 'multiple') {
                            $value = $attrs_values[$attr->slug];
                        }

                        $ad->custom_attributes()->attach($attr->id, ['value' => $value]);
                    }

                }

            }

            //saving media
            $media_ids = Request::get('media_ids', []);

            if (count($media_ids)) {
                $i = 0;

                foreach ($media_ids as $media_id) {
                    $ad->media()->attach($media_id, ['order' => $i]);

                    $i++;
                }
            }

            //saving tags
            $ad->sync_tags(Request::get("tags", []));

            Action::fire("ad.saved", $ad);

            Index::save($ad->id);

            return Redirect::route("admin.ads.edit", array("id" => $ad->id))
                ->with("message", trans("ads::ads.events.created"));
        }

        $this->data["ad"] = $ad;

        $this->data["attributes"] = Category::get_attributes($category);

        $this->data["ad_tags"] = [];

        $this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

        $this->data["cities"] = [];

        $this->data["regions"] = [];

        return view("ads::edit", $this->data);
    }

    /**
     * Edit ad by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {


        $ad = Ad::where('id', $id)->with(['categories', 'tags', 'media', 'user', 'custom_attributes', 'revisions'])->first();

        if (!$ad) {
            abort(404);
        }

        $direct_category = $ad->categories->where('pivot.direct', 1)->first();

        $custom_attributes = Category::get_attributes($direct_category);

        if (Request::isMethod("post")) {
            $old_title = $ad->title;
            $old_slug = $ad->slug;

            $ad->title = Request::get('title');
            $ad->price = Request::get('price');
            $ad->content = Request::get('content');
            $ad->country_id = Request::get('country_id');
            $ad->city_id = Request::get('city_id');
            $ad->region_id = Request::get('region_id');
            $ad->email = Request::get('email');
            $ad->phone = Request::get('phone');
            $ad->featured = Request::get('featured');
            $ad->sponsored = Request::get('sponsored');

            if (Request::get('status') == 1) {

                if (count($ad->revisions)) {
                    foreach ($ad->revisions as $rev) {
                        $this->ad_delete($rev);
                    }
                }

            }

            $ad->status = Request::get('status');
            $ad->user_id = request('user_id', Auth::user()->id);
            $ad->data_entry_id = Auth::user()->id;

            if (Request::get('status') == 3) {
                $ad->rejection_reason_id = Request::get('rejection_reason_id');
                $ad->rejection_reason_message = Request::get('rejection_reason_message');
            } else {
                $ad->rejection_reason_id = 0;
                $ad->rejection_reason_message = null;
            }

            $ad->expired_at = Request::get('expire_at');

            if (in_array($ad->expired_at, [NULL, ""])) {
               // $ad->expired_at = date("Y-m-d H:i:s");
            }

            if (in_array($ad->published_at, [NULL, ""]) && Request::get('status') == 1) {
                $ad->published_at = date("Y-m-d H:i:s");
            }


            //validating attributes

            $valid_attrs = true;

            if ($custom_attributes->count()) {
                $attrs_values = Request::get('attrs');

                foreach ($custom_attributes as $attr) {

                    if ($attr->required) {
                        if (!isset($attrs_values[$attr->slug])) {
                            $valid_attrs = false;
                        } else if ($attr->data_type == 'text' && $attr->template == 'input_text' && trim($attrs_values[$attr->slug]) == '') {
                            $valid_attrs = false;
                        } else if ($attr->data_type == 'number' && $attr->template == 'input_text' && !is_numeric($attrs_values[$attr->slug])) {
                            $valid_attrs = false;
                        }
                    }

                }

            }

            if (!$ad->validate() || !$valid_attrs) {
                $attrs_errors = '';

                if (!$valid_attrs) {
                    $attrs_errors = trans("ads::ads.missing_attrs");
                }

                return Redirect::back()->withErrors($ad->errors() ? $ad->errors()->add('attrs', $attrs_errors) : ['attrs' => $attrs_errors])->withInput(Request::all());
            }

            if ($old_title != Request::get('title')) {
                $new_slug = $ad->new_slug(str_slug_utf8(Request::get('title')), 'slug');

                $ad_old_slug = new AdOldSlugs();

                $ad_old_slug->ad_id = $ad->id;

                $ad_old_slug->slug = $old_slug;

                $ad_old_slug->save();

                $ad->slug = $new_slug;
            }

            $ad->save();

            //saving media
            $ad->media()->detach();

            $media_ids = Request::get('media_ids', []);

            if (count($media_ids)) {
                $i = 0;

                foreach ($media_ids as $media_id) {
                    $ad->media()->attach($media_id, ['order' => $i]);

                    $i++;
                }
            } else {
                $ad->media()->sync([]);
            }

            //saving tags
            $ad->sync_tags(Request::get("tags", []));

            if (Request::filled('category_id') && Request::filled('confirm_category_change') && Request::get('confirm_category_change')) {
                $category = Category::where('id', Request::get('category_id'))->with(['custom_attributes', 'parent_category'])->first();

                if ($category) {
                    $ad->categories()->detach();

                    $cat = $category;

                    while (true) {
                        $ad->categories()->attach($cat->id, ['direct' => ($cat->id == Request::get('category_id') ? 1 : 0)]);

                        if ($cat->parent == 0) {
                            break;
                        } else {
                            $cat = $cat->parent_category;
                        }
                    }

                    $new_custom_attributes = Category::get_attributes($category);

                    foreach ($ad->custom_attributes as $ad_att) {
                        if (!$new_custom_attributes->where('id', $ad_att->id)->count()) {
                            $ad->custom_attributes()->detach($ad_att->id);
                        }
                    }

                    Action::fire("ad.saved", $ad);

                    Index::save($ad->id);

                    return Redirect::route("admin.ads.edit", array("id" => $id))->with("message", trans("ads::ads.events.updated"));
                }
            }

            //saving attributes
            $ad->custom_attributes()->detach();

            if ($custom_attributes->count()) {
                $attrs_values = Request::get('attrs');

                foreach ($custom_attributes as $attr) {
                    $value = '';

                    if (isset($attrs_values[$attr->slug])) {
                        if ($attr->type == 'single') {
                            $value = [$attrs_values[$attr->slug]];
                        } elseif ($attr->type == 'multiple') {
                            $value = $attrs_values[$attr->slug];
                        }

                        $ad->custom_attributes()->attach($attr->id, ['value' => $value]);
                    }

                }

            }

            Action::fire("ad.saved", $ad);

            Index::save($ad->id);

            return Redirect::route("admin.ads.edit", array("id" => $id))->with("message", trans("ads::ads.events.updated"));
        }

        $this->data["ad"] = $ad;

        $this->data["direct_category"] = $direct_category;

        $this->data["categories"] = Category::where('parent', 0)->where('status', 1)->get();;

        $this->data["attributes"] = $custom_attributes;

        $this->data["ad_tags"] = $ad->tags->pluck("name")->toArray();

        $this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

        $this->data["cities"] = $ad->country_id ? Place::where('parent', $ad->country_id)->where('status', 1)->get() : [];

        $this->data["regions"] = $ad->city_id ? Place::where('parent', $ad->city_id)->where('status', 1)->get() : [];

        $this->data["rejection_reasons"] = RejectionReasons::all();

        return view("ads::edit", $this->data);
    }

    public function get_users()
    {
        $q = Request::get('query');

        $res = ['suggestions' => []];

        if ($q) {
            $users = User::where('backend', 0)->search(urldecode($q))->where('status', 1)->take(10)->get();

            foreach ($users as $user) {
                $temp = [];

                $temp['value'] = $user->first_name;

                $temp['data'] = $user;

                $res['suggestions'][] = $temp;
            }
        }

        return response()->json($res);
    }

}
