<?php

namespace Dot\Ads\Jobs;

use App\Index;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AdJob implements ShouldQueue
{
    use Dispatchable, Queueable;

    /**
     * Ad ID
     *
     * @var integer
     */
    protected $id;

    /**
     * Create a new job instance
     * AdIndex constructor.
     *
     * @param Mixed $id
     */
    public function __construct($id)
    {
        $this->id = (integer)$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Index::save($this->id);
    }
}
