<?php

namespace Dot\Ads\Jobs;

use Dot\Options\Facades\Option;
use Dot\Users\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Pusher\Laravel\Facades\Pusher;

class NotificationJob implements ShouldQueue
{
    use Dispatchable, Queueable;

    /**
     * notification type
     *
     * @var string
     */
    protected $type;

    /**
     * notification title
     *
     * @var string
     */
    protected $title;

    /**
     * notification message
     *
     * @var string
     */
    protected $message;

    /**
     * notification user id
     *
     * @var integer
     */
    protected $user_id;

    /**
     * object id
     *
     * @var integer
     */
    protected $object_id;

    /**
     * object url
     *
     * @var string
     */
    protected $object_url;


    /**
     * Create a new job instance
     *
     * @param String $type
     * @param Integer $id
     */
    public function __construct($type, $object_id = 0, $object_url = '', $user_id = 0)
    {
        $this->type = $type;

        $this->user_id = $user_id;

        $this->object_id = $object_id;

        $this->object_url = $object_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->type) {

            case 'new_article' :
                $offset = 0;

                $this->title = 'blah blah blah';

                $this->message = 'this is test';

                $this->send_web_notification();

                while (true) {

                    $users_tokens = User::whereNotNull('mobile_token')->where('status', 1)->skip($offset)->take(1000)->get()->pluck('mobile_token')->toArray();

                    if (count($users_tokens)) {

                        $temp = $this->get_notification($users_tokens);

                        $this->send_notification($temp);

                        $offset += 1000;
                    } else {
                        break;
                    }

                }

                break;

            case 'livestream' :
                $offset = 0;

                $this->title = Option::get('livestream_title');

                $this->message = Option::get('livestream_message');

                $this->send_web_notification();

                while (true) {

                    $users_tokens = User::whereNotNull('mobile_token')->where('status', 1)->skip($offset)->take(1000)->get()->pluck('mobile_token')->toArray();

                    if (count($users_tokens)) {

                        $temp = $this->get_notification($users_tokens);

                        $this->send_notification($temp);

                        $offset += 1000;
                    } else {
                        break;
                    }

                }

                break;

        }
    }

    public function get_notification($users_tokens)
    {
        $key = is_array($users_tokens) ? 'registration_ids' : 'to';

        $temp = [
            $key => $users_tokens,
            'notification' => [
                'title' => $this->title,
                'body' => $this->message,
                'sound' => true,
            ],
            'data' => [
                'title' => $this->title,
                "message" => $this->message,
                "type" => $this->type,
                "object_id" => $this->object_id,
                "object_url" => $this->object_url,
            ]
        ];

        return $temp;
    }

    public function send_notification($notification)
    {
        $legacy_server_key = 'AIzaSyDGY1rKbHYB5xo4tSKOCwgc9fHXRFlH80g';

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $headers = [
            'Authorization: key=' . $legacy_server_key,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notification));
        $result = curl_exec($ch);

        curl_close($ch);
    }

    public function send_web_notification($user_id = 0)
    {
        Pusher::trigger('public-notifications-channel', 'new-content', ['title' => $this->title, 'message' => $this->message, 'object_url' => $this->object_url]);
    }
}
