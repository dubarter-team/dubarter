<?php

namespace Dot\Ads;

use Illuminate\Support\Facades\Auth;
use Navigation;
use Dot\Categories\Models\Category;
use URL;

class Ads extends \Dot\Platform\Plugin
{

    protected $permissions = [
        "manage"
    ];

    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("ads.manage")) {

                $menu->item('ads', trans("ads::ads.title"), "#")->icon("fa-newspaper-o")->order(0.1);

                $menu->item('ads.all', trans("ads::ads.all_ads"), route("admin.ads.show"))->icon("fa-folder")->order(0);

                foreach (Category::where("parent", 0)->get() as $category) {
                    $menu->item('ads.' . $category->slug, $category->name, route("admin.ads.show", ["category_id" => $category->id]))->icon("fa-folder")->order(0);
                }
            }

        });

        Navigation::menu("topnav", function ($menu) {
            $menu->make("ads::media_popup");
        });
    }
}
