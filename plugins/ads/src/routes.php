<?php

/*
 * WEB
 */
Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:ads.manage"],
    "namespace" => "Dot\\Ads\\Controllers"
], function ($route) {
    $route->group(["prefix" => "ads"], function ($route) {

        $route->any('/', ["as" => "admin.ads.show", "uses" => "AdsController@index"]);
        $route->any('/create/{cat_id}', ["as" => "admin.ads.create", "uses" => "AdsController@create"]);
        $route->any('/delete', ["as" => "admin.ads.delete", "uses" => "AdsController@delete"]);
        $route->any('/{id}/edit', ["as" => "admin.ads.edit", "uses" => "AdsController@edit"]);

        $route->any('/get_users', ["as" => "admin.ads.get_users", "uses" => "AdsController@get_users"]);

        $route->get('/export', ["as" => "admin.ads.export", "uses" => "AdsController@export"]);
    });
});



