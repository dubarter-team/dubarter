<?php
/**
 * Created by PhpStorm.
 * User: elbardeny
 * Date: 5/8/18
 * Time: 11:24 AM
 */

namespace Dot\Ads\Classes;

use Dot\Ads\Models\Ad;
use Dot\Categories\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Request;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AdsExport implements FromCollection, WithHeadings
{
    //data to export
    public function headings(): array
    {
        return ['id', 'title', 'slug', 'user_id', 'price', 'source', 'views', 'featured', 'status', 'published_at'];
    }

    public function collection()
    {

        $sort = (Request::filled("sort")) ? Request::get("sort") : 'created_at';
        $order = (Request::filled("order")) ? Request::get("order") : "DESC";


        $category = Request::get('category_id') ? Category::where('id', Request::get('category_id'))->with(['custom_attributes'])->first() : null;

        $attributes = Category::get_attributes($category);

        $query = Ad::orderBy($sort, $order);

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        if (Request::filled("category_id") && Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });

            if (count($attributes)) {
                foreach ($attributes as $attr) {
                    $post_attrs = Request::get('attrs');

                    if (isset($post_attrs[$attr->slug])) {
                        $val = $post_attrs[$attr->slug];
                        $id = $attr->id;

                        $query->whereHas("custom_attributes", function ($query) use ($id, $val) {
                            $query->where('id', $id);
                            $query->whereRaw('JSON_CONTAINS(value, \'"' . $val . '"\')');
                        });

                    }

                }
            }
        }

        if (Request::filled("tag_id") && Request::get("tag_id") != 0) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        if (Request::filled("user_id") && Request::get("user_id") != 0) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("status")) {
            if (in_array(request('status'), [0, 1, 2, 3, 4])) {
                $query->where("status", Request::get("status"));
            } else if (request('status') == 5) {
                $query->has('revisions');
            }
        }

        if (Request::filled('date_type')) {

            if (Request::filled("from")) {
                $query->where(request('date_type'), ">=", Request::get("from"));
            }

            if (Request::filled("to")) {
                $query->where(request('date_type'), "<=", Request::get("to"));
            }
        }

        if (Request::filled("type") && Request::get("type") == 'featured') {
            $query->where("featured", 1);
        }

        if (Request::filled("country_id") && Request::get("country_id") != 0) {
            $query->where("country_id", Request::get("country_id"));
        }

        if (Request::filled("city_id") && Request::get("city_id") != 0) {
            $query->where("city_id", Request::get("city_id"));
        }

        if (Request::filled("rejection_reason")) {
            $query->where("rejection_reason_id", Request::get("rejection_reason"));
        }

        if (Request::filled("region_id") && Request::get("region_id") != 0) {
            $query->where("region_id", Request::get("region_id"));
        }

        $query->where('parent', 0);

        $query->select(['id', 'title', 'slug', 'user_id', 'price', 'source', 'views', 'featured', 'status', 'published_at']);

        $ads = $query->get();

        return $ads;
    }
}
