<?php

/**
 * Get new place id from old one.
 *
 * @param $old_id
 *
 * @return int
 */
function get_new_place_id($old_id)
{

    $city = \Illuminate\Support\Facades\DB::table("places")->where("old_id", $old_id)->first();

    return $city ? $city->id : 0;
}

/**
 * Upload file to server and return media id.
 *
 * @param bool $path
 *
 * @return int|mixed
 */
function get_file_id($path = false)
{

    if ($path) {

        $path = rawurlencode($path);

        $image = new \Dot\Media\Models\Media();

        $image = $image->saveLink("https://dubarter.com/" . $path, "backend",  1);

        if ($image) {
            return $image->id;
        }
    }

    return 0;
}


/**
 * Generate tags from old category name
 *
 * @param int $old_category_id
 *
 * @return array
 */
function get_tag_ids_from_ad_category_id($old_category_id = 0)
{

    $category = \Illuminate\Support\Facades\DB::connection("sqlsrv")->table("CATEGORY")->where("ID", $old_category_id)->first();

    $tag_ids = \Dot\Tags\Models\Tag::saveNames([$category->ArabicCategoryName]);

    if ($tag_ids) {
        return $tag_ids;
    }

    return [];
}

function get_ad_category_ids_from_model_id($old_model_id)
{

    if ($old_model_id) {

        $old_model = \Illuminate\Support\Facades\DB::connection("sqlsrv")->table("MODELS")
            ->where("Id", $old_model_id)
            ->first();

        if ($old_model) {

            $new_category = DB::table("categories")
                ->where("name->ar", $old_model->ArName)
                ->where("name->en", $old_model->EnName)
                ->first();

            if ($new_category) {
                return get_top_category_ids($new_category->id);
            }

        }

    }

    return [];

}

/**
 * Get new category ids from old category id
 *
 * @param int $old_category_id
 *
 * @return array
 */
function get_new_ad_category_ids($old_category_id = 0)
{

    $categories_mappings = config("migration.categories");

    if (array_key_exists($old_category_id, $categories_mappings)) {
        return get_top_category_ids($categories_mappings [$old_category_id]);
    }

    return [];
}


/**
 * Recursion function to get hight category ids
 *
 * @param int   $category_id
 * @param array $category_ids
 *
 * @return array
 */
function get_top_category_ids($category_id = 0, $category_ids = [])
{

    if ($category_id) {

        $category = \Illuminate\Support\Facades\DB::table("categories")->where("id", $category_id)->first();

        if($category){

            $category_ids[] = $category->id;

            return get_top_category_ids($category->parent, $category_ids);
        }
    }

    return $category_ids;

}


/**
 * Get new status from old one
 *
 * @param bool $old_ad_status
 *
 * @return mixed
 */
function get_new_ad_status($old_ad_status = false)
{

    if ($old_ad_status) {

        $statuses_mappings = config("migration.statuses");

        if (array_key_exists($old_ad_status, $statuses_mappings)) {
            return $statuses_mappings[$old_ad_status];
        }

    }

}

function prepare_attribute_value($value)
{
    return snake_case(str_replace("/", "", $value));
}

function get_ad_attributes($ad)
{

    $attributes = [];

    /**
     * Listing type (item_listing_type, job_listing_type)
     */

    $ad_listing_types = config("migration.ad_listing_types");

    $listing_type = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("LISTINGTYPE")->where("ID", $ad->LISTINGTYPEID)->first();


    if ($listing_type) {

        if (array_key_exists($listing_type->ID, $ad_listing_types)) {
            $attributes[$ad_listing_types[$listing_type->ID]] = prepare_attribute_value($listing_type->LISTTYPENAME);
        }

    }

    /**
     * year
     */

    $year = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("YEARS")->where("Id", $ad->YEAR)->first();


    if ($year) {
        $attributes["year"] = prepare_attribute_value($year->Year);
    }

    /**
     * propery_type
     */

    $property_type = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("PropertyTypes")->where("ID", $ad->PROPERTYTYPE)->first();

    if ($property_type) {
        $attributes["property_type"] = prepare_attribute_value($property_type->Name);
    }

    /**
     * bedrooms
     */

    if ($ad->NUMBEROFBEDROOMS) {
        $attributes["bedrooms"] = prepare_attribute_value($ad->NUMBEROFBEDROOMS);
    }

    /**
     * bathrooms
     */

    if ($ad->NUMBEROFBATHROOMS) {
        $attributes["bathrooms"] = prepare_attribute_value($ad->NUMBEROFBATHROOMS);
    }

    /**
     * kilometers
     */

    if ($ad->KILOMETER) {
        $attributes["kilometers"] = prepare_attribute_value($ad->KILOMETER);
    }

    /**
     * Area
     */

    if ($ad->AREA) {
        $attributes["area"] = prepare_attribute_value($ad->AREA);
    }

    /**
     * field
     */

    $field = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("JobTitles")->where("ID", $ad->JobTitleId)->first();

    if ($field) {
        $attributes["field"] = prepare_attribute_value($field->EnglishJobTitle);
    }

    /**
     * experience
     */

    if ($ad->WorkExp) {
        $attributes["experience"] = prepare_attribute_value($ad->WorkExp);
    }

    /**
     * designation
     */

    if ($ad->Designation) {
        $attributes["designation"] = $ad->Designation;
    }

    /**
     * job_type
     */

    $job_type = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("JobType")->where("Id", $ad->JobType)->first();

    if ($job_type) {
        $attributes["job_type"] = prepare_attribute_value($job_type->JobType);
    }

    /**
     * qualification
     */

    $qualification = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("QualificationLevel")->where("Id", $ad->QualificationLevel)->first();

    if ($qualification) {
        $attributes["qualification"] = prepare_attribute_value($qualification->QualificationLevel);
    }

    /**
     * nationality
     */

    $nationality = \Illuminate\Support\Facades\DB::connection("sqlsrv")
        ->table("Nationalty")->where("Id", $ad->Nationalty)->first();

    if ($nationality) {
        $attributes["nationality"] = prepare_attribute_value($nationality->Nationalty);
    }


    /**
     * Getting attr_id, attr_value pairs
     */
    $new_attributes = [];

    foreach ($attributes as $name => $value) {

        $attribute = \Illuminate\Support\Facades\DB::table("attributes")->where("slug", $name)
            ->first();

        if ($attribute) {
            $new_attributes[$attribute->id] = $value;
        }

    }

    return $new_attributes;
}


function get_ad_media_files($old_ad_id = false)
{
    if ($old_ad_id) {

        $files = \Illuminate\Support\Facades\DB::connection("sqlsrv")->table("ATTACHMENTAD")
            ->where("CreateAd", $old_ad_id)
            ->get();

        $media_ids = [];

        foreach ($files as $file) {

            $media_id = get_file_id(str_replace("~/", "", $file->ImageURL));

            if ($media_id) {
                $media_ids[] = $media_id;
            }

        }

        return $media_ids;
    }
}

function get_post_categories($old_post_id)
{
    $old_category = \Illuminate\Support\Facades\DB::connection("sqlsrv")->table("ArticleCategories")
        ->where("ArticleId", $old_post_id)
        ->first();

    if ($old_category) {

        $articles_categories_mapping = config("migration.articles_categories");

        if (array_key_exists($old_category->ArticleCategoryId, $articles_categories_mapping)) {
            return get_top_category_ids($articles_categories_mapping[$old_category->ArticleCategoryId]);
        }
    }

    return [];
}

/**
 * Clean string
 *
 * @param $string
 *
 * @return mixed|string
 */
function clean_trim($string)
{
    $string = str_replace("\r\n", '', $string);

    $string = trim($string);

    return $string;
}

