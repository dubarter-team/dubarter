<?php

namespace Migration;

use Dot\Platform\Plugin;

class Migration extends Plugin
{

    /*
     * Migrate commands
     */
    protected $commands = [
        Commands\DataMigrateCommand::class
    ];

    public function boot()
    {
        require $this->getPath("routes.php");
        require $this->getPath("helpers.php");
    }

    /*
     * Plugin registration
     * Extending core classes
     */
    public function register()
    {

        if (file_exists($config = $this->getPath() . '/config/migration.php')) {
            $this->mergeConfigFrom(
                $config, "migration"
            );
        }
    }
}
