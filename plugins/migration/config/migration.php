<?php

return [

    /*
     * Statuses old-new flag mapper
     */
    "statuses" => [
        1 => 2, // created => pending
        2 => 1, // published => activated
        3 => 3, // rejected => rejected
        4 => 4, // sold => sold
        5 => 1, // corrected => activated
    ],

    /*
     * Categories old-new ID mapper
     */
    "categories" => [

        // mrkbat : Motors
        232 => 1, // 7 is sayarar mosta3mla

        // syarat : Cars
        233 => 7,
        // motsklat-o-skotr-llbyaa : motorcycle
        234 => 72,
        // koarb : Boats
        235 => 72,
        // aarbyat-atfal : baby car
        236 => 72,
        // atobysat-o-aarbyat-nkl : bus and truck for sale
        237 => 72,
        // akssoarat-oktaa-ghyar : car accessories and spare parts
        239 => 72,
        // mrkbat-akhr : Other Motors
        240 => 72,
        // drajat-ryadyh-llbyaa : bikes
        364 => 72,


        // aqarat : Real Estate
        241 => 2,

        // fll-llbyaa : Villa for sale
        242 => 35,
        // shkk-llbyaa : Apartments for sale
        243 => 34,
        // mkatb-llbyaa : Offices for sale
        244 => 38,
        // arady-llbyaa : Land for sale
        245 => 37,
        // khdmat-smsr-ookalat : brokers and agencies
        246 => 39,
        // shalyhat-llbyaa : chalet for sale
        247 => 36,
        // ghrf-oshrka-llghrf : Rooms & Roommates
        248 => 39,
        // aakarat-akhr : Other Real Estate
        249 => 39,
        // mhlat-llbyaa : shops for sale
        357 => 39,
        // shkk-llayjar : Apartments for rent
        366 => 40,
        // fll-llayjar : villa for rent
        367 => 41,
        // mkatb-llayjar : offices for rent
        368 => 44,
        // arady-llayjar : land for rent
        369 => 43,
        // mhlat-llayjar : shops for rent
        370 => 45,
        // shalyhat-llayjar : chalet for rent
        371 => 45,


        // mbobat : Classifieds
        250 => 5,

        // thf : Antiques
        251 => 55,
        // fn : Art
        252 => 55,
        // mstlzmat-atfal : Baby Items
        253 => 55,
        // ktb-o-mjlat : Books & Magazines
        254 => 55,
        // tjar-o-snaaa : Business & Industrial
        255 => 55,
        // ajhz-tablt : Tablets & PDAs
        256 => 47,
        // mlabs-omlhkatha : Clothing & Accessories
        257 => 51,
        // mktnyat : Collectibles
        258 => 55,
        // kmbyotr-o-shbkat : Computers & Networking
        259 => 50,
        // estoanat-d-f-d-o-aflam : DVD & Movies
        260 => 55,
        // elktronyat-oajhz-mnzly : Electronics & Home Appliances
        261 => 48,
        // athath : Furniture
        262 => 53,
        // bstn-o-mstlzmat-hdaek-o-mnazl : Indoor and greenhouse plants, garden
        265 => 55,
        // hoatf-mhmol : Mobile Phones
        266 => 47,
        // alat-mosyky : Musical Instruments
        267 => 55,
        // lohat-fny : Paintings
        268 => 55,
        // hyoanat-alyf : Pets
        269 => 49,
        // tsoyr-fotoghrafy : Photographic
        270 => 55,
        // aaamal-nht : Sculpture
        271 => 55,
        // slaa-ryady-otrfyhy : Sports Equipment
        272 => 55,
        // tthakr-o-ksaem : Tickets & Vouchers
        299 => 55,
        // mbobat-akhr : Other Classified
        300 => 55,
        // adoat-almtabkh : Kitchen Tools
        313 => 52,


        // khdmat : Services
        273 => 54,

        // eaalanat : Advertising
        274 => 54,
        // khdmat-syarat : cars services
        275 => 54,
        // fn-tsmym-eaalam : Art & Design & Media
        276 => 54,
        // raaay-alatfal-o-almsnyn : Child & Elder Care
        277 => 54,
        // khdmat-altnthyf : Cleaning Services
        278 => 54,
        // khdmat-alkmbyotr : Computer Services
        279 => 54,
        // khdmat-mbtkr : Creative
        280 => 54,
        // syan-ajhz-elktrony : Electronic Repair
        281 => 54,
        // maly : Financial
        283 => 54,
        // mtaaam-o-tjhyz-almakolat : Food Services & Restaurants
        284 => 54,
        // syanat : General Maintenance
        285 => 54,
        // mrakz-tjmyl : Health & Beauty
        286 => 54,
        // mnzly : Household
        287 => 54,
        // tnsyk-hdaek-o-tshjyr-msthat : Landscaping
        288 => 54,
        // dros-khsosy : Lessons
        289 => 54,
        // khdmat-kanony : Legal
        290 => 54,
        // nkl-aamlyat-alezal : Moving/Removals
        291 => 54,
        // dhanat : Painters
        292 => 54,
        // etsalat : Telecommunications
        293 => 54,
        // trjm : Translation
        294 => 54,
        // o-sael-entkalat-o-sfryat : Transportation & Travel
        295 => 54,
        // aaml-mn-almnzl : Work at Home
        296 => 54,
        // khdmat-akhr : Other Services
        297 => 54,


        // othaef : Jobs
        306 => 46,

        // othaef-khaly : Job Opportunities
        308 => 67,
        // ybhth-aan-othyfh : Looking for a Job
        309 => 68,
        // mkaydh-othyfh : Job Parter
        310 => 68,
        // othaef-tsoyk-o-mbyaaat : sales and marketing jobs
        372 => 56,
        // othaef-taalym-otdrys : teaching jobs
        373 => 57,
        // othaef-tknolojya-almaalomat-o-atsalat : IT and communications jobs
        374 => 58,
        // othaef-almoard-albshry : HR jobs
        375 => 59,
        // othaef-skrtary : secretary jobs
        376 => 60,
        // othaef-syah-ofnadk : tourism and hotels jobs
        377 => 61,
        // othaef-tb-o-sh : Medical and Health jobs
        378 => 62,
        // othaef-aamar-o-mhndsyn : engineering jobs
        379 => 63,
        // othaef-mhasb : accounting jobs
        380 => 64,
        // othaef-adary-ao-aaml-adary : administrative jobs
        381 => 65,
        // othaef-fn-o-tsmym : designers jobs
        382 => 66,


        // rkn-almra : Women Corner
        358 => 3,

        // mrakz-tjmyl : Health & Beauty
        263 => 25,
        // mod-oahthy : Fashion & Shoes
        282 => 17,
        // fsatyn-zfaf-oshr : Wedding/Soiree Dresses
        305 => 29,
        // makyaj-o-msthdrat-tjmyl : Makeup and Cosmetics
        342 => 26,
        // shrashf-o-mfarsh-sryr : linnen and mattresses
        353 => 3, ///////// take main category
        // aator : Perfumes
        360 => 28,
        // saaaat-oakssorat : Watches and accessories
        361 => 24,
        // hkaeb-omhafth : Bags & Wallets
        362 => 23,
        // akl-byt : Home eating
        363 => 15,


        // ksm-almtaaam : Restaurants section
        383 => 8,

        // kafyhat : cafe
        384 => 8,
        // mtaaam : Restaurants
        385 => 8,
        // dylyfry : delivery
        386 => 8,
    ],

    /*
     * Articles categories old-new ID mapper
     */
    "articles_categories" => [
        2 => 1, // sayarat
        3 => 2, // aqarat
        4 => 47, // mobailat
        5 => 48, // aghza kahrabya
        6 => 46, // wazayef
        7 => 54, // khdamat
        8 => 0, // safr w sya7a
        9 => 3, // hya
    ],

    /*
     * Ads listing vs attributes name in new website
     */
    "ad_listing_types" => [
        1 => "item_listing_type",
        2 => "item_listing_type",
        3 => "item_listing_type",
        4 => "item_listing_type",
        5 => "item_listing_type",
        6 => "item_listing_type",
        7 => "item_listing_type",
        8 => "job_listing_type",
        9 => "job_listing_type",
    ],

    "offers_statuses" => [
        
    ]


];
