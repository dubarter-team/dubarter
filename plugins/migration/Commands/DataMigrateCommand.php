<?php

namespace Migration\Commands;

use Dot\Platform\Command;
use Migration\Models\User;

/*
 * Class DataMigrateCommand
 */
class DataMigrateCommand extends Command
{

    /*
     * @var string
     */
    protected $name = 'data:migrate';

    /*
     * @var string
     */
    protected $description = "Migrate old database";

    /*
     * Fire the command
     */
    public function handle()
    {
        $this->info("migrating...");

        $count = User::count();

        $this->info($count);
    }
}
