<?php

namespace Migration\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Migration\Models\User;

class UsersController extends Controller
{

    protected $limit = 100;

    public function index($hreflang, $offset = 0)
    {

        $users = User::select("USER.*", "Person.FirstName as person_first_name", "Person.LastName as person_last_name", "Person.CountryID", "Person.CityID", "Person.Address")
            ->take($this->limit)->skip($offset)
            ->join("Person", "USER.ID", "=", "Person.UserID")
            ->orderBy("ID", "ASC")->get();

        if (count($users)) {

            foreach ($users as $user) {

                // Continue if user exists

                if(DB::table("users")->where("id", $user->ID)->count()){
                    continue;
                }

                $provider = NULL;
                $provider_id = NULL;

                if ($user->IsFacebook) {
                    $provider = "facebook";
                    $provider_id = $user->FACEBOOKID;
                }

                if ($user->IsTwitter) {
                    $provider = "twitter";
                }


                // if no first name set it as username

                $first_name = !in_array($user->person_first_name, [NULL, ""]) ? $user->person_first_name : $user->FirstName;

                if(!$first_name){
                    $first_name = $user->USERNAME;
                }

                DB::table("users")->insert([
                    "id" => $user->ID,
                    "first_name" => $first_name,
                    "last_name" => !in_array($user->person_last_name, [NULL, ""]) ? $user->person_last_name : $user->LastName,
                    "email" => $user->USEREMAIL,
                    "password" => $user->IsFacebook ? Hash::make("q^#(ej><dffsfwe324") : Hash::make($user->PASSWORD),
                    "username" => $user->USERNAME,
                    "provider" => $provider,
                    "provider_id" => $provider_id,
                    "role_id" => $user->ISAdmin ? 1 : 0,
                    "status" => !$user->DISABLE,
                    "backend" => $user->ISAdmin,
                    "photo_id" => 0,
                    "lang" => "ar",
                    "country_id" => get_new_place_id($user->CountryID),
                    "city_id" => get_new_place_id($user->CityID),
                    "address" => $user->Address,
                    "created_at" => $user->RegistrationDate ? date("Y-m-d H:i:s", strtotime($user->RegistrationDate)) : date("Y-m-d H:i:s"),
                    "updated_at" => $user->RegistrationDate ? date("Y-m-d H:i:s", strtotime($user->RegistrationDate)) : date("Y-m-d H:i:s")
                ]);
            }

            return redirect(route("users.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
