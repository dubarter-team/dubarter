<?php

namespace Migration\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\DB;
use Migration\Models\Subscription;

class SubscriptionsController extends Controller
{

    protected $limit = 20;

    public function index($hreflang, $offset = 0)
    {

        $rows = Subscription::take($this->limit)->skip($offset)
            ->orderBy("Id", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if reason exists

                if (DB::table("subscriptions")->where("id", $row->Id)->count()) {
                    continue;
                }

                DB::table("subscriptions")->insert([
                    "id" => $row->Id,
                    "category_id" => $row->CategoryId,
                    "email" => $row->Email,
                    "created_at" => $row->DateCreated,
                    "updated_at" => $row->LastSentDate,
                ]);

            }

            return redirect(route("subscriptions.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
