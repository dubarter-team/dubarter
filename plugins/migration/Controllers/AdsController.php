<?php

namespace Migration\Controllers;

use Dot\Ads\Jobs\AdJob;
use Dot\Platform\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use Migration\Models\Ad;

class AdsController extends Controller
{

    use DispatchesJobs;


    protected $limit = 1;

    /*
     * Migrate ads to new database
     *
     * @param int $offset
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function index($hreflang, $offset = 0)
    {

        // $sizes = ['thumbnail' => [165, 108], 'medium' => [500, 300]];

        // Config::set("media.sizes", $sizes);
        //Config::get("media.s3.status", false);

        $rows = Ad::take($this->limit)
            ->skip($offset)
            ->orderBy("ID", "DESC")
            ->leftJoin("AdRejectionReason", "AdRejectionReason.AdId", "=", "CREATEAD.ID")
            ->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if ad exists

                if (DB::table("ads")->where("id", $row->ID)->count()) {
                    return redirect(route("ads.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));
                }

                // Saving to the main table

                $ad = [
                    'id' => (int)$row->ID,
                    'slug' => $row->URLId,
                    'title' => $row->ITEMTITLE,
                    'content' => $row->ITEMDESC,
                    'created_at' => date("Y-m-d H:i:s", strtotime($row->CREATIONDATE)),
                    'updated_at' => date("Y-m-d H:i:s", strtotime($row->CREATIONDATE)),
                    'published_at' => date("Y-m-d H:i:s", strtotime($row->CREATIONDATE)),
                    'expired_at' => date("Y-m-d H:i:s", strtotime($row->ExpiryDate)),
                    'country_id' => get_new_place_id($row->ITEMCOUNTRYID),
                    'city_id' => get_new_place_id($row->ITEMCITYID),
                    'region_id' => 0,
                    'lat' => $row->LAT ? $row->LAT : NULL,
                    'lng' => $row->LNG ? $row->LNG : NULL,
                    'user_id' => (int)$row->USERID,
                    'email' => $row->EMAIL ? $row->EMAIL : NULL,
                    'phone' => $row->PHONENUMBER ? $row->PHONENUMBER : NULL,
                    'status' => get_new_ad_status($row->STATUS),
                    'parent' => 0,
                    'rejection_reason_id' => $row->RejectionReasonId ? $row->RejectionReasonId : 0,
                    'rejection_reason_message' => $row->RejectionReasonComments,
                    'price' => $row->ITEMVALUE,
                    'currency' => json_encode([
                        "en" => $row->Currency,
                        "ar" => $row->ArabicCurrency,
                    ]),
                    'views' => (int)$row->ViewCount,
                    'mobile_views' => (int)$row->ShowMobileCount,
                    'featured' => (int)$row->ISFEATURE,
                    "need_media" => 1
                ];


                try {
                    DB::table("ads")->insert($ad);
                } catch(QueryException $ex){

                    return redirect(route("ads.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));
                  //  dd($ex->getMessage());
                    // Note any method of class PDOException can be called on $ex.
                }

                // Saving ad media

                /*$ad_media_files = get_ad_media_files($row->ID);

                $index = 1;

                foreach ($ad_media_files as $file_id) {

                    DB::table("ads_media")->insert([
                        "ad_id" => $row->ID,
                        "media_id" => $file_id,
                        "order" => $index
                    ]);

                    $index++;
                }*/

                // Saving ad attributes

                $attributes = get_ad_attributes($row);

                foreach ($attributes as $attribute_id => $attribute_value) {
                    DB::table("ads_attributes_values")->insert([
                        "ad_id" => $row->ID,
                        "attribute_id" => $attribute_id,
                        "value" => json_encode($attribute_value)
                    ]);
                }

                // Saving to new categories
                // car brands and models become categories in new website
                // so we will check if the current ad if it has a model then
                // we will recognize the category structure

                $new_category_ids = [];

                if ($row->ModelId) {
                    $new_category_ids = get_ad_category_ids_from_model_id($row->ModelId);
                } else {
                    $old_category = DB::connection("sqlsrv")->table("CATEGORYAD")
                        ->where("AdID", $row->ID)->first();

                    if ($old_category) {
                        $new_category_ids = get_new_ad_category_ids($old_category->CategoryID);
                    }
                }

                // Saving to categories

                $index = 0;

                foreach ($new_category_ids as $new_category_id) {

                    DB::table("ads_categories")->insert([
                        "ad_id" => $row->ID,
                        "category_id" => $new_category_id,
                        "direct" => $index == 0
                    ]);

                    $index++;
                }

                // Saving to tags

                // generating tags from old category name

                $old_category = DB::connection("sqlsrv")->table("CATEGORYAD")
                    ->where("AdID", $row->ID)->first();

                if ($old_category) {

                    $ad_tag_ids = get_tag_ids_from_ad_category_id($old_category->CategoryID);

                    foreach ($ad_tag_ids as $ad_tag_id) {

                        DB::table("ads_tags")->insert([
                            "ad_id" => $row->ID,
                            "tag_id" => $ad_tag_id
                        ]);
                    }

                }

                dispatch(new AdJob($row->ID));
                unset($row);
                unset($ad);
                unset($attributes);
                unset($old_category);
                // sleep(1);

            }

            return redirect(route("ads.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
