<?php

namespace Migration\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\DB;
use Migration\Models\Offer;
use Migration\Models\Message;

class OffersController extends Controller
{

    protected $limit = 20;

    public function index($hreflang, $offset = 0)
    {

        $rows = Offer::take($this->limit)->skip($offset)
            ->orderBy("OfferID", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if offer exists

                if (DB::table("offers")->where("id", $row->OfferID)->count()) {
                    continue;
                }

                DB::table("offers")->insert([
                    "id" => $row->OfferID,
                    "ad_id" => $row->AdID,
                    "sender_id" => $row->OfferUserID,
                    "receiver_id" => $row->OwnerUserID,
                    "price" => $row->OfferValue ? $row->OfferValue : 0,
                    "type" => is_null($row->BarterAdID) ? "bid" : "bart",
                    "barter_ad_id" => $row->BarterAdID ? $row->BarterAdID : 0,
                    "comment" => $row->OfferComment ? $row->OfferComment : 0,
                    "parent" => 0,
                    "status" => $row->OfferStatusID ? $row->OfferStatusID : 1, // default:1 in old DB
                    "created_at" => $row->CreationDate,
                    "updated_at" => $row->CreationDate
                ]);

                // Getting offer messages

                // Continue if offer messages exists

                if (DB::table("offers")->where("parent", $row->OfferID)->count()) {

                    continue;

                } else {

                    $messages = Message::where("OfferId", $row->OfferID)->get();

                    foreach ($messages as $message) {

                        DB::table("offers")->insert([
                            "ad_id" => $row->AdID,
                            "sender_id" => $message->SenderUserId,
                            "receiver_id" => $message->ReciverUserId,
                            "price" => $row->OfferValue ? $row->OfferValue : 0,
                            "type" => is_null($row->BarterAdID) ? "bid" : "bart",
                            "barter_ad_id" => $row->BarterAdID ? $row->BarterAdID : 0,
                            "comment" => $message->MessageContent,
                            "parent" => $row->OfferID,
                            "status" => 1,
                            "created_at" => $message->MessageDate,
                            "updated_at" => $message->MessageDate
                        ]);

                    }
                }

            }

            return redirect(route("offers.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
