<?php

namespace Migration\Controllers;

use App\Index;
use Dot\Platform\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use Migration\Models\Ad;

class CarsController extends Controller
{

    use DispatchesJobs;


    protected $limit = 1;

    /*
     * Migrate ads to new database
     *
     * @param int $offset
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function index($hreflang, $offset = 0)
    {

        $rows = Ad::orderBy("CREATEAD.ID", "ASC")
            ->take($this->limit)
            ->skip($offset)
            ->leftJoin("AdRejectionReason", "AdRejectionReason.AdId", "=", "CREATEAD.ID")
            ->join("CATEGORYAD", "CATEGORYAD.AdID", "=", "CREATEAD.ID")
            ->where("CREATEAD.CREATIONDATE", ">", "2018-01-01 00:00:00")
            ->where("CATEGORYAD.CategoryID", 233)
            ->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if ad exists

                $ad = DB::table("ads")->where("slug", $row->URLId)->where("phone", $row->PHONENUMBER)->first();

                if (!$ad) {
                    continue;
                }

                $row->ID = $ad->id;

                // Saving to new categories
                // car brands and models become categories in new website
                // so we will check if the current ad if it has a model then
                // we will recognize the category structure

                $new_category_ids = [];

                if ($row->ModelId) {
                    $new_category_ids = get_ad_category_ids_from_model_id($row->ModelId);
                } else {
                    $old_category = DB::connection("sqlsrv")->table("CATEGORYAD")
                        ->where("AdID", $row->ID)->first();

                    if ($old_category) {
                        $new_category_ids = get_new_ad_category_ids($old_category->CategoryID);
                    }
                }

                // Saving to categories

                DB::table("ads_categories")->where("ad_id", $row->ID)->delete();

                $index = 0;

                foreach ($new_category_ids as $new_category_id) {

                    DB::table("ads_categories")->insert([
                        "ad_id" => $row->ID,
                        "category_id" => $new_category_id,
                        "direct" => $index == 0
                    ]);

                    $index++;
                }

                // Saving to tags

                DB::table("ads_tags")->where("ad_id", $row->ID)->delete();

                // generating tags from old category name

                $old_category = DB::connection("sqlsrv")->table("CATEGORYAD")
                    ->where("AdID", $row->ID)->first();

                if ($old_category) {

                    $ad_tag_ids = get_tag_ids_from_ad_category_id($old_category->CategoryID);

                    foreach ($ad_tag_ids as $ad_tag_id) {

                        DB::table("ads_tags")->insert([
                            "ad_id" => $row->ID,
                            "tag_id" => $ad_tag_id
                        ]);
                    }

                }

                Index::save($row->ID);

                unset($row);
            }
            return redirect(route("cars.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
