<?php

namespace Migration\Controllers;


use Dot\Platform\Controller;
use Illuminate\Support\Facades\DB;
use Migration\Models\Car;
use Migration\Models\Category;
use Migration\Models\Model;


class CategoriesController extends Controller
{

    protected $limit = 10000;

    public function index($hreflang, $offset = 0)
    {

        return "Categories are already migrated";

        foreach (Car::get() as $car) {

            DB::table("categories")->insert([
                "parent" => 7,
                "name" => json_encode([
                    "en" => trim(str_replace("\r\n", "", $car->EnName)),
                    "ar" => trim(str_replace("\r\n", "", $car->ArabicName))
                ]),
                "slug" => json_encode([
                    "en" => str_slug_utf8(trim(str_replace("\r\n", "", $car->EnName))),
                    "ar" => str_slug_utf8(trim(str_replace("\r\n", "", $car->ArabicName))),
                ]),
                "order" => 0,
                "status" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ]);

            $car_id = DB::getPdo()->lastInsertId();

            foreach (Model::where("CarId", $car->Id)->get() as $model) {

                DB::table("categories")->insert([
                    "parent" => $car_id,
                    "name" => json_encode([
                        "en" => trim(str_replace("\r\n", "", $model->EnName)),
                        "ar" => trim(str_replace("\r\n", "", $model->ArName))
                    ]),
                    "slug" => json_encode([
                        "en" => str_slug_utf8(trim(str_replace("\r\n", "", $model->EnName))),
                        "ar" => str_slug_utf8(trim(str_replace("\r\n", "", $model->ArName))),
                    ]),
                    "order" => 0,
                    "status" => 1,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ]);

            }
        }

        $rows = Category::take($this->limit)->skip($offset)
            ->orderBy("ID", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if category exists

                if (DB::table("categories")->where("id", $row->ID)->count()) {
                    continue;
                }

                DB::table("categories")->insert([
                    "id" => $row->ID,
                    "parent" => $row->ParentID ? $row->ParentID : 0,
                    "name" => json_encode([
                        "en" => trim(str_replace("\r\n", "", $row->EnglishCategoryName)),
                        "ar" => trim(str_replace("\r\n", "", $row->ArabicCategoryName))
                    ]),
                    "slug" => json_encode([
                        "en" => trim($row->URLId),
                        "ar" => trim($row->URLIdAr)
                    ]),
                    "order" => $row->CategoryOrder ? $row->CategoryOrder : 0,
                    "status" => 1,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ]);

                // SEO

                if (DB::table("seo")->where("id", $row->ID)->where("type", "category")->count()) {
                    continue;
                }

                DB::table("seo")->insert([
                    "type" => "category",
                    "id" => $row->ID,
                    "meta_title" => json_encode([
                        "en" => $row->PageTitleEn,
                        "ar" => $row->PageTitleAr
                    ]),
                    "meta_description" => json_encode([
                        "en" => $row->MetaDescriptionEn,
                        "ar" => $row->MetaDescriptionAr
                    ]),
                    "meta_keywords" => json_encode([
                        "en" => $row->PageMetaKeywordEn,
                        "ar" => $row->PageMetaKeywordAr
                    ])
                ]);
            }

            // Importing cars
            // we need to insert all car brands into cars/used categry
            // that thas new ID: 7

            foreach (Car::get() as $car) {

                DB::table("categories")->insert([
                    "parent" => 7,
                    "name" => json_encode([
                        "en" => trim(str_replace("\r\n", "", $car->EnName)),
                        "ar" => trim(str_replace("\r\n", "", $car->ArabicName))
                    ]),
                    "slug" => json_encode([
                        "en" => str_slug_utf8(trim(str_replace("\r\n", "", $car->EnName))),
                        "ar" => str_slug_utf8(trim(str_replace("\r\n", "", $car->EnName))),
                    ]),
                    "order" => 0,
                    "status" => 1,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ]);

            }

            return redirect(route("categories.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
