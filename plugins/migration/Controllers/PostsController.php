<?php

namespace Migration\Controllers;

use Dot\Platform\Controller;
use Dot\Tags\Models\Tag;
use Illuminate\Support\Facades\DB;
use Migration\Models\Post;


class PostsController extends Controller
{

    protected $limit = 20;

    public function index($hreflang, $offset = 0)
    {

        $rows = Post::take($this->limit)->skip($offset)
            ->orderBy("ID", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if category exists

                if (DB::table("posts")->where("id", $row->Id)->count()) {
                    continue;
                }

                $post = [
                    "id" => $row->Id,
                    "title" => $row->Title,
                    "excerpt" => $row->Summary,
                    "content" => $row->Body,
                    "created_at" => $row->PublishDate,
                    "updated_at" => $row->PublishDate,
                    "published_at" => $row->PublishDate,
                    "featured" => $row->IsFeatured ? $row->IsFeatured : 0,
                    "slug" => $row->URLId,
                    "format" => "article",
                    "status" => 1,
                    "lang" => "ar",
                    "image_id" => \get_file_id($row->ImageURL),
                    "country_id" => 0,
                    "city_id" => 0
                ];

                DB::table("posts")->insert($post);

                // Save post categories

                $post_categories = get_post_categories($row->Id);

                foreach ($post_categories as $category_id) {
                    DB::table("posts_categories")->insert([
                        "post_id" => $row->Id,
                        "category_id" => $category_id
                    ]);
                }

                // Saving to tags

                // generating tags from old category name

                $old_category = DB::connection("sqlsrv")->table("ArticleCategories")
                    ->where("ArticleId", $row->Id)
                    ->first();

                if ($old_category) {

                    $old_category_details = DB::connection("sqlsrv")->table("ArticleCategory")
                        ->where("Id", $old_category->ArticleCategoryId)
                        ->where("Id", "!=", 10)
                        ->first();

                    if ($old_category_details) {

                        $tag_ids = Tag::saveNames([str_replace("ال", "", $old_category_details->ArabicCategoryName)]);

                        if ($tag_ids) {

                            foreach ($tag_ids as $tag_id) {

                                DB::table("posts_tags")->insert([
                                    "post_id" => $row->Id,
                                    "tag_id" => $tag_id
                                ]);
                            }
                        }
                    }

                }

                // SEO

                if (DB::table("seo")->where("id", $row->Id)->where("type", "post")->count()) {
                    continue;
                }

                DB::table("seo")->insert([
                    "type" => "post",
                    "id" => $row->Id,
                    "meta_title" => json_encode([
                        "ar" => $row->Summary
                    ]),
                    "meta_keywords" => json_encode([
                        "ar" => $row->Keywords
                    ])
                ]);
            }

            return redirect(route("posts.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
