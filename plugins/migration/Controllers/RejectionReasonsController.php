<?php

namespace Migration\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\DB;
use Migration\Models\RejectionReason;

class RejectionReasonsController extends Controller
{

    protected $limit = 20;

    public function index($hreflang, $offset = 0)
    {

        $rows = RejectionReason::take($this->limit)->skip($offset)
            ->orderBy("Id", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if reason exists

                if (DB::table("rejection_reasons")->where("id", $row->Id)->count()) {
                    continue;
                }

                DB::table("rejection_reasons")->insert([
                    "id" => $row->Id,
                    "title" => json_encode([
                        "ar" => $row->ArabicValue,
                        "en" => $row->Value
                    ]),
                ]);

            }

            return redirect(route("rejection_reasons.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
