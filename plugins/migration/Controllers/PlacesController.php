<?php

namespace Migration\Controllers;

use Dot\Platform\Controller;
use Illuminate\Support\Facades\DB;
use Migration\Models\City;
use Migration\Models\Country;

class PlacesController extends Controller
{

    protected $limit = 100;

    public function index($hreflang, $offset = 0)
    {

        return "Places are already migrated";

        $rows = Country::take($this->limit)->skip($offset)
            ->orderBy("ID", "ASC")->get();

        if (count($rows)) {

            foreach ($rows as $row) {

                // Continue if country exists

                if (DB::table("places")->where("id", $row->id)->count()) {
                    continue;
                }

                DB::table("places")->insert([
                    "id" => $row->id,
                    "parent" => 0,
                    "name" => json_encode([
                        "en" => clean_trim($row->countryName),
                        "ar" => clean_trim($row->arabicname),
                    ]),
                    "currency" => json_encode([
                        "en" => clean_trim($row->EnglishCurrency),
                        "ar" => clean_trim($row->Currency),
                    ]),
                    "order" => $row->Order ? $row->Order : 0,
                    "lat" => clean_trim($row->latitude),
                    "lng" => clean_trim($row->longitude),
                    "image_id" => 0,
                    "status" => 1,
                    "old_id" => $row->id ? $row->id : 0
                ]);

            }

            foreach (DB::table("places")->where("parent", 0)->get() as $place) {

                // Insert country cities

                $cities = City::where("countryId", $place->id)->orderBy("ID", "ASC")->get();

                foreach ($cities as $city) {

                    // Continue if city exists

                    if (DB::table("places")->where("old_id", $city->Id)->count()) {
                        continue;
                    }

                    DB::table("places")->insert([
                        "parent" => $place->id,
                        "name" => json_encode([
                            "en" => clean_trim($city->City),
                            "ar" => clean_trim($city->CityArabicName),
                        ]),
                        "currency" => json_encode([
                            "en" => clean_trim(json_decode($place->currency)->en),
                            "ar" => clean_trim(json_decode($place->currency)->ar)
                        ]),
                        "order" => 0,
                        "lat" => clean_trim($place->lat),
                        "lng" => clean_trim($place->lng),
                        "image_id" => 0,
                        "status" => 1,
                        "old_id" => $city->Id ? $city->Id : 0
                    ]);
                }
            }

            return redirect(route("places.migration", ["hreflang" => $hreflang, "offset" => $offset + $this->limit]));

        } else {
            return "done";
        }
    }
}
