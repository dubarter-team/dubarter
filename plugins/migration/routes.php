<?php

Route::group([
    "prefix" => "{hreflang}/migration",
    "namespace" => "Migration\\Controllers"
], function ($route){
    $route->get("ads/{offset?}", ["uses" => "AdsController@index", "as" => "ads.migration"]);
    $route->get("cars/{offset?}", ["uses" => "CarsController@index", "as" => "cars.migration"]);
    $route->get("users/{offset?}", ["uses" => "UsersController@index", "as" => "users.migration"]);
    $route->get("places/{offset?}", ["uses" => "PlacesController@index", "as" => "places.migration"]);
    $route->get("categories/{offset?}", ["uses" => "CategoriesController@index", "as" => "categories.migration"]);
    $route->get("posts/{offset?}", ["uses" => "PostsController@index", "as" => "posts.migration"]);
    $route->get("offers/{offset?}", ["uses" => "OffersController@index", "as" => "offers.migration"]);
    $route->get("rejection_reasons/{offset?}", ["uses" => "RejectionReasonsController@index", "as" => "rejection_reasons.migration"]);
    $route->get("subscriptions/{offset?}", ["uses" => "SubscriptionsController@index", "as" => "subscriptions.migration"]);
});
