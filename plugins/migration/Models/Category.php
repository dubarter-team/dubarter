<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model{

    protected $connection = "sqlsrv";

    protected $table = "CATEGORY";
}
