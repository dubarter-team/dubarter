<?php

namespace Migration\Models;

class Model extends \Illuminate\Database\Eloquent\Model{

    protected $connection = "sqlsrv";

    protected $table = "MODELS";
}
