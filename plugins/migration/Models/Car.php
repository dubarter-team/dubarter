<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model{

    protected $connection = "sqlsrv";

    protected $table = "CARS";
}
