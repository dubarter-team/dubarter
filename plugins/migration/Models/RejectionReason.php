<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class RejectionReason extends Model
{

    protected $connection = "sqlsrv";

    protected $table = "RejectionReason";
}
