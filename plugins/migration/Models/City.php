<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model{

    protected $connection = "sqlsrv";

    protected $table = "Cities";
}
