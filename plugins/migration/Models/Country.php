<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model{

    protected $connection = "sqlsrv";

    protected $table = "Countries";
}
