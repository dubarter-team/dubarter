<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $connection = "sqlsrv";

    protected $table = "CREATEAD";
}
