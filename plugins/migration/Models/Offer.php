<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    protected $connection = "sqlsrv";

    protected $table = "AdOffer";
}
