<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{

    protected $connection = "sqlsrv";

    protected $table = "Articles";
}
