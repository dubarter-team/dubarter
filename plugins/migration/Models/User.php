<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;


class User extends Model{

    protected $connection = "sqlsrv";

    protected $table = "USER";
}
