<?php

namespace Migration\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

    protected $connection = "sqlsrv";

    protected $table = "NewsletterSubscriptions";
}
