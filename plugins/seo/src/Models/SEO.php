<?php

namespace Dot\Seo\Models;

use Dot\Media\Models\Media;
use Dot\Platform\Model;

class SEO extends Model
{

    public $timestamps = false;
    protected $table = 'seo';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function facebook()
    {
        return $this->hasOne(Media::class, 'id', 'facebook_image');
    }

    public function twitter()
    {
        return $this->hasOne(Media::class, 'id', 'twitter_image');
    }

}
