<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:comments.manage"],
    "namespace" => "Dot\\Comments\\Controllers"
], function ($route) {
    $route->group(["prefix" => "comments"], function ($route) {
        $route->any('/', ["as" => "admin.comments.show", "uses" => "CommentsController@index"]);
        $route->any('/delete', ["as" => "admin.comments.delete", "uses" => "CommentsController@delete"]);
        $route->any('/change_status', ["as" => "admin.comments.change_status", "uses" => "CommentsController@change_status"]);
        $route->any('/{id}/view', ["as" => "admin.comments.view", "uses" => "CommentsController@view"]);
    });
});



