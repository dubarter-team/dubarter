<?php
return [

    'module' => 'Comments',

    'title' => 'Comments',
    'comment' => 'Comment',
    'back_to_comments' => 'Back To comments',
    'no_records' => 'No comments Found',
    'search' => 'search',
    'search_comments' => 'Search comments',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'view' => 'View',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'sure_delete' => 'Are you sure to delete ?',
    'comment_details' => 'Comment Details',
    'replies' => 'Replies',
    'view_comment' => 'View Comment',
    'no_replies' => 'There are no replies',
    'status_not_changed' => 'something went wrong in changing the status',
    'ajax_is_busy' => 'please wait till the other request finishes',
    'article' => 'Article',
    'video' => 'Video',
    'post_type' => 'Post type',
    'post' => 'Post',
    'all' => 'All',
    'to' => 'To',
    'from' => 'From',

    'status' => [
        '1' => 'accepted',
        '2' => 'rejected',
        '3' => 'spam',
    ],

    'attributes' => [

        'article' => 'Article',
        'video' => 'Video',
        'user' => 'User',
        'date' => 'Date',
        'content' => 'Content',
        'status' => 'Status',

    ],

    "events" => [
        'created' => 'Comment created successfully',
        'updated' => 'Comment updated successfully',
        'deleted' => 'Comment deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage comments"
    ]

];
