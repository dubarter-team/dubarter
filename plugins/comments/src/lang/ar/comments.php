<?php

return [

    'module' => 'التعليقات',

    'title' => 'التعليقات',
    'comments' => 'التعليقات',
    'comment' => 'تعليق',
    'back_to_comments' => 'العودة إلى التعليقات',
    'no_records' => 'لا يوجد تعليقات',
    'save_comment' => 'حفظ التعليق',
    'search' => 'البحث',
    'search_comments' => 'البحث فى التعليقات',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'اختر أمر',
    'delete' => 'حذف',
    'apply' => 'حفظ',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب حسب',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'الأمر',
    'filter' => 'عرض',
    'language' => 'اللغة',
    'sure_delete' => 'هل أنت متأكد من الحذف ؟',
    'comment_details' => 'تفاصيل التعليق',
    'replies' => 'الردود',
    'view_comment' => 'مشاهدة التعليق',
    'no_replies' => 'لا يوجد ردود',
    'status_not_changed' => 'حدث خطأ أثناء حفظ الحاله',
    'ajax_is_busy' => 'من فضلك انتظر حتى ينتهى الطلب الحالى',
    'article' => 'مقال',
    'video' => 'فيديو',
    'post_type' => 'نوع الموضوع',
    'post' => 'الموضوع',
    'all' => 'الكل',
    'to' => 'إلى',
    'from' => 'من',

    'status' => [
        '1' => 'مقبول',
        '2' => 'مرفوض',
        '3' => 'مزعج',
    ],

    'attributes' => [
        'article' => 'مقال',
        'video' => 'فيديو',
        'user' => 'المستخدم',
        'date' => 'التاريخ',
        'content' => 'المحتوى',
        'status' => 'الحالة',
    ],
    "events" => [
        'created' => 'تم إنشاء التعليق بنجاح',
        'updated' => 'تم حفظ التعليق بنجاح',
        'deleted' => 'تم الحذف بنجاح',

    ],
    "permissions" => [
        "manage" => "التحكم بالتعليقات"
    ]

];
