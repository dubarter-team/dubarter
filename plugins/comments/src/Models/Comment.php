<?php

namespace Dot\Comments\Models;

use DB;
use Dot\Platform\Model;
use Dot\Posts\Models\Post;
use Dot\Users\Models\User;

/**
 * Class topic
 * @package Dot\Topics\Models
 */
class Comment extends Model
{

    /**
     * @var string
     */
    protected $module = 'comments';

    /**
     * @var string
     */
    protected $table = 'comments';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    /**
     * post relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function post()
    {
        return $this->hasOne(Post::class, "id", "object_id");
    }

    /**
     * user relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * parent relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function parent_comment()
    {
        return $this->hasOne(Comment::class, "id", "parent");
    }

    /**
     * replies relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function replies()
    {
        return $this->hasMany(Comment::class, 'parent')->orderBy('created_at', 'asc');
    }
}
