<?php

namespace Dot\Comments\Controllers;

use Action;
use Dot\Comments\Models\Comment;
use Dot\Platform\Controller;
use Redirect;
use Request;

class CommentsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all comments
     * @return mixed
     */
    function index()
    {
        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? "name->" . app()->getLocale() : 'created_at';
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Comment::orderBy($this->data["sort"], $this->data["order"]);

        if(Request::filled('object_id')){
            $query->where('object_id', Request::get('object_id'));
        }

        if(Request::filled('object_type')){
            $query->where('object_type', Request::get('object_type'));
        }

        if(Request::filled('user_id')){
            $query->where('user_id', Request::get('user_id'));
        }

        if(Request::filled('status')){
            $query->where('status', Request::get('status'));
        }

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }

        $query->with(['user', 'post']);

        $this->data["comments"] = $query->where('parent', 0)->paginate($this->data['per_page']);

        return view("comments::show", $this->data);
    }

    /**
     * Delete comment by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $topic = Comment::findOrFail($id);

            $topic->replies()->delete();

            $topic->delete();
        }

        return Redirect::back()->with("message", trans("comments::comments.events.deleted"));
    }

    /**
     * view comment by id
     * @param $id
     * @return mixed
     */
    public function view($id)
    {

        $comment = Comment::where('id', $id)->with(['user', 'replies', 'post'])->first();

        if (!$comment) {
            abort(404);
        }

        $this->data["comment"] = $comment;

        return view("comments::view", $this->data);
    }

    /**
     * change comment status
     * @return mixed
     */
    public function change_status()
    {
        $res = ['status' => false];
        sleep(5);
        if (Request::get('id') && in_array(Request::get('status'), [1, 2, 3])) {
            $comment = Comment::where('id', Request::get('id'))->first();

            if ($comment) {
                $comment->status = Request::get('status');

                $comment->save();

                $res['status'] = true;
            }
        }

        return response()->json($res);
    }

}
