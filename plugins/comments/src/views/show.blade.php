@extends("admin::layouts.master")

@section("content")

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-folder"></i>
                {{ trans("comments::comments.title") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.comments.show") }}">{{ trans("comments::comments.title") }}
                        ({{ $comments->total() }})</a>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">

        <div id="content-wrapper">

            @include("admin::partials.messages")

            <form action="" method="get" class="filter-form">
                <input type="hidden" name="per_page" value="{{ Request::get('per_page') }}"/>

                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">

                            <select name="status" id="input-status" class="form-control chosen-select chosen-rtl">
                                <option value="">{{ trans("comments::comments.all") }}</option>
                                <option {{ Request::get("status") == "1" ? "selected='selected'" : '' }} value="1">{{ trans("comments::comments.status.1") }}</option>
                                <option {{ Request::get("status") == "2" ? "selected='selected'" : '' }} value="2">{{ trans("comments::comments.status.2") }}</option>
                                <option {{ Request::get("status") == "3" ? "selected='selected'" : '' }} value="3">{{ trans("comments::comments.status.3") }}</option>
                            </select>

                            <select name="object_type" id="input-status" class="form-control chosen-select chosen-rtl">
                                <option value="">{{ trans("comments::comments.all") }}</option>
                                <option {{ Request::get("object_type") == "article" ? "selected='selected'" : '' }} value="article">{{ trans("comments::comments.article") }}</option>
                                <option {{ Request::get("object_type") == "video" ? "selected='selected'" : '' }} value="video">{{ trans("comments::comments.video") }}</option>
                            </select>

                            <button type="submit" class="btn btn-primary">{{ trans("comments::comments.filter") }}</button>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">

                        <div class="form-group">
                            <select name="order" class="form-control chosen-select chosen-rtl">
                                <option value="DESC"
                                        @if($order == "DESC") selected='selected' @endif>{{ trans("comments::comments.desc") }}</option>
                                <option value="ASC"
                                        @if($order == "ASC") selected='selected' @endif>{{ trans("comments::comments.asc") }}</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4">

                        <div class="form-group">
                            <div class="input-group date datetimepick col-sm-6 pull-left" style="margin-top: 0px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="from" type="text" value="{{ @Request::get("from") }}"
                                       class="form-control"
                                       placeholder="{{ trans("comments::comments.from") }}">
                            </div>

                            <div class="input-group date datetimepick col-sm-6 pull-left" style="margin-top: 0px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="to" type="text" value="{{ @Request::get("to") }}"
                                       class="form-control"
                                       placeholder="{{ trans("comments::comments.to") }}">
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            <form action="" method="post" class="action_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <i class="fa fa-folder"></i>
                            {{ trans("comments::comments.title") }}
                        </h5>
                    </div>
                    <div class="ibox-content">
                        @if(count($comments))
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 action-box">

                                    <select name="action" class="form-control pull-left">
                                        <option value="-1"
                                                selected="selected">{{ trans("comments::comments.bulk_actions") }}</option>
                                        <option value="delete">{{ trans("comments::comments.delete") }}</option>
                                    </select>

                                    <button type="submit"
                                            class="btn btn-primary pull-right">{{ trans("comments::comments.apply") }}</button>

                                </div>

                                <div class="col-lg-6 col-md-4 hidden-sm hidden-xs"></div>

                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select class="form-control per_page_filter">
                                        <option value="" selected="selected">
                                            -- {{ trans("comments::comments.per_page") }} --
                                        </option>
                                        @foreach (array(10, 20, 30, 40) as $num)
                                            <option value="{{ $num }}"
                                                    @if ($num == $per_page) selected="selected" @endif>{{ $num }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0"
                                       class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:35px"><input type="checkbox" class="i-checks check_all"
                                                                      name="ids[]"/></th>
                                        <th>{{ trans("comments::comments.post") }}</th>
                                        <th>{{ trans("comments::comments.attributes.user") }}</th>
                                        <th>{{ trans("comments::comments.post_type") }}</th>
                                        <th>{{ trans("comments::comments.attributes.status") }}</th>
                                        <th>{{ trans("comments::comments.attributes.date") }}</th>
                                        <th>{{ trans("comments::comments.actions") }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($comments as $comment)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="i-checks" name="id[]"
                                                       value="{{ $comment->id }}"/>
                                            </td>

                                            <td>
                                                <a href="{{ route("admin.posts.edit", array("id" => $comment->object_id)) }}"><strong>{{ $comment->post->title }}</strong></a>
                                            </td>

                                            <td>
                                                <a href="{{ route("admin.users.edit", array("id" => $comment->user_id)) }}"><strong>{{ $comment->user->username }}</strong></a>
                                            </td>

                                            <td>
                                                <strong>{{ trans("comments::comments." . $comment->object_type) }}</strong>
                                            </td>

                                            <td>
                                                <strong>{{ trans("comments::comments.status." . $comment->status) }}</strong>
                                            </td>

                                            <td>
                                                <strong>{{ $comment->created_at }}</strong>
                                            </td>

                                            <td class="center">
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("comments::comments.view") }}"
                                                   href="{{ route("admin.comments.view", array("id" => $comment->id)) }}">
                                                    <i class="fa fa-eye text-navy"></i>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("comments::comments.delete") }}"
                                                   class="delete_user ask"
                                                   message="{{ trans("comments::comments.sure_delete") }}"
                                                   href="{{ URL::route("admin.comments.delete", array("id" => $comment->id)) }}">
                                                    <i class="fa fa-times text-navy"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    {{ trans("comments::comments.page") }} {{ $comments->currentPage() }} {{ trans("comments::comments.of") }} {{ $comments->lastPage() }}
                                </div>
                                <div class="col-lg-12 text-center">
                                    {{ $comments->appends(Request::all())->render() }}
                                </div>
                            </div>
                        @else
                            {{ trans("comments::comments.no_records") }}
                        @endif
                    </div>
                </div>
            </form>
        </div>

    </div>

@stop

@section("head")

    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"
          type="text/css">

@stop

@section("footer")
    <script type="text/javascript" src="{{ assets('admin::js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $(document).ready(function () {

            $('[data-toggle="tooltip"]').tooltip();

            $('.datetimepick').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });
            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });
            $(".filter-form input[name=per_page]").val($(".per_page_filter").val());
            $(".per_page_filter").change(function () {
                var base = $(this);
                var per_page = base.val();
                $(".filter-form input[name=per_page]").val(per_page);
                $(".filter-form").submit();
            });
        });

    </script>

@stop

