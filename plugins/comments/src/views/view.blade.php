@extends("admin::layouts.master")

@section("content")

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-folder"></i>
                {{ trans("comments::comments.view_comment") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.comments.show") }}">{{ trans("comments::comments.title") }}</a>
                </li>
                <li class="active">
                    <strong>
                        {{ trans("comments::comments.view_comment") }}
                    </strong>
                </li>
            </ol>
        </div>

        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

            <a href="{{ route("admin.comments.delete", ['id' => $comment->id, 'redirect' => 'home']) }}"
               class="btn btn-flat btn-danger btn-main">{{ trans("comments::comments.delete") }}</a>

        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-th-large"></i>
                        {{ trans("comments::comments.comment_details") }}
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label for="input-color">{{ trans("comments::comments.attributes." . $comment->object_type) }}</label>
                            <div>
                                <a href="{{ route("admin.posts.edit", array("id" => $comment->object_id)) }}">{{ $comment->post->title }}</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("comments::comments.attributes.user") }}</label>
                            <div>
                                <a href="{{ route("admin.users.edit", array("id" => $comment->user_id)) }}">{{ $comment->user->username }}
                                    ( {{ $comment->user->email }} )</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("comments::comments.attributes.status") }}</label>

                            <div class="input-group status-changer col-md-6" style="margin-bottom: 10px;">
                                <select class="form-control status-dropdown">
                                    <option {{ $comment->status == 1 ? 'selected' : '' }} value="1">{{ trans("comments::comments.status.1") }}</option>
                                    <option {{ $comment->status == 2 ? 'selected' : '' }} value="2">{{ trans("comments::comments.status.2") }}</option>
                                    <option {{ $comment->status == 3 ? 'selected' : '' }} value="3">{{ trans("comments::comments.status.3") }}</option>
                                </select>

                                <span class="input-group-btn">
                                    <button class="btn btn-secondary btn-success btn-add apply-status"
                                            data-id="{{ $comment->id }}" type="button">
                                        {{ trans("comments::comments.apply") }}
                                        <i class="fa" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("comments::comments.attributes.date") }}</label>
                            <div>{{ $comment->created_at }}</div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("comments::comments.attributes.content") }}</label>
                            <div>{{ $comment->content }}</div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-file-text-o"></i>
                        {{ trans("comments::comments.replies") }}
                    </div>
                    <div class="panel-body" style="max-height: 400px;overflow-y: scroll;">
                        @if($comment->replies->count())

                            @foreach($comment->replies as $reply)

                                <div class="panel panel-default">

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="input-color"><a
                                                        href="{{ route("admin.users.edit", array("id" => $reply->user_id)) }}">{{ $reply->user->username }}
                                                    ( {{ $reply->user->email }} )</a></label>
                                        </div>

                                        <div class="form-group">
                                            <label for="input-color">{{ trans("comments::comments.attributes.status") }}</label>

                                            <div class="input-group status-changer col-md-6"
                                                 style="margin-bottom: 10px;">
                                                <select class="form-control status-dropdown">
                                                    <option {{ $reply->status == 1 ? 'selected' : '' }} value="1">{{ trans("comments::comments.status.1") }}</option>
                                                    <option {{ $reply->status == 2 ? 'selected' : '' }} value="2">{{ trans("comments::comments.status.2") }}</option>
                                                    <option {{ $reply->status == 3 ? 'selected' : '' }} value="3">{{ trans("comments::comments.status.3") }}</option>
                                                </select>

                                                <span class="input-group-btn">
                                                    <button class="btn btn-secondary btn-success btn-add apply-status"
                                                            data-id="{{ $reply->id }}" type="button">
                                                        {{ trans("comments::comments.apply") }}
                                                        <i class="fa" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div>{{ $reply->created_at }}</div>
                                        </div>

                                        <div class="form-group">
                                            <div>{{ $reply->content }}</div>
                                        </div>

                                    </div>
                                </div>

                            @endforeach

                        @else

                            <div class="form-group">{{ trans("comments::comments.no_replies") }}</div>

                        @endif
                    </div>
                </div>

            </div>

        </div>

    </div>

@stop

@push("footer")

    <script>

        $(document).ready(function () {
            var flag = true;

            $(document).on('click', '.apply-status', function (e) {
                e.preventDefault();

                var ths = $(this);

                var status = ths.parents('.status-changer').find('.status-dropdown').val();

                var comment_id = ths.attr('data-id');

                if (flag) {
                    if (status > 0 && comment_id != undefined) {
                        flag = false;

                        ths.find('i').addClass('fa-refresh').addClass('fa-spin');

                        $.ajax({
                            url: '{{ route('admin.comments.change_status') }}',
                            data: {id: comment_id, status: status},
                            type: 'get',
                            dataType: 'json',
                            timeout: 10000,
                            success: function (data) {
                                if (data.status != true) {
                                    alert_box('{{ trans("comments::comments.status_not_changed") }}');
                                }
                            },
                            error: function (x, y, z) {
                                alert_box('{{ trans("comments::comments.status_not_changed") }}');
                            },
                            complete: function (jqXHR, textStatus) {
                                flag = true;

                                ths.find('i').removeClass('fa-refresh').removeClass('fa-spin');
                            }
                        });
                    }
                } else {
                    alert_box('{{ trans("comments::comments.ajax_is_busy") }}');
                }

            });

        });

    </script>

@endpush