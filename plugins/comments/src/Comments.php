<?php

namespace Dot\Comments;

use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class Comments extends \Dot\Platform\Plugin
{

    protected $permissions = [
        "manage"
    ];

    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("comments.manage")) {
                $menu->item('comments', trans("comments::comments.title"), route("admin.comments.show"))->icon("fa-comments-o")->order(1);
            }

        });
    }
}
