<?php

namespace Dot\Branches\Models;

use DB;
use Dot\Ads\Models\Ad;
use Dot\Platform\Model;
use Dot\Users\Models\User;

/**
 * Generated  by plugin factory
 * Class Branch
 * @package Dot\Branches\Models
 */
class Branch extends Model
{

    /**
     *  Modules name
     * @var string
     */
    protected $module = 'branches';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branches';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $perPage = 20;

    protected $searchable = ['title'];
    /**
     * Validation rules for create
     *
     * @var array
     */
    protected $creatingRules = [
        'title' => 'required',
        'address' => 'required',
        'phone' => 'required|numeric'
    ];

    /**
     * updates rules for create
     *
     * @var array
     */
    protected $updatingRules = [
        'title' => 'required',
        'address' => 'required',
        'phone' => 'required|numeric'
    ];

    /**
     * ad relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function ad()
    {
        return $this->hasOne(Ad::class, "id", "ad_id");
    }


    /**
     * user relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }


}
