<?php

return [

    'branches' => 'الفروع',

    'title' => 'الفروع',
    'branch' => 'الفروع',
    'branch' => 'فرع',
    'no_records' => 'لا يوجد فروع',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'اختر أمر',
    'delete' => 'حذف',
    'apply' => 'حفظ',
    'page' => 'الصفحة',
    'search' => 'بحث',
    'user' => 'مستخدم',
    'all' => 'الكل',
    'of' => 'من',
    'activate' => 'تفعيل',
    'activated' => 'مفعل',
    'add_new_for' => 'اضاف فرع  للى ',
    'add_new' => 'اضاف فرع',
    'edit' => 'تعديل الفرع',
    'branch_status' => 'حاله الفرع',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب حسب',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'الأمر',
    'filter' => 'فرع',
    'language' => 'اللغة',
    'deactivate' => 'غير مفعل',
    'deactivated' => 'غير مفعل',
    'sure_activate' => "هل تريد تفعيل الخبر؟",
    'sure_deactivate' => "هل تريد إلغاء تفعيل الخبر",
    'sure_delete' => 'هل تريد حذف الخبر؟',
    'branch_details' => 'تفاصيل الفرع',
    'replies' => 'الردود',
    'view_branch' => 'مشاهدة الفرع',
    'no_replies' => 'لا يوجد ردود',
    'from' => 'من',
    'to' => 'إلى',
    'save_branch'=>'حفظ',
    'status' => [
        '1' => 'مفعل',
        '2' => 'بإنتظار المراجعه',
        '3' => 'مرفوض',
        '4' => 'مقبول',
    ],

    'attributes' => [
        'title'=>'اسم الفرع',
        'ad' => 'الإعلان',
        'address'=>'عنوان',
        'phone' => 'رقم الهاتف',
        'form' => 'من',
        'to' => 'الى',
        'available'=>'متاح',
        'sender' => 'المرسل',
        'receiver' => 'المستقبل',
        'type' => 'النوع',
        'date' => 'التاريخ',
        'comment' => 'التعليق',
        'price' => 'السعر',
        'status' => 'الحاله',
        'replies_count' => 'عدد الردود',
        'bid' => 'شراء',
        'bart' => 'مقايضه',
        'created_at' => 'تاريخ الإضافة',
        'updated_at' => 'تاريخ التعديل',

    ],
    "events" => [
        'created' => 'تم إنشاء الفرع بنجاح',
        'updated' => 'تم حفظ الفرع بنجاح',
        'deleted' => 'تم الحذف بنجاح',

    ],
    "permissions" => [
        "manage" => "التحكم بالفروع"
    ]

];
