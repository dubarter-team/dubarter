<?php
return [

    'branches' => 'Branches',

    'title' => 'Branches',
    'Branch' => 'Branch',
    'no_records' => 'No branches Found',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'add_new_for' => 'Add new branch for',
    'add_new' => 'Add new',
    'search' => 'search',
    'user'=>'User',
    'all' => 'all',

    'deactivate' => 'Deactivate',
    'deactivated' => 'Deactivated',
    'sure_activate' => "Are you sure to activate ?",
    'sure_deactivate' => "Are you sure to deactivate ?",
    'order' => 'Order',
    'edit' => 'Edit Branch',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'activate' => 'activate',
    'activated' => 'activated',
    'filter' => 'Filter',
    'branch_status' => 'Branch status',
    'language' => 'Language',
    'sure_delete' => 'Are you sure to delete ?',
    'Branch_details' => 'Branch Details',
    'replies' => 'Replies',
    'view_Branch' => 'View Branch',
    'no_replies' => 'There are no replies',
    'from' => 'From',
    'to' => 'To',

    'status' => [
        '1' => 'active',
        '2' => 'pending',
        '3' => 'rejected',
        '4' => 'accepted',
    ],
    'save_branch'=>'Save Branch',
    'attributes' => [
        'title'=>'Title',
        'address'=>'Address',
        'phone'=>'phone',
        'ad' => 'Ad',
        'user' => 'User',
        'sender' => 'Sender',
        'receiver' => 'Receiver',
        'type' => 'Type',
        'form' => 'Form',
        'to' => 'To',
        'available'=>'available',
        'status' => 'Status',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',

    ],

    "events" => [
        'created' => 'Branch created successfully',
        'updated' => 'Branch updated successfully',
        'deleted' => 'Branch deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage Branch"
    ]

];
