<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:branches.manage"],
    "namespace" => "Dot\\Branches\\Controllers"
], function ($route) {
    $route->group(["prefix" => "branches"], function ($route) {
        $route->any('/create/{ad_id}', ["as" => "admin.branches.create", "uses" => "BranchesController@create"]);
        $route->any('/delete', ["as" => "admin.branches.delete", "uses" => "BranchesController@delete"]);
        $route->any('/{ad_id}', ["as" => "admin.branches.show", "uses" => "BranchesController@index"]);

        $route->any('/{id}/edit', ["as" => "admin.branches.edit", "uses" => "BranchesController@edit"]);
        $route->any('/{status}/status', ["as" => "admin.branches.status", "uses" => "BranchesController@status"]);
    });
});



