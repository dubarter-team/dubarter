@extends("admin::layouts.master")

@section("content")

    <form action="" method="post">

        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-map-marker"></i>
                    {{ $branch->id ? trans("branches::branches.edit") :  trans("branches::branches.add_new_for")."($ad->title)"  }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.branches.show",['ad_id'=>$ad->id]) }}">{{ trans("branches::branches.branches") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $branch->id ? trans("branches::branches.edit") : trans("branches::branches.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>

            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($branch->id)
                    <a href="{{ route("admin.branches.create",['ad_id'=>$ad->id]) }}"
                       class="btn btn-primary btn-labeled btn-main"> <span
                                class="btn-label icon fa fa-plus"></span>
                        {{ trans("branches::branches.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("branches::branches.save_branch") }}
                </button>
            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

            <div class="row">

                <div class="col-md-8">

                    <div class="panel panel-default">

                        <div class="panel-body">

                            <div class="form-group">
                                <label for="branch_title">{{ trans("branches::branches.attributes.title") }}</label>
                                <input type="text" value="{{@Request::old("title", $branch->title)}}"
                                       class="form-control" id="branch_title"
                                       name="title"
                                       placeholder="{{ trans("branches::branches.attributes.title") }}">
                            </div>

                            <div class="form-group">
                                <label for="branch_address">{{ trans("branches::branches.attributes.address") }}</label>
                                <input type="text" value="{{@Request::old("address", $branch->address)}}"
                                       class="form-control" id="branch_address"
                                       name="address"
                                       placeholder="{{ trans("branches::branches.attributes.address") }}">
                            </div>

                            <div class="form-group">
                                <label for="branch_phone">{{ trans("branches::branches.attributes.phone") }}</label>
                                <input type="text" value="{{@Request::old("phone", $branch->phone)}}"
                                       class="form-control" id="branch_phone"
                                       name="phone"
                                       placeholder="{{ trans("branches::branches.attributes.phone") }}">
                            </div>

                            <div class="form-group">
                                <label>{{ trans("branches::branches.attributes.available") }}</label>

                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="available_from" class="form-control "
                                               id="branch_available_from"
                                               placeholder="{{ trans("branches::branches.attributes.form") }}"
                                               value="{{@Request::old("available_from", $branch->available_from) }}">
                                    </div>

                                    <div class="col-md-6">
                                        <input type="text" name="available_to" class="form-control"
                                               id="branch_available_to"
                                               placeholder="{{ trans("branches::branches.attributes.to") }}"
                                               value="{{@Request::old("available_to", $branch->available_to) }}">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div id="map" style="min-height: 500px;"></div>
                            </div>

                            <input type="hidden" name="lat" id="lat-branches" value="{{$branch->lat}}"/>
                            <input type="hidden" name="lng" id="lng-branches" value="{{$branch->lng}}"/>

                        </div>
                    </div>
                </div>

                @foreach(Action::fire("branch.form.featured", $branch) as $output)
                    {!!  $output !!}
                @endforeach

                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-check-square"></i>
                            {{ trans("branches::branches.branch_status") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-status">{{ trans("branches::branches.attributes.status") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("status", $branch->status)) checked="checked" @endif
                                    type="checkbox" id="input-status" name="status" value="1"
                                           class="status-switcher switcher-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-map-marker"></i>
                            {{ trans("ads::ads.place") }}
                        </div>
                        <div class="panel-body">

                            @include('i18n::places.places_dropdown', ['countries' => $countries,
                            'cities' =>$cities,
                            'regions' =>$regions,
                            'country_id' => $branch->id?$branch->country_id:$ad->country_id,
                            'city_id' => $branch->id?$branch->city_id:$ad->city_id,
                            'region_id' => $branch->id?$branch->region_id:$ad->region_id,
                            'all' => false,
                            'only_selects' => false
                            ])

                        </div>
                    </div>
                    @foreach(Action::fire("branch.form.sidebar") as $output)
                        {!! $output !!}
                    @endforeach

                </div>

            </div>

        </div>

    </form>

@stop


@push("head")

    <link href="{{ assets("admin::tagit") }}/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{{ assets("admin::tagit") }}/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

    <link href="{{ assets('admin::css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet" type="text/css">


    <style>
        .custom-field-name {
            width: 40%;
            margin: 5px;
        }

        .custom-field-value {
            width: 50%;
            margin: 5px;
        }

        .remove-custom-field {
            margin: 10px;
        }

        .meta-rows {

        }

        .meta-row {
            background: #f1f1f1;
            overflow: hidden;
            margin-top: 4px;
        }

        .margin label {
            padding-top: 5px;
        }

    </style>

@endpush

@push("footer")

    <script type="text/javascript" src="{{ assets("admin::tagit") }}/tag-it.js"></script>
    <script type="text/javascript" src="{{ assets('admin::js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ assets('admin::js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $(document).ready(function () {

            $('.datetimepick').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
            });


            $("[name=format]").on('ifChecked', function () {
                $(this).iCheck('check');
                $(this).change();
                switch_format($(this));
            });

            switch_format($("[name=format]:checked"));

            function switch_format(radio) {

                var format = radio.val();

                $(".format-area").hide();
                $("." + format + "-format-area").show();
            }


            var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html, {size: 'small'});
            });

            var elems = Array.prototype.slice.call(document.querySelectorAll('.featured-switcher'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html, {size: 'small'});
            });

            $("body").on("click", ".remove-custom-field", function () {

                var item = $(this);
                confirm_box("{{ trans("branches::branches.sure_delete_field") }}", function () {
                    item.parents(".meta-row").remove();
                });

            });

            $(".add-custom-field").click(function () {

                var html = ' <div class="meta-row">'
                    + '<input type="text" name="custom_names[]"'
                    + 'class="form-control input-md pull-left custom-field-name"'
                    + ' placeholder="{{ trans("branches::branches.custom_name") }}"/>'
                    + '   <textarea name="custom_values[]" class="form-control input-lg pull-left custom-field-value"'
                    + '   rows="1"'
                    + '   placeholder="{{ trans("branches::branches.custom_value") }}"></textarea>'
                    + '   <a class="remove-custom-field pull-right" href="javascript:void(0)">'
                    + '   <i class="fa fa-times text-navy"></i>'
                    + '   </a>'
                    + '   </div>';

                $(".meta-rows").append(html);


            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.tree-views input[type=checkbox]').on('ifChecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('check');
                checkbox.change();
            });

            $('.tree-views input[type=checkbox]').on('ifUnchecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('uncheck');
                checkbox.change();
            });

            $(".expand").each(function (index, element) {
                var base = $(this);
                if (base.parents("li").find("ul").first().length > 0) {
                    base.text("+");
                } else {
                    base.text("-");
                }
            });
            $("body").on("click", ".expand", function () {
                var base = $(this);
                if (base.text() == "+") {
                    if (base.closest("li").find("ul").length > 0) {
                        base.closest("li").find("ul").first().slideDown("fast");
                        base.text("-");
                    }
                    base.closest("li").find(".expand").last().text("-");
                } else {
                    if (base.closest("li").find("ul").length > 0) {
                        base.closest("li").find("ul").first().slideUp("fast");
                        base.text("+");
                    }
                }
                return false;
            });
        });


    </script>

@endpush

@push("footer")
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCw-GKQL--li9_CxcdeBrQelj5HXulk38&callback=initMap">
    </script>

    <script>

        var map;
        var marker;

        function initMap() {
            var myLatlng = {lat: 30.0594699, lng: 31.1884238};

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: myLatlng
            });

            map.addListener('click', function (e) {
                add_marker(e, map);
            });

                    @if($branch->id && !empty($branch->lat) && !empty($branch->lng))

            var myLatLng = {
                    lat: {{ ($branch->id?$branch->lat:$ad->lat) }},
                    lng: {{ ($branch->id?$branch->lng:$ad->lng)  }} };

            marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });

            @endif
        }


        function add_marker(e, map) {
            if (marker != undefined) {
                marker.setMap(null);
            }

            marker = new google.maps.Marker({
                position: e.latLng,
                map: map
            });

            document.getElementById('lat-branches').value = e.latLng.lat();
            document.getElementById('lng-branches').value = e.latLng.lng();

            marker.setMap(map);
        }
    </script>


@endpush

