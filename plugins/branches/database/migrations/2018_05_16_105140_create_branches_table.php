<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("branches")) {
            Schema::create('branches', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('ad_id');

                $table->string('title')->default(0);
                $table->string('address', 100)->nullable();
                $table->string('phone', 13)->nullable();


                $table->string('lat', 100)->nullable();
                $table->string('lng', 100)->nullable();

                // City
                $table->integer('city_id')->nullable();
                $table->integer('country_id')->nullable();
                $table->integer('region_id')->nullable();

                $table->string('available_from');
                $table->string('available_to');

                $table->tinyInteger('status')->default(0);
                $table->integer('user_id');
                $table->nullableTimestamps();

                // Add index
                $table->index('ad_id', 'ad_id');
                $table->index('lat', 'lat');
                $table->index('lng', 'lng');
                $table->index('phone', 'phone');
                $table->index('address', 'address');
                $table->index('created_at', 'created_at');
                $table->index('updated_at', 'updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branches');
    }
}
