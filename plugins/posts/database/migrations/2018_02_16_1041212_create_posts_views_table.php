<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 *
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreatePostsViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("posts_views")) {
            Schema::create('posts_views', function (Blueprint $table) {
                $table->integer('post_id');
                $table->string('user_ip');
                $table->string('agent');

                $table->index('post_id', 'post_id');
                $table->index('user_ip', 'user_ip');
                $table->index('agent', 'agent');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts_views');
    }
}
