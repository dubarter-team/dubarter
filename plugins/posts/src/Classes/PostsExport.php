<?php
/**
 * Created by PhpStorm.
 * User: Ibrahim
 */

namespace Dot\Posts\Classes;

use Dot\Posts\Models\Post;
use Dot\Categories\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Request;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PostsExport implements FromCollection, WithHeadings
{
    //data to export
    public function headings(): array
    {
        return ['id', 'title', 'slug', 'user_id', 'status', 'published_at'];
    }

    public function collection()
    {

        $sort = (Request::filled("sort")) ? Request::get("sort") : 'created_at';
        $order = (Request::filled("order")) ? Request::get("order") : "DESC";

        $query = Post::with('image', 'user', 'tags')->orderBy($sort, $order);

        if (Request::filled("tag_id")) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        if (Request::filled("category_id") and Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });
        }

        if (Request::filled("block_id") and Request::get("block_id") != 0) {
            $query->whereHas("blocks", function ($query) {
                $query->where("blocks.id", Request::get("block_id"));
            });
        }

        if (Request::filled("format")) {
            $query->where("format", Request::get("format"));
        }

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }

        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("status")) {
            $query->where("status", Request::get("status"));
        }

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }


        $query->select(['id', 'title', 'slug', 'user_id', 'status', 'published_at']);

        $ads = $query->get();

        return $ads;
    }
}
