<?php

return [

    'welcome' => 'Welcome',
    "ads" => "Ads",
    'articles' => 'Articles',
    'tags' => 'Tags',
    'categories' => 'Categories',
    'users' => 'Users',
    'videos' => 'Videos',
];
