<?php
return [

    'module' => 'Posts',

    'posts' => 'Posts',
    'videos' => 'Videos',
    'post' => 'post',
    'add_new' => 'Add New Post',
    'edit' => 'Edit post',
    'back_to_posts' => 'Back To Posts',
    'no_records' => 'No Posts Found',
    'save_post' => 'Save Post',
    'search' => 'search',
    'search_posts' => 'Search Posts',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',

    'all_posts' => 'All',

    'from' => "From",
    'to' => "To",

    "country" => "country",
    "all_countries" => "All Countries",
    'post_status' => 'Post status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate post ?",
    'sure_deactivate' => "Are you sure to deactivate post ?",
    'sure_delete' => 'Are you sure to delete ?',

    'add_image' => 'Add image',
    'change_image' => 'change image',

    'add_thumbnail' => 'Add thumbnail',
    'change_thumbnail' => 'Change thumbnail',

    'change_media' => 'change media',

    'add_media' => "Add media",

    'not_image_file' => 'Please select a valid image file',
    'not_media_file' => 'Please select a valid video file',

    'user' => 'User',
    'tags' => 'Tags',
    'add_tag' => 'Add tags',
    'templates' => 'Templates',

    'all_categories' => "All Categories",
    'all_blocks' => "All Blocks",
    'all_formats' => "All Formats",
    'add_category' => "Add To Category",

    "format_post" => "Post",
    "format_article" => "Article",
    "format_video" => "Video",
    "format_album" => "Album",
    "format_event" => "Event",

    "add_fields" => "Add custom fields",
    "custom_name" => "Name",
    "custom_value" => "Value",
    "sure_delete_field" => "Are you sure to delete custom field ?",

    "add_block" => "Add To Blocks",
    "no_blocks" => "No Blocks Found",

    "add_gallery" => "Add gallery",
    "no_galleries_found" => "No Galleries Found",
    'comments' => 'Comments',
    'live_stream' => 'Livestream',
    'save_live' => 'Save',
    'live_stream_url' => 'Livestream url',

    'attributes' => [
        'title' => 'Title',
        'excerpt' => 'Excerpt',
        'content' => 'Content',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',
        'published_at' => 'Event date',
        'status' => 'Status',
        'featured' => 'Featured',
        'template' => 'Template',
        'default' => 'Default',
        'live_status' => 'Status',
        'live_stream_url' => 'Url',
        'live_stream_title' => 'Title',
        'livestream_message' => 'Message',
        "format" => "Post format"
    ],

    "events" => [
        'created' => 'Post created successfully',
        'updated' => 'Post updated successfully',
        'deleted' => 'Post deleted successfully',
        'activated' => 'Post activated successfully',
        'deactivated' => 'Post deactivated successfully',
        'livestream_saved' => 'livestream saved'
    ],

    "permissions" => [
        "manage" => "Manage posts"
    ]

];
