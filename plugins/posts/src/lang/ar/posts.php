<?php
return [

    'posts' => 'المحتوي',
    'post' => 'محتوي',
    'add_new' => 'أضف محتوي جديد',
    'edit' => 'تعديل المحتوي',
    'back_to_posts' => 'العودة للأخبار',
    'no_records' => 'لا يوجد محتوي',
    'save_post' => 'حفظ',
    'search' => 'بحث',
    'search_posts' => 'بحث عن خبر',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'اختر أمر',
    'delete' => 'حذف',
    'apply' => 'حفظ',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب بــ',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'تعديل',
    'filter' => 'عرض',
    'post_status' => 'حالة الخبر',
    'activate' => 'تفعيل',
    'activated' => 'مفعل',
    'all' => 'الكل',
    'deactivate' => 'غير مفعل',
    'deactivated' => 'غير مفعل',
    'sure_activate' => "هل تريد تفعيل الخبر؟",
    'sure_deactivate' => "هل تريد إلغاء تفعيل الخبر",
    'sure_delete' => 'هل تريد حذف الخبر؟',
    'add_category' => "أضف إلي قسم",

    'add_image' => 'أضف صورة',
    'change_image' => 'تغيير الصورة',

    'add_thumbnail' => 'أضف صورة رمزية',
    'change_thumbnail' => 'تغيير الصورة الرمزية',

    'change_media' => 'تغيير ',
    'add_media' => 'أضف فيديو',

    'all_categories' => "كل التصنيفات",
    'all_formats' => "كل أنواع الأخبار",
    'all_blocks' => "كل أماكن الأخبار",
    'not_image_file' => 'ملف غير مسموح به',
    'not_media_file' => 'ملف غير مسموح به',

    'all_posts' => 'الكل',

    'from' => "من",
    'to' => "إلي",

    'user' => 'الكاتب',
    'tags' => 'الوسوم',
    'add_tag' => 'أضف وسوم',
    'templates' => 'القوالب',

    "format_post" => "خبر",
    "format_article" => "مقال",
    "format_video" => "فيديو",
    "format_album" => "ألبوم",
    "format_event" => "حدث",

    "add_fields" => "أضف بيانات أخري",
    "custom_name" => "الإسم",
    "custom_value" => "القيمة",
    "sure_delete_field" => "هل أنت متأكد من الحذف ؟",

    "add_block" => "أضف إلي أماكن الأخبار",
    "no_blocks" => "لا توجد أماكن للأخبار",

    "add_gallery" => "أضف ألبوم",
    "no_galleries_found" => "لا توجد ألبومات",
    'comments' => 'التعليقات',
    'live_stream' => 'بث مباشر',
    'save_live' => 'حفظ',
    "country" => "الدولة",
    "all_countries" => "كل الدول",
    'live_stream_url' => 'رابط البث المباشر',

    'attributes' => [
        'title' => 'العنوان',
        'excerpt' => 'المقتطف',
        'content' => 'المحتوى',
        'created_at' => 'تاريخ الإضافة',
        'updated_at' => 'تاريخ التعديل',
        'published_at' => 'تاريخ الحدث',
        'status' => 'الحالة',
        'featured' => 'مميز',
        'template' => 'القالب',
        'default' => 'إفتراضى',
        'live_status' => 'الحاله',
        'live_stream_url' => 'الرابط',
        'live_stream_title' => 'العنوان',
        'livestream_message' => 'الرساله',
        "format" => "نوع المحتوي"
    ],

    "events" => [
        'created' => 'تمت الإضافة بنجاح',
        'updated' => 'تم التحديث بنجاح',
        'deleted' => 'تم الحذف بنجاح',
        'activated' => 'تم التفعيل بنجاح',
        'deactivated' => 'تم إلغاء التفعيل بنجاح',
        'livestream_saved' => 'تم الحفظ'
    ],

    "permissions" => [
        "manage" => "التحكم المحتوي"
    ]

];
