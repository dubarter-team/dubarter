<?php

return [

    'welcome' => 'مرحبا',
    "ads" => "الإعلانات",
    'articles' => 'المقالات',
    'tags' => 'الوسوم',
    'categories' => 'التصنيفات',
    'users' => 'الأعضاء',
    'videos' => 'الفيديوهات',

];
