@extends("admin::layouts.master")

@section("content")

    <form action="" method="post">

        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-newspaper-o"></i>
                    {{ trans("posts::posts.live_stream") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.posts.show") }}">{{ trans("posts::posts.posts") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ trans("posts::posts.live_stream") }}
                        </strong>
                    </li>
                </ol>
            </div>

            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("posts::posts.save_live") }}
                </button>

            </div>

        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

            <div class="row">

                <div class="col-md-8">

                    <div class="panel panel-default">

                        <div class="panel-body">

                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-status">{{ trans("posts::posts.attributes.live_status") }}</label>
                                <div class="col-sm-3">
                                    <input type="checkbox"
                                           @if (@\Dot\Options\Facades\Option::get('livestream_status')) checked="checked"
                                           @endif id="input-status" name="status" value="1"
                                           class="status-switcher switcher-sm">
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <input type="text" name="url"
                                       value="{{ @\Dot\Options\Facades\Option::get('livestream_url') }}"
                                       class="form-control input-md pull-left custom-field-name"
                                       placeholder="{{ trans("posts::posts.attributes.live_stream_url") }}"/>
                            </div>

                            <div class="form-group clearfix">
                                <input type="text" name="title"
                                       value="{{ @\Dot\Options\Facades\Option::get('livestream_title') }}"
                                       class="form-control input-md pull-left custom-field-name"
                                       placeholder="{{ trans("posts::posts.attributes.live_stream_title") }}"/>
                            </div>

                            <div class="form-group clearfix">
                                <textarea name="message" class="form-control input-lg" rows="1" id="post_title"
                                          placeholder="{{ trans("posts::posts.attributes.livestream_message") }}">{{ @\Dot\Options\Facades\Option::get('livestream_message') }}</textarea>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </form>

@stop

@push("footer")

    <script>

        $(document).ready(function () {


            $("[name=format]").on('ifChecked', function () {
                $(this).iCheck('check');
                $(this).change();
                switch_format($(this));
            });

            switch_format($("[name=format]:checked"));

            function switch_format(radio) {

                var format = radio.val();

                $(".format-area").hide();
                $("." + format + "-format-area").show();
            }


            var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html, {size: 'small'});
            });

        });


    </script>

@endpush
