<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:paid_ads.manage"],
    "namespace" => "Dot\\PaidAds\\Controllers"
], function ($route) {
    $route->group(["prefix" => "pa_blocks"], function ($route) {
        $route->any('/', ["as" => "admin.pa_blocks.show", "uses" => "PaBlocksController@index"]);
        $route->any('/create', ["as" => "admin.pa_blocks.create", "uses" => "PaBlocksController@create"]);
        $route->any('/delete', ["as" => "admin.pa_blocks.delete", "uses" => "PaBlocksController@delete"]);
        $route->any('/{id}/edit', ["as" => "admin.pa_blocks.edit", "uses" => "PaBlocksController@edit"]);
        $route->any('/{id}/add_paid_ads', ["as" => "admin.pa_blocks.add_paid_ads", "uses" => "PaBlocksController@add_paid_adds"]);
    });
});

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:paid_ads.manage"],
    "namespace" => "Dot\\PaidAds\\Controllers"
], function ($route) {
    $route->group(["prefix" => "paid_ads"], function ($route) {
        $route->any('/', ["as" => "admin.paid_ads.show", "uses" => "PaidAdsController@index"]);
        $route->any('/create', ["as" => "admin.paid_ads.create", "uses" => "PaidAdsController@create"]);
        $route->any('/delete', ["as" => "admin.paid_ads.delete", "uses" => "PaidAdsController@delete"]);
        $route->any('/{id}/edit', ["as" => "admin.paid_ads.edit", "uses" => "PaidAdsController@edit"]);
    });
});


