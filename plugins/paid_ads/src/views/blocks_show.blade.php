@extends("admin::layouts.master")

@section("content")

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-folder"></i>
                {{ trans("paid_ads::pa_blocks.title") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.pa_blocks.show") }}">{{ trans("paid_ads::pa_blocks.title") }} ({{ $pa_blocks->total() }})</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">
            <a href="{{ route("admin.pa_blocks.create") }}" class="btn btn-primary btn-labeled btn-main">
                <span class="btn-label icon fa fa-plus"></span>
                {{ trans("paid_ads::pa_blocks.add_new") }}</a>
        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">

        <div id="content-wrapper">

            @include("admin::partials.messages")

            <form action="" method="get" class="filter-form">
                <input type="hidden" name="per_page" value="{{ Request::get('per_page') }}"/>

                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <select name="sort" class="form-control chosen-select chosen-rtl">
                                <option value="name" @if($sort == "name") selected='selected' @endif>{{ trans("categories::categories.attributes.name") }}</option>
                            </select>

                            <select name="order" class="form-control chosen-select chosen-rtl">
                                <option value="DESC" @if($order == "DESC") selected='selected' @endif>{{ trans("paid_ads::pa_blocks.desc") }}</option>
                                <option value="ASC" @if($order == "ASC") selected='selected' @endif>{{ trans("paid_ads::pa_blocks.asc") }}</option>
                            </select>

                            <button type="submit" class="btn btn-primary">{{ trans("paid_ads::pa_blocks.order") }}</button>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <form action="" method="get" class="search_form">
                            <div class="input-group">
                                <input name="q" value="{{ Request::get("q") }}" type="text" class=" form-control"
                                       placeholder="{{ trans("paid_ads::pa_blocks.search_pa_blocks") }} ...">
                                <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"> <i class="fa fa-search"></i> </button>
                        </span>
                            </div>
                        </form>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="action_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <i class="fa fa-folder"></i>
                            {{ trans("paid_ads::pa_blocks.title") }}
                        </h5>
                    </div>
                    <div class="ibox-content">
                        @if(count($pa_blocks))
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 action-box">

                                    <select name="action" class="form-control pull-left">
                                        <option value="-1" selected="selected">{{ trans("paid_ads::pa_blocks.bulk_actions") }}</option>
                                        <option value="delete">{{ trans("paid_ads::pa_blocks.delete") }}</option>
                                    </select>

                                    <button type="submit" class="btn btn-primary pull-right">{{ trans("paid_ads::pa_blocks.apply") }}</button>

                                </div>

                                <div class="col-lg-6 col-md-4 hidden-sm hidden-xs"></div>

                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select class="form-control per_page_filter">
                                        <option value="" selected="selected">-- {{ trans("paid_ads::pa_blocks.per_page") }} --</option>
                                        @foreach (array(10, 20, 30, 40) as $num)
                                            <option value="{{ $num }}"
                                                    @if ($num == $per_page) selected="selected" @endif>{{ $num }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0"
                                       class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:35px"><input type="checkbox" class="i-checks check_all" name="ids[]"/></th>
                                        <th>{{ trans("paid_ads::pa_blocks.attributes.name") }}</th>
                                        <th>{{ trans("paid_ads::pa_blocks.actions") }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($pa_blocks as $pa_block)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="i-checks" name="id[]" value="{{ $pa_block->id }}"/>
                                            </td>

                                            <td>
                                                <a href="{{ route("admin.pa_blocks.edit", array("id" => $pa_block->id)) }}">
                                                    <strong>{{ $pa_block->name }}</strong>
                                                </a>
                                            </td>

                                            <td class="center">
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("paid_ads::pa_blocks.edit") }}"
                                                   href="{{ route("admin.pa_blocks.edit", array("id" => $pa_block->id)) }}">
                                                    <i class="fa fa-pencil text-navy"></i>
                                                </a>

                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("paid_ads::pa_blocks.add_paid_ads") }}"
                                                   href="{{ route("admin.pa_blocks.add_paid_ads", array("id" => $pa_block->id)) }}">
                                                    <i class="fa fa-tasks text-navy"></i>
                                                </a>

                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("paid_ads::pa_blocks.delete") }}"
                                                   class="delete_user ask"
                                                   message="{{ trans("paid_ads::pa_blocks.sure_delete") }}"
                                                   href="{{ URL::route("admin.pa_blocks.delete", array("id" => $pa_block->id)) }}">
                                                    <i class="fa fa-times text-navy"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    {{ trans("paid_ads::pa_blocks.page") }} {{ $pa_blocks->currentPage() }} {{ trans("paid_ads::pa_blocks.of") }} {{ $pa_blocks->lastPage() }}
                                </div>
                                <div class="col-lg-12 text-center">
                                    {{ $pa_blocks->appends(Request::all())->render() }}
                                </div>
                            </div>
                        @else
                            {{ trans("paid_ads::pa_blocks.no_records") }}
                        @endif
                    </div>
                </div>
            </form>
        </div>

    </div>

@stop

@section("footer")

    <script>

        $(document).ready(function () {

            $('[data-toggle="tooltip"]').tooltip();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });
            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });
            $(".filter-form input[name=per_page]").val($(".per_page_filter").val());
            $(".per_page_filter").change(function () {
                var base = $(this);
                var per_page = base.val();
                $(".filter-form input[name=per_page]").val(per_page);
                $(".filter-form").submit();
            });
        });

    </script>

@stop

