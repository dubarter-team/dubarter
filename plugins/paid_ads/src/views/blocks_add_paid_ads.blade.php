@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="category-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $pa_block ? trans("paid_ads::pa_blocks.edit") : trans("paid_ads::pa_blocks.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.pa_blocks.show") }}">{{ trans("paid_ads::pa_blocks.title") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ trans("paid_ads::pa_blocks.add_paid_ads") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("paid_ads::pa_blocks.save_paid_ads") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-option-en">{{ trans("paid_ads::pa_blocks.attributes.add_paid_ads") }}</label>

                                <div class="input-group">
                                    <select class="form-control chosen-select chosen-rtl" id="paid-ads-dropdown">
                                        <option value="0">{{ trans("paid_ads::pa_blocks.attributes.choose_paid_ads") }}</option>

                                        @if(count($paid_ads))

                                            @foreach($paid_ads as $ad)
                                                <option value="{{ $ad->id }}">{{ $ad->name }}</option>
                                            @endforeach

                                        @endif
                                    </select>

                                    <span class="input-group-btn" style="margin: 0 0 4px 0;">
                                        <button class="btn btn-secondary btn-success btn-add" id="add-paid-ad"
                                                type="button">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>


                            <div class="dd">

                                <ul class="dd-list" id="sortable">

                                    @if($pa_block && count($pa_block->paid_ads))

                                        @foreach($pa_block->paid_ads as $ad)

                                            <li class="dd-item">
                                                <div class="dd-handle">
                                                    <i class="fa fa-cogs"></i> &nbsp; {{ $ad->name }}
                                                </div>

                                                <input type="hidden" class="dd-hidden" name="paid_ads[]"
                                                       value="{{ $ad->id }}"/>
                                                <a href="javascript:void(0)"
                                                   class="pull-right remove-item remove-paid-ad"> <i
                                                            class="fa fa-times"></i></a>
                                            </li>

                                        @endforeach

                                    @endif

                                </ul>

                            </div>

                        </div>
                    </div>


                </div>

            </div>

        </div>


        </div>

    </form>

    <div style="display: none;" id="dummy-sortable">
        <li class="dd-item">
            <div class="dd-handle">
                <i class="fa fa-cogs"></i> &nbsp; xxdd
            </div>

            <input type="hidden" class="dd-hidden" name="paid_ads[]" value=""/>
            <a href="javascript:void(0)" class="pull-right remove-item remove-paid-ad"> <i
                        class="fa fa-times"></i></a>
        </li>
    </div>

@stop

@section("head")

    <link href="{{ assets('admin::css/plugins/nestable/nestable.ltr.css') }}" rel="stylesheet"/>

    <style>

        .chosen-container {
            margin: 0 !important;
        }

    </style>
@stop

@section("footer")
    <script>
        $("#sortable").sortable();

        $(document).ready(function () {

            $('#add-paid-ad').click(function () {
                var paid_ad = $('#paid-ads-dropdown option:selected');

                if (paid_ad.val() != 0) {
                    var existed = $('#sortable').find('.dd-hidden[value="' + paid_ad.val() + '"]');

                    if (existed.size() == 0) {
                        var sortable_li = $('#dummy-sortable li').clone();

                        sortable_li.find('.dd-hidden').val(paid_ad.val());

                        sortable_li.find('.dd-handle').html(sortable_li.find('.dd-handle').html().replace("xxdd", paid_ad.text()));

                        $('#sortable').append(sortable_li);
                    } else {
                        alert_box("{{ trans("paid_ads::pa_blocks.duplicate_paid_ad_value") }}");
                    }
                }
            });

            $(document).on('click', '.remove-paid-ad', function () {
                $(this).parents('.dd-item').remove();
            });

        });
    </script>
@stop