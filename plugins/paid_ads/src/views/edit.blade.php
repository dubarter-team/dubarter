@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="category-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $paid_ad ? trans("paid_ads::paid_ads.edit") : trans("paid_ads::paid_ads.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.paid_ads.show") }}">{{ trans("paid_ads::paid_ads.title") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $paid_ad ? trans("paid_ads::paid_ads.edit") : trans("paid_ads::paid_ads.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($paid_ad)
                    <a href="{{ route("admin.paid_ads.create") }}"
                       class="btn btn-primary btn-labeled btn-main"> <span
                                class="btn-label icon fa fa-plus"></span>
                        &nbsp; {{ trans("paid_ads::paid_ads.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("paid_ads::paid_ads.save_paid_ad") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-name">{{ trans("paid_ads::paid_ads.attributes.name") }}</label>
                                <input name="name" type="text"
                                       value="{{ @Request::old("name", $paid_ad->name) }}"
                                       class="form-control" id="input-name"
                                       placeholder="{{ trans("paid_ads::paid_ads.attributes.name") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-link">{{ trans("paid_ads::paid_ads.attributes.link") }}</label>
                                <input name="link" type="text"
                                       value="{{ @Request::old("link", $paid_ad->link) }}"
                                       class="form-control" id="input-link"
                                       placeholder="{{ trans("paid_ads::paid_ads.attributes.link") }}">
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-th-large"></i>
                            {{ trans("paid_ads::paid_ads.add_country") }}
                        </div>
                        <div class="panel-body">
                            @if ($countries->count())
                                <ul class='tree-views'>
                                    @foreach($countries as $country)
                                        <li>
                                            <div class='tree-row checkbox i-checks'>
                                                <label>
                                                    <input type='checkbox'
                                                           @if ($paid_ad and in_array($country->id, $paid_ad->countries->pluck("id")->toArray())) checked="checked"
                                                           @endif
                                                           name='countries[]'
                                                           value='{{ $country->id }}'>
                                                    &nbsp; {{ $country->name }}
                                                </label>
                                            </div>
                                    @endforeach
                                </ul>
                            @else
                                {{ trans("paid_ads::paid_ads.no_countries") }}
                            @endif
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-check-square"></i>
                            {{ trans("paid_ads::paid_ads.attributes.status") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-status">{{ trans("paid_ads::paid_ads.attributes.status") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("status", $paid_ad->status)) checked="checked" @endif
                                    type="checkbox" id="input-status" name="status" value="1"
                                           class="status-switcher switcher-sm">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("paid_ads::paid_ads.add_image") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="image_id" class="post-image-id" value="
                                {{ ($paid_ad and @$paid_ad->image->path != "") ? @$paid_ad->image->id : 0 }}">
                                <a class="change-post-image label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("paid_ads::paid_ads.change_image") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="post-image"
                                         src="{{ ($paid_ad and @$paid_ad->image->id != "") ? thumbnail(@$paid_ad->image->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>


        </div>

    </form>

@stop


@section("footer")

    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        $(document).ready(function () {

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.tree-views input[type=checkbox]').on('ifChecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('check');
                checkbox.change();
            });

            $('.tree-views input[type=checkbox]').on('ifUnchecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('uncheck');
                checkbox.change();
            });

            $(".change-post-image").filemanager({
                panel: "media",
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-image-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-image").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("paid_ads::paid_ads.not_allowed_file") }}");
                }
            });

        });
    </script>

@stop

