@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="category-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $pa_block ? trans("paid_ads::pa_blocks.edit") : trans("paid_ads::pa_blocks.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.pa_blocks.show") }}">{{ trans("paid_ads::pa_blocks.title") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $pa_block ? trans("paid_ads::pa_blocks.edit") : trans("paid_ads::pa_blocks.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($pa_block)
                    <a href="{{ route("admin.pa_blocks.create") }}"
                       class="btn btn-primary btn-labeled btn-main"> <span
                                class="btn-label icon fa fa-plus"></span>
                        &nbsp; {{ trans("paid_ads::pa_blocks.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("paid_ads::pa_blocks.save_pa_block") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-name">{{ trans("paid_ads::pa_blocks.attributes.name") }}</label>
                                <input name="name" type="text"
                                       value="{{ @Request::old("name", $pa_block->name) }}"
                                       class="form-control" id="input-name"
                                       placeholder="{{ trans("paid_ads::pa_blocks.attributes.name") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-slug">{{ trans("paid_ads::pa_blocks.attributes.slug") }}</label>
                                <input name="slug" type="text"
                                       value="{{ @Request::old("slug", $pa_block->slug) }}"
                                       class="form-control" id="input-slug"
                                       placeholder="{{ trans("paid_ads::pa_blocks.attributes.slug") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-limit">{{ trans("paid_ads::pa_blocks.attributes.limit") }}</label>
                                <input name="limit" type="text"
                                       value="{{ @Request::old("limit", $pa_block->limit) }}"
                                       class="form-control" id="input-limit"
                                       placeholder="{{ trans("paid_ads::pa_blocks.attributes.limit") }}">
                            </div>

                        </div>
                    </div>


                </div>

            </div>

        </div>


        </div>

    </form>

@stop

