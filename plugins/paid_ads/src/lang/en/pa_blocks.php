<?php
return [

    'title' => 'Paid Ads Blocks',
    'topic' => 'Paid Ads Blocks',
    'add_new' => 'Add New Paid Ad Block',
    'edit' => 'Edit Paid Ad Block',
    'back_to_pa_blocks' => 'Back To Paid Ads Blocks',
    'no_records' => 'No Paid Ads Blocks Found',
    'save_pa_block' => 'Save Paid Ad Block',
    'save_paid_ads' => 'Save Paid Ads',
    'add_paid_ads' => 'Add Paid Ads',
    'search' => 'search',
    'search_pa_blocks' => 'Search Paid Ads Blocks',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'language' => 'Language',
    'sure_delete' => 'Are you sure to delete ?',
    'duplicate_paid_ad_value' => 'This add already added',

    'attributes' => [

        'name' => 'Name',
        'slug' => 'Slug',
        'limit' => 'Limit',
        'add_paid_ads' => 'Add Paid Ads',
        'choose_paid_ads' => 'Choose Paid Ads',

    ],

    "events" => [
        'created' => 'Paid Ad Block created successfully',
        'updated' => 'Paid Ad Block updated successfully',
        'deleted' => 'Paid Ad Block deleted successfully',
        'paid_ads_added' => 'Paid Ad added to the block successfully',
    ],
    "permissions" => [
        "manage" => "Manage Paid Ads Blocks"
    ]

];
