<?php
return [

    'title' => 'Paid Ads',
    'paid_ad' => 'Paid Ads',
    'add_new' => 'Add New Paid Ad',
    'edit' => 'Edit Paid Ad',
    'back_to_paid_ads' => 'Back To Paid Ads',
    'no_records' => 'No Paid Ads Found',
    'save_paid_ad' => 'Save Paid Ad',
    'search' => 'search',
    'search_paid_ads' => 'Search Paid Ads',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'language' => 'Language',
    'sure_delete' => 'Are you sure to delete ?',
    'change_image' => 'change image',
    'add_image' => 'Add image',
    'not_allowed_file' => 'not an allowed file type',

    'add_country' => 'Add Country',
    'no_countries' => 'There are no countries',

    'add_block' => 'Add Block',
    'no_blocks' => 'There are no blocks',

    'attributes' => [

        'name' => 'Name',
        'link' => 'Link',
        'status' => 'Status',

    ],

    "events" => [
        'created' => 'Paid Ad created successfully',
        'updated' => 'Paid Ad updated successfully',
        'deleted' => 'Paid Ad deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage Paid Ads"
    ]

];
