<?php

namespace Dot\PaidAds\Models;

use DB;
use Dot\Media\Models\Media;
use Dot\Platform\Model;
use Dot\I18n\Models\Place;

/**
 * Class paid_ads
 * @package Dot\PaidAds\Models
 */
class PaidAd extends Model
{

    /**
     * @var string
     */
    protected $module = 'paid_ads';

    /**
     * @var string
     */
    protected $table = 'paid_ads';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    /*
     * @var array
     */
    protected $searchable = ['name'];

    /*
     * @var array
     */
    protected $creatingRules = [
        "name" => "required|unique:pa_blocks,name"
    ];

    /*
     * @var array
     */
    protected $updatingRules = [
        "name" => "required|unique:pa_blocks,name,[id],id"
    ];

    /**
     * image relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function image()
    {
        return $this->hasOne(Media::class, "id", "image_id");
    }

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function countries()
    {
        return $this->belongsToMany(Place::class, 'paid_ads_places', 'paid_ad_id', 'place_id');
    }

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function pa_blocks()
    {
        return $this->belongsToMany(PaBlock::class, 'paid_ads_blocks', 'paid_ad_id', 'block_id');
    }

}
