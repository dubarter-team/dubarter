<?php

namespace Dot\PaidAds\Models;

use DB;
use Dot\Platform\Model;

/**
 * Class PaBlock
 * @package Dot\PaidAds\Models
 */
class PaBlock extends Model
{

    /**
     * @var string
     */
    protected $module = 'paid_ads';

    /**
     * @var string
     */
    protected $table = 'pa_blocks';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    /*
     * @var array
     */
    protected $searchable = ['name', 'slug'];

    /*
     * @var array
     */
    protected $sluggable = [
        'slug' => 'name',
    ];

    /*
     * @var array
     */
    protected $creatingRules = [
        "name" => "required|unique:pa_blocks,name",
        "slug" => "unique:pa_blocks,slug"
    ];

    /*
     * @var array
     */
    protected $updatingRules = [
        "name" => "required|unique:pa_blocks,name,[id],id",
        "slug" => "required|unique:pa_blocks,slug,[id],id"
    ];

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function paid_ads()
    {
        return $this->belongsToMany(PaidAd::class, 'paid_ads_blocks', 'block_id', 'paid_ad_id')->orderBy('order', 'asc');
    }

}
