<?php

namespace Dot\PaidAds\Controllers;

use Action;
use Dot\PaidAds\Models\PaBlock;
use Dot\PaidAds\Models\PaidAd;
use Dot\Platform\Controller;
use Redirect;
use Request;

class PaBlocksController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all pa_plocks
     * @return mixed
     */
    function index()
    {
        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = PaBlock::orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        $this->data["pa_blocks"] = $query->paginate($this->data['per_page']);

        return view("paid_ads::blocks_show", $this->data);
    }

    /**
     * Delete pa_plocks by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $topic = PaBlock::findOrFail($id);

            $topic->delete();
        }

        return Redirect::back()->with("message", trans("paid_ads::pa_blocks.events.deleted"));
    }

    /**
     * Create a new pa_plocks
     * @return mixed
     */
    public function create()
    {
        if (Request::isMethod("post")) {
            $pa_block = new paBlock();

            $pa_block->name = Request::get('name');
            $pa_block->slug = Request::get('slug');
            $pa_block->limit = Request::get('limit');

            if (!$pa_block->validate()) {
                return Redirect::back()->withErrors($pa_block->errors())->withInput(Request::all());
            }

            $pa_block->save();

            return Redirect::route("admin.pa_blocks.edit", array("id" => $pa_block->id))
                ->with("message", trans("paid_ads::pa_blocks.events.created"));
        }

        $this->data["pa_block"] = false;

        return view("paid_ads::blocks_edit", $this->data);
    }

    /**
     * Edit pa_plocks by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $pa_block = PaBlock::findOrFail($id);

        if (Request::isMethod("post")) {

            $pa_block->name = Request::get('name');
            $pa_block->slug = Request::get('slug');
            $pa_block->limit = Request::get('limit');

            if (!$pa_block->validate()) {
                return Redirect::back()->withErrors($pa_block->errors())->withInput(Request::all());
            }

            $pa_block->save();

            return Redirect::route("admin.pa_blocks.edit", array("id" => $id))->with("message", trans("paid_ads::pa_blocks.events.updated"));
        }

        $this->data["pa_block"] = $pa_block;

        return view("paid_ads::blocks_edit", $this->data);
    }

    /**
     * add paid ads to pa_plocks
     * @return mixed
     */
    public function add_paid_adds($id)
    {
        $pa_block = PaBlock::findOrFail($id);

        if (Request::isMethod("post")) {

            if(count(Request::get('paid_ads'))){
                $pa_block->paid_ads()->detach();

                $paid_ads = PaidAd::whereIn('id', Request::get('paid_ads'))
                    ->orderByRaw(\DB::raw("FIELD(id, " . implode(",", array_filter(Request::get('paid_ads'))) . ")"))->get();

                if(count($paid_ads)){
                    $i = 0;

                    foreach ($paid_ads as $ad){
                        $pa_block->paid_ads()->save($ad, ['order' => $i]);

                        $i++;
                    }
                }
            }

            return Redirect::route("admin.pa_blocks.add_paid_ads", array("id" => $pa_block->id))
                ->with("message", trans("paid_ads::pa_blocks.events.paid_ads_added"));
        }

        $this->data["pa_block"] = $pa_block;

        $this->data["paid_ads"] = PaidAd::where('status', 1)->get();

        return view("paid_ads::blocks_add_paid_ads", $this->data);
    }

}
