<?php

namespace Dot\PaidAds\Controllers;

use Action;
use Dot\I18n\Models\Place;
use Dot\PaidAds\Models\PaBlock;
use Dot\PaidAds\Models\PaidAd;
use Dot\Platform\Controller;
use Redirect;
use Request;

class PaidAdsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all paid_ads
     * @return mixed
     */
    function index()
    {
        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "created_at";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = PaidAd::orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        $this->data["paid_ads"] = $query->paginate($this->data['per_page']);

        return view("paid_ads::show", $this->data);
    }

    /**
     * Delete paid_ads by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $paid_ad = PaidAd::findOrFail($id);

            $paid_ad->pa_blocks()->detach();

            $paid_ad->delete();
        }

        return Redirect::back()->with("message", trans("paid_ads::paid_ads.events.deleted"));
    }

    /**
     * Create a new paid_ad
     * @return mixed
     */
    public function create()
    {
        if (Request::isMethod("post")) {
            $paid_ad = new PaidAd();

            $paid_ad->name = Request::get('name');
            $paid_ad->link = Request::get('link');
            $paid_ad->image_id = Request::get('image_id');
            $paid_ad->status = Request::get('status');

            if (!$paid_ad->validate()) {
                return Redirect::back()->withErrors($paid_ad->errors())->withInput(Request::all());
            }

            $paid_ad->save();

            $paid_ad->countries()->sync(Request::get('countries', []));

            return Redirect::route("admin.paid_ads.edit", array("id" => $paid_ad->id))
                ->with("message", trans("paid_ads::paid_ads.events.created"));
        }

        $this->data["paid_ad"] = false;

        $this->data['countries'] = Place::where('parent', 0)->where('status', 1)->get();

        return view("paid_ads::edit", $this->data);
    }

    /**
     * Edit paid_ad by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $paid_ad = PaidAd::findOrFail($id);

        if (Request::isMethod("post")) {

            $paid_ad->name = Request::get('name');
            $paid_ad->link = Request::get('link');
            $paid_ad->image_id = Request::get('image_id');
            $paid_ad->status = Request::get('status');

            if (!$paid_ad->validate()) {
                return Redirect::back()->withErrors($paid_ad->errors())->withInput(Request::all());
            }

            $paid_ad->save();

            $paid_ad->countries()->sync(Request::get('countries', []));

            return Redirect::route("admin.paid_ads.edit", array("id" => $id))->with("message", trans("paid_ads::paid_ads.events.updated"));
        }

        $this->data["paid_ad"] = $paid_ad;

        $this->data['countries'] = Place::where('parent', 0)->where('status', 1)->get();

        return view("paid_ads::edit", $this->data);
    }

}
