<?php

namespace Dot\PaidAds;

use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class PaidAds extends \Dot\Platform\Plugin
{

    protected $permissions = [
        "manage"
    ];

    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("paid_ads.manage")) {
                $menu->item('paid_ads', trans("paid_ads::paid_ads.title"), '')->icon("fa-sliders")->order(1);
                $menu->item('paid_ads.ads', trans("paid_ads::paid_ads.title"), route("admin.paid_ads.show"))->icon("fa-sliders")->order(1);
                $menu->item('paid_ads.pa_blocks', trans("paid_ads::pa_blocks.title"), route("admin.pa_blocks.show"))->icon("fa-server")->order(2);
            }

        });
    }
}
