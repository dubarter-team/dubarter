<?php

namespace Dot\Topics\Models;

use DB;
use Dot\Categories\Models\Category;
use Dot\Platform\Model;
use Dot\Media\Models\Media;
use Dot\Tags\Models\Tag;

/**
 * Class topic
 * @package Dot\Topics\Models
 */
class Topic extends Model
{

    /**
     * @var string
     */
    protected $module = 'topics';

    /**
     * @var string
     */
    protected $table = 'topics';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /*
     * @var array
     */
    protected $creatingRules = [
        "name" => "required|unique:topics,name"
    ];

    /*
     * @var array
     */
    protected $updatingRules = [
        "name" => "required|unique:topics,name,[id],id"
    ];

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    protected $casts = [
        'name' => 'json',
    ];

    /*
     * lang fields
     */
    protected $translatable = [
        'name',
    ];

    /**
     * big icon relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function image()
    {
        return $this->hasOne(Media::class, "id", "image_id");
    }

    /**
     * categories relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function categories()
    {
        return $this->belongsToMany(Category::class, 'topics_categories', 'topic_id', 'category_id');
    }

    /**
     * attributes relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function tags()
    {
        return $this->belongsToMany(Tag::class, 'topics_tags', 'topic_id', 'tag_id');
    }

    /**
     * Sync tags
     * @param $tags
     */
    public function sync_tags($tags)
    {
        $tag_ids = array();

        if ($tags = @explode(",", $tags)) {
            $tags = array_filter($tags);
            $tag_ids = Tag::saveNames($tags);
        }

        $this->tags()->sync($tag_ids);
    }
}
