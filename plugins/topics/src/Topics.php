<?php

namespace Dot\Topics;

use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class Topics extends \Dot\Platform\Plugin
{

    protected $permissions = [
        "manage"
    ];

    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("topics.manage")) {
                $menu->item('topics', trans("topics::topics.title"), route("admin.topics.show"))->icon("fa-lightbulb-o")->order(1);
            }

        });
    }
}
