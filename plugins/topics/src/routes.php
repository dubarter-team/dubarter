<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:topics.manage"],
    "namespace" => "Dot\\Topics\\Controllers"
], function ($route) {
    $route->group(["prefix" => "topics"], function ($route) {
        $route->any('/', ["as" => "admin.topics.show", "uses" => "TopicsController@index"]);
        $route->any('/create', ["as" => "admin.topics.create", "uses" => "TopicsController@create"]);
        $route->any('/delete', ["as" => "admin.topics.delete", "uses" => "TopicsController@delete"]);
        $route->any('/{id}/edit', ["as" => "admin.topics.edit", "uses" => "TopicsController@edit"]);
    });
});



