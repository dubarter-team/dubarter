<?php

namespace Dot\Topics\Controllers;

use Action;
use Dot\Categories\Models\Category;
use Dot\Topics\Models\Topic;
use Dot\Platform\Controller;
use Redirect;
use Request;

class TopicsController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all topics
     * @return mixed
     */
    function index()
    {
        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? "name->" . app()->getLocale() : 'created_at';
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Topic::orderBy($this->data["sort"], $this->data["order"]);

        $query->with(['tags']);

        if (Request::filled("q")) {
            $q = Request::get("q");

            $query->where(function ($c_q) use ($q) {
                $c_q->where('name->ar', 'LIKE', '%' . $q . '%');
                $c_q->orWhere('name->en', 'LIKE', '%' . $q . '%');
            });
        }

        if (Request::filled("category_id") && Request::get("category_id") != 0) {
            $query->whereHas("categories", function ($query) {
                $query->where("categories.id", Request::get("category_id"));
            });
        }

        if (Request::filled("tag_id") && Request::get("tag_id") != 0) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        $this->data["topics"] = $query->paginate($this->data['per_page']);

        $this->data["categories"] = Category::where('parent', 0)->where('status', 1)->get();

        return view("topics::show", $this->data);
    }

    /**
     * Delete topic by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $topic = Topic::findOrFail($id);

            $topic->categories()->detach();

            $topic->tags()->detach();

            $topic->delete();
        }

        return Redirect::back()->with("message", trans("topics::topics.events.deleted"));
    }

    /**
     * Create a new topic
     * @return mixed
     */
    public function create()
    {
        if (Request::isMethod("post")) {
            $topic = new Topic();

            $topic->name = Request::get('name');
            $topic->image_id = Request::get('image_id');
            $topic->status = Request::get('status', 0);

            if (!$topic->validate()) {
                return Redirect::back()->withErrors($topic->errors())->withInput(Request::all());
            }

            $topic->save();

            $topic->categories()->sync(Request::get("categories", []));

            $topic->sync_tags(Request::get("tags", []));

            return Redirect::route("admin.topics.edit", array("id" => $topic->id))
                ->with("message", trans("topics::topics.events.created"));
        }

        $this->data["topic"] = false;

        $this->data['categories'] = Category::where('parent', 0)->where('status', 1)->get();

        $this->data["topic_categories"] = collect([]);

        $this->data["topic_tags"] = [];

        return view("topics::edit", $this->data);
    }

    /**
     * Edit topic by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $topic = Topic::findOrFail($id);

        if (Request::isMethod("post")) {

            $topic->name = Request::get('name');
            $topic->image_id = Request::get('image_id');
            $topic->status = Request::get('status', 0);

            if (!$topic->validate()) {
                return Redirect::back()->withErrors($topic->errors())->withInput(Request::all());
            }

            $topic->save();

            $topic->categories()->sync(Request::get("categories", []));

            $topic->sync_tags(Request::get("tags", []));

            return Redirect::route("admin.topics.edit", array("id" => $id))->with("message", trans("topics::topics.events.updated"));
        }

        $this->data["topic"] = $topic;

        $this->data['categories'] = Category::where('parent', 0)->where('status', 1)->get();

        $this->data["topic_categories"] = $topic->categories;

        $this->data["topic_tags"] = $topic->tags->pluck("name")->toArray();

        return view("topics::edit", $this->data);
    }

}
