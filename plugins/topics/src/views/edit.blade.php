@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="category-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $topic ? trans("topics::topics.edit") : trans("topics::topics.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.topics.show") }}">{{ trans("topics::topics.title") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $topic ? trans("topics::topics.edit") : trans("topics::topics.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($topic)
                    <a href="{{ route("admin.topics.create") }}"
                       class="btn btn-primary btn-labeled btn-main"> <span
                                class="btn-label icon fa fa-plus"></span>
                        &nbsp; {{ trans("topics::topics.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("topics::topics.save_topic") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-body">


                            <div class="panel-options">
                                <ul class="nav nav-tabs">
                                    @if(count(config("i18n.locales")))
                                        @foreach(config("i18n.locales") as $key => $locale)
                                            <li @if($key == app()->getLocale()) class="active" @endif>
                                                <a data-toggle="tab" href="#tab-{{ $key }}" aria-expanded="true">
                                                    {{ $locale["title"] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <br/>

                            <div class="tab-content">

                                @foreach(config("i18n.locales") as $key => $locale)

                                    <div id="tab-{{ $key }}"
                                         class="tab-pane @if($key == app()->getLocale()) active @endif"
                                         style="direction: {{ $locale["direction"] }}">

                                        <div class="form-group">
                                            <label for="input-name-{{ $key }}">{{ trans("topics::topics.attributes.name", [], $key) }}</label>
                                            <input name="name[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("name." . $key, $topic->name->$key) }}"
                                                   class="form-control" id="input-name-{{ $key }}"
                                                   placeholder="{{ trans("topics::topics.attributes.name", [], $key) }}">
                                        </div>

                                    </div>

                                @endforeach

                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-tags"></i>
                            {{ trans("topics::topics.add_tag") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group" style="position:relative">
                                <input type="hidden" name="tags" id="tags_names"
                                       value="{{ join(",", $topic_tags) }}">
                                <ul id="mytags"></ul>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-check-square"></i>
                            {{ trans("topics::topics.attributes.status") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-status">{{ trans("topics::topics.attributes.status") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("status", $topic->status)) checked="checked" @endif
                                    type="checkbox" id="input-status" name="status" value="1"
                                           class="status-switcher switcher-sm">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("topics::topics.add_image") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="image_id" class="post-image-id" value="
                                {{ ($topic and @$topic->image->path != "") ? @$topic->image->id : 0 }}">
                                <a class="change-post-image label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("topics::topics.change_image") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="post-image"
                                         src="{{ ($topic and @$topic->image->id != "") ? thumbnail(@$topic->image->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-folder"></i>
                            {{ trans("topics::topics.add_category") }}
                        </div>
                        <div class="panel-body">

                            @if ($categories->count())
                                <ul class='tree-views'>

                                    @foreach($categories as $category)
                                        <li>
                                            <div class='tree-row checkbox i-checks'>

                                                <label>
                                                    <input type='checkbox'
                                                           {{ ($topic and in_array($category->id, $topic_categories->pluck("id")->toArray())) ? 'checked="checked"' : '' }}
                                                           name='categories[]'
                                                           value="{{ $category->id }}">&nbsp; {{ $category->name }}
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach

                                </ul>
                            @else
                                {{ trans("categories::categories.no_records") }}
                            @endif
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </form>

@stop


@section("head")

    <link href="{{ assets("admin::tagit") }}/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{{ assets("admin::tagit") }}/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

@stop


@section("footer")

    <script type="text/javascript" src="{{ assets("admin::tagit") }}/tag-it.js"></script>

    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        $(document).ready(function () {

            $(".change-post-image").filemanager({
                panel: "media",
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-image-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-image").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("topics::topics.not_allowed_file") }}");
                }
            });


            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.tree-views input[type=checkbox]').on('ifChecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('check');
                checkbox.change();
            });

            $('.tree-views input[type=checkbox]').on('ifUnchecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('uncheck');
                checkbox.change();
            });

            $(".expand").each(function (index, element) {
                var base = $(this);
                if (base.parents("li").find("ul").first().length > 0) {
                    base.text("+");
                } else {
                    base.text("-");
                }
            });

            $("body").on("click", ".expand", function () {
                var base = $(this);
                if (base.text() == "+") {
                    if (base.closest("li").find("ul").length > 0) {
                        base.closest("li").find("ul").first().slideDown("fast");
                        base.text("-");
                    }
                    base.closest("li").find(".expand").last().text("-");
                } else {
                    if (base.closest("li").find("ul").length > 0) {
                        base.closest("li").find("ul").first().slideUp("fast");
                        base.text("+");
                    }
                }
                return false;
            });

            $("#mytags").tagit({
                singleField: true,
                singleFieldNode: $('#tags_names'),
                allowSpaces: true,
                minLength: 2,
                placeholderText: "",
                removeConfirmation: true,
                tagSource: function (request, response) {
                    $.ajax({
                        url: "{{ route("admin.tags.search") }}",
                        data: {q: request.term},
                        dataType: "json",
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.name
                                }
                            }));
                        }
                    });
                },
                beforeTagAdded: function (event, ui) {
                    $("#metakeywords").tagit("createTag", ui.tagLabel);
                }
            });

        });
    </script>

@stop
