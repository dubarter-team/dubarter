<?php
return [

    'module' => 'topics',

    'title' => 'topics',
    'topic' => 'topic',
    'add_new' => 'Add New topic',
    'edit' => 'Edit topic',
    'back_to_topics' => 'Back To topics',
    'no_records' => 'No topics Found',
    'save_topic' => 'Save topic',
    'search' => 'search',
    'search_topics' => 'Search topics',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'language' => 'Language',
    'sure_delete' => 'Are you sure to delete ?',
    'change_image' => 'change image',
    'add_image' => 'Add image',
    'not_allowed_file' => 'not an allowed file type',

    'add_category' => 'Add category',
    'no_categories' => 'There are no categories',
    'add_tag' => 'Add tag',
    'tags' => 'Tags',
    'created_at' => 'Created at',

    'attributes' => [

        'name' => 'Name',
        'status' => 'Status',

    ],

    "events" => [
        'created' => 'Topic created successfully',
        'updated' => 'Topic updated successfully',
        'deleted' => 'Topic deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage topics"
    ]

];
