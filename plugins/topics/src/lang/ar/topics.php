<?php

return [

    'module' => 'المواضيع',

    'title' => 'المواضيع',
    'topics' => 'المواضيع',
    'topic' => 'موضوع',
    'add_new' => 'أضف موضوع جديده',
    'edit' => 'تحرير الموضوع',
    'back_to_topics' => 'العودة إلى المواضيع',
    'no_records' => 'لا يوجد مواضيع',
    'save_topic' => 'حفظ الموضوع',
    'search' => 'البحث',
    'search_topics' => 'البحث فى المواضيع',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'اختر أمر',
    'delete' => 'حذف',
    'apply' => 'حفظ',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب حسب',
    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',
    'actions' => 'الأمر',
    'filter' => 'عرض',
    'language' => 'اللغة',
    'sure_delete' => 'هل أنت متأكد من الحذف ؟',
    'change_image' => 'تغيير الصورة',
    'add_image' => 'أضف صورة',
    'not_allowed_file' => 'ملف غير مسموح به',

    'add_category' => 'أضف تصنيف',
    'no_categories' => 'لا يوجد تصنيفات',
    'add_tag' => 'أضف وسم',
    'tags' => 'الوسوم',
    'created_at' => 'تاريخ الإنشاء',

    'attributes' => [
        'name' => 'الإسم',
        'status' => 'الحاله',
    ],
    "events" => [
        'created' => 'تم إنشاء الموضوع بنجاح',
        'updated' => 'تم حفظ الموضوع بنجاح',
        'deleted' => 'تم الحذف بنجاح',

    ],
    "permissions" => [
        "manage" => "التحكم بالمواضيع"
    ]

];
