<?php
return [

    'module' => 'offers',

    'title' => 'offers',
    'offer' => 'offer',
    'no_records' => 'No offers Found',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'language' => 'Language',
    'sure_delete' => 'Are you sure to delete ?',
    'offer_details' => 'Offer Details',
    'replies' => 'Replies',
    'view_offer' => 'View Offer',
    'no_replies' => 'There are no replies',
    'from' => 'From',
    'to' => 'To',

    'status' => [
        '1' => 'active',
        '2' => 'pending',
        '3' => 'rejected',
        '4' => 'accepted',
    ],

    'attributes' => [

        'ad' => 'Ad',
        'user' => 'User',
        'sender' => 'Sender',
        'receiver' => 'Receiver',
        'type' => 'Type',
        'date' => 'Date',
        'comment' => 'Comment',
        'price' => 'Price',
        'status' => 'Status',
        'replies_count' => 'Replies count',
        'bid' => 'Bid',
        'bart' => 'Bart',

    ],

    "events" => [
        'created' => 'Offer created successfully',
        'updated' => 'Offer updated successfully',
        'deleted' => 'Offer deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage Offers"
    ]

];
