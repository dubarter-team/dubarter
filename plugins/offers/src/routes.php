<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:offers.manage"],
    "namespace" => "Dot\\Offers\\Controllers"
], function ($route) {
    $route->group(["prefix" => "offers"], function ($route) {
        $route->any('/', ["as" => "admin.offers.show", "uses" => "OffersController@index"]);
        $route->any('/delete', ["as" => "admin.offers.delete", "uses" => "OffersController@delete"]);
        $route->any('/{id}/view', ["as" => "admin.offers.view", "uses" => "OffersController@view"]);
    });
});



