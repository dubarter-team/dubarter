<?php

namespace Dot\Offers;

use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class Offers extends \Dot\Platform\Plugin
{

    protected $permissions = [
        "manage"
    ];

    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("offers.manage")) {
                $menu->item('offers', trans("offers::offers.title"), route("admin.offers.show"))->icon("fa-dollar")->order(1);
            }

        });
    }
}
