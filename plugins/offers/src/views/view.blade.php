@extends("admin::layouts.master")

@section("content")

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-folder"></i>
                {{ trans("offers::offers.view_offer") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.offers.show") }}">{{ trans("offers::offers.title") }}</a>
                </li>
                <li class="active">
                    <strong>
                        {{ trans("offers::offers.view_offer") }}
                    </strong>
                </li>
            </ol>
        </div>

        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

            <a href="{{ route("admin.offers.delete", ['id' => $offer->id, 'redirect' => 'home']) }}"
               class="btn btn-flat btn-danger btn-main">{{ trans("offers::offers.delete") }}</a>

        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-th-large"></i>
                        {{ trans("offers::offers.offer_details") }}
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.ad") }}</label>
                            <div>
                                <a href="{{ route("admin.ads.edit", array("id" => $offer->ad_id)) }}">{{ $offer->ad->title }}</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.user") }}</label>
                            <div>
                                <a href="{{ route("admin.users.edit", array("id" => $offer->sender_id)) }}">{{ $offer->sender->username }}
                                    ( {{ $offer->sender->email }} )</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.type") }}</label>
                            <div>{{ trans("offers::offers.attributes." . $offer->type) }}</div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.status") }}</label>
                            <div>{{ trans("offers::offers.status." . $offer->status) }}</div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.price") }}</label>
                            <div>{{ $offer->price }}</div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.date") }}</label>
                            <div>{{ $offer->created_at }}</div>
                        </div>

                        <div class="form-group">
                            <label for="input-color">{{ trans("offers::offers.attributes.comment") }}</label>
                            <div>{{ $offer->comment }}</div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-file-text-o"></i>
                        {{ trans("offers::offers.replies") }}
                    </div>
                    <div class="panel-body" style="max-height: 400px;overflow-y: scroll;">
                        @if($offer->replies->count())

                            @foreach($offer->replies as $reply)

                                <div class="panel panel-default">

                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label for="input-color"><a
                                                        href="{{ route("admin.users.edit", array("id" => $reply->sender_id)) }}">{{ $reply->sender->username }}
                                                    ( {{ $reply->sender->email }} )</a></label>
                                            <div>{{ $reply->created_at }}</div>
                                            <div>{{ $reply->comment }}</div>
                                        </div>

                                    </div>
                                </div>

                            @endforeach

                        @else

                            <div class="form-group">{{ trans("offers::offers.no_replies") }}</div>

                        @endif
                    </div>
                </div>

            </div>

        </div>

    </div>

@stop