<?php

namespace Dot\Offers\Models;

use DB;
use Dot\Ads\Models\Ad;
use Dot\Platform\Model;
use Dot\Users\Models\User;

/**
 * Class topic
 * @package Dot\Topics\Models
 */
class Offer extends Model
{

    /**
     * @var string
     */
    protected $module = 'offers';

    /**
     * @var string
     */
    protected $table = 'offers';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    /**
     * ad relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function ad()
    {
        return $this->hasOne(Ad::class, "id", "ad_id");
    }

    /**
     * sender relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function sender()
    {
        return $this->hasOne(User::class, "id", "sender_id");
    }

    /**
     * receiver relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function receiver()
    {
        return $this->hasOne(User::class, "id", "receiver_id");
    }

    /**
     * replies relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function replies()
    {
        return $this->hasMany(Offer::class, 'parent')->orderBy('created_at', 'asc');
    }



    /**
     * replies relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function newestReplies()
    {
        return $this->hasMany(Offer::class, 'parent')->orderBy('created_at', 'desc');
    }
}
