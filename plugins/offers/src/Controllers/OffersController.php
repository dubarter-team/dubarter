<?php

namespace Dot\Offers\Controllers;

use Action;
use Dot\Offers\Models\Offer;
use Dot\Platform\Controller;
use Redirect;
use Request;

class OffersController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all offers
     * @return mixed
     */
    function index()
    {
        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = 'created_at';
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Offer::orderBy($this->data["sort"], $this->data["order"]);

        $query->where('parent', 0);

        if(Request::filled("sender_id")){
            $query->whereHas("sender", function ($query) {
                $query->where("users.id", Request::get("sender_id"));
            });
        }

        if(Request::filled("ad_id")){
            $query->whereHas("ad", function ($query) {
                $query->where("ads.id", Request::get("ad_id"));
            });
        }

        if (Request::filled("from")) {
            $query->where("created_at", ">=", Request::get("from"));
        }

        if (Request::filled("to")) {
            $query->where("created_at", "<=", Request::get("to"));
        }

        $this->data["offers"] = $query->with(['sender', 'receiver', 'ad', 'replies'])->paginate($this->data['per_page']);

        return view("offers::show", $this->data);
    }

    /**
     * Delete offer by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $back = Request::get("redirect", 'home');

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $offer = Offer::findOrFail($id);

            $offer->replies()->delete();

            $offer->delete();
        }

        if ($back == 'home') {
            return redirect()->route('admin.offers.show')->with("message", trans("offers::offers.events.deleted"));
        } else {
            return Redirect::back()->with("message", trans("offers::offers.events.deleted"));
        }
    }

    /**
     * View offer by id
     * @param $id
     * @return mixed
     */
    public function view($id)
    {

        $offer = Offer::where('id', $id)->with(['sender', 'replies', 'ad'])->first();

        if (!$offer) {
            abort(404);
        }

        $this->data["offer"] = $offer;

        return view("offers::view", $this->data);
    }

}
