<?php

namespace Dot\Attributes;

use Illuminate\Support\Facades\Auth;
use Navigation;
use URL;

class Attributes extends \Dot\Platform\Plugin
{

    protected $permissions = [
        "manage"
    ];

    function boot()
    {

        parent::boot();

        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("attributes.manage")) {
                $menu->item('attributes', trans("attributes::attributes.title"), route("admin.attributes.show"))->icon("fa-code-fork")->order(1);
            }

        });
    }
}
