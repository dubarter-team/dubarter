<?php
return [

    'module' => 'attributes',

    'title' => 'Attributes',
    'attribute' => 'attribute',
    'add_new' => 'Add New Attribute',
    'edit' => 'Edit Attribute',
    'back_to_attributes' => 'Back To Attributes',
    'no_records' => 'No Attributes Found',
    'save_attribute' => 'Save Attribute',
    'search' => 'search',
    'search_attributes' => 'Search Attributes',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'language' => 'Language',
    'sure_delete' => 'Are you sure to delete ?',
    'change_small_icon' => 'change small icon',
    'add_small_icon' => 'Add small icon',
    'change_big_icon' => 'change big icon',
    'add_big_icon' => 'Add big icon',
    'not_allowed_file' => 'not an allowed file type',
    'missing_option_attribute' => 'please enter option slug, arabic value and english value',
    'duplicate_option_value' => 'the values you entered already exists',
    'slug_not_valid' => 'slug is required and can only contain letters, digits and -.',

    'add_option' => 'Add Option',
    'remove_option' => 'Remove Option',

    'attributes' => [

        'name' => 'Name',
        'slug' => 'Slug',
        'default_value' => 'Default Value',
        'template' => 'Template',
        'type' => 'Type',
        'input_text' => 'Text Input',
        'dropdown' => 'Dropdown',
        'single_value' => 'Single Value',
        'multiple_values' => 'Multiple Values',
        'option_slug' => 'Option Slug',
        'option_ar' => 'Arabic Option Value',
        'option_en' => 'English Option Value',
        'options_ar' => 'Arabic Options',
        'options_en' => 'English Options',
        'searchable' => 'Searchable',
        'can_search' => 'yes',
        'not_search' => 'no',
        'priority' => 'Priority',
        'high_priority' => 'High',
        'low_priority' => 'Low',
        'data_type' => 'Data Type',
        'number' => 'Number',
        'text' => 'Text',
        'visible' => 'Visible',
        'is_visible' => 'yes',
        'not_visible' => 'no',
        'required' => 'Required',
        'is_required' => 'yes',
        'not_required' => 'no',

    ],

    "events" => [
        'created' => 'Attribute created successfully',
        'updated' => 'Attribute updated successfully',
        'deleted' => 'Attribute deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage attributes"
    ]

];
