<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ["web", "auth:backend", "can:attributes.manage"],
    "namespace" => "Dot\\Attributes\\Controllers"
], function ($route) {
    $route->group(["prefix" => "attributes"], function ($route) {
        $route->any('/', ["as" => "admin.attributes.show", "uses" => "AttributesController@index"]);
        $route->any('/create', ["as" => "admin.attributes.create", "uses" => "AttributesController@create"]);
        $route->any('/delete', ["as" => "admin.attributes.delete", "uses" => "AttributesController@delete"]);
        $route->any('/{id}/edit', ["as" => "admin.attributes.edit", "uses" => "AttributesController@edit"]);
    });
});



