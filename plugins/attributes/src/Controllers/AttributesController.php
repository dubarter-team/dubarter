<?php

namespace Dot\Attributes\Controllers;

use Action;
use Dot\Attributes\Models\Attribute;
use Dot\Platform\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Request;

class AttributesController extends Controller
{

    /**
     * View payload
     * @var array
     */
    protected $data = [];

    /**
     * Show all attributes
     * @return mixed
     */
    function index()
    {
        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : 70;

        $query = Attribute::orderBy("created_at", $this->data["order"]);

        if (Request::filled("q")) {
            $q = Request::get("q");

            $query->where(function ($c_q) use ($q) {
                $c_q->where('name->ar', 'LIKE', '%' . $q . '%');
                $c_q->orWhere('name->en', 'LIKE', '%' . $q . '%');
                $c_q->orWhere('slug', 'LIKE', '%' . $q . '%');
            });
        }

        $this->data["attributes"] = $query->paginate($this->data['per_page']);

        return view("attributes::show", $this->data);
    }

    /**
     * Delete attribute by id
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $attribute = Attribute::findOrFail($id);

            $attribute->categories()->detach();

            $attribute->ads()->detach();

            $attribute->delete();
        }

        return Redirect::back()->with("message", trans("attributes::attributes.events.deleted"));
    }

    /**
     * Create a new attribute
     * @return mixed
     */
    public function create()
    {
        if (Request::isMethod("post")) {
            $attribute = new Attribute();

            $attribute->name = Request::get('name');
            $attribute->slug = Request::get('slug');
            $attribute->required = Request::get('required');
            $attribute->template = Request::get('template');
            $attribute->type = Request::get('type');
            $attribute->small_icon_id = Request::get('small_icon_id');
            $attribute->big_icon_id = Request::get('big_icon_id');
            $attribute->options = Request::get('options');
            $attribute->searchable = Request::get('searchable', 1);
            $attribute->visible = Request::get('visible', 1);
            $attribute->priority = Request::get('priority', 0);
            $attribute->data_type = Request::get('data_type', 'data_type');

            if (!$attribute->validate()) {
                return Redirect::back()->withErrors($attribute->errors())->withInput(Request::all());
            }

            $attribute->save();

            return Redirect::route("admin.attributes.edit", array("id" => $attribute->id))
                ->with("message", trans("attributes::attributes.events.created"));
        }

        $this->data["attribute"] = false;

        return view("attributes::edit", $this->data);
    }

    /**
     * Edit attribute by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $attribute = Attribute::findOrFail($id);

        if (Request::isMethod("post")) {

            $attribute->name = Request::get('name');
            $attribute->slug = Request::get('slug');
            $attribute->required = Request::get('required');
            $attribute->template = Request::get('template');
            $attribute->type = Request::get('type');
            $attribute->small_icon_id = Request::get('small_icon_id');
            $attribute->big_icon_id = Request::get('big_icon_id');
            $attribute->options = Request::get('options');
            $attribute->searchable = Request::get('searchable', 1);
            $attribute->visible = Request::get('visible', 1);
            $attribute->priority = Request::get('priority', 0);
            $attribute->data_type = Request::get('data_type', 'data_type');

            if (!$attribute->validate()) {
                return Redirect::back()->withErrors($attribute->errors())->withInput(Request::all());
            }

            $attribute->save();

            return Redirect::route("admin.attributes.edit", array("id" => $id))->with("message", trans("attributes::attributes.events.updated"));
        }

        $this->data["attribute"] = $attribute;

        return view("attributes::edit", $this->data);
    }

}
