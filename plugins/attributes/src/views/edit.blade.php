@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="attribute-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $attribute ? trans("attributes::attributes.edit") : trans("attributes::attributes.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.attributes.show") }}">{{ trans("attributes::attributes.title") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $attribute ? trans("attributes::attributes.edit") : trans("attributes::attributes.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($attribute)
                    <a href="{{ route("admin.attributes.create") }}"
                       class="btn btn-primary btn-labeled btn-main"> <span
                                class="btn-label icon fa fa-plus"></span>
                        &nbsp; {{ trans("attributes::attributes.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("attributes::attributes.save_attribute") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="panel-options">
                                <ul class="nav nav-tabs">
                                    @if(count(config("i18n.locales")))
                                        @foreach(config("i18n.locales") as $key => $locale)
                                            <li @if($key == app()->getLocale()) class="active" @endif>
                                                <a data-toggle="tab" href="#tab-{{ $key }}" aria-expanded="true">
                                                    {{ $locale["title"] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <br/>


                            <div class="tab-content">

                                @foreach(config("i18n.locales") as $key => $locale)

                                    <div id="tab-{{ $key }}"
                                         class="tab-pane @if($key == app()->getLocale()) active @endif"
                                         style="direction: {{ $locale["direction"] }}">

                                        <div class="form-group">
                                            <label for="input-name-{{ $key }}">{{ trans("categories::categories.attributes.name", [], $key) }}</label>
                                            <input name="name[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("name." . $key, $attribute->name->$key) }}"
                                                   class="form-control" id="input-name-{{ $key }}"
                                                   placeholder="{{ trans("attributes::attributes.attributes.name", [], $key) }}">
                                        </div>

                                    </div>

                                @endforeach

                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-slug">{{ trans("attributes::attributes.attributes.slug") }}</label>
                                <input name="slug" type="text"
                                       value="{{ @Request::old("default", $attribute->slug) }}"
                                       class="form-control" id="input-slug"
                                       placeholder="{{ trans("attributes::attributes.attributes.slug") }}">
                            </div>

                            <div class="form-group">
                                <label for="option-searchable">{{ trans("attributes::attributes.attributes.searchable") }}</label>
                                <select name="searchable" id="option-searchable"
                                        class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->searchable == 1) ? 'selected' : '' }} value="1">{{ trans("attributes::attributes.attributes.can_search") }}</option>
                                    <option {{ ($attribute && $attribute->searchable == 0) ? 'selected' : '' }} value="0">{{ trans("attributes::attributes.attributes.not_search") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="option-visible">{{ trans("attributes::attributes.attributes.visible") }}</label>
                                <select name="visible" id="option-visible"
                                        class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->visible == 1) ? 'selected' : '' }} value="1">{{ trans("attributes::attributes.attributes.is_visible") }}</option>
                                    <option {{ ($attribute && $attribute->visible == 0) ? 'selected' : '' }} value="0">{{ trans("attributes::attributes.attributes.not_visible") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="option-searchable">{{ trans("attributes::attributes.attributes.priority") }}</label>
                                <select name="priority" id="option-searchable"
                                        class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->priority == 1) ? 'selected' : '' }} value="1">{{ trans("attributes::attributes.attributes.high_priority") }}</option>
                                    <option {{ ($attribute && $attribute->priority == 0) ? 'selected' : '' }} value="0">{{ trans("attributes::attributes.attributes.low_priority") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="option-required">{{ trans("attributes::attributes.attributes.required") }}</label>
                                <select name="required" id="option-required"
                                        class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->required == 1) ? 'selected' : '' }} value="1">{{ trans("attributes::attributes.attributes.is_required") }}</option>
                                    <option {{ ($attribute && $attribute->required == 0) ? 'selected' : '' }} value="0">{{ trans("attributes::attributes.attributes.not_required") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="option-template">{{ trans("attributes::attributes.attributes.template") }}</label>
                                <select name="template" id="option-template"
                                        class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->template == 'input_text') ? 'selected' : '' }} value="input_text">{{ trans("attributes::attributes.attributes.input_text") }}</option>
                                    <option {{ ($attribute && $attribute->template == 'dropdown') ? 'selected' : '' }} value="dropdown">{{ trans("attributes::attributes.attributes.dropdown") }}</option>
                                </select>
                            </div>

                            <div class="form-group if-input-text">
                                <label for="data-type-template">{{ trans("attributes::attributes.attributes.data_type") }}</label>
                                <select name="data_type" id="data-type-template"
                                        class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->data_type == 'number') ? 'selected' : '' }} value="number">{{ trans("attributes::attributes.attributes.number") }}</option>
                                    <option {{ ($attribute && $attribute->data_type == 'text') ? 'selected' : '' }} value="text">{{ trans("attributes::attributes.attributes.text") }}</option>
                                </select>
                            </div>

                            <div class="form-group if-dropdown">
                                <label for="input-type">{{ trans("attributes::attributes.attributes.type") }}</label>
                                <select name="type" class="form-control chosen-select chosen-rtl">
                                    <option {{ ($attribute && $attribute->type == 'single') ? 'selected' : '' }} value="single">{{ trans("attributes::attributes.attributes.single_value") }}</option>
                                    <option {{ ($attribute && $attribute->type == 'multiple') ? 'selected' : '' }} value="multiple">{{ trans("attributes::attributes.attributes.multiple_values") }}</option>
                                </select>
                            </div>

                        </div>
                    </div>


                </div>
                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("attributes::attributes.add_small_icon") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="small_icon_id" class="small-icon-id" value="
                                {{ ($attribute and @$attribute->small_icon->path != "") ? @$attribute->small_icon->id : 0 }}">
                                <a class="change-post-image change-small-icon label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("attributes::attributes.change_small_icon") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="small-icon"
                                         src="{{ ($attribute and @$attribute->small_icon->id != "") ? thumbnail(@$attribute->small_icon->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("attributes::attributes.add_big_icon") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="big_icon_id" class="big-icon-id" value="
                                {{ ($attribute and @$attribute->big_icon->path != "") ? @$attribute->big_icon->id : 0 }}">
                                <a class="change-post-image change-big-icon label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("attributes::attributes.change_big_icon") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="big-icon"
                                         src="{{ ($attribute and @$attribute->big_icon->id != "") ? thumbnail(@$attribute->big_icon->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row if-dropdown">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="input-option-slug">{{ trans("attributes::attributes.attributes.option_slug") }}</label>
                                <input type="text" class="form-control" id="input-option-slug"
                                       placeholder="{{ trans("attributes::attributes.attributes.option_slug") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-option-ar">{{ trans("attributes::attributes.attributes.option_ar") }}</label>
                                <input type="text" class="form-control" id="input-option-ar"
                                       placeholder="{{ trans("attributes::attributes.attributes.option_ar") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-option-en">{{ trans("attributes::attributes.attributes.option_en") }}</label>

                                <input type="text" class="form-control" id="input-option-en"
                                       placeholder="{{ trans("attributes::attributes.attributes.option_en") }}">
                            </div>

                            <button type="submit" class="btn btn-primary btn-labeled add-attribute-option">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                {{ trans("attributes::attributes.add_option") }}
                            </button>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="attribute-options-ar">{{ trans("attributes::attributes.attributes.options_ar") }}</label>

                                <select id="attribute-options-ar" class="form-control duo-updated">
                                    @if(isset($attribute->options['ar']) && count($attribute->options['ar']))

                                        @foreach($attribute->options['ar'] as $k => $v)

                                            <option selected value='{{ $k }}'>{{ $v }}</option>

                                        @endforeach

                                    @endif
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="attribute-options-en">{{ trans("attributes::attributes.attributes.options_en") }}</label>

                                <select id="attribute-options-en" class="form-control duo-updated">

                                    @if(isset($attribute->options['en']) && count($attribute->options['en']))

                                        @foreach($attribute->options['en'] as $k => $v)

                                            <option selected value='{{ $k }}'>{{ $v }}</option>

                                        @endforeach

                                    @endif

                                </select>
                            </div>

                            <button type="submit" class="btn btn-flat btn-danger remove-option-value">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                {{ trans("attributes::attributes.remove_option") }}
                            </button>

                        </div>

                        <div class="row" id="atrributes-options-values-hd">

                            @if(isset($attribute->options['ar']) && count($attribute->options['ar']))

                                @foreach($attribute->options['ar'] as $k => $v)

                                    <input type='hidden' data-id='{{ $k }}' class='ar-options-values-hidden'
                                           name='{{ 'options[ar][' . $k . ']' }}' value='{{ $v }}'/>

                                @endforeach

                            @endif

                            @if(isset($attribute->options['en']) && count($attribute->options['en']))

                                @foreach($attribute->options['en'] as $k => $v)

                                    <input type='hidden' data-id='{{ $k }}' class='en-options-values-hidden'
                                           name='{{ 'options[en][' . $k . ']' }}' value='{{ $v }}'/>

                                @endforeach

                            @endif

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </form>

@stop

@section("footer")

    <script>
        $(document).ready(function () {

            $(".change-small-icon").filemanager({
                panel: "media",
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".small-icon-id").first().val(file.id);
                        base.parents(".post-image-block").find(".small-icon").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("attributes::attributes.not_allowed_file") }}");
                }
            });

            $(".change-big-icon").filemanager({
                panel: "media",
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".big-icon-id").first().val(file.id);
                        base.parents(".post-image-block").find(".big-icon").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("attributes::attributes.not_allowed_file") }}");
                }
            });

        });
    </script>

    <script>

        $(document).ready(function () {

            choose_template();

            $('#option-template').change(function () {
                choose_template();
            });

            function choose_template() {
                var temp = $('#option-template').val();

                if (temp == 'dropdown') {
                    $('.if-dropdown').show();
                    $('.if-input-text').hide();
                } else {
                    $('.if-dropdown').hide();
                    $('.if-input-text').show();

                    $('#atrributes-options-values-hd .ar-options-values-hidden').remove();
                    $('#atrributes-options-values-hd .en-options-values-hidden').remove();

                    $('#attribute-options-ar').html('');
                    $('#attribute-options-en').html('');
                }
            }

            $('.add-attribute-option').click(function (e) {
                e.preventDefault();

                var option_slug = $('#input-option-slug').val().trim();

                var ar_option = $('#input-option-ar').val().trim();

                var en_option = $('#input-option-en').val().trim();

                if (option_slug != '' && ar_option != '' && en_option != '') {

                    var ar_existed = $('#atrributes-options-values-hd').find('.ar-options-values-hidden[value="' + ar_option + '"]');

                    var en_existed = $('#atrributes-options-values-hd').find('.en-options-values-hidden[value="' + en_option + '"]');

                    var slug_existed = $('#atrributes-options-values-hd').find('.en-options-values-hidden[data-id="' + option_slug + '"]');

                    if (ar_existed.size() == 0 && en_existed.size() == 0 && slug_existed.size() == 0) {

                        $('#attribute-options-ar').append("<option selected value='" + option_slug + "'>" + ar_option + "</option>");
                        $('#attribute-options-en').append("<option selected value='" + option_slug + "'>" + en_option + "</option>");

                        $('#atrributes-options-values-hd').append("<input type='hidden' data-id='" + option_slug + "' class='ar-options-values-hidden' name='options[ar][" + option_slug + "]' value='" + ar_option + "' />");
                        $('#atrributes-options-values-hd').append("<input type='hidden' data-id='" + option_slug + "' class='en-options-values-hidden' name='options[en][" + option_slug + "]' value='" + en_option + "' />");

                    } else {
                        alert_box("{{ trans("attributes::attributes.duplicate_option_value") }}");
                    }

                } else {
                    alert_box("{{ trans("attributes::attributes.missing_option_attribute") }}");
                }
            });

            options_dropdowns_change($('#attribute-options-ar'));

            $('.duo-updated').change(function () {
                options_dropdowns_change($(this));
            });

            function options_dropdowns_change(el) {
                $('.duo-updated').val(el.val());

                $('#input-option-slug').val(el.val());

                $('#input-option-ar').val($('#attribute-options-ar option:selected').text());

                $('#input-option-en').val($('#attribute-options-en option:selected').text());
            }

            $('.remove-option-value').click(function (e) {
                e.preventDefault();

                $('#atrributes-options-values-hd').find('.ar-options-values-hidden[data-id="' + $('#attribute-options-ar').val() + '"]').remove();

                $('#atrributes-options-values-hd').find('.en-options-values-hidden[data-id="' + $('#attribute-options-en').val() + '"]').remove();

                $('option:selected', $('.duo-updated')).remove();
            });

            $('#attribute-form').submit(function (e) {

                var slug = $('#input-slug').val();

                var test = /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/.test(slug);

                if (slug.trim() == '' || !test) {
                    alert_box("{{ trans("attributes::attributes.slug_not_valid") }}");

                    return false;
                } else {
                    return true;
                }
            });

        });
    </script>
@stop
