<?php

namespace Dot\Attributes\Models;

use DB;
use Dot\Ads\Models\Ad;
use Dot\Categories\Models\Category;
use Dot\Platform\Model;
use Dot\Media\Models\Media;

/**
 * Class Attribute
 * @package Dot\Attributes\Models
 */
class Attribute extends Model
{

    /**
     * @var string
     */
    protected $module = 'attributes';

    /**
     * @var string
     */
    protected $table = 'attributes';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    protected $casts = [
        'name' => 'json',
        'options' => 'json'
    ];

    /*
     * lang fields
     */
    protected $translatable = [
        'name'
    ];

    /*
     * @var array
     */
    protected $creatingRules = [
        "slug" => "unique:attributes,slug|regex:/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/"
    ];

    /*
     * @var array
     */
    protected $updatingRules = [
        "slug" => "required|unique:attributes,slug,[id],id|regex:/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/"
    ];

    /**
     * big icon relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function big_icon()
    {
        return $this->hasOne(Media::class, "id", "big_icon_id");
    }

    /**
     * small icon relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function small_icon()
    {
        return $this->hasOne(Media::class, "id", "small_icon_id");
    }

    /**
     * categories relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_attributes', 'attribute_id', 'category_id');
    }

    /**
     * ads relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function ads()
    {
        return $this->belongsToMany(Ad::class, 'ads_attributes_values', 'attribute_id', 'ad_id');
    }
}
