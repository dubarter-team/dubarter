<?php

namespace Dot\Categories\Jobs;

use Dot\Categories\Models\Category;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CategoryJob implements ShouldQueue
{
    use Dispatchable, Queueable;

    /**
     * Category ID
     *
     * @var integer
     */
    protected $id;

    protected $countries_ids;

    protected $attributes_ids;

    /**
     * Create a new job instance
     * AdIndex constructor.
     *
     * @param Mixed $id
     */
    public function __construct($id, $countries_ids, $attributes_ids)
    {
        $this->id = (integer)$id;

        $this->countries_ids = $countries_ids;

        $this->attributes_ids = $attributes_ids;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $category = Category::where('id', $this->id)->first();

        if ($category) {

            // saving countries

            $cats = Category::all_childs($category);

            foreach ($cats as $cat) {
                $cat->countries()->detach();
                $cat->countries()->attach($this->countries_ids);
            }

            // saving attributes

//            $category->custom_attributes()->detach();
//
//            if (count($this->attributes_ids)) {
//
//                $i = 0;
//
//                foreach ($this->attributes_ids as $attr_id) {
//                    $category->custom_attributes()->attach($attr_id, ['order' => $i]);
//
//                    $i++;
//                }
//            }
        }
    }
}
