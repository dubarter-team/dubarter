<?php

namespace Dot\Categories\Controllers;

use Action;
use App\Indices\Ad as AdIndex;
use Dot\Ads\Jobs\AdJob;
use Dot\Attributes\Models\Attribute;
use Dot\Categories\Jobs\CategoryJob;
use Dot\Categories\Models\Category;
use Dot\Categories\Models\CategoryMeta;
use Dot\I18n\Models\Place;
use Dot\Platform\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Request;

class CategoriesController extends Controller
{

    /**
     * View payload
     *
     * @var array
     */
    protected $data = [];

    /**
     * Show all categories
     *
     * @param int $parent
     *
     * @return mixed
     */
    function index($parent = 0)
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? "name->" . app()->getLocale() : 'created_at';
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "ASC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : 50;

        $query = Category::with("posts", "topics", "categories")
            ->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("q")) {
            $q = Request::get("q");

            $query->where(function ($c_q) use ($q) {
                $c_q->where('name->ar', 'LIKE', '%' . $q . '%');
                $c_q->orWhere('name->en', 'LIKE', '%' . $q . '%');
                $c_q->orWhere('slug->ar', 'LIKE', '%' . $q . '%');
                $c_q->orWhere('slug->en', 'LIKE', '%' . $q . '%');
            });
        } else {
            $query->parent($parent);
        }

        if (Request::filled("country_id") && Request::get("country_id") != 0) {
            $query->whereHas("countries", function ($query) {
                $query->where("places.id", Request::get("country_id"));
            });
        }

        $this->data["categories"] = $query->with(['categories', 'topics', 'posts'])->paginate($this->data['per_page']);

        $this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

        $this->data['parent'] = Category::where('id', $parent)->first();

        return view("categories::show", $this->data);
    }

    /**
     * Delete category by id
     *
     * @return mixed
     */
    public function delete()
    {
        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            $category = Category::findOrFail($id);

            $category->delete();
        }

        return Redirect::back()->with("message", trans("categories::categories.events.deleted"));
    }

    /**
     * Create a new category
     *
     * @return mixed
     */
    public function create()
    {

        if (Request::isMethod("post")) {

            $category = new Category();

            $category->name = Request::get('name');


            $category->listing_type = Request::get('listing_type');
            $category->logo_id = Request::get('logo_id');
            $category->image_id = Request::get('image_id');
            $category->banner_id = Request::get('banner_id');
            $category->parent = Request::get('parent');
            $category->user_id = Auth::user()->id;
            $category->color = Request::get('color');
            $category->template = Request::get('template');
            $category->status = Request::get('status', 0);
            $category->featured = Request::get('featured', 0);
            $category->in_landing_nav = Request::get('in_landing_nav', 0);
            $category->has_price = Request::get('has_price', 1);
            $category->frontend_user_enabled = Request::get('frontend_user_enabled', 1);

            if (!$category->validate()) {
                return Redirect::back()->withErrors($category->errors())->withInput(Request::all());
            }

            $category->save();

            CategoryMeta::insert([
                    "category_id" => $category->id,
                    "country_id" => config("country.id"),
                    "choose_cat_text" => json_encode(Request::get('choose_cat_text')),
                    "all_cat_text" => json_encode(Request::get('all_cat_text')),
                    "h1" => json_encode(Request::get('choose_cat_text')),
                    "h2" => json_encode(Request::get('h2')),
                    "key_words" => json_encode(Request::get('key_words')),
                    "page_title" => json_encode(Request::get('page_title')),
                    "description" => json_encode(Request::get('description'))
                ]);

            $category->countries()->detach();
            $category->countries()->attach(Request::get('countries', []));

            $category->custom_attributes()->detach();

            if (count(Request::get('attributes_ids', []))) {

                $i = 0;

                foreach (Request::get('attributes_ids', []) as $attr_id) {
                    $category->custom_attributes()->attach($attr_id, ['order' => $i]);

                    $i++;
                }
            }

            dispatch(new CategoryJob($category->id, Request::get('countries', []), Request::get('attributes_ids', [])));

            return Redirect::route("admin.categories.edit", array("id" => $category->id))
                ->with("message", trans("categories::categories.events.created"));
        }

        $this->data["category"] = false;

        $this->data["categories"] = Category::where('parent', 0)->where('status', 1)->get();

        $this->data['attributes'] = Attribute::all();

        $this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

        $this->data['parent'] = Request::filled('parent_id') ? Category::where('parent', request('parent_id'))->first() : null;

        return view("categories::edit", $this->data);
    }

    /**
     * Edit category by id
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {

        $category = Category::where('id', $id)->with(['custom_attributes', 'parent_category'])
            ->leftJoin("categories_meta", function ($query) {
                $query->on("categories_meta.category_id", "=", "categories.id")
                    ->where("categories_meta.country_id", config("country.id"));
            })
            ->first();

        if (!$category) {
            abort(404);
        }

        if (Request::isMethod("post")) {

            $category->name = Request::get('name');
            $category->slug = Request::get('slug');
            $category->listing_type = Request::get('listing_type');
            $category->logo_id = Request::get('logo_id');
            $category->image_id = Request::get('image_id');
            $category->banner_id = Request::get('banner_id');
            $category->parent = Request::get('parent');
            $category->user_id = Auth::user()->id;
            $category->color = Request::get('color');
            $category->template = Request::get('template');
            $category->status = Request::get('status', 0);
            $category->featured = Request::get('featured', 0);
            $category->in_landing_nav = Request::get('in_landing_nav', 0);
            $category->has_price = Request::get('has_price', 1);
            $category->frontend_user_enabled = Request::get('frontend_user_enabled', 1);

            if (!$category->validate()) {
                return Redirect::back()->withErrors($category->errors())->withInput(Request::all());
            }

            $category->save();

            CategoryMeta::where("category_id", $category->id)
                ->where("country_id", config("country.id"))->delete();

            CategoryMeta::where("category_id", $category->id)
                ->where("country_id", config("country.id"))
                ->insert([
                    "category_id" => $category->id,
                    "country_id" => config("country.id"),
                    "choose_cat_text" => json_encode(Request::get('choose_cat_text')),
                    "all_cat_text" => json_encode(Request::get('all_cat_text')),
                    "h1" => json_encode(Request::get('choose_cat_text')),
                    "h2" => json_encode(Request::get('h2')),
                    "key_words" => json_encode(Request::get('key_words')),
                    "page_title" => json_encode(Request::get('page_title')),
                    "description" => json_encode(Request::get('description'))
                ]);


            $category->countries()->detach();
            $category->countries()->attach(Request::get('countries', []));

            $category->custom_attributes()->detach();

            if (count(Request::get('attributes_ids', []))) {

                $i = 0;

                foreach (Request::get('attributes_ids', []) as $attr_id) {
                    $category->custom_attributes()->attach($attr_id, ['order' => $i]);

                    $i++;
                }
            }

             dispatch(new CategoryJob($category->id, Request::get('countries', []), Request::get('attributes_ids', [])));

            //$this->reindex($id);

            return Redirect::route("admin.categories.edit", array("id" => $id))->with("message", trans("categories::categories.events.updated"));
        }

//        $fields = [
//            "choose_cat_text",
//            "all_cat_text",
//            "h1",
//            "h2",
//            "key_words",
//            "page_title",
//            "description"
//        ];
//
//        foreach ($fields as $field) {
//        //    $category->{$field} = json_decode($category->{$field});
//        }


        $this->data["category"] = $category;

        $this->data["categories"] = Category::where('parent', 0)->where('status', 1)->get();

        $this->data['attributes'] = Attribute::get();

        $this->data['countries'] = Place::where('parent', 0)->orderBy('order', 'asc')->where('status', 1)->get();

        $this->data['parent'] = $category->parent_category;

        return view("categories::edit", $this->data);
    }

    public function get_sub_categories()
    {
        $ret = ['status' => false, 'html' => ''];

        if (Request::filled('cat_id')) {
            $has_form_group = Request::get('has_formgroup', 1);

            $categories = Category::where('status', 1)->where('parent', Request::get('cat_id'))->get();

            if ($categories->count()) {
                $ret['status'] = true;

                $ret['html'] = view('categories::partials.categories_dropdown', ['categories' => $categories, 'has_label' => false, 'parent' => false, 'has_form_group' => $has_form_group])->render();
            }
        }


        return response()->json($ret);
    }

    function reindex($category_id, $offset = 0)
    {

        $limit = 1000;

        $ads = AdIndex::select("_id")
            ->type("ads")
            ->where("categories.id", (int)$category_id)
            ->take($limit)
            ->skip($offset)
            ->get();

        if (count($ads)) {

            foreach ($ads as $ad) {
                dispatch(new AdJob($ad->_id));
            }

            $offset += $limit;
            $this->reindex($category_id, $offset);
        }
    }
}

