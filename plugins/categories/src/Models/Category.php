<?php

namespace Dot\Categories\Models;

use DB;
use Dot\Ads\Models\Ad;
use Dot\Attributes\Models\Attribute;
use Dot\I18n\Models\Place;
use Dot\Media\Models\Media;
use Dot\Platform\Model;
use Dot\Posts\Models\Post;
use Dot\Topics\Models\Topic;
use Dot\Users\Models\User;

/**
 * Class Category
 *
 * @package Dot\Categories\Models
 */
class Category extends Model
{

    /**
     * @var string
     */
    protected $module = 'categories';

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $parentKey = 'parent';

    /**
     * @var array
     */
    protected $fillable = array('*');

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    protected $visible = array();

    /**
     * @var array
     */
    protected $hidden = array();

    /**
     * @var int
     */
    protected $perPage = 20;

    protected $translatable = [
        'name',
        'slug',
        'description',
        'h1',
        'h2',
        'page_title',
        'key_words',
        'all_cat_text',
        'choose_cat_text',
    ];

    protected $casts = [
        'name' => 'json',
        'slug' => 'json',
        'description' => 'json',
        'h1' => 'json',
        'h2' => 'json',
        'page_title' => 'json',
        'key_words' => 'json',
        'all_cat_text' => 'json',
        'choose_cat_text' => 'json',
    ];

    protected $sluggable = [
        'slug' => 'name'
    ];

    /**
     * logo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function logo()
    {
        return $this->hasOne(Media::class, "id", "logo_id");
    }

    /**
     * image relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function image()
    {
        return $this->hasOne(Media::class, "id", "image_id");
    }

    /**
     * banner relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function banner()
    {
        return $this->hasOne(Media::class, "id", "banner_id");
    }

    /**
     * user relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * parent relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function parent_category()
    {
        return $this->hasOne(Category::class, "id", "parent");
    }

    /**
     * categories relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function categories()
    {
        return $this->hasMany(Category::class, 'parent');
    }

    /**
     * countries relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function countries()
    {
        return $this->belongsToMany(Place::class, 'categories_countries', 'category_id', 'country_id');
    }

    /**
     * ads relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function ads()
    {
        return $this->belongsToMany(Ad::class, 'ads_categories', 'category_id', 'ad_id');
    }

    /**
     * posts relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function posts()
    {
        return $this->belongsToMany(Post::class, 'posts_categories', 'category_id', 'post_id');
    }

    /**
     * topics relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    function topics()
    {
        return $this->belongsToMany(Topic::class, 'topics_categories', 'category_id', 'topic_id');
    }

    /**
     * attributes relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsThttp://52.51.223.229oMany
     */
    function custom_attributes()
    {
        return $this->belongsToMany(Attribute::class, 'categories_attributes', 'category_id', 'attribute_id')->orderBy('order', 'asc');
    }

    /**
     * @param     $query
     * @param int $parent
     */
    function scopeParent($query, $parent = 0)
    {
        $query->where("categories.parent", $parent);
    }

    static function get_attributes($category, $flag = true)
    {
        $attributes = $category ? $category->custom_attributes : collect([]);

        if ($category && $category->parent) {
            $attributes = $attributes->merge(self::get_attributes($category->parent_category, false));
        }

        return $flag ? $attributes->reverse() : $attributes;
    }

    static function get_parents($id)
    {
        $parents = [];

        while (true) {
            $parents[] = $id;

            $cat = self::with(['parent_category'])->where('id', $id)->first();

            if ($cat && $cat->parent) {
                $id = $cat->parent;
            } else {
                break;
            }
        }

        $parents = array_reverse($parents);

        return $parents;
    }

    static function get_childs($id)
    {
        $childs = self::where('status', 1)->with(['parent_category'])->where('parent', $id)->get();

        return $childs;
    }

    static function all_childs($category)
    {
        $childs = collect([$category]);

        $cats = $category->categories;

        if ($cats->count()) {
            foreach ($cats as $cat) {
                $childs = $childs->merge(self::all_childs($cat, false));
            }
        }

        return $childs;
    }
}
