<?php

namespace Dot\Categories\Models;

use Dot\Platform\Model;

class CategoryMeta extends Model
{

    public $timestamps = false;

    protected $table = 'categories_meta';

    protected $translatable = [
        'description',
        'h1',
        'h2',
        'page_title',
        'key_words',
        'all_cat_text',
        'choose_cat_text',
    ];

    protected $casts = [
        'description' => 'json',
        'h1' => 'json',
        'h2' => 'json',
        'page_title' => 'json',
        'key_words' => 'json',
        'all_cat_text' => 'json',
        'choose_cat_text' => 'json',
    ];
}
