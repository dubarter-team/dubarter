@extends("admin::layouts.master")

@section("content")

    <form action="" method="post" id="category-form">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-folder"></i>
                    {{ $category ? trans("categories::categories.edit") : trans("categories::categories.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.categories.show") }}">{{ trans("categories::categories.categories") }}</a>
                    </li>

                    @if($parent)
                        <li>
                            <a href="{{ route("admin.categories.show", ['id' => $parent->id]) }}">{{ $parent->name }}</a>
                        </li>
                    @endif

                    <li class="active">
                        <strong>
                            {{ $category ? trans("categories::categories.edit") : trans("categories::categories.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($category)
                    <a href="{{ route("admin.categories.create") }}"
                       class="btn btn-primary btn-labeled btn-main"> <span
                                class="btn-label icon fa fa-plus"></span>
                        &nbsp; {{ trans("categories::categories.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("categories::categories.save_category") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-body">


                            <div class="panel-options">
                                <ul class="nav nav-tabs">
                                    @if(count(config("i18n.locales")))
                                        @foreach(config("i18n.locales") as $key => $locale)
                                            <li @if($key == app()->getLocale()) class="active" @endif>
                                                <a data-toggle="tab" href="#tab-{{ $key }}" aria-expanded="true">
                                                    {{ $locale["title"] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <br/>

                            <div class="tab-content">

                                @foreach(config("i18n.locales") as $key => $locale)

                                    <div id="tab-{{ $key }}"
                                         class="tab-pane @if($key == app()->getLocale()) active @endif"
                                         style="direction: {{ $locale["direction"] }}">

                                        <div class="form-group">
                                            <label for="input-name-{{ $key }}">{{ trans("categories::categories.attributes.name", [], $key) }}</label>
                                            <input name="name[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("name." . $key, $category->name->$key) }}"
                                                   class="form-control" id="input-name-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.name", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-slug-{{ $key }}">{{ trans("categories::categories.attributes.slug", [], $key) }}</label>
                                            <input name="slug[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("slug." . $key, $category->slug->$key) }}"
                                                   class="form-control" id="input-slug-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.slug", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-choose_cat_text-{{ $key }}">{{ trans("categories::categories.attributes.choose_cat_text", [], $key) }}</label>
                                            <input name="choose_cat_text[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("choose_cat_text." . $key, $category->choose_cat_text->$key) }}"
                                                   class="form-control" id="input-choose_cat_text-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.choose_cat_text", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-all_cat_text-{{ $key }}">{{ trans("categories::categories.attributes.all_cat_text", [], $key) }}</label>
                                            <input name="all_cat_text[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("all_cat_text." . $key, $category->all_cat_text->$key) }}"
                                                   class="form-control" id="input-all_cat_text-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.all_cat_text", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-h1-{{ $key }}">{{ trans("categories::categories.attributes.h1", [], $key) }}</label>
                                            <input name="h1[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("h1." . $key, $category->h1->$key) }}"
                                                   class="form-control" id="input-h1-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.h1", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-h2-{{ $key }}">{{ trans("categories::categories.attributes.h2", [], $key) }}</label>
                                            <input name="h2[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("h2." . $key, $category->h2->$key) }}"
                                                   class="form-control" id="input-h2-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.h2", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-page-title-{{ $key }}">{{ trans("categories::categories.attributes.page_title", [], $key) }}</label>
                                            <input name="page_title[{{ $key }}]" type="text"
                                                   value="{{ @Request::old("page_title." . $key, $category->page_title->$key) }}"
                                                   class="form-control" id="input-page-title-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.page_title", [], $key) }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="input-description-{{ $key }}">{{ trans("categories::categories.attributes.description", [], $key) }}</label>
                                            <textarea name="description[{{ $key }}]"
                                                   class="form-control" id="input-description-{{ $key }}"
                                                   placeholder="{{ trans("categories::categories.attributes.description", [], $key) }}">{{ @Request::old("description." . $key, $category->description->$key) }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="input-key-words-{{ $key }}">{{ trans("categories::categories.attributes.key_words", [], $key) }}</label>
                                            <textarea name="key_words[{{ $key }}]"
                                                      class="form-control" id="input-key-words-{{ $key }}"
                                                      placeholder="{{ trans("categories::categories.attributes.key_words", [], $key) }}">{{ @Request::old("key_words." . $key, $category->key_words->$key) }}</textarea>
                                        </div>

                                    </div>

                                @endforeach

                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-color">{{ trans("categories::categories.attributes.color") }}</label>
                                <input name="color" type="text"
                                       value="{{ @Request::old("color", $category->color) }}"
                                       class="form-control color-input" id="input-color"
                                       placeholder="{{ trans("categories::categories.attributes.color") }}">
                            </div>

                            <div class="form-group">
                                <label for="input-listing-type">{{ trans("categories::categories.attributes.listing_type") }}</label>
                                <select name="listing_type" class="form-control chosen-select chosen-rtl" id="input-listing-type">
                                    <option {{ $category && $category->listing_type == 'ads' ? 'selected' : '' }} value="ads">{{ trans("categories::categories.attributes.ads") }}</option>
                                    <option {{ $category && $category->listing_type == 'categories_1' ? 'selected' : '' }} value="categories_1">{{ trans("categories::categories.attributes.categories_1") }}</option>
                                    <option {{ $category && $category->listing_type == 'categories_2' ? 'selected' : '' }} value="categories_2">{{ trans("categories::categories.attributes.categories_2") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="input-frontend_user_enabled">{{ trans("categories::categories.attributes.frontend_user_enabled") }}</label>
                                <select name="frontend_user_enabled" class="form-control chosen-select chosen-rtl" id="input-frontend_user_enabled">
                                    <option {{ $category && $category->frontend_user_enabled == 1 ? 'selected' : '' }} value="1">{{ trans("categories::categories.attributes.is_frontend_user_enabled") }}</option>
                                    <option {{ $category && $category->frontend_user_enabled == 0 ? 'selected' : '' }} value="0">{{ trans("categories::categories.attributes.not_frontend_user_enabled") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="input-has_price">{{ trans("categories::categories.attributes.has_price") }}</label>
                                <select name="has_price" class="form-control chosen-select chosen-rtl" id="input-has_price">
                                    <option {{ $category && $category->has_price == 1 ? 'selected' : '' }} value="1">{{ trans("categories::categories.attributes.is_has_price") }}</option>
                                    <option {{ $category && $category->has_price == 0 ? 'selected' : '' }} value="0">{{ trans("categories::categories.attributes.not_has_price") }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="input-template">{{ trans("categories::categories.attributes.template") }}</label>
                                <select name="template" class="form-control chosen-select chosen-rtl" id="input-template">
                                    <option value="">{{ trans("categories::categories.attributes.choose_template") }}</option>
                                    <option {{ $category && $category->template == 'grid_3' ? 'selected' : '' }} value="grid_3">{{ trans("categories::categories.attributes.grid_3") }}</option>
                                    <option {{ $category && $category->template == 'grid_2' ? 'selected' : '' }} value="grid_2">{{ trans("categories::categories.attributes.grid_2") }}</option>
                                    <option {{ $category && $category->template == 'list' ? 'selected' : '' }} value="list">{{ trans("categories::categories.attributes.list") }}</option>
                                </select>
                            </div>

                            @include('categories::partials.choose_category', ['categories' => $categories, 'type' => 'choose_parent', 'cat_id' => $category ? $category->parent : request('parent_id', 0), 'show_label' => true, 'has_form_group' => true])

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">


                            <div class="form-group">
                                <label for="input-option-en">{{ trans("categories::categories.attributes.add_attribute") }}</label>

                                <div class="input-group">
                                    <select class="form-control chosen-select chosen-rtl" id="attributes-dropdown">
                                        <option value="0">{{ trans("categories::categories.attributes.choose_attribute") }}</option>

                                        @if(count($attributes))

                                            @foreach($attributes as $attr)
                                                <option value="{{ $attr->id }}">{{ $attr->name }}</option>
                                            @endforeach

                                        @endif
                                    </select>

                                    <span class="input-group-btn" style="margin: 0 0 4px 0;">
                                        <button class="btn btn-secondary btn-success btn-add" id="add-attribute"
                                                type="button">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>


                            <div class="dd">

                                <ul class="dd-list" id="sortable">

                                    @if($category && count($category->custom_attributes))

                                        @foreach($category->custom_attributes as $attr)

                                            <li class="dd-item">
                                                <div class="dd-handle">
                                                    <i class="fa fa-cogs"></i> &nbsp; {{ $attr->name }}
                                                </div>

                                                <input type="hidden" class="dd-hidden" name="attributes_ids[]"
                                                       value="{{ $attr->id }}"/>
                                                <a href="javascript:void(0)"
                                                   class="pull-right remove-item remove-attribute"> <i
                                                            class="fa fa-times"></i></a>
                                            </li>

                                        @endforeach

                                    @endif

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-check-square"></i>
                            {{ trans("categories::categories.attributes.status") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-status">{{ trans("categories::categories.attributes.status") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("status", $category->status)) checked="checked" @endif
                                    type="checkbox" id="input-status" name="status" value="1"
                                           class="status-switcher switcher-sm">
                                </div>

                            </div>

                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-featured">{{ trans("categories::categories.attributes.featured") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("featured", $category->featured)) checked="checked" @endif
                                    type="checkbox" id="input-featured" name="featured" value="1"
                                           class="featured-switcher switcher-sm">
                                </div>
                            </div>

                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-in_landing_nav">{{ trans("categories::categories.attributes.in_landing_nav") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("in_landing_nav", $category->in_landing_nav)) checked="checked" @endif
                                    type="checkbox" id="input-in_landing_nav" name="in_landing_nav" value="1"
                                           class="in_landing_nav-switcher switcher-sm">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-th-large"></i>
                            {{ trans("categories::categories.add_country") }}
                        </div>
                        <div class="panel-body">
                            @if ($countries->count())
                                <ul class='tree-views'>
                                    @foreach($countries as $country)
                                        <li>
                                            <div class='tree-row checkbox i-checks'>
                                                <label>
                                                    <input type='checkbox'
                                                           @if ($category and in_array($country->id, $category->countries->pluck("id")->toArray())) checked="checked"
                                                           @endif
                                                           name='countries[]'
                                                           value='{{ $country->id }}'>
                                                    &nbsp; {{ $country->name }}
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                {{ trans("categories::categories.no_countries") }}
                            @endif
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("categories::categories.add_logo") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="logo_id" class="post-logo-id" value="
                                {{ ($category and @$category->logo->path != "") ? @$category->logo->id : 0 }}">
                                <a class="change-post-image change-post-logo label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("categories::categories.change_logo") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="post-logo"
                                         src="{{ ($category and @$category->logo->id != "") ? thumbnail(@$category->logo->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("categories::categories.add_image") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="image_id" class="post-image-id" value="
                                {{ ($category and @$category->image->path != "") ? @$category->image->id : 0 }}">
                                <a class="change-post-image change-category-image label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("categories::categories.change_image") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="post-image"
                                         src="{{ ($category and @$category->image->id != "") ? thumbnail(@$category->image->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("categories::categories.add_banner") }}
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="banner_id" class="post-banner-id" value="
                                {{ ($category and @$category->banner->path != "") ? @$category->banner->id : 0 }}">
                                <a class="change-post-image change-category-banner label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("categories::categories.change_banner") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="post-banner"
                                         src="{{ ($category and @$category->banner->id != "") ? thumbnail(@$category->banner->path) : assets("admin::default/image.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>


        </div>

    </form>

    <div style="display: none;" id="dummy-sortable">
        <li class="dd-item">
            <div class="dd-handle">
                <i class="fa fa-cogs"></i> &nbsp; xxdd
            </div>

            <input type="hidden" class="dd-hidden" name="attributes_ids[]" value=""/>
            <a href="javascript:void(0)" class="pull-right remove-item remove-attribute"> <i
                        class="fa fa-times"></i></a>
        </li>
    </div>

@stop


@section("head")

    <link href="{{ assets('admin::css/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"/>
    <link href="{{ assets('admin::css/plugins/nestable/nestable.ltr.css') }}" rel="stylesheet"/>

    <style>

        .colorpicker {
            right: unset;
        }

        .chosen-container {
            margin: 0 !important;
        }

    </style>

@stop

@section("footer")
    <script src="{{ assets('admin::js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>

    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.featured-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.in_landing_nav-switcher'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });

        $(document).ready(function () {

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.tree-views input[type=checkbox]').on('ifChecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('check');
                checkbox.change();
            });

            $('.tree-views input[type=checkbox]').on('ifUnchecked', function () {
                var checkbox = $(this).closest('ul').parent("li").find("input[type=checkbox]").first();
                checkbox.iCheck('uncheck');
                checkbox.change();
            });

            $(".change-post-logo").filemanager({
                panel: "media",
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-logo-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-logo").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("categories::categories.not_allowed_file") }}");
                }
            });



            $(".change-category-banner").filemanager({
                panel: "media",
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-banner-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-banner").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert_box("{{ trans("categories::categories.not_allowed_file") }}");
                }
            });

            $('.color-input').colorpicker();

            $("#sortable").sortable();

        });
    </script>

    <script>

        $(document).ready(function () {

            $('#add-attribute').click(function () {
                var attribute_option = $('#attributes-dropdown option:selected');

                if (attribute_option.val() != 0) {
                    var existed = $('#sortable').find('.dd-hidden[value="' + attribute_option.val() + '"]');

                    if (existed.size() == 0) {
                        var sortable_li = $('#dummy-sortable li').clone();

                        sortable_li.find('.dd-hidden').val(attribute_option.val());

                        sortable_li.find('.dd-handle').html(sortable_li.find('.dd-handle').html().replace("xxdd", attribute_option.text()));

                        $('#sortable').append(sortable_li);
                    } else {
                        alert_box("{{ trans("categories::categories.duplicate_option_value") }}");
                    }
                }
            });

            $(document).on('click', '.remove-attribute', function () {
                $(this).parents('.dd-item').remove();
            });

        });
    </script>
@stop
