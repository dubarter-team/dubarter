@php
    $parents = (isset($cat_id) && $cat_id) ? \Dot\Categories\Models\Category::get_parents($cat_id) : [];
@endphp

<div id="category-choose">

    @if(count($parents))

        @foreach($parents as $parent)

            @include('categories::partials.categories_dropdown', ['categories' => $categories, 'has_label' => ($loop->first && $show_label) ? true : false, 'cat_id' => $parent, 'type' => $type, 'has_form_group' => $has_form_group])

            @php
                $categories = \Dot\Categories\Models\Category::get_childs($parent);
            @endphp

        @endforeach

        @if($categories->count())

            @include('categories::partials.categories_dropdown', ['categories' => $categories, 'has_label' => false, 'has_form_group' => $has_form_group])

        @endif

    @else

        @include('categories::partials.categories_dropdown', ['categories' => $categories, 'has_label' => $show_label ? true : false, 'type' => $type, 'has_form_group' => $has_form_group])

    @endif

</div>

<input type="hidden" name="{{ $type == 'choose_parent' ? 'parent' : 'category_id' }}" id="choosen-category-id" value="{{ isset($cat_id) ? $cat_id : 0 }}"/>

@push("footer")

    <script>

        $(document).ready(function () {

            $(document).on('change', '.category-dynamic-change', function () {
                var ths = $(this);

                var cat_id = ths.val();

                ths.parents('.category-dropdown').nextAll('.category-dropdown').remove();

                if (ths.val() != 0) {
                    $('#choosen-category-id').val(ths.val());

                    $.ajax({
                        url: '{{ route('admin.categories.get_sub_cats') }}',
                        data: { cat_id: cat_id, has_formgroup: '{{ $has_form_group ? 1 : 0 }}' },
                        type: 'get',
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == true) {
                                $('#category-choose').append(data.html);

                                $(".category-dynamic-change").chosen();
                            }
                        }
                    });
                } else {
                    var prev_dropdown = ths.parents('.category-dropdown').prev('.category-dropdown').find('.category-dynamic-change');

                    if (prev_dropdown.length > 0) {
                        $('#choosen-category-id').val(prev_dropdown.val());
                    } else {
                        $('#choosen-category-id').val(0);
                    }
                }

            });

        });

    </script>

@endpush