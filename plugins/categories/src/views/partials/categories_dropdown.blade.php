<div class="category-dropdown {{ (isset($has_form_group) && $has_form_group) ? 'form-group' : '' }}">

    @if($has_label)
        @if($type == 'choose_parent')
            <label for="input-name">{{ trans("categories::categories.attributes.parent") }}</label>
        @elseif($type == 'choose_category')
            <label for="input-name">{{ trans("categories::categories.category") }}</label>
        @endif
    @endif

    <select class="form-control chosen-select chosen-rtl category-dynamic-change">

        <option value="0">{{ trans("categories::categories.choose_category") }}</option>

        @if(count($categories))

            @foreach($categories as $category)

                <option {{ (isset($cat_id) && $cat_id == $category->id) ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>

            @endforeach

        @endif
    </select>

</div>