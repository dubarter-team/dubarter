<?php

namespace Dot\Auth\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;

/*
 * Class AuthMiddleware
 */
class AuthMiddleware
{

    /*
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return \Illuminate\Http\JsonResponse|\  Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $guard == "api" || $request->segment(1) == "api") {
                return response()->json(['scode'=>401,'message'=>__('api.error'),'data'=>null,'errors'=>[__('api.mustlogdIn')]]);
            } else {
                return redirect()->route('admin.auth.login')->with("url", $request->url());
            }
        }

        return $next($request);
    }
}
