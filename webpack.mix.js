let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix

    // Frontend


    .styles(
        [
            "public/frontend/assets/css/bootstrap.min.css",
            "public/frontend/assets/css/bootstrap-rtl.min.css",
            "public/frontend/assets/css/fontawesome-all.min.css",
            "public/frontend/assets/css/jquery.scrolltop.min.css",
            "public/frontend/assets/css/owl.carousel.min.css",
            "public/frontend/assets/css/owl.theme.default.css",
            "public/frontend/assets/css/select2.min.css",
            "public/frontend/assets/css/accordion.css",
            "public/frontend/assets/css/footer.css",
            "public/frontend/assets/css/main.css",
            "public/frontend/assets/css/main-rtl.css",
            "public/frontend/assets/css/add-post.css",
            "public/frontend/assets/css/edit-css.css",
            "public/frontend/assets/plugins/slider/css/style2.css",
            "public/frontend/assets/plugins/slider/css/style2-rtl.css",
            "public/frontend/assets/css/my-account.css",
            "public/frontend/assets/css/lightbox.css",
            "public/frontend/assets/css/custom_1.css",
            "public/frontend/assets/css/responsive.css"
        ], "public/frontend/assets/css/app.css")

    .scripts(
        [
            "public/frontend/assets/js/popper.min.js",
            "public/frontend/assets/js/bootstrap.min.js",
            "public/frontend/assets/js/fontawesome-all.min.js",
            "public/frontend/assets/js/jquery.nicescroll.min.js",
            "public/frontend/assets/js/jquery.scrolltop.min.js",
            "public/frontend/assets/js/bootstrap3-typeahead.min.js",
            "public/frontend/assets/js/owl.carousel.min.js",
            "public/frontend/assets/js/jquery.fittext.js",
            "public/frontend/assets/js/select2.min.js",
            "public/frontend/assets/js/main.js",
            //"public/frontend/assets/js/accordion.js",
            "public/frontend/assets/js/lazy.js",
            "public/frontend/assets/js/master.js",
            "public/frontend/assets/plugins/slider/js/jquery.easing.js",
            "public/frontend/assets/plugins/slider/js/script.js",
            "public/js/share.js",
        ], "public/frontend/assets/js/app.js")


    // Backend

    .styles(
        [
            "public/plugins/admin/css/bootstrap.min.css",
            "public/plugins/admin/font-awesome/css/font-awesome.css",
            "public/plugins/admin/css/animate.css",
            "public/plugins/admin/css/style.css",
            "public/plugins/admin/css/plugins/switchery/switchery.css",
            "public/plugins/admin/css/login.css",
        ], "public/plugins/admin/css/auth.css")

    .scripts(
        [
            "public/plugins/admin/js/jquery-2.1.1.js",
            "public/plugins/admin/js/bootstrap.min.js"
        ], "public/plugins/admin/js/auth.js")

    .styles(
        [
            "public/plugins/admin/css/bootstrap.min.css",
            "public/plugins/admin/font-awesome/css/font-awesome.css",
            "public/plugins/admin/css/font-awesome-animation.css",
            "public/plugins/admin/css/plugins/iCheck/custom.css",
            "public/plugins/admin/css/animate.css",
            "public/plugins/admin/css/style.css",
            "public/plugins/admin/uploader/popup.css",
            "public/plugins/admin/css/plugins/toastr/toastr.min.css",
            "public/plugins/admin/css/plugins/switchery/switchery.css",
            "public/plugins/admin/css/plugins/chosen/chosen.css",
            "public/plugins/admin/css/master.css",
            "public/plugins/admin/uploader/cropper.css",
            "public/plugins/admin/uploader/jquery.fileupload.css",
        ], "public/plugins/admin/css/app.css")

    .styles(
        [
            "public/plugins/admin/css/rtl.css",
            "public/plugins/admin/css/plugins/bootstrap-rtl/bootstrap-rtl.min.css",
            "public/plugins/admin/uploader/rtl.css",
        ], "public/plugins/admin/css/app.rtl.css")


    .scripts(
        [
            "public/plugins/admin/js/jquery-2.1.1.js",
            "public/plugins/admin/uploader/cropper.js",
            "public/plugins/admin/js/jquery-ui-1.10.4.min.js",
            "public/plugins/admin/js/bootbox.min.js",
            "public/plugins/admin/js/bootstrap.min.js",
            "public/plugins/admin/js/plugins/metisMenu/jquery.metisMenu.js",
            "public/plugins/admin/js/plugins/slimscroll/jquery.slimscroll.min.js",
            "public/plugins/admin/js/plugins/switchery/switchery.js",
            "public/plugins/admin/js/plugins/chosen/chosen.jquery.js",
            "public/plugins/admin/js/jquery.cookie.js",
            "public/plugins/admin/js/plugins/peity/jquery.peity.min.js",
            "public/plugins/admin/js/inspinia.js",
            "public/plugins/admin/js/plugins/pace/pace.min.js",
            "public/plugins/admin/js/plugins/iCheck/icheck.min.js",
            "public/plugins/admin/js/demo/peity-demo.js",
            "public/plugins/admin/uploader/jquery.ui.widget.js",
            "public/plugins/admin/uploader/jquery.iframe-transport.js",
            "public/plugins/admin/uploader/jquery.fileupload.js",
            "public/plugins/admin/uploader/popup.js",
            "public/plugins/admin/js/plugins/moment/moment.min.js",
            "public/plugins/admin/js/plugins/toastr/toastr.min.js",
        ], "public/plugins/admin/js/app.js");
